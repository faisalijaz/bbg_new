<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'ubold/assets/plugins/morris/morris.css',
        'ubold/assets/css/core.css',
        'ubold/assets/plugins/jquery.steps/demo/css/jquery.steps.css',
        'ubold/assets/css/components.css',
        'ubold/assets/css/icons.css',
        'ubold/assets/css/pages.css',
        'ubold/assets/css/responsive.css',
        'ubold/assets/plugins/custombox/dist/custombox.min.css',
        'ubold/assets/plugins/sweetalert/dist/sweetalert.css',
        'ubold/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        'ubold/assets/plugins/timepicker/bootstrap-datetimepicker.min.css',
        'ubold/assets/css/font-awesome.css',
        'ubold/assets/css/swich-theme.css',
        'ubold/assets/css/custom.css',
        "https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css",
        'ubold/assets/plugins/print_page_part/demo/css/normalize.min.css'
    ];
    public $js = [

        'ubold/assets/js/detect.js',
        'ubold/assets/js/fastclick.js',
        'ubold/assets/js/jquery.slimscroll.js',
        'ubold/assets/js/jquery.blockUI.js',
        'ubold/assets/js/waves.js',
        'ubold/assets/js/wow.min.js',
        'ubold/assets/plugins/peity/jquery.peity.min.js',
        'ubold/assets/plugins/waypoints/lib/jquery.waypoints.js',
        'ubold/assets/plugins/counterup/jquery.counterup.min.js',
        'ubold/assets/plugins/raphael/raphael-min.js',
        'ubold/assets/plugins/jquery-knob/jquery.knob.js',
        'ubold/assets/js/jquery.core.js',
        'ubold/assets/js/jquery.app.js',
        'ubold/assets/plugins/RWD-Table-Patterns/dist/js/rwd-table.js',
        'ubold/assets/plugins/Chart.js/Chart.min.js', // Chart Js Core
        'ubold/assets/pages/chartjs.customer.profile.init.js', // chart JS initializing
        'ubold/assets/plugins/sweetalert/dist/sweetalert.min.js', // Sweet alerts Core
        'ubold/assets/pages/jquery.sweet-alert.init.js', // Sweet Alert initializing
        'ubold/assets/plugins/tinymce/tinymce.min.js', // Sweet Alert initializing
        'ubold/assets/js/text-editor.js', // Sweet Alert initializing
        'ubold/assets/plugins/timepicker/bootstrap-datetimepicker.min.js', // Sweet alerts Core
        'ubold/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', // Sweet alerts Core
        'ubold/assets/plugins/morris/morris.min.js', // Sweet alerts Core
        'ubold/assets/pages/jquery.dashboard_crm.js', // Sweet alerts Core
        'ajax-modal-popup.js',
        'Chart.StackedBar.js',
        'ubold/assets/plugins/custombox/dist/legacy.min.js',
        'ubold/assets/plugins/jquery.steps/build/jquery.steps.min.js',
        'ubold/assets/plugins/jquery-validation/dist/jquery.validate.min.js',
        'ubold/assets/pages/jquery.wizard-init.js',
        'ubold/assets/js/promo-code.js',
        'ubold/assets/js/custom.js',
        'ubold/assets/js/swal.js',
        'functions.js',
        "https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js",
        "ubold/assets/js/events_forms.js",
        'ubold/assets/plugins/print_page_part/jQuery.print.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
