<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaxGroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tax-groups-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']) ?>
        </div>

        <div class="col-lg-4 col-sm-12">

            <label style="width: 100%">&nbsp;</label>

            <div class="form-group pull-right">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
