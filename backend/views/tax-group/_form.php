<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaxGroups */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="tax-groups-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']) ?>


    <table id="tax_rates"
           class="table table-striped table-bordered table-hover images-table upload-preview">
        <thead>
        <tr>
            <td class="text-left">Tax Rates</td>
            <td class="text-right">Sort Order</td>
            <td></td>
        </tr>
        </thead>
        <tbody>

        <?php $tax_row = 0; ?>
        <?php foreach ($model->taxGroupRates as $taxRate ) { ?>

            <tr id="tax-row<?php echo $tax_row; ?>">
                <td class="">
                    <div class="form-group">

                        <select
                                class="form-control"
                                name="TaxGroups[rates][<?php echo $tax_row; ?>][tax_id]"
                                required>
                            <?php foreach ($tax_rates as $key => $value) { ?>
                                <?php if ($taxRate->tax_id == $key) { ?>
                                    <option value="<?= $key ?>" selected><?= $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?= $key ?>"><?= $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>

                    </div>
                </td>

                <td class="">
                    <div class="form-group">
                        <input type="text"
                               name="TaxGroups[rates][<?php echo $tax_row; ?>][sort_order]"
                               value="<?= $taxRate->sort_order; ?>" placeholder="Sort order"
                               class="form-control"/>
                    </div>
                </td>

                <td class="text-left">
                    <button type="button"
                            onclick="$('#tax-row<?php echo $tax_row; ?>, .tooltip').remove();"
                            data-toggle="tooltip" title="Remove" class="btn btn-danger"><i
                                class="fa fa-minus-circle"></i></button>
                </td>
            </tr>
            <?php $tax_row++; ?>

        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td class="text-left">
                <button type="button" onclick="addTaxRates();" data-toggle="tooltip" title="Add"
                        class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
            </td>
        </tr>
        </tfoot>
    </table>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>




<script type="text/javascript">
    //Start of Images as tour_images.
    var tax_row = <?= $tax_row ?>;

    function addTaxRates() {

        html = '<tr id="tax-row' + tax_row + '">';

        html += ' <td class="text-left">';
        html += '     <div class="form-group">';
        html += ' <select class="form-control" name="TaxGroups[rates][' + tax_row + '][tax_id]" required>';
        <?php foreach($tax_rates as $key => $value) { ?>
        html += ' <option value="<?= $key ?>"><?= $value; ?></option>';
        <?php } ?>
        html += ' </select>';
        html += ' </div>';
        html += ' </td>';


        html += '  <td style="width: 10%;">';
        html += '    <div class="form-group">';
        html += '   <input type="text" name="TaxGroups[rates][' + tax_row + '][sort_order]" value="" placeholder="Sort order" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class=""><button type="button" onclick="$(\'#tax-row' + tax_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#tax_rates tbody').append(html);

        tax_row++;
    }
</script>