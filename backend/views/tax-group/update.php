<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaxGroups */

$this->title = 'Update Tax Groups: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tax Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tax-groups-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
        'tax_rates' => $tax_rates,
    ]) ?>

</div>
