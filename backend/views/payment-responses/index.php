<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentResponsesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Responses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-responses-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'payment_reference',
            /*'payment_id',*/
            'invoice_id',
            'response_code',
            'response_text:ntext',

           /* ['class' => 'yii\grid\ActionColumn'],*/
        ],
    ]); ?>
</div>
