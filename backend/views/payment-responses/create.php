<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaymentResponses */

$this->title = 'Create Payment Responses';
$this->params['breadcrumbs'][] = ['label' => 'Payment Responses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-responses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
