<?php

$this->title = 'Booking Confirmation';
$payment_types = ['' => 'Cash on Deliver', 'COD' => 'Cash', 'CCO' => 'Credit Card Online', 'CP' => 'Contract Payment', 'RP' => 'Reward Points'];

$name = '';
$phone = '';
$email = '';
$address = '';
$city = '';
$area = '';
?>
<div style="width: 900px;margin: 0px auto;" class="detail-wrapper card-box" id="PrintInvoice">
    <div style="height: 50px;width: 100%;">
        <div style="width: 50%; float: left;">
            <div class="" style="font-size: 22px;padding: 5px;">
                <p class="order-summry-title">Booking Information</p>
            </div>
        </div>
        <div style="width: 50%; float: right;">
            <img style="float: right;width: 130px;"
                 src="<?= Yii::$app->appSettings->getByAttribute('app_logo'); ?>"
                 alt="lets travel">
        </div>
    </div>
    <hr/>
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-12 ">
                <?php if (isset($booking) && count($booking)) {
                    if ($booking->account <> null) {
                        $acc = $booking->account;
                        $name = $acc->first_name . ' ' . $acc->last_name;
                        $phone = $acc->phone_number;
                        $email = $acc->email;

                        if ($booking->bookingAddress <> null) {
                            $add = $booking->bookingAddress;
                            if ($add->addressCity <> null) {
                                $city = $add->addressCity->name;
                            }
                            if ($add->addressArea <> null) {
                                $area = $add->addressArea->name;
                            }
                            $address = $add->address . ', ' . $add->street . ', ' . $area . ', ' . $city;
                        }
                    }
                    ?>
                    <div class="row" style="w">
                        <div class="col-md-9  m-t-30">
                            <p>
                                Customer Name: <strong><?= $name; ?></strong><br/>
                                Phone Number: <strong><?= $phone; ?></strong><br/>
                                Email: <strong><?= $email; ?></strong><br/>
                                Address: <strong><?= $address; ?></strong><br/>
                            </p>

                        </div>
                        <div class="col-md-3  m-t-30">
                            <p>Booking number: <strong><?= $booking->id; ?></strong><br/>
                                Date
                                <strong><?= formatDate($booking->created_at); ?></strong>
                                <br/>
                                Total <strong>AED <?= round($booking->total_sum, 2); ?></strong>
                                <br/>Payment Method
                                <strong><?= $booking->payment_type ? $payment_types[$booking->payment_type] : 'Cash On Deliver'; ?></strong>
                            </p>
                        </div>
                    </div>
                    <div class="detail-content-block">
                        <h3 class="large-title">Booking Details</h3>
                        <div class="table-responsive">
                            <table class="table style-1 type-2 striped">
                                <tr class="table-heading">
                                    <td class="table-label color-grey" style="width:75%;">
                                        <strong>Experience</strong>
                                    </td>
                                    <td class="table-label color-grey" style="width:20%;"><strong>Amount</strong>
                                    </td>
                                </tr>

                                <tbody>
                                <?php
                                $i = 0;
                                if (count($booking->tourBookingDetails) > 0) {
                                    foreach ($booking->tourBookingDetails as $tourDetails) {
                                        $style = '';
                                        if ($i % 2 == 0) {
                                            $style = 'style = "background:#ddd;"';
                                        }
                                        ?>
                                        <tr <?= $style; ?>>
                                            <td class="table-label color-grey">
                                                <span class="item"><?= $tourDetails->tour->title; ?></span>
                                            </td>
                                            <td class="table-label color-dark-2">
                                                AED <?= $tourDetails->price; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-3  m-t-30" style="float: right">
                                <p>
                                    Adults Price:
                                    <strong>AED <?= round($booking->getTourBookingDetails()->sum('adult_price'), 2); ?></strong>
                                    <br/>
                                    <?php
                                    if (round($booking->getTourBookingDetails()->sum('childern') * $booking->getTourBookingDetails()->sum('child_price'), 2) > 0) {
                                        ?>
                                        Childern Price:
                                        <strong>AED <?= round($booking->getTourBookingDetails()->sum('childern') * $booking->getTourBookingDetails()->sum('child_price'), 2); ?></strong>
                                        <br/>
                                        <?php
                                    }
                                    ?>
                                    Addons:
                                    <strong>AED <?= round($booking->getTourBookingDetails()->sum('addons_price'), 2); ?></strong>
                                    <br/>
                                    Pickup Rate:
                                    <strong>AED <?= round($booking->getBookingPickups()->sum('pickup_price'), 2); ?></strong>
                                    <br/>
                                    Sub Total:
                                    <strong>AED <?= round($booking->price, 2); ?></strong>
                                    <br/>
                                    Total:
                                    <strong>AED <?= round($booking->total_sum, 2); ?></strong>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="detail-content-block margin-20-bottom">
                        <p class="order-summry-title"></p>
                    </div>
                <?php }
                ?>
            </div>

        </div>
    </div>
    <hr/>
    <div style="height: 50px;width: 100%;">
        <div style="width: 50%; font-size: 10px; float: left;color: #000;">
            <footer class="text-left">
                <?= Yii::$app->appSettings->getByAttribute('address'); ?> <br/>
                <a href="tel:<?= Yii::$app->appSettings->getByAttribute('telephone'); ?>"><span class="fa fa-phone"
                                                                                                aria-hidden="true"></span> <?= Yii::$app->appSettings->getByAttribute('telephone'); ?>
                </a>
                <a href="mailto:<?= Yii::$app->appSettings->getByAttribute('admin_email'); ?>"><span
                            class="fa fa-envelope-o" aria-hidden="true"></span>
                    <?= Yii::$app->appSettings->getByAttribute('admin_email'); ?>
                </a>
            </footer>
        </div>
        <div style="width: 50%; float: right;">
            <a href="<?= Yii::$app->appSettings->getByAttribute('app_url'); ?>" target="_blank" class="pull-right"
               style="margin-top: 5px;font-size: 10px;">
                <span class="fa fa-external-link" aria-hidden="true"></span> Visit Website
            </a>
        </div>
    </div>
</div>
<?= $this->registerJs('
$(document).find("#Print").on("click", function() {
    // Print ele4 with custom options
    $("#PrintInvoice").print({
            globalStyles : true, 
            prepend : "Customer Invoice",
            stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
        });
    });
  ');
?>
<?php

$this->registerJs('
    swal({
            title: "Success!",
            text: "Thank You! Your order has been placed and confirmation email has been sent to your registered email.",
            timer: 5000,
            type: "success",
            html: true,
            showConfirmButton: true
        });
    ');
?>

