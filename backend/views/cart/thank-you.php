<?php

$this->title = 'Booking Confirmation';
$payment_types = \Yii::$app->params['paymentTypes'];

$name = '';
$phone = '';
$email = '';
$address = '';
$city = '';
$area = '';
?>
<div style="width: 900px;margin: 0px auto;padding: 20px;border: 1px solid rgba(54, 64, 74, 0.05);-webkit-border-radius: 5px;border-radius: 5px;-moz-border-radius: 5px;background-clip: padding-box;margin-bottom: 20px;background-color: #ffffff;"
     class="detail-wrapper card-box" id="PrintInvoice">
    <div style="height: 50px;width: 100%;">
        <div style="width: 50%; float: left;">
            <div style="font-size: 22px;padding: 5px;">
                <p class="order-summry-title">Booking Information</p>
            </div>
        </div>
        <div style="width: 50%; float: right;">
            <img style="float: right;width: 170px;margin-top: -20px;"
                 src="<?= Yii::$app->appSettings->getByAttribute('app_logo'); ?>"
                 alt="lets travel">
        </div>
    </div>
    <hr/>

    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-12 ">
                <?php if (isset($booking) && count($booking)) {
                    if ($booking->account <> null) {
                        $acc = $booking->account;
                        $name = $acc->first_name . ' ' . $acc->last_name;
                        $phone = $acc->phone_number;
                        $email = $acc->email;

                        if ($booking->bookingAddress <> null) {
                            $add = $booking->bookingAddress;
                            if ($add->addressCity <> null) {
                                $city = $add->addressCity->name;
                            }
                            if ($add->addressArea <> null) {
                                $area = $add->addressArea->name;
                            }
                            $address = $add->address . ', ' . $add->street . ', ' . $area . ', ' . $city;
                        }
                    }
                    ?>
                    <div class="row" style="w">
                        <div class="col-md-9  m-t-30">
                            <p>
                                Customer Name: <strong><?= $name; ?></strong><br/>
                                Phone Number: <strong><?= $phone; ?></strong><br/>
                                Email: <strong><?= $email; ?></strong><br/>
                                Address: <strong><?= $address; ?></strong><br/>
                            </p>
                        </div>
                        <div class="col-md-3  m-t-30">
                            <p>Booking number: <strong><?= $booking->id; ?></strong><br/>
                                Booking Date
                                <strong><?= formatDate($booking->created_at); ?></strong>
                                <br/>
                                Booking status: <strong><?php if ($booking->status == 'created') {
                                        echo 'Pending';
                                    } else {
                                        echo ucwords($booking->status);
                                    } ?></strong><br/>
                                <?php
                                if ($booking->cancellation_time <> "") {
                                    ?>
                                    Cancellation Time: <strong><?= $booking->cancellation_time; ?></strong> <br/>
                                <?php } ?>
                                Total <strong>AED <?= round($booking->total_sum, 2); ?></strong>
                                <br/>Payment Method
                                <strong><?= $booking->payment_type ? $payment_types[$booking->payment_type] : 'Cash'; ?></strong>
                            </p>
                        </div>
                    </div>
                    <div class="detail-content-block">
                        <h3 class="large-title">Booking Details</h3>
                        <div class="table-responsive">
                            <table class="table style-1 type-2 striped">
                                <tr class="table-heading">
                                    <td class="table-label color-grey">
                                        <strong>Experience</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Tour Date</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Tour Time</strong>
                                    </td>
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Adults</strong>
                                    </td>
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Childrens</strong>
                                    </td>
                                    <td class="table-label color-grey" style="width:20%;"><strong>Amount</strong>
                                    </td>
                                </tr>

                                <tbody>
                                <?php
                                $i = 0;
                                if (count($booking->tourBookingDetails) > 0) {
                                    foreach ($booking->tourBookingDetails as $tourDetails) {
                                        $isExclusive = $tourDetails->isExclusive;
                                        $style = '';
                                        if ($i % 2 == 0) {
                                            $style = 'style = "background:#C4CDC6;color: #800000"';
                                        }
                                        ?>
                                        <tr <?= $style; ?>>
                                            <td class="table-label ">
                                                <span class="item"><?= ($isExclusive) ? $tourDetails->exclusive_title : $tourDetails->tour->title; ?></span>
                                            </td>
                                            <td class="table-label">
                                                <span class="item"><?= $tourDetails->check_in; ?></span>
                                            </td>
                                            <td class="table-label">
                                        <span class="item"><?php if ($isExclusive) {
                                                echo $tourDetails->tour_time;
                                            } ?></span>

                                            </td>
                                            <td style="border: 1px solid #f1f1f1;color: #800000;padding-left: 20px;">
                                                <span class="item"><?= $tourDetails->adults; ?>
                                                    <?php if (!$isExclusive) {
                                                        echo ' x ' . $tourDetails->adult_price . ' = ' . $tourDetails->adults * $tourDetails->adult_price;
                                                    } ?>
                                            </td>
                                            <td style="border: 1px solid #f1f1f1;color: #800000;padding-left: 20px;">
                                                <span class="item">
                                                    <?php
                                                    if ($tourDetails->childern > 0) { ?>
                                                        ?>
                                                        <?= $tourDetails->childern; ?>
                                                        <?php if (!$isExclusive) {
                                                            echo ' x ' . $tourDetails->child_price . ' = ' . $tourDetails->childern * $tourDetails->child_price;
                                                        } ?>

                                                    <?php } else {
                                                        echo 0;
                                                    } ?>
                                            </span>
                                            </td>
                                            <td class="table-label color-dark-2">
                                                AED <?= $tourDetails->price; ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                            if ($booking->touristInfo <> null) {
                            ?>
                        </div>
                        <div class="table-responsive">
                            <h3 class="large-title">Guests Information</h3>
                            <table class="table style-1 type-2 striped">
                                <tr class="table-heading">
                                    <td class="table-label color-grey" style="width:5%;">
                                        <strong>#</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Name</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Gender</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Age</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Nationality</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Pickup From</strong>
                                    </td>
                                </tr>
                                <tbody>
                                <?php
                                $i = 0;
                                foreach ($booking->touristInfo as $touristInfo) {
                                    $style = '';
                                    if ($i % 2 == 0) {
                                        $style = 'style = "background:#C4CDC6;color: #800000"';
                                    }
                                    ?>
                                    <tr <?= $style; ?>>
                                        <td class="table-label ">
                                            <span class="item"><?= $i + 1; ?></span>
                                        </td>
                                        <td class="table-label ">
                                            <span class="item"><?= $touristInfo->name; ?></span>
                                        </td>
                                        <td class="table-label">
                                            <span class="item"><?= $touristInfo->gender; ?></span>
                                        </td>
                                        <td class="table-label color-dark-2">
                                            <span class="item"><?= $touristInfo->age; ?></span>
                                        </td>
                                        <td class="table-label color-dark-2">
                                            <span class="item"><?= $touristInfo->nationality; ?></span>
                                        </td>
                                        <td class="table-label color-dark-2">
                                            <span class="item"><?= $touristInfo->pick_from_address; ?></span>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            if ($booking->bookingPickups <> null) {
                            $pickup = $booking->bookingPickups;
                            ?>
                        </div>
                        <div class="table-responsive" style="width:100%;">
                            <h3 class="large-title">Pickup Information</h3>
                            <table class="table style-1 type-2 striped">
                                <tr class="table-heading">
                                    <td class="table-label color-grey">
                                        <strong>Hotel Name</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Room Number</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Phone Number</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Area</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>City</strong>
                                    </td>
                                </tr>
                                <tbody>
                                <tr>
                                    <td class="table-label ">
                                        <span class="item"><?= $pickup->pickup_hotel_id; ?></span>
                                    </td>
                                    <td class="table-label ">
                                        <span class="item"><?= $pickup->pickup_room_number; ?></span>
                                    </td>
                                    <td class="table-label">
                                        <span class="item"><?= $pickup->pickup_phone_number; ?></span>
                                    </td>
                                    <td class="table-label color-dark-2">
                                        <span class="item"><?= $pickup->pickup_area; ?></span>
                                    </td>
                                    <td class="table-label color-dark-2">
                                        <span class="item">
                                            <?php if ($pickup->pickupCity <> null) {
                                                echo $pickup->pickupCity->name;
                                            } ?>
                                        </span>
                                    </td>
                                </tr>
                                <?php
                                ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-md-3  m-t-30" style="float: right">
                                <p>
                                <table style="width: 100%;">
                                    <tr>
                                        <td>Adults Price:</td>
                                        <td>
                                            <strong>AED <?= round($booking->getTourBookingDetails()->sum('adults') * $booking->getTourBookingDetails()->sum('adult_price'), 2); ?></strong>
                                        </td>
                                    </tr>
                                    <?php
                                    if (round($booking->getTourBookingDetails()->sum('childern') * $booking->getTourBookingDetails()->sum('child_price'), 2) > 0) {
                                        ?>
                                        <tr>
                                            <td>Childern Price:</td>
                                            <td>
                                                <strong>AED <?= round($booking->getTourBookingDetails()->sum('childern') * $booking->getTourBookingDetails()->sum('child_price'), 2); ?></strong>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td>Addons:</td>
                                        <td>
                                            <strong>AED <?= round($booking->getTourBookingDetails()->sum('addons_price'), 2); ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Pickup Rate:</td>
                                        <td>
                                            <strong>AED <?= round($booking->getBookingPickups()->sum('pickup_price'), 2); ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Total:</td>
                                        <td>
                                            <strong>AED <?= round($booking->price, 2); ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Total:</td>
                                        <td>
                                            <strong>AED <?= round($booking->total_sum, 2); ?></strong>
                                        </td>
                                    </tr>
                                </table>
                                </p>
                            </div>
                        </div>
                        <?php
                        //if ($booking->cancellation_time <> "") {
                        ?>
                        <div class="row"
                             style="margin-top: 20px;font-size: 16px;color: red;font-weight: 400;font-family: inherit;">
                            <p>
                                <strong>Note:</strong> We have tentatively blocked your booking request. Once payment is
                                done you
                                will
                                receive our final booking confirmation.
                            </p>
                        </div>
                        <?php
                        // }
                        ?>
                    </div>
                <?php } else { ?>
                    <div class="detail-content-block margin-20-bottom">
                        <p class="order-summry-title"></p>
                    </div>
                <?php }
                ?>
            </div>

        </div>
    </div>
    <hr/>
    <div style="height: 200px;width: 100%;">
        <div style="width: 100%; font-size: 10px; float: left;color: #000;">
            <footer class="text-left">
                <div style="text-align: center">
                    <div class="social-icons">
                        <a href="<?= Yii::$app->appSettings->getByAttribute('facebook'); ?>" target="_blank">
                            <img src="https://s3-ap-southeast-1.amazonaws.com/tour-booking/social-icons/fb59a801f3b01c9.png"
                                 data-placeholder="no_image.png">
                        </a>
                        <a href="<?= Yii::$app->appSettings->getByAttribute('twitter'); ?>" target="_blank">
                            <img src="https://s3-ap-southeast-1.amazonaws.com/tour-booking/social-icons/twitter59a802016d2e4.png"
                                 data-placeholder="no_image.png">
                        </a>
                        <a href="<?= Yii::$app->appSettings->getByAttribute('linkedin'); ?>" target="_blank">
                            <img src="https://s3-ap-southeast-1.amazonaws.com/tour-booking/social-icons/linkedin59a801ec6d894.png"
                                 data-placeholder="no_image.png">
                        </a>
                        <a href="<?= Yii::$app->appSettings->getByAttribute('instagram'); ?>" target="_blank">
                            <img src="https://s3-ap-southeast-1.amazonaws.com/tour-booking/social-icons/pintrest59a801fac9fb1.png"
                                 data-placeholder="no_image.png">
                        </a>
                        <a href="<?= Yii::$app->appSettings->getByAttribute('youtube'); ?>" target="_blank">
                            <img src="https://s3-ap-southeast-1.amazonaws.com/tour-booking/social-icons/youtube59a80219b1318.png"
                                 data-placeholder="no_image.png">
                        </a>
                    </div>
                    <p>
                        <?= Yii::$app->appSettings->getByAttribute('app_name'); ?> <br/>
                        UAE - Dubai - +971 43368406/07/09 , +97143368411 <br/>
                        <a href="mailto:<?= Yii::$app->appSettings->getByAttribute('admin_email'); ?>"><span
                                    class="fa fa-envelope-o" aria-hidden="true"></span>
                            <?= Yii::$app->appSettings->getByAttribute('admin_email'); ?>
                        </a> - <a href="<?= Yii::$app->appSettings->getByAttribute('app_url'); ?>" target="_blank"
                                  style="margin-top: 5px;font-size: 10px;">
                            <span class="fa fa-external-link" aria-hidden="true"></span>
                            <?= Yii::$app->appSettings->getByAttribute('app_url'); ?>
                        </a>
                    </p>
                    <p>
                        We sent out this message to all existing Tour Dubai customers <br/>
                        If you want more information about our privacy policy, please visit <a
                                href="<?= Yii::$app->appSettings->getByAttribute('app_url'); ?>" target="_blank"
                                style="margin-top: 5px;font-size: 10px;">
                            <span class="fa fa-external-link" aria-hidden="true"></span>
                            <?= Yii::$app->appSettings->getByAttribute('app_url'); ?>
                        </a> <br/>
                        If you no longer wish to receive these emails, simply click on the following link <a href="#"
                                                                                                             target="_blank"
                                                                                                             style="margin-top: 5px;font-size: 10px;">
                            Un-subscribe
                        </a> <br/>
                        &copy; <?= date('Y'); ?> <?= Yii::$app->appSettings->getByAttribute('app_name'); ?>, All rights
                        reserved

                    </p>

                </div>
            </footer>
        </div>
    </div>
</div>
<?= $this->registerJs('
$(document).find("#Print").on("click", function() {
    // Print ele4 with custom options
    $("#PrintInvoice").print({
            globalStyles : true, 
            prepend : "Customer Invoice",
            stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
        });
    });
  ');
?>
<?php

$this->registerJs('
    swal({
            title: "Success!",
            text: "Thank You! Your order has been placed and confirmation email has been sent to your registered email.",
            timer: 5000,
            type: "success",
            html: true,
            showConfirmButton: true
        });
    ');
?>

