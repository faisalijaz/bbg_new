<?php

use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Newslettertemplates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newslettertemplates-form">

    <div class="col-md-6">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-12">
            <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'category')->dropDownList(['event' => 'Events', 'news' => 'news'], ['prompt' => 'Select Template']) ?>
        </div>

        <div class="col-md-12 hidden" id="eventsList">
            <label class="control-label" for="newslettertemplates-event_id">Select Event</label>
            <?= $form->field($model, 'event_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \common\models\Events::find()
                        ->where(['>=', 'event_startDate', date('Y-m-d')])
                        ->andWhere(['active' => '1'])->all(), 'id', 'title'
                ), ['prompt' => 'Select Event ...', 'class' => 'form-control']
            )->label(false); ?>
        </div>


        <div class="col-md-12 hidden" id="newsMemberList">
            <label class="control-label" for="newslettertemplates-event_id">Members News</label>
            <?= Html::dropDownList('news', '',
                \yii\helpers\ArrayHelper::map(
                    \common\models\News::find()
                        //->where(['>=', 'FROM_UNIXTIME(created_at,"%Y-%m-%d")', date('Y-m-d')])
                        ->where(['news_type' => 'member', 'isPublished' => '1'])->all(), 'id', 'title'
                ), ['prompt' => 'Select Event ...', 'class' => 'getNews form-control']
            ); ?>
        </div>

        <div class="col-md-12 hidden" id="newsHeadline">
            <?= $form->field($model, 'title')->textarea(['rows' => 4])->label("Headline") ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-9">
                    <?= $form->field($model, 'testemail')->textInput(['type' => 'email', 'maxlength' => true]); ?>
                </div>
                <div class="col-md-3">
                    <?= Html::button('Send Test Email', ['id' => 'testEmail', 'class' => 'btn btn-primary', 'style' => 'margin-top: 30px;']); ?>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="accounts-form col-md-6">
                <div id="images" class="form-group">
                    <label class="control-label" for="input-image">Image</label>
                    <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                        <img src="<?= ($model->image != "" && $model->image <> null) ? $model->image : Yii::$app->params['no_image']; ?>"
                             alt="" width="125" height="125" title=""
                             data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                    </a>
                    <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'active')->widget(CheckboxX::className(), [
                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                'pluginOptions' => [
                    'theme' => 'krajee-flatblue',
                    'enclosedLabel' => true,
                    'threeState' => false,
                ]
            ])->label(false); ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'onlySubscription')->widget(CheckboxX::className(), [
                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                'pluginOptions' => [
                    'theme' => 'krajee-flatblue',
                    'enclosedLabel' => true,
                    'threeState' => false,
                ]
            ])->label(false); ?>

        </div>

        <?= $form->field($model, 'description')->textarea([
            'class' => 'hidden'
        ])->label(false); ?>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6" id="template_div">
        <label for="">Template</label>
        <div class="template">
            <?php if ($model->description == '') { ?>
                <table width="624" height="" border="0" align="center" cellpadding="0" cellspacing="0"
                       style="border: solid 1px #ccc; background-color: #fff;">
                    <tr>
                        <td height="268" colspan="3">
                            <img src="<?= Yii::$app->params['email_header_image'];?>" width="624"
                                 height="268" alt=""></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="537px" align="right" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td colspan="5">&nbsp;</td>
                                    <td colspan="2" width="30%" height="30" style="background-color: #fff;"
                                        align="right"><a href="http://bbgdubai.org/site/apply-membership" style="-moz-user-select: none;
                                        background-image: none;
                                        border: 1px solid transparent;
                                        border-radius: 4px;
                                        cursor: pointer;
                                        display: inline-block;
                                        font-size: 12px;
                                        font-weight: 400;
                                        line-height: 1.42857;
                                        margin-bottom: 0;
                                        padding-top: 6px;padding-bottom: 6px;padding-left: 6px;padding-right: 6px;
                                        text-align: center;
                                        vertical-align: middle;
                                        white-space: nowrap;

    color: #fff;text-align: right;text-decoration: none;" class="btn btn-primary">Not yet a member? Join us</a></td>
                                    <td width="8%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="8">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="43" valign="top">&nbsp;</td>
                        <td valign="top">
                            <table width="537" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="50" valign="bottom">
                                        <h2 style="font-size:20px; font-weight: normal; color: #D11349; margin: 15px 0px;"
                                            id='temp-title'>
                                            <?php echo $model->title; ?>
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="100%" valign="bottom">
                                        <p id='temp-message'>
                                            <?php echo $model->content; ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="main-table">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                               style="border-bottom: dashed 1px #CCCCCC;">
                                            <tr>
                                                <td height="20" colspan="2">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td id="main-table_nullimage">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td height="20" colspan="2">&nbsp;</td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td id='message_id'>
                                        <?php echo $model->title; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="43" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="top">
                            <img src="<?= Yii::$app->params['email_footer_image'];?>" width="624"
                                 height="127" alt=""></td>
                    </tr>
                </table>
            <?php } else {
                echo $model->description;
            } ?>
        </div>
    </div>
</div>

<?= $this->registerJs('
    
    //on page load
    
    var category = $("#newslettertemplates-category option:selected").val();
        
    if(category == "event"){ 
       
        $("#eventsList").removeClass("hidden");
        $("#newsHeadline").addClass("hidden"); 
        $("#newsTopicList").addClass("hidden"); 
        $("#newsMemberList").addClass("hidden"); 
        
    } else{
         
        $("#eventsList").addClass("hidden");
        $("#newsHeadline").removeClass("hidden"); 
        $("#newsTopicList").removeClass("hidden"); 
        $("#newsMemberList").removeClass("hidden");
    }
        
    
    
   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
   
    $("#newslettertemplates-category").change(function(){
        
        var category = $(this).val();
        
        if(category == "event"){ 
        
            $("#main-table, #main-table_nullimage, #temp-title, #temp-message").html(null); 
            $("#eventsList").removeClass("hidden");
            $("#newsHeadline").addClass("hidden"); 
            $("#newsTopicList").addClass("hidden"); 
            $("#newsMemberList").addClass("hidden"); 
            
        } else{
            
            $("#main-table, #main-table_nullimage, #temp-title, #temp-message").html(null); 
            $("#eventsList").addClass("hidden");
            $("#newsHeadline").removeClass("hidden"); 
            $("#newsTopicList").removeClass("hidden"); 
            $("#newsMemberList").removeClass("hidden");
        }
        
    });
    
        
    $("#newslettertemplates-title").on("keyup", function () {
       
        if ($("#newslettertemplates-category").val() == "news") {
            $("#temp-title").html($(this).val());
        } else {
            $("#message_id").html($(this).val());
        }
        
    });

    $("#newslettertemplates-content").on("keyup", function () {
        $("#temp-message").html($(this).val());
    });
    
    
    $("#newslettertemplates-event_id").change(function(){
        
        var event_id = $(this).val();
        
        $.ajax({
        
            url: "/newslettertemplates/get-event",
            type: "POST",
            cache: false,
            data : {id : event_id},
            success: function (data) {
               
               $("#main-table").html(data);
               $("#newslettertemplates-description").val($(".template").html());
               $("#subscribeNewsletter").prop("disabled", false)
            },
            error: function (e) {
      
            }
        });
    });
    
    
    $(".getNews").on("change", function () {
        
        var id = $(this).val();
        $("option:selected", this).remove();
        $.ajax({
            type: "POST",
            cache: false,
            url: "/newslettertemplates/get-news/",
            data: {
                "id": id,
            },
            success: function (data) {
                
                $("#main-table").append(data); 
                $("#countnews").val(parseInt($("#countnews").val()) + 1);
                $("#newslettertemplates-description").val($(".template").html());
            }
        });
        return false;
    });
    
    
    
    $("#testEmail").click(function () {
        
        var email = $("#newslettertemplates-testemail").val();
        var newsletter = $("#newslettertemplates-id").val();
        
        $("#testEmail").prop("disbaled", true);
        
        if(email == ""){
            swal({
                title: "Error!",
                text: "Please enter correct email address!",
                type: "error",
                timer: 5000,
                confirmButtonText: "OK"
            });
            $("#testEmail").prop("disbaled", false);
            return false; 
        }
        
        $.ajax({
            type: "POST",
            cache: false,
            url: "/newslettertemplates/send-test-email/",
            data: {
                "id": newsletter, "email" : email
            },
            success: function (data) {
                
                if(data){
                    swal({
                        title: "Great!",
                        text: "Test Email Sent!",
                        type: "success",
                        timer: 5000,
                        confirmButtonText: "OK"
                    });
                } else{
                    swal({
                        title: "Error!",
                        text: "Test Email not Sent!",
                        type: "error",
                        timer: 5000,
                        confirmButtonText: "OK"
                    });
                }
                
                 $("#testEmail").prop("disbaled", false);
            }
        });
        return false;
    });
    
    $("body").on("click", "a.close_news_div", function(e) {  
        
        e.preventDefault();
        
        var id = $(this).attr("id");
        
        swal({
            title: "Alert!",
            text: "Are you sure to delete this?",
            type: "error",
            timer: 5000,
            confirmButtonText: "OK" 
             
        },  function(isConfirm){
            if (isConfirm) {
            console.log(id);
                 $("body").find("#news_section_" + id).remove();
            } 
        });
    });
    
'); ?>
<!--if (parse["image"] == null) {
    $("#main-table_nullimage").append("<div><a href="#" class="close_div"><i class="fa fa-trash-o bg-danger"></i></a> " + parse["value"] + "</div>");
} else {
    $("#main-table").append("<div><a href="#" class="close_div"><i class="fa fa-trash-o bg-danger"></i></a> " + parse["value"] + "</div>");
}-->