<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="bottom">
            <h2><a href="#"><?= ($event <> null) ? $event->title : ""; ?></a></h2>
        </td>
    </tr>

    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                   style="border-top: dashed 1px #CCCCCC; border-bottom: dashed 1px #CCCCCC;">
                <tr>
                    <td height="20" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="41%"
                        style="color:#002D5D; margin: 5px 0px; line-height:1.5; font-size: 13px; font-weight: bold;">
                        Event
                    </td>
                    <td width="59%"
                        style="color:#D11349; font-weight: normal;"><?= ($event <> null) ? $event->title : ""; ?></td>
                </tr>
                <tr>
                    <td style="color:#002D5D; margin: 5px 0px; line-height:1.5; font-size: 13px; font-weight: bold;">
                        Venue
                    </td>
                    <td style="color:#D11349; font-weight: normal;"><?= ($event <> null) ? $event->venue : ""; ?></td>
                </tr>
                <tr>
                    <td style="color:#002D5D; margin: 5px 0px; line-height:1.5; font-size: 13px; font-weight: bold;">
                        Date
                    </td>
                    <td style="color:#D11349; font-weight: normal;">
                        <?= ($event <> null) ? formatDate($event->event_startDate) : ""; ?>
                        -
                        <?= ($event <> null) ? formatDate($event->event_endDate) : ""; ?>
                    </td>
                </tr>
                <tr>
                    <td style="color:#002D5D; margin: 5px 0px; line-height:1.5; font-size: 13px; font-weight: bold;">
                        Time
                    </td>
                    <td style="color:#D11349; font-weight: normal;">

                        <?= ($event <> null) ? $event->event_startTime : ""; ?>
                        to <?= ($event <> null) ? $event->event_endTime: ""; ?>
                    </td>
                </tr>
                <tr>
                    <td style="color:#002D5D; margin: 5px 0px; line-height:1.5; font-size: 13px; font-weight: bold;">
                        Member rate
                    </td>
                    <td style="color:#D11349; font-weight: normal;">AED <?= ($event <> null) ? $event->member_fee : ""; ?>
                    </td>
                </tr>
                <tr>
                    <td style="color:#002D5D; margin: 5px 0px; line-height:1.5; font-size: 13px; font-weight: bold;">
                        Non-Member rate
                    </td>
                    <td style="color:#D11349; font-weight: normal;">AED  <?= ($event <> null) ? $event->nonmember_fee : ""; ?>
                    </td>
                </tr>
                <tr>
                    <td style="color:#002D5D; margin: 5px 0px; line-height:1.5; font-size: 13px; font-weight: bold;">
                        Address
                    </td>
                    <td style="color:#D11349; font-weight: normal;"><?= ($event <> null) ? $event->address : ""; ?></td>
                </tr>
                <tr>
                    <td height="20" colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php if ($event <> null && $event->image != '') { ?>
                            <p><img width="100%" width="375" height="280" class="newsImage"
                                    src="<?= ($event->image <> null) ? $event->image : ""; ?>"/></p>
                        <?php } ?>
                        <p><?= ($event->short_description <> null) ? $event->short_description : ""; ?></p>
                        <p><?= ($event->description <> null) ? $event->description : ""; ?></p>
                    </td>
                </tr>
                <tr>

                    <td colspan="2">

                        <table cellspacing="2" cellpadding="2" align="left">
                            <tbody>
                            <tr>
                                <td width="100" height="30" style="background-color: #1e3560;text-align: center;">
                                    <a class="btn btn-primary" style="-moz-user-select: none;
                                        background-image: none;border: 1px solid transparent;border-radius: 4px;
                                        cursor: pointer;display: inline-block;font-size: 12px;font-weight: 400;
                                        line-height: 1.42857;margin-bottom: 0;padding-top: 6px;padding-bottom: 6px;
                                        padding-left: 6px;padding-right: 6px;text-align: left;vertical-align: middle;
                                        white-space: nowrap;text-decoration: none;
                                        color: #fff;"
                                       href="<?= ($event->id <> null) ? \Yii::$app->basePath . "/events/event-register?id=" . $event->id . "&s=1" : ""; ?>">
                                        Register Now
                                    </a>
                                </td>
                                <td width="60%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="20" colspan="2"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
