<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NewslettertemplatesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newslettertemplates-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'content') ?>

    <?= $form->field($model, 'event_id') ?>

    <?php // echo $form->field($model, 'onlySubscription') ?>

    <?php // echo $form->field($model, 'category') ?>

    <?php // echo $form->field($model, 'testemail') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'createDate') ?>

    <?php // echo $form->field($model, 'subject') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
