<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewslettertemplatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Newslettertemplates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newslettertemplates-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Newslettertemplates', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [ 
            'id',
            'title',
            'subject',
            [
                'attribute' => 'onlySubscription',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->onlySubscription <> null) {
                        if ($model->onlySubscription == "1") {
                            return '<span class="label label-success"> Yes </span>';
                        } else {
                            return '<span class="label label-danger">' . No . '</span>';
                        }
                    }
                    return '-';
                },
                'filter' => ['0' => 'No', '1' => 'Yes'],
                'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'category',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->category <> null) {
                        if ($model->category == "event") {
                            return '<span class="label label-info"> Event </span>';
                        } else {
                            return '<span class="label label-warning"> News </span>';
                        }
                    }
                    return '-';
                },
                'filter' => ['event' => 'Event', 'news' => 'News'],
                'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
                'contentOptions' => ['class' => 'text-center'],
            ],
            //'testemail:email',
            //'image',
            [
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->active <> null) {
                        if ($model->active) {
                            return '<span class="label label-success"> Published </span>';
                        } else {
                            return '<span class="label label-warning"> Un-Published </span>';
                        }
                    }
                    return '-';
                },
                'filter' => ['1' => 'Published', '0' => 'Un-Published'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
                'contentOptions' => ['class' => 'text-center'],
            ],
            //'createDate',
            //'subject',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{send} {view} {update} {delete}',
                'buttons' => [

                    'send' => function ($url, $model) {

                        return Html::a('<span class="label label-info"><span class="fa fa-envelope"></span></span>', '#', [
                            'class' => 'showModalButton',
                            'value' => Url::to(['/newslettertemplates/send-newsletter/', 'id' => $model->id]),
                            'title' => Yii::t('yii', 'Send Newsletter'),
                            'id' => 'sendNewsletter-' . $model->id
                        ]);

                    },
                    'view' => function ($url) {
                        return Html::a(
                            '<span class="label label-info"><span class="fa fa-search-plus"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            '<span class="label label-warning"><span class="fa fa-pencil"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'delete' => function ($url) {
                        return Html::a(
                            '<span class="label label-danger"><span class="fa fa-trash"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    }
                ]
            ]
        ],
        'responsive' => true
    ]); ?>
</div>
