<?php

use kartik\checkbox\CheckboxX;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Invoices */

$this->title = 'Send Newsletter';
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row card-box">
    <div class="invoices-create">
        <div class="modal-body">
            <?php $form = ActiveForm::begin([
                    'id' => 'newsletter'
            ]); ?>
            <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'subscribers')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'label' => "All Subscirbers (" . $subscribers . ")"
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <h3>Choose Categories</h3>
                </div>
                <?php
                if (count($categories) > 0) {
                    foreach ($categories as $category) {
                        ?>
                        <div class="col-md-6">

                            <?= $form->field($model, 'categories[]')->widget(CheckboxX::classname(), [
                                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                'options' => [
                                    'label' => $category->title,
                                    'value' => $category->id
                                ],
                                'pluginOptions' => [
                                    'theme' => 'krajee-flatblue',
                                    'enclosedLabel' => true,
                                    'threeState' => false,
                                ]
                            ])->label(false); ?>
                        </div>
                        <?php
                    }
                }
                ?></div>
        </div>
        <div class="row">
            <div class="modal-footer">
                <button type="button" id="sendNewsletter" class="btn btn-default">Send</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<script>
    $("body").on("click", "#sendNewsletter", function(e) {
        $("#newsletter").submit();
    });


</script>