<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Invoices */

$this->title = 'Update Invoice: ' . str_pad($model->invoice_id,'7','0',STR_PAD_LEFT);
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->invoice_id, 'url' => ['view', 'id' => $model->invoice_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="invoices-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'members' => $members,
        'adjustments' => $adjustments
    ]) ?>

</div>
