<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InvoicesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoices-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="row">
        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'invoice_id')->label("Invoice #") ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'first_name'); ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'last_name'); ?>
        </div>

        <div class="col-lg-3 col-sm-12">
            <label>Select Type</label>
            <?= \kartik\select2\Select2::widget([
                'model' => $model,
                'attribute' => 'invoice_related_to',
                'data' => ['member' => 'Member', 'event' => 'Events'],
                'options' => ['placeholder' => 'Search Type ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-3 col-sm-12">
            <label>User</label>
            <?= \kartik\select2\Select2::widget([
                'model' => $model,
                'attribute' => 'invoice_rel_id',
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Members::find()->all(), 'id', function ($model) {
                    return $model->first_name . " " . $model->last_name;
                }),
                'options' => ['placeholder' => 'Search Member ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-sm-12">
            <?= $form->field($model, 'company'); ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <label>Payment Status</label>
            <?= \kartik\select2\Select2::widget([
                'model' => $model,
                'attribute' => 'payment_status',
                'data' => [
                    'paid' => 'Paid',
                    'unpaid' => 'Pending',
                    'partialy_paid' => 'Partially Paid',
                    'cancelled' => 'Cancelled',
                    'refunded' => 'Refunded'
                ],
                'options' => ['placeholder' => 'Search Payment Status ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-lg-1 col-sm-12">
            <?= $form->field($model, 'total')->label("Amount") ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'invoice_date')->widget(\kartik\widgets\DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Invoice date'],
                'type' => \kartik\widgets\DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <label>Invoice Sent</label>
            <?= \kartik\select2\Select2::widget([
                'model' => $model,
                'attribute' => 'invoice_sent',
                'data' => ['1' => 'Sent', '0' => 'Not Sent'],
                'options' => ['placeholder' => 'Invoice sent? ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row" style="padding-top: 10px;">
        <div class="col-lg-12 col-sm-12">
            <div class="form-group pull-right" >
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
