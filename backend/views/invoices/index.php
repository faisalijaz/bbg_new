<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-index">

    <?= ExportMenu::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\Invoices::find()
        ]),
        'columns' => [
            [
                'label' => 'Invoice #',
                'attribute' => 'invoice_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(str_pad($model->invoice_id, 7, "0", STR_PAD_LEFT),
                        ['/invoices/invoice-pdf', 'invoice' => $model->invoice_id],
                        [
                            'target' => '_blank',
                            'data-pjax' => "0"
                        ]);
                }
            ],
            [
                'attribute' => 'invoice_date',
                'label' => 'Date',
                'format' => 'raw',
                'value' => function ($model) {
                    return \Yii::$app->formatter->asDate($model->invoice_date, 'php:D, d M, Y');;
                }
            ],
            [
                'attribute' => 'invoice_related_to',
                'label' => 'Type',
                'format' => 'raw',
                'value' => function ($model) {
                    return ucwords($model->invoice_related_to);
                },
                'filter' => ['member' => 'Member', 'event' => 'Events'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'attribute' => 'invoice_related_to',
                'label' => 'User',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->invoice_related_to == "member") {

                        if ($model->member <> null) {
                            return $model->member->first_name . " " . $model->member->last_name;
                        }

                    } else {

                        if ($model->user_id <> null) {
                            
                            $member = \common\models\Members::findOne($model->user_id);

                            if ($member <> null) {
                                return $model->member->first_name . " " . $model->member->last_name;
                            }

                        }else {

                            if ($model->invoiceForEventReg <> null) {

                                if (isset($model->invoiceForEventReg) && count($model->invoiceForEventReg) > 0) {
                                    return $model->invoiceForEventReg->firstname . " " . $model->invoiceForEventReg->lastname;
                                }
                            }
                        }
                    }
                },
            ],
            [
                'label' => 'Company',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->invoice_related_to == "member") {
                        if ($model->member <> null) {
                            return ($model->member->accountCompany <> null) ?  $model->member->accountCompany->name : "-";
                        }
                    } else {

                        if ($model->user_id <> null) {

                            $member = \common\models\Members::findOne($model->user_id);

                            if ($member <> null) {
                                if ($member->accountCompany <> null) {
                                    return $member->accountCompany->name;
                                }
                            }
                        }else {

                            if ($model->invoiceForEventReg <> null) {
                                if (isset($model->invoiceForEventReg)) {
                                    return $model->invoiceForEventReg->company;
                                }
                            }
                        }
                    }
                },
            ],
            [
                'label' => 'Description',
                'attribute' => 'invoice_category',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->invoice_category;
                }
            ],
            [
                'label' => 'Payment Status',
                'attribute' => 'payment_status',
                'format' => 'raw',
                'value' => function ($model) {

                    if ($model->payment_status == 'paid') {
                        return '<span class="label label-success">' . ucwords($model->payment_status) . '</span>';
                    }

                    if ($model->payment_status == 'unpaid') {
                        return '<span class="label label-danger">' . ucwords($model->payment_status) . '</span>';
                    }

                    if ($model->payment_status == 'cancelled') {
                        return '<span class="label label-warning">' . ucwords($model->payment_status) . '</span>';
                    }

                    if ($model->payment_status == "refunded") {
                        return '<span class="label label-primary">' . ucwords($model->payment_status) . '</span>';
                    }
                },
                'filter' => [
                    'paid' => 'Paid',
                    'unpaid' => 'Pending',
                    'partialy_paid' => 'Partially Paid',
                    'cancelled' => 'Cancelled',
                    'refunded' => 'Refunded'
                ],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'label' => 'Subtotal',
                'attribute' => 'subtotal',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->total;
                }
            ],
            [
                'label' => 'Tax',
                'attribute' => 'tax',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->total;
                }
            ],
            [
                'label' => 'Total',
                'attribute' => 'total',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->total;
                }
            ],
            [
                'label' => 'Adjustment',
                'format' => 'raw',
                'value' => function ($model) {

                    $invoice = \common\models\Invoices::findOne($model->invoice_id);

                    if($invoice <> null){
                        $adjustment = \common\models\InvoiceAdjustments::findOne(['invoice_id' => $invoice->invoice_id]);

                        if($adjustment <> null){
                           return $adjustment->type .  $adjustment->adjustment;
                        }
                    }

                    return "";
                }
            ],
            [
                'label' => 'Total',
                'format' => 'raw',
                'value' => function ($model) {
                    $fee  = $model->total;
                    $invoice = \common\models\Invoices::findOne($model->invoice_id);

                    if($invoice <> null){
                        $adjustment = \common\models\InvoiceAdjustments::findOne(['invoice_id' => $invoice->invoice_id]);

                        if($adjustment <> null){
                            if($adjustment->type == "-"){
                                $fee  = $model->total - $adjustment->adjustment;
                            }else{
                                $fee  = $model->total + $adjustment->adjustment;
                            }
                        }
                    }

                    return $fee;
                }
            ],
            [
                'label' => 'Adjustment Reason',
                'format' => 'raw',
                'value' => function ($model) {

                    $invoice = \common\models\Invoices::findOne($model->invoice_id);

                    if($invoice <> null){
                        $adjustment = \common\models\InvoiceAdjustments::findOne(['invoice_id' => $invoice->invoice_id]);

                        if($adjustment <> null){
                            return $adjustment->reason;
                        }
                    }

                    return "";
                }
            ],
            [
                'attribute' => 'invoice_sent',
                'label' => 'Invoice sent',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->invoice_sent) ? '<span class="label label-success">Sent</span>' : '<span class="label label-danger">Not Sent</span>';
                },
                'filter' => ['1' => 'Sent', '0' => 'Not Sent'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
        ],
        "exportConfig" => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_PDF => true,
            ExportMenu::FORMAT_EXCEL => true,
        ],
        'target' => '_blank',
        'showColumnSelector' => false,
        "contentBefore" => [
            ["value" => "Invoices"]
        ],
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-warning'
        ]

    ]); ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'label' => 'Invoice #',
                'attribute' => 'invoice_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(str_pad($model->invoice_id, 7, "0", STR_PAD_LEFT),
                        ['/invoices/invoice-pdf', 'invoice' => $model->invoice_id],
                        [
                            'target' => '_blank',
                            'data-pjax' => "0"
                        ]);
                }
            ],
            [
                'attribute' => 'invoice_related_to',
                'label' => 'Type',
                'format' => 'raw',
                'value' => function ($model) {
                    return ucwords($model->invoice_related_to);
                },
                'filter' => ['member' => 'Member', 'event' => 'Events'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'attribute' => 'invoice_related_to',
                'label' => 'First Name',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->invoice_related_to == "member") {
                        if ($model->member <> null) {
                            return $model->member->first_name;
                        }
                    } else {
                        if ($model->invoiceEventReg <> null) {
                            if (isset($model->invoiceEventReg[0])) {
                                return $model->invoiceEventReg[0]->firstname;
                            }
                        }
                    }
                },
            ],
            [
                'attribute' => 'invoice_related_to',
                'label' => 'Last Name',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->invoice_related_to == "member") {
                        if ($model->member <> null) {
                            return $model->member->last_name;
                        }
                    } else {
                        if ($model->invoiceEventReg <> null) {
                            if (isset($model->invoiceEventReg[0])) {
                                return $model->invoiceEventReg[0]->lastname;
                            }
                        }
                    }
                },
            ],
            [
                'label' => 'Company',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->invoice_related_to == "member") {
                        if ($model->member <> null) {
                            return ($model->member->accountCompany <> null) ?  $model->member->accountCompany->name : "-";
                        }
                    } else {
                        if ($model->invoiceEventReg <> null) {
                            if (isset($model->invoiceEventReg[0])) {
                                return $model->invoiceEventReg[0]->company;
                            }
                        }
                    }
                },
            ],
            [
                'label' => 'Description',
                'attribute' => 'invoice_category',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->invoice_category;
                }
            ],
            [
                'label' => 'Payment Status',
                'attribute' => 'payment_status',
                'format' => 'raw',
                'value' => function ($model) {

                    if ($model->payment_status == 'paid') {
                        return '<span class="label label-success">' . ucwords($model->payment_status) . '</span>';
                    }

                    if ($model->payment_status == 'unpaid') {
                        return '<span class="label label-danger">' . ucwords($model->payment_status) . '</span>';
                    }

                    if ($model->payment_status == 'cancelled') {
                        return '<span class="label label-warning">' . ucwords($model->payment_status) . '</span>';
                    }

                    if ($model->payment_status == "refunded") {
                        return '<span class="label label-primary">' . ucwords($model->payment_status) . '</span>';
                    }
                },
                'filter' => [
                    'paid' => 'Paid',
                    'unpaid' => 'Pending',
                    'partialy_paid' => 'Partially Paid',
                    'cancelled' => 'Cancelled',
                    'refunded' => 'Refunded'
                ],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'label' => 'Amount',
                'attribute' => 'total',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->total;
                }
            ],
            [
                'attribute' => 'invoice_date',
                'label' => 'Date',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->invoice_date;
                }
            ],
            [
                'attribute' => 'invoice_sent',
                'label' => 'Invoice sent',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->invoice_sent) ? '<span class="label label-success">Sent</span>' : '<span class="label label-danger">Not Sent</span>';
                },
                'filter' => ['1' => 'Sent', '0' => 'Not Sent'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{record_payment} {update}',
                'buttons' => [
                    'record_payment' => function ($url, $model) {
                        /*return Html::a('<span class="label label-info"><span class="fa fa-plus"></span> Record Payment</span>', '#', [
                            'class' => 'showModalButton',
                            'value' => Url::to(['/invoices/make-payemnt/', 'invoice' => $model->invoice_id]),
                            'title' => Yii::t('yii', 'Make Payment'),
                            'id' => 'sendInvoice-' . $model->invoice_id
                        ]);*/
                        if ($model->payment_status <> 'paid') {
                            return Html::a('<span class="label label-info"><span class="fa fa-money"></span> Pay Now</span>', ['/invoices/make-payemnt/', 'invoice' => $model->invoice_id], [
                                /*'class' => 'showModalButton',
                                'value' => Url::to(),
                                'title' => Yii::t('yii', 'Make Payment'),
                                'id' => 'sendInvoice-' . $model->invoice_id*/
                                'target' => '_blank'
                            ]);
                        } else{
                            return Html::a('<span class="label label-success"><span class="fa fa-task"></span> View Payment</span>', ['/invoices/make-payemnt/', 'invoice' => $model->invoice_id], [
                                /*'class' => 'showModalButton',
                                'value' => Url::to(),
                                'title' => Yii::t('yii', 'Make Payment'),
                                'id' => 'sendInvoice-' . $model->invoice_id*/
                                'target' => '_blank'
                            ]);
                        }
                    },
                    'update' => function($url, $model) {
                        return
                            Html::a('<span class="label label-warning"><span class="fa fa-pencil"></span> Update</span>', $url, [

                            'id' => 'sendInvoice-' . $model->invoice_id
                        ]);
                    }
                ]
            ]
        ],
        'emptyText' => ' - ',
        'showFooter' => true,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'toolbar' => [
            '{export}',
        ],
        'export' => [
            'fontAwesome' => true,
        ],
        'exportConfig' => [
            GridView::EXCEL => true,
            GridView::PDF => true,
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => false,
        ],
    ]); ?>

</div>
