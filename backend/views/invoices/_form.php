<?php

use common\models\Events;
use common\models\Members;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Invoices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoices-form">

    <div class="row">


        <div class="col-md-8 card-box">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
            <div class="col-md-3">

                <label class="control-label" for="membersearch-last_name">Created by</label>
                <?= \kartik\select2\Select2::widget([
                    'model' => $model,
                    'attribute' => 'user_id',
                    'data' => $members,
                    'options' => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
            <div class="col-md-3">

                <?= $form->field($model, 'invoice_related_to')->dropDownList(
                    ['member' => 'Member', 'event' => 'Event'],
                    ['prompt' => 'select ...', 'id' => 'invoice_related_to']
                )->label("Invoice Type"); ?>
            </div>
            <div class="col-md-3">

                <?php
                if ($model->invoice_related_to == "") {
                    $data = ArrayHelper::map(Members::find()->all(), 'id', function ($model) {
                        return $model->first_name . " " . $model->last_name;
                    });

                } else {
                    $data = ArrayHelper::map(Events::find()->all(), 'id', 'title');
                }
                ?>


                <?= $form->field($model, 'invoice_rel_id')->dropDownList(
                    $data,
                    ['prompt' => 'select ...', 'id' => 'invoice_related_id']
                )->label("Invoice Type"); ?>


            </div>
            <div class="col-md-3">

                <?= $form->field($model, 'invoice_category')->textInput(['maxlength' => true]) ?>
            </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'payment_status')->dropDownList(['paid' => 'Paid', 'unpaid' => 'Unpaid', 'partialy_paid' => 'Partialy paid', 'cancelled' => 'Cancelled','refunded' => 'Refunded'], ['prompt' => 'select payment status..']) ?>
                </div>

                <div class="col-md-2">
                    <?= $form->field($model, 'subtotal')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
                </div>

                <div class="col-md-2">
                    <?= $form->field($model, 'tax')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
                </div>

                <div class="col-md-2">
                    <?= $form->field($model, 'total')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'invoice_date')->widget(DateTimePicker::classname(), ['options' => ['placeholder' => 'Registration end date'],
                        'type' => DateTimePicker::TYPE_INPUT,
                        'pluginOptions' => ['autoclose' => true,
                            'format' => 'yyyy-MM-dd']]);
                    ?>
                </div>

                <div class="col-md-12">

                    <?= $form->field($model, 'note')->textarea(['rows' => 8]) ?>
                </div>
            </div>
            <div class="col-md-12">

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'pull-right ' . $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>


        </div>
        <div class="col-md-4  card-box">
            <h2>
                Invoice
                Adjustment <?= Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', '#', ['id' => 'edit_invoice_btn',
                    'title' => Yii::t('yii', 'Edit'),]); ?>
            </h2>
            <div id="adjustment_form" class="<?= ($adjustments <> null) ? '' : 'hidden'; ?>"
                 style="width: 100%; margin-top: 30px; clear: both;">
                <?php $form = ActiveForm::begin(['id' => 'adjustmentForm']); ?>
                <div class="col-md-6">
                    <?= $form->field($adjustments, 'adjustment')->textInput(['maxlength' => true,
                        'required' => 'required',
                        'class' => 'adjustment_amount form-control'])->label("Amount"); ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($adjustments, 'type')->dropDownList(['-' => '-',
                        '+' => '+'], ['required' => 'required', 'class' => 'adjustment_type form-control']); ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($adjustments, 'reason')->textarea(['maxlength' => true,
                        'required' => 'required',
                        'class' => 'adjustment_note form-control',]); ?>
                    <?= $form->field($adjustments, 'invoice_id')->hiddenInput()->label(false); ?>
                </div><!---->
                <button type="button" class="btn btn-default pull-right " id="save_adjustment">save
                </button>
                <?php
                ActiveForm::end();
                ?>
            </div>
        </div>
    </div>

</div>

<?= $this->registerJs('
     $("#edit_invoice_btn").click(function (e) {
        e.preventDefault();
        $("#adjustment_form").toggleClass("hidden");

    });


    $("#save_adjustment").click(function () {

        var $amount = parseFloat($(".adjustment_amount").val());
        var $type = $(".adjustment_type").val();
        var $note = $(".adjustment_note").val();
        var $actual_total = parseFloat($("#actual_total").val());

        if (typeof $amount == "") {

            swal({
                title: "Error",
                text: "Amount Cannot be empty or less than zero",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($amount < 0 ) {
            swal({
                title: "Error",
                text: "Amount Cannot be empty or zero",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($type == "") {

            swal({
                title: "Error",
                text: "Type Cannot be empty",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($note == "") {

            swal({
                title: "Error",
                text: "Reason Cannot be empty",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($type == "-" && $amount > $actual_total) {
            swal({
                title: "Error",
                text: "Adjustment amount should be equal or less than actual amount",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }


        var form = $("#adjustmentForm");
        $.ajax({
            url: "/invoices/invoice-adjustment/",
            type: "post",
            data: form.serialize(),
            success: function (res) {
            console.log(res);
                $("body #custom_bbg_loader").css("display", "none");
                $("html").css("opacity", "1");
                if (res) {

                    $("#adjustment_values").text($type + " " + $amount);

                    if($type == "-"){
                        $("#after_adjust_total").text(parseFloat($actual_total - $amount));
                    }

                    if($type=="+"){
                        $("#after_adjust_total").text(parseFloat($actual_total + $amount));
                    }

                    swal({
                        title: "Good job!",
                        text: "Invoice adjustment saved successfully.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                }
                $(this).attr("disabled", false);
            },
            error: function (data) {
                console.log(data);
            }
        });


    });
    
    
    $("#invoice_related_to").change(function () {
        
        $.ajax({
            url: "/invoices/fetch-relation",
            type: "post",
            data: {"invoice_rel_to" : $("#invoice_related_to").val()},
            success: function (res) {
                
                $("#invoice_related_id").html(res);
                 
                $(this).attr("disabled", false);
            },
            error: function (data) {
                console.log(data);
            }
        });
    
    });
    
');
?>

<?= $this->registerJs("
    function validationFloat() {

        var el = $('.adjustment_amount');

        el.prop('autocomplete', false); // remove autocomplete (optional)
        el.on('keydown', function (e) {
            var allowedKeyCodesArr = [9, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 37, 39, 109, 189, 46, 110, 190];  // allowed keys
            if ($.inArray(e.keyCode, allowedKeyCodesArr) === -1 && (e.keyCode != 17 && e.keyCode != 86)) {  // if event key is not in array and its not Ctrl+V (paste) return false;
                e.preventDefault();
            } else if ($.trim($(this).val()).indexOf('.') > -1 && $.inArray(e.keyCode, [110, 190]) != -1) {  // if float decimal exists and key is not backspace return fasle;
                e.preventDefault();
            } else {
                return true;
            }
            ;
        }).on('paste', function (e) {  // on paste
            var pastedTxt = e.originalEvent.clipboardData.getData('Text').replace(/[^0-9.]/g, '');  // get event text and filter out letter characters
            if ($.isNumeric(pastedTxt)) {  // if filtered value is numeric
                e.originalEvent.target.value = pastedTxt;
                e.preventDefault();
            } else {  // else
                e.originalEvent.target.value = ''; // replace input with blank (optional)
                e.preventDefault();  // retur false
            }
            ;
        });
    }

    validationFloat();
"); ?>
