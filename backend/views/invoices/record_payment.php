<?php

use common\models\Members;
use kartik\form\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Invoices */

$this->title = 'Create Invoices';
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row card-box">
    <div class="invoices-create">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-4">
            <?= $form->field($model, 'invoice_id')->textInput([
                'readonly' => 'readonly',
                'required' => 'required'
            ])->label('Invoice Number') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(Members::find()->all(), 'id', function ($model) {
                return $model->first_name . " " . $model->last_name;
            }), [
                'readonly' => 'readonly',
                'required' => 'required'
            ]); ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'amount_payable')->textInput(['readonly' => 'readonly'])->label('Amount Payable') ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'detail')->textarea([
                'maxlength' => true,
                'required' => 'required'
            ]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'amount')->textInput([
                'required' => 'required',
                'class' => 'validate_float_number'
            ])->label('Amount Paid') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'payment_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'payment date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ],
            ]);
            ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(
                [
                    'done' => 'Paid',
                    'cancelled' => 'Cancelled',
                    'pending' => 'Pending',
                    'authorized' => 'Authorized',
                    'captured' => 'Captured',
                    'refunded' => 'Refunded',
                    'partially_refunded' => 'Partially refunded',
                    'failed' => 'Failed'
                ],
                [
                    'prompt' => 'Select ...',
                    'required' => 'required'
                ]); ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'payment_method')->dropDownList([
                'online' => 'Online',
                'cash' => 'Cash',
                'cheque' => 'Cheque'
            ], ['prompt' => 'Select ...'], [
                'required' => 'required'
            ]);
            ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'transaction_id')->textInput(['maxlength' => true, 'required' => 'required'])->label("Transaction Reference") ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
                <?= Html::submitButton($model->isNewRecord ? 'Pay' : 'Update', [
                    'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-left'
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<script>
    function validationFloat() {

        var el = $('.validate_float_number');

        el.prop("autocomplete", false); // remove autocomplete (optional)
        el.on('keydown', function (e) {
            var allowedKeyCodesArr = [9, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 37, 39, 109, 189, 46, 110, 190];  // allowed keys
            if ($.inArray(e.keyCode, allowedKeyCodesArr) === -1 && (e.keyCode != 17 && e.keyCode != 86)) {  // if event key is not in array and its not Ctrl+V (paste) return false;
                e.preventDefault();
            } else if ($.trim($(this).val()).indexOf('.') > -1 && $.inArray(e.keyCode, [110, 190]) != -1) {  // if float decimal exists and key is not backspace return fasle;
                e.preventDefault();
            } else {
                return true;
            }
            ;
        }).on('paste', function (e) {  // on paste
            var pastedTxt = e.originalEvent.clipboardData.getData('Text').replace(/[^0-9.]/g, '');  // get event text and filter out letter characters
            if ($.isNumeric(pastedTxt)) {  // if filtered value is numeric
                e.originalEvent.target.value = pastedTxt;
                e.preventDefault();
            } else {  // else
                e.originalEvent.target.value = ""; // replace input with blank (optional)
                e.preventDefault();  // retur false
            }
            ;
        });
    }
</script>