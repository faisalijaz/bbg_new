<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Addons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addons-form card-box">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">

        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">

                <li class="active tab">
                    <a href="#add-ons" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">Addons</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#Images" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Images</span>
                    </a>
                </li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="add-ons">

                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'price')->textInput() ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'short_description')->textarea(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'description')->textarea(['maxlength' => true, 'id' => 'elm1']) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div id="images" class="form-group">
                                <label class="control-label" for="input-image">Banner Image</label>
                                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                    <img src="<?= $model->image; ?>" alt="" width="125" height="125" title=""
                                         data-placeholder="no_image.png"/>
                                </a>
                                <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <?= $form->field($model, 'status')->dropDownList(['0' => 'In-active', '1' => 'Active'], ['prompt' => 'Select status']) ?>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <?= $form->field($model, 'max_qty')->textInput() ?>
                        </div>

                        <div class="col-md-6">
                            <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="Images">

                    <table id="images"
                           class="table table-striped table-bordered table-hover images-table upload-preview">
                        <thead>
                        <tr>
                            <td class="text-left">Title</td>
                            <td class="text-left">Description</td>
                            <td class="text-left">Image</td>
                            <td class="text-right">Sort Order</td>
                            <td></td>
                        </tr>
                        </thead>

                        <tbody>
                        <?php $image_row = 0; ?>
                        <?php foreach ($model->addonImages as $addonImage) { ?>
                            <tr id="image-row<?php echo $image_row; ?>">

                                <td class="">
                                    <div class="form-group">
                                        <input type="text" name="Addons[images][<?php echo $image_row; ?>][title]"
                                               value="<?= $addonImage->title ?>" placeholder="Title"
                                               class="form-control"/>
                                    </div>
                                </td>

                                <td class="">
                                    <div class="form-group">
                                        <input type="text" name="Addons[images][<?php echo $image_row; ?>][description]"
                                               value="<?= $addonImage->description ?>" placeholder="description"
                                               class="form-control"/>
                                    </div>
                                </td>
                                <td class="">
                                    <a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image"
                                       class="img-thumbnail">
                                        <img src="<?php echo $addonImage->image; ?>" alt="" title=""
                                             data-placeholder="<?php echo $addonImage->image; ?>"/>
                                    </a>
                                    <input type="hidden" name="Addons[images][<?php echo $image_row; ?>][image]"
                                           value="<?php echo $addonImage->image; ?>"
                                           id="input-image<?php echo $image_row; ?>"/>
                                </td>

                                <td class="">
                                    <div class="form-group">
                                        <input type="text" name="Addons[images][<?php echo $image_row; ?>][sort_order]"
                                               value="<?php echo $addonImage->sort_order; ?>" placeholder="Sort order"
                                               class="form-control"/>
                                    </div>
                                </td>

                                <td class="text-left">
                                    <button type="button"
                                            onclick="$('#image-row<?php echo $image_row; ?>, .tooltip').remove();"
                                            data-toggle="tooltip" title="Remove" class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $image_row++; ?>
                        <?php } ?>
                        </tbody>

                        <tfoot>
                        <tr>
                            <td colspan="4"></td>
                            <td class="text-left">
                                <button type="button" onclick="addImage();" data-toggle="tooltip" title="Add"
                                        class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                            </td>
                        </tr>
                        </tfoot>

                    </table>

                </div>

            </div>

        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    var image_row = <?= $image_row ?>;

    function addImage() {
        html = '<tr id="image-row' + image_row + '">';
        html += '<td>';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="Addons[images][' + image_row + '][title]" value="" placeholder="Title" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="Addons[images][' + image_row + '][description]" value="" placeholder="description" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class=""><a href="" id="thumb-image' + image_row + '" data-toggle="image" class="img-thumbnail"><img src="<?= Yii::getAlias('@web') . '/images/no_image.png' ?>" alt="" title="" data-placeholder="<?= Yii::getAlias('@web') . '/images/no_image.png' ?>" /></a><input type="hidden" name="Addons[images][' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';

        html += '  <td style="width: 10%;">';
        html += '    <div class="form-group">';
        html += '   <input type="text" name="Addons[images][' + image_row + '][sort_order]" value="" placeholder="Sort order" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class=""><button type="button" onclick="$(\'#image-row' + image_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';

        $('#images tbody').append(html);

        image_row++;
    }
</script>
