<?php

use backend\models\User;
use common\models\Categories;
use common\models\Groups;
use common\models\InterestedCategories;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Members;

$interested_categories_selected = [];
$interested_categories = ArrayHelper::map(Categories::find()->where(['type' => 'general'])->all(), 'id', 'title');
if(isset($_GET['id'])) {
    $interested_categories_selected = ArrayHelper::map(InterestedCategories::find()->where(['member_id' => $_GET['id']])->all(), 'category_id', 'id');
}

/* @var $this yii\web\View */
/* @var $model common\models\Accounts */
/* @var $form yii\widgets\ActiveForm */
$paymentCycle = [];
for ($i = 0; $i <= 30; $i++) {
    $paymentCycle[$i] = $i;
    if ($i === 0) {
        $paymentCycle['cash'] = 'Cash';
    }
}
$companyInfo = "";
if ($model->accountCompany <> null) {
    $companyInfo = $model->accountCompany;
}

?>
<div class="row">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card-box col-md-12">
        <h3>Personal Information</h3>

        <div class="accounts-form col-md-12">
            <div class="row">
                <div class="accounts-form col-md-6">
                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Main Image</label>
                        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                            <img src="<?= ($model->picture != "" && $model->picture <> null) ? $model->picture : Yii::$app->params['no_image']; ?>"
                                 alt="" width="125" height="125" title=""
                                 data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                        </a>
                        <?= $form->field($model, 'picture')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
                    </div>
                </div>
                <div class="accounts-form col-md-4">
                    <?= $form->field($model, 'title')->dropDownList(Yii::$app->params['userTitle'], ['prompt' => 'Select ...'])->label() ?>
                </div>

            </div>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'user_name')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'email')->textInput([
                'maxlength' => true,
                /* 'readOnly' => ($model->isNewRecord) ? false : true */
            ]) ?>
            <?php
            if (!$model->isNewRecord) {
                echo $form->field($model, 'last_email')->hiddenInput([])->label(false);
            }
            ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'secondry_email')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'gender')->dropDownList(['Female', 'Male'], ['prompt' => 'Select Gender']) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'nationality')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'country_code')->dropDownList(ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'country_name'), ['prompt' => 'Select Status']); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'vat_number')->textInput(['maxlength' => true]); ?>
        </div>
        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'howDidYouHear')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\ReferralSources::find()->all(), 'id', 'title')
                ,['prompt' => 'Select ...','maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'tellUsAboutYourself')->textarea(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'purposeOfJoining')->textarea(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <label class="control-label" for="membersearch-last_name">Company</label>
            <?= \kartik\select2\Select2::widget([
                'model' => $model,
                'attribute' => 'company',
                'data' => ArrayHelper::map(\common\models\AccountCompany::find()->where(['is_active' => '1'])->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Select ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>


        <div class="accounts-form col-md-12">
            <hr/>
            <h3>Account Information</h3>
            <hr/>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'status')->dropDownList(\Yii::$app->params['statusTitle'], ['prompt' => 'Select Status']) ?>
        </div>


        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(Groups::find()->orderBy(['title' => SORT_ASC])->all(), 'id', 'title'))->label('User Group') ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'account_type')->dropDownList(\Yii::$app->params['accountTypes']) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'expiry_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Expiry date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>




            <?php /*= $form->field($model, 'parent_id')->dropDownList(
                ArrayHelper::map(User::find()->all(),
                    'id',
                    function ($model) {
                        return $model->first_name . ' ' . $model->last_name;
                    }))->label('Parent User');
            */ ?>


        <div class="accounts-form col-md-6">
            <label class="control-label" for="membersearch-last_name">Parent User</label>
            <?= \kartik\select2\Select2::widget([
                'model' => $model,
                'attribute' => 'parent_id',
                'data' => ArrayHelper::map(Members::find()->all(),
                    'id',
                    function ($model) {
                        return $model->first_name . ' ' . $model->last_name;
                    }),
                'options' => ['placeholder' => 'Select ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

        </div>
        <div class="col-md-6">
            <label class="control-label" for="membersearch-last_name">Industry</label>
            <?= \kartik\select2\Select2::widget([
                'model' => $model,
                'attribute' => 'user_industry',
                'data' => ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title'),
                'options' => ['placeholder' => 'Select ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <!-- <div class="row">
            <div class="accounts-form col-md-12">
                <hr/>
                <h3>Company Information</h3>
                <hr/>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Name</label>
                        <input
                                type="text" class="form-control" name="Members[companyData][name]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->name : ''; */ ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Category</label>
                        <? /*= Html::dropDownList('Members[companyData][category]', ($companyInfo <> null) ? $companyInfo->category : '',
                            ArrayHelper::map(Categories::find()->all(), 'id', 'title'), ['class' => 'form-control', 'prompt' => 'Select Category']) */ ?>
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Emirates</label>
                        <input type="text" class="form-control" name="Members[companyData][emirates_number]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->emirates_number : ''; */ ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Url</label>
                        <input type="text" class="form-control" name="Members[companyData][url]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->url : ''; */ ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Phone Number</label>
                        <input type="text" class="form-control" name="Members[companyData][phonenumber]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->phonenumber : ''; */ ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Fax</label>
                        <input type="text" class="form-control" name="Members[companyData][fax]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->fax : ''; */ ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Postal Code</label>
                        <input type="text" class="form-control" name="Members[companyData][postal_code]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->postal_code : ''; */ ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Address</label>
                        <input type="text" class="form-control" name="Members[companyData][address]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->address : ''; */ ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Address</label>
                        <input type="text" class="form-control" name="Members[companyData][vat_num]"
                               value="<? /*= ($companyInfo <> null) ? $companyInfo->vat_num : ''; */ ?>">
                    </div>
                </div>
                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">About</label>
                        <textarea name="Members[companyData][about_company]"
                                  class="textarea form-control"><? /*= ($companyInfo <> null) ? $companyInfo->about_company : ''; */ ?></textarea>
                    </div>
                </div>

            </div>
        </div>-->



        <div class="row">
            <div class="accounts-form col-md-12">
                <hr/>
                <h3>User Docs</h3>
                <hr/>
                <div class="accounts-form col-md-3">

                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Trade Licencse</label>
                        <p>
                            <a href="" id="thumb-image-1" data-toggle="image"
                               class="img-thumbnail">
                                <img src="<?= ($model->memberDocs <> "") ? $model->memberDocs->trade_licence : Yii::$app->params['no_image']; ?>" alt="" title=""
                                     data-placeholder=""  height="150"/>
                            </a>
                            <input type="hidden" name="Members[user_docs][trade_licence]"
                                   value="<?= ($model->memberDocs <> "") ? $model->memberDocs->trade_licence :  Yii::$app->params['no_image']; ?>"
                                   id="input-image-1"/>
                        </p>
                    </div>
                </div>
                <div class="accounts-form col-md-3">
                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Passport Copy</label>
                        <p>
                            <a href="" id="thumb-image-2" data-toggle="image"
                               class="img-thumbnail">
                                <img src="<?= ($model->memberDocs <> "") ? $model->memberDocs->passport_copy : Yii::$app->params['no_image']; ?>" alt="" title=""
                                     data-placeholder="" height="150"/>
                            </a>
                            <input type="hidden" name="Members[user_docs][passport_copy]"
                                   value="<?= ($model->memberDocs <> "") ? $model->memberDocs->passport_copy :  Yii::$app->params['no_image']; ?>"
                                   id="input-image-2"/>
                        </p>
                    </div>
                </div>
                <div class="accounts-form col-md-3">
                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Residence VISA</label>
                        <p>
                            <a href="" id="thumb-image-3" data-toggle="image"
                               class="img-thumbnail">
                                <img src="<?= ($model->memberDocs <> "") ? $model->memberDocs->residence_visa : Yii::$app->params['no_image']; ?>" alt="" title=""
                                     data-placeholder=""  height="150"/>
                            </a>
                            <input type="hidden" name="Members[user_docs][residence_visa]"
                                   value="<?= ($model->memberDocs <> "") ? $model->memberDocs->residence_visa :  Yii::$app->params['no_image']; ?>"
                                   id="input-image-3"/>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="accounts-form col-md-12">
                <hr/>
                <h3>Interested Categories</h3>
                <hr/>
                <?php
                if ($interested_categories <> null) {
                    foreach ($interested_categories as $key => $value){ ?>
                        <div class="checkbox checkbox-info col-md-4">
                            <input name="Members[interested_category][]"
                                <?php if (array_key_exists($key, $interested_categories_selected)) { echo "checked";
                                } ?>
                                   id="interested_category_<?=$key?>" class="styled" value="<?= $key ?>" type="checkbox" >
                            <label for="interested_category_<?=$key?>"><?= $value ?></label>
                        </div>
                    <?php }
                }
                ?>
            </div>
        </div>
    </div>
</div>


<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>