<div class="row">
    <div class="card-box col-md-12">

        <div class="col-md-12 form-group">
            <label>Subject</label>
            <input type="text" name="Number" value="" id="subject" class="form-control" />
        </div>

        <div class="col-md-12 form-group">
            <label>Message</label>
            <textarea class="form-control elm1" id="elm1"></textarea>
        </div>

    </div>
</div>

<div class="row">
    <div class="modal-footer">
        <button type="button" class="btn btn-default" id="btn_send_message">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModalBox">Close</button>
    </div>
</div>

<?= $this->registerJs('
      
      $("body").find("#btn_send_message").click(function(){ 
             alert("dsa");
             if($("#elm1").text() == ""){
                 swal({
                    title: "Sorry",
                    text: "Message cannot be empty",
                    timer: 10000,
                    type: "error",
                    html: true,
                    showConfirmButton: true
                 });
             }
      });
  
'); ?>
