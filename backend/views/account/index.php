<?php
ini_set('memory_limit', '2048M');

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;

$Statusmembers = "";
if (Yii::$app->request->get('status')) {
    $Statusmembers = Yii::$app->request->get('status');
}

?>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="header">
                <h2 class="title"><?= ucwords(($Statusmembers) ? $Statusmembers : "All"); ?> Members</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="row">
        <div class="content table-responsive table-full-width">
            <div class="col-md-6  col-sm-12">
                <?= ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'label' => 'Membership ID',
                            'attribute' => 'id',
                            'value' => function ($model) {
                                return $model->id;
                            }
                        ],
                        [
                            'label' => 'Membership Group',
                            'value' => function ($model) {
                                return ($model->group <> null) ? $model->group->title : "-";
                            }
                        ],
                        [
                            'label' => 'Account Type',
                            'value' => function ($model) {
                                return ($model->account_type <> null) ? $model->account_type : "-";
                            }
                        ],
                        [
                            'label' => 'Membership Type',
                            'value' => function ($model) {
                                return Yii::$app->params['UserTypesforEvent'][$model->user_type_relation];
                            }
                        ],
                        [
                            'attribute' => 'user_name',
                            'value' => function ($model) {
                                return $model->user_name;
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'value' => function ($model) {
                                return $model->title;
                            }
                        ],
                        [
                            'attribute' => 'first_name',
                            'value' => function ($model) {
                                return $model->first_name;
                            }
                        ],
                        [
                            'attribute' => 'last_name',
                            'value' => function ($model) {
                                return $model->last_name;
                            }
                        ],
                        [
                            'attribute' => 'designation',
                            'label' => 'Designation',
                            'value' => function ($model) {
                                return $model->designation;
                            }
                        ],
                        [
                            'attribute' => 'user_industry',
                            'value' => function ($model) {
                                if ($model->userIndustry <> null) {
                                    return $model->userIndustry->title;
                                } else if ($model->accountCompany <> null) {
                                    if ($model->accountCompany->companyCategory <> null) {
                                        return $model->accountCompany->companyCategory->title;
                                    }
                                }
                            }
                        ],
                        [
                            'attribute' => 'email',
                            'value' => function ($model) {
                                return $model->email;
                            }
                        ],
                        [
                            'attribute' => 'secondry_email',
                            'label' => 'Secondary Email',
                            'value' => function ($model) {
                                return $model->secondry_email;
                            }
                        ],
                        [
                            'attribute' => 'phone_number',
                            'value' => function ($model) {
                                return ucwords($model->phone_number);
                            }
                        ],
                        [
                            'attribute' => 'Country',
                            'value' => function ($model) {
                                return ($model->accountCountry <> null) ? $model->accountCountry->country_name : "-";
                            }
                        ],
                        [
                            'attribute' => 'city',
                            'value' => function ($model) {
                                return $model->city;
                            }
                        ],
                        [
                            'label' => 'Nationality',
                            'value' => function ($model) {
                                return ucwords($model->nationality);
                            }
                        ],
                        [
                            'label' => 'Address',
                            'value' => function ($model) {
                                return ucwords($model->address);
                            }
                        ],

                        [
                            'label' => 'Status',
                            'value' => function ($model) {
                                return Yii::$app->params['statusTitle'][$model->status];
                            }
                        ],
                        [
                            'label' => 'Invoiced',
                            'value' => function ($model) {
                                return ($model->invoiced) ? "Yes" : "No";
                            }
                        ],
                      /*  [
                            'label' => 'Registration Date',
                            'value' => function ($model) {
                                return formatDate($model->registeration_date);
                            }
                        ],
                        [
                            'label' => 'Expiry Date',
                            'value' => function ($model) {
                                return formatDate($model->expiry_date);
                            }
                        ],
                        [
                            'label' => 'Last Renewal',
                            'value' => function ($model) {
                                return formatDate($model->last_renewal);
                            }
                        ],*/
                        [
                            'label' => 'Registration Date',
                            'value' => function ($model) {
                                $newDate1 = '';
                                if($model->registeration_date <> null && $model->registeration_date != "0000-00-00") {

                                    $newDate1 = date("d-m-Y", strtotime($model->registeration_date));
                                    $newDate1 = str_replace("-","/",$newDate1);
                                }
                                return $newDate1;
                            }
                        ],
                        [
                            'label' => 'Expiry Date',
                            'value' => function ($model) {

                                $newDate2 = '';
                                if($model->expiry_date <> null && $model->expiry_date != "0000-00-00") {

                                    $newDate2 = date("d-m-Y", strtotime($model->expiry_date));
                                    $newDate2 = str_replace("-","/",$newDate2);
                                }
                                return $newDate2;
                            }
                        ],
                        [
                            'label' => 'Last Renewal',
                            'value' => function ($model) {

                                $newDate3 = '';
                                if($model->last_renewal <> null && $model->last_renewal != "0000-00-00") {

                                    $newDate3 = date("d-m-Y", strtotime($model->last_renewal));
                                    $newDate3 = str_replace("-","/",$newDate3);
                                }
                                return $newDate3;
                            }
                        ],
                        [
                            'label' => 'Newsletter Subscription',
                            'value' => function ($model) {
                                return ($model->newsletter <> null) ? "Yes" : "No";
                            }
                        ],
                        [
                            'label' => 'VAT Number',
                            'value' => function ($model) {
                                return $model->vat_number;
                            }
                        ],
                        [
                            'label' => 'LinkedIn',
                            'value' => function ($model) {
                                return $model->linkedin;
                            }
                        ],
                        [
                            'label' => 'Twitter',
                            'value' => function ($model) {
                                return $model->twitter;
                            }
                        ],
                        [
                            'label' => 'Approved',
                            'value' => function ($model) {
                                return ($model->approved) ? "Yes" : "No";
                            }
                        ],
                        [
                            'attribute' => 'company',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return ucwords($model->accountCompany->name);
                                }
                                return '-';
                            },
                        ],
                        [
                            'label' => 'Category',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    if ($model->accountCompany->companyCategory <> null) {
                                        return ucwords($model->accountCompany->companyCategory->title);
                                    }
                                }
                                return '-';
                            }
                        ],
                        [
                            'label' => 'Url',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->url;
                                }
                                return '-';
                            }
                        ],
                        [
                            'label' => 'Phone Number',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->phonenumber;
                                }
                                return '-';
                            }
                        ],
                        [
                            'label' => 'Fax',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->fax;
                                }
                                return '-';
                            },
                        ],
                        [
                            'label' => 'Postal Code',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->postal_code;
                                }
                                return '-';
                            },
                        ],
                        [
                            'label' => 'Emirate',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->emirates_number;
                                }
                                return '-';
                            },
                        ],
                        [
                            'label' => 'Company Address',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->address;
                                }
                                return '-';
                            },
                        ],
                        [
                            'label' => 'Postal Code',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->about_company;
                                }
                                return '-';
                            },
                        ],

                    ],
                    "exportConfig" => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_CSV => false,
                        ExportMenu::FORMAT_PDF => true,
                        ExportMenu::FORMAT_EXCEL => true,
                    ],
                    'target' => '_blank',
                    'showColumnSelector' => false,
                    "contentBefore" => [
                        ["value" => "BBG Members List"]
                    ],
                    'dropdownOptions' => [
                        'label' => 'Export All',
                        'class' => 'btn btn-warning'
                    ]

                ]); ?>
            </div>
            <div class="col-md-6 col-sm-12">

                <p class="pull-right">
                    <?= Html::a('Create Accounts', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
            <div class="card-box col-md-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'condensed' => true,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'user_name',
                            'label' => 'User Name',
                            'headerOptions' => ['style' => 'width: 5%;'],
                        ],
                        [
                            'attribute' => 'first_name',
                            'headerOptions' => ['style' => 'width: 5%;'],
                        ],
                        [
                            'attribute' => 'last_name',
                            'headerOptions' => ['style' => 'width: 5%;'],
                        ],
                        [
                            'attribute' => 'user_industry',
                            'value' => function ($model) {
                                if ($model->userIndustry <> null) {
                                    return $model->userIndustry->title;
                                } else if ($model->accountCompany <> null) {
                                    if ($model->accountCompany->companyCategory <> null) {
                                        return $model->accountCompany->companyCategory->title;
                                    }
                                }
                            },
                            'headerOptions' => ['style' => 'width: 5%;'],
                        ],
                        [
                            'attribute' => 'company',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->name;
                                }
                                return '-';
                            },
                            'headerOptions' => ['style' => 'width: 8%;'],
                        ],
                        [
                            'attribute' => 'email',
                            'format' => 'email',
                            'headerOptions' => ['style' => 'width: 5%;'],

                        ],
                        [
                            'attribute' => 'phone_number',
                            'headerOptions' => ['style' => 'width: 5%;'],
                        ],
                        [
                            'attribute' => 'Membership Type',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->group_id <> null) {
                                    if ($model->group <> null) {
                                        return '<span class="label label-info"> ' . $model->group->title . ' </span>';
                                    }
                                }
                                return '-';
                            },
                            'filter' => ['Additional' => 'additional', 'business' => 'Business', 'nominee' => 'Nominee', 'named_associate' => 'Named Associate'],
                            'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 12%;']
                        ],
                        [
                            'attribute' => 'account_type',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->account_type <> null) {
                                    if ($model->account_type == "named_associate") {
                                        return '<span class="label label-info"> Named Associate </span>';
                                    } else {
                                        return '<span class="label label-info">' . ucwords($model->account_type) . '</span>';
                                    }
                                }
                                return '-';
                            },
                            'filter' => ['Additional' => 'additional', 'business' => 'Business', 'nominee' => 'Nominee', 'named_associate' => 'Named Associate'],
                            'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 10%;']
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($model) {

                                $status = $model->status;
                                $class = "label-warning";

                                if ($status == '1') {
                                    $class = "label-success";
                                }

                                if ($status == '2') {
                                    $class = "label-info";
                                }

                                if ($status == '3') {
                                    $class = "label-danger";
                                }

                                if ($status == '4') {
                                    $class = "label-primary";
                                }

                                return '<span id="_status_' . $model->id . '" class="label ' . $class . '">' . ucwords(Yii::$app->params['statusTitle'][$status]) . '</span>';
                            },
                            'filter' => ['In-active', 'Active', 'Invoiced', 'Deleted', 'Renewal required'],
                            'filterInputOptions' => ['prompt' => 'Select Status...', 'class' => 'form-control', 'id' => null],
                            'contentOptions' => ['class' => 'text-center'],
                            //'headerOptions' => ['class' => 'text-center', 'style' => 'width: 12%;'],
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{gen_invoice} {message} {change_status} {view} {update} {delete}',
                            'headerOptions' => ['class' => 'text-center', 'style' => 'width: 10%;'],
                            'buttons' => [

                                'gen_invoice' => function ($url, $model) {


                                    if ($model->status == '4' && $model->renw_invoice_gen == 0) {

                                        $url = Url::to(['/account/send-renewal-invoice/', 'member' => $model->id]);

                                        return Html::a('<span class="label label-info"><i class="fa fa-money"></i></span>', '#', [
                                            'class' => 'showModalButton',
                                            'value' => $url,
                                            'title' => Yii::t('yii', 'Send Renewal Invoice'),
                                            'id' => 'sendInvoice-' . $model->id
                                        ]);

                                    } else {

                                        $url = Url::to(['/account/send-invoice/', 'member' => $model->id]);

                                        $title = "Send Invoice";
                                        if ($model->invoiced) {
                                            $title = "Re-Send Invoice";
                                        }

                                        if ($model->status == '4') {
                                            $url = Url::to(['/account/send-renewal-invoice/', 'member' => $model->id]);
                                        }

                                        return Html::a('<span class="label label-info"><i class="fa fa-money"></i></span>', '#', [
                                            'class' => 'showModalButton',
                                            'value' => $url,
                                            'title' => Yii::t('yii', $title),
                                            'id' => 'sendInvoice-' . $model->id
                                        ]);
                                    }

                                },
                                'change_status' => function ($url, $model) {

                                    $label = '<span class="label label-danger"><i class="fa fa-check-square-o"></i></span>';
                                    $class = 'approve-true change_status';
                                    $title = 'Approve';

                                    if ($model->status <> 'approved') {
                                        $label = '<span class="label label-success"><i class="fa fa-check-square-o"></i></span>';
                                        $class = 'approve-false change_status';
                                        $title = 'UnApprove';
                                    }

                                    return Html::a($label, '#', ['class' => $class, 'title' => $title, 'id' => $model->id]);
                                },
                                'message' => function ($url) {
                                    return Html::a(
                                        '<span class="label label-info"><i class="fa fa-envelope"></i></span>',
                                        ['#'],
                                        [
                                            'class' => 'showModalButton',
                                            'value' => \yii\helpers\Url::to(['/account/send-message', 'event_id' => 1]),
                                            'title' => Yii::t('yii', 'Delete'),
                                            'title' => 'Send Message',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'view' => function ($url) {
                                    return Html::a(
                                        '<span class="label label-info"><i class="fa fa-search-plus"></i></span>',
                                        $url,
                                        [
                                            'title' => 'View Member',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'update' => function ($url) {
                                    return Html::a(
                                        '<span class="label label-warning"><i class="fa fa-pencil"></i></span>',
                                        $url,
                                        [
                                            'title' => 'Update Member Data',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'delete' => function ($url, $model) {
                                    return Html::a(
                                        '<span class="label label-danger"><i class="fa fa-trash"></i></span>',
                                        ['/account/delete', 'id' => $model->id, 'event_id' => $model->id],
                                        [
                                            'title' => 'Delete Record',
                                            'data-pjax' => '0',
                                            'data' => [
                                                'confirm' => "Are you sure you want to delete?",
                                                'method' => 'post',
                                            ]
                                        ]
                                    );
                                },
                            ]
                        ]
                    ],
                    'emptyText' => ' - ',
                    'showFooter' => true,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                    ],
                    'toolbar' => [
                        '{export}',
                    ],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'exportConfig' => [
                        GridView::EXCEL => true,
                        GridView::PDF => true,
                    ],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => false,
                    ],
                ]);
                ?>

            </div>
        </div>
    </div>
    </div>
<?= $this->registerJs('
    
    $("body").on("click", "a.change_status", function(e) {
    
        // $(".change_status").click(function(e){
        
        e.preventDefault(); 
        $(this).attr("disabled",true);
        var subscription = $(this).attr("id"); 
        
        var status = 0;
        
        if($(this).hasClass("approve-true")){
            status = 1;
        }
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/account/update-status/",
            type: "POST",
            data: { status: status , id : subscription }, 
            success : function(res){
                 
                var text = "UnApproved"; 
                
                if(res == "1"){ 
                   
                    if(status == "1"){
                        
                        text = "Approved"; 
                        $(this).addClass("approve-false");
                        $(this).removeClass("approve-true");
                        
                        $("#" +subscription + " .label").removeClass("label-danger"); 
                        $("#" +subscription + " .label").addClass("label-success");
                        
                    } if(status == "0"){
                    
                        $(this).removeClass("approve-false");
                        $(this).addClass("approve-true");
                        
                        
                        $("#" +subscription + " .label").removeClass("label-success");
                        $("#" +subscription + " .label").addClass("label-danger");
                    } else{
                        
                    }
                    
                    swal({
                        title: "Success",
                        text: "Member " + text,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }     
                 
                $(this).attr("disabled",false);
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
'); ?>