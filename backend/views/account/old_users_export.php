<?php
use kartik\export\ExportMenu;

?>

<div class="col-md-6">
    <?= ExportMenu::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => \common\models\TblUser::find()
        ]),
        'columns' => [
            [
                'label' => 'Membership ID',
                'attribute' => 'id',
                'value' => function ($model) {
                    return $model->id;
                }
            ],
            [
                'label' => 'Category',
                'value' => function ($model) {
                    return $model->category;
                }
            ],
            [
                'label' => 'Type',
                'value' => function ($model) {
                    return $model->type;
                }
            ],
            [
                'label' => 'Parent Id',
                'value' => function ($model) {
                    return $model->parent_id;
                }
            ],
            [
                'attribute' => 'User Name',
                'value' => function ($model) {
                    return $model->username;
                }
            ],
            [
                'attribute' => 'BBG Membership ID',
                'value' => function ($model) {
                    return $model->bbg_membershipid;
                }
            ],

            [
                'label' => 'Title',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->title: "";
                }
            ],

            [
                'label' => 'Name',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->firstname . " " . 	$model->profile->lastname: "";
                }
            ],

            [
                'label' => 'Email',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->email: "";
                }
            ],


            [
                'label' => 'Secondery Email',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->secondery_email: "";
                }
            ],


            [
                'label' => 'Company Name',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->companyname: "";
                }
            ],

            [
                'label' => 'Position',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->position: "";
                }
            ],

            [
                'label' => 'Nationality',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->nationality: "";
                }
            ],


            [
                'label' => 'Company Url',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->companyurl: "";
                }
            ],

            [
                'label' => 'Linkedinurl',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->linkedinurl: "";
                }
            ],



            [
                'label' => 'Vat Number',
                'value' => function ($model) {
                    return ($model->profile <> null) ? 	$model->profile->vat_number: "";
                }
            ],


            /////////////////////////////////////////////
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->status;
                }
            ],
            [
                'attribute' => 'approved',
                'value' => function ($model) {
                    return $model->approved;
                }
            ],
            [
                'label' => 'Honorary',
                'value' => function ($model) {
                    return ($model->honourary == 'true') ? "Yes" : "No";
                }
            ],
            [
                'label' => 'Focus Chair',
                'value' => function ($model) {
                    return ($model->focus_chair == 'true') ? "Yes" : "No";
                }
            ],
            [
                'label' => 'Committee',
                'value' => function ($model) {
                    return ($model->committee == 'true') ? "Yes" : "No";
                }
            ],
            [
                'label' => 'Sponsor',
                'value' => function ($model) {
                    return ($model->sponsor == 'true') ? "Yes" : "No";
                }
            ],
            [
                'label' => 'Charity',
                'value' => function ($model) {
                    return ($model->charity == 'true') ? "Yes" : "No";
                }
            ],
            [
                'attribute' => 'Last renewal',
                'value' => function ($model) {
                    return $model->lastrenewal;
                }
            ],
            [
                'label' => 'Request Renewal',
                'value' => function ($model) {
                    return $model->request_renewal;
                }
            ],

        ],
        "exportConfig" => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_PDF => true,
            ExportMenu::FORMAT_EXCEL => true,
        ],
        'target' => '_blank',
        'showColumnSelector' => false,
        "contentBefore" => [
            ["value" => "BBG Members List"]
        ],
        'dropdownOptions' => [
            'label' => 'Export All Old Members',
            'class' => 'btn btn-warning'
        ]

    ]); ?>
</div>