<?php

use kartik\select2\Select2;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounts-search">

    <?php $form = ActiveForm::begin([
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-2 col-sm-12">
                <?= $form->field($model, 'id')->textInput()->label("Membership ID");
                ?>
            </div>

            <div class="col-lg-3 col-sm-12">
                <?= $form->field($model, 'first_name')->textInput();
                ?>
            </div>

            <div class="col-lg-3 col-sm-12">
                <?= $form->field($model, 'last_name')->textInput();
                ?>
            </div>
            <div class="col-lg-4 col-sm-12">
                <label class="control-label" for="membersearch-last_name">Company</label>
                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'company',
                    'data' => ArrayHelper::map(\common\models\AccountCompany::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Select a company ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-3 col-sm-12">
                <?= $form->field($model, 'email')->textInput();
                ?>
            </div>
            <div class="col-lg-2 col-sm-12">
                <?= $form->field($model, 'group_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\common\models\Groups::find()->all(), 'id', 'title'),
                    ['prompt' => 'Select Type']
                )->label('Membership Type'); ?>
            </div>

            <div class="col-lg-2 col-sm-12">
                <?= $form->field($model, 'account_type')->dropDownList(Yii::$app->params['accountTypes'], ['prompt' => 'Select Type']) ?>
            </div>

            <div class="col-lg-2 col-sm-12">
                <?= $form->field($model, 'user_type_relation')->dropDownList(array_filter(Yii::$app->params['UserTypesforEvent']), ['prompt' => 'Select Type']); ?>
            </div>

            <div class="col-lg-2 col-sm-12">
                <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['statusTitle'], ['prompt' => 'Select Status ...']) ?>
            </div>


            <div class="col-lg-2 col-sm-12">
                <?= $form->field($model, 'approved')->dropDownList(['0' => 'Un-Approved', '1' => 'Approved'], ['prompt' => 'Select Status ...']) ?>
            </div>

            <div class="col-lg-4 col-sm-12">
                <?= $form->field($model, 'registeration_date')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Registration date'],
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-MM-dd'
                    ]
                ])->label('Registration date');
                ?>
            </div>


            <div class="col-lg-4 col-sm-12">
                <?= $form->field($model, 'expiry_date')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Expiry date'],
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-MM-dd'
                    ]
                ]);
                ?>            </div>
            <div class="col-lg-2 col-sm-12">
                <div class="form-group inline-row " style="margin-top: 25px;" style="width: 100%;margin: 8px;">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
