<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use common\models\InvoiceAdjustments;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;

$type = ($invoice <> null)  ? ($invoice->invoice_related_to == "event") ? "E-" : "M-" : "";

$_invoice_id = ($invoice <> null)  ? $invoice->invoice_id : 0;

$inv_subtotal = 0;
$inv_tax = 0;
$inv_total = 0;
if($invoice <> null){
    $inv_subtotal = $invoice->subtotal;
    $inv_tax = $invoice->tax;
    $inv_total = $invoice->total;
}

if(isset($adjustments)){
    if($adjustments == null){
        $adjustments = new InvoiceAdjustments();
    }
}else{
    $adjustments = new InvoiceAdjustments();
}
$adjustments->invoice_id = $_invoice_id;
?>

<section class="MainArea" style="font-family: 'Century Gothic'">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" id="invoice_logo">
                    <a href="/"><img src="<?= Yii::$app->params['invoice_header_image']; ?>" alt="BBG-Dubai"></a>
                </div>

                <div class="col-3 col-sm-3 col-md-4 col-lg-4 col-xl-4" id="invoice_date_number">
                    <h1 style="color: #000;"><b><?= Yii::$app->params['invoice_title']; ?></b></h1>
                    <strong><span>Invoice date: <?= ($invoice <> null) ? formatDate($invoice->invoice_date) : ""; ?></span></strong><br/>
                    <strong><span>Invoice number: <?= ($invoice <> null) ? $type . str_pad($invoice->invoice_id, 7, "0", STR_PAD_LEFT) : 0; ?></span></strong>
                </div>

                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" id="invoice_comapny_info">
                    <br/><br/>
                    <strong><br/><?= (\Yii::$app->appSettings <> null) ? strip_tags(\Yii::$app->appSettings->settings->about, '<br>')  : ""; ?></strong>
                    <strong><br/>Telehephone: <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->telephone : ""; ?><br/></strong>
                    <strong>Email: <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->admin_email : ""; ?><br/></strong>
                    <strong>VAT Registration no:
                        <u><?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->app_vat : ""; ?></u><br/></strong>
                </div>

                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" id="invoice_client_info">
                    <br/>
                    <strong>Bill To:</strong><br/>
                    <strong>Name: <?= ($member <> null) ? $member->first_name . ' ' . $member->last_name : ""; ?>
                        <?= ($member <> null && $member->account_type == 'guest') ? '(Guest)' : '(' . ucwords($member->account_type) . ')'; ?></strong>
                    <br/>
                    <strong>Company: <?= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->name : ""; ?></strong>
                    <br/>
                    <strong>Address: <?= ($member <> null) ? $member->address : ""; ?><br/></strong>
                    <strong>P.O
                        Box: <?= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->postal_code : ""; ?><br/></strong>
                    <strong>Telephone: <?= ($member <> null) ? $member->phone_number : ""; ?></strong>
                    <br/>
                    <strong>VAT Registration no:
                        <u><?= ($member <> null) ? $member->vat_number : ""; ?></u><br/><br/></strong>
                </div>

                <div style="width: 100%;" class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                    <div style="width: 100%;">
                        <table class="table" style="width: 100%; float: left;" cellpadding="5">
                            <thead>
                            <tr>
                                <th class="col-md-8"
                                    style="width:70%; background: #1d355f; color: #ffffff; text-align: left;">Item
                                </th>
                                <th class="col-md-4"
                                    style="width:30%; background: #1d355f; color: #ffffff; text-align: center">Amount
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($invoice <> null) {
                                if ($invoice->invoiceItems <> null) {
                                    foreach ($invoice->invoiceItems as $key => $item) {
                                        $itme_data = \common\models\InvoiceItems::find()->where([
                                            'id' => $item->id,
                                        ])->one();


                                        if ($member->group_id != $itme_data->invoice_rel_id) {
                                            $group = ($member->group <> null) ? $member->group : null;
                                            $title = ($group <> null) ? $group->title : "";


                                            if ($item->invoice_category == 'Business' || $item->invoice_category == 'Individual' || $item->invoice_category == 'Business Lite') {
                                                $item->invoice_category = $title;
                                                Yii::$app->db->createCommand('UPDATE invoice_items SET invoice_rel_id=' . $member->group_id . ',invoice_category="' . $title . '" WHERE id=' . $item->id)
                                                    ->execute();
                                            } else {
                                                //  $item->invoice_category = $title;
                                                $item->invoice_category = $item->invoice_category;
                                                Yii::$app->db->createCommand('UPDATE invoice_items SET invoice_rel_id=' . $member->group_id . ' WHERE id=' . $item->id)
                                                    ->execute();
                                            }

                                        }

                                        if ($item->invoice_category == 'Business' || $item->invoice_category == 'Individual' || $item->invoice_category == 'Business Lite') {
                                        } else {
                                            $group = ($member->group <> null) ? $member->group : null;
                                            $title = ($group <> null) ? $group->title : "";
                                            $item->invoice_category = $item->invoice_category. ' ('.$title.')';
                                        }



                                        ?>
                                        <tr>
                                            <td><?= $item->invoice_category; ?></td>
                                            <td style="text-align: right;">AED <?= $item->subtotal; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div style="width: 100%;border-top: #000 solid 1px;margin-top: 30px;clear: both;">
                        <table class="table" style="width: 100%; float: left;" cellpadding="5">
                            <tbody>
                            <tr>
                                <td style="width:70%;text-align: left;">
                                    <strong>Subtotal</strong>
                                    <br/><strong>VAT(5%)</strong>
                                </td>
                                <td style="width:30%;text-align: right;">
                                    <strong><?= $inv_subtotal; ?></strong>
                                    <br/><strong><?= $inv_tax; ?></strong>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:70%;text-align: left;">
                                    <br/><strong>Total</strong>
                                </td>
                                <td style="width:30%;text-align: right;">
                                    <br/>
                                    <strong>
                                        AED <span id="before_adjust_total">
                                                    <?= $inv_subtotal + $inv_tax; ?>
                                                </span>
                                        <input type="hidden"
                                               value="<?= $inv_subtotal + $inv_tax; ?>"
                                               id="actual_total"/>
                                    </strong>

                                    <?= Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', '#', [
                                        'id' => 'edit_invoice_btn',
                                        'title' => Yii::t('yii', 'Edit'),
                                    ]); ?>
                                </td>
                            </tr>

                            <?php
                            if ($invoice <> null) {
                                if ($invoice->adjustment <> null) {
                                    $adjustment = $invoice->adjustment;
                                    ?>
                                    <tr>
                                        <td style="width:70%;text-align: left;">
                                            <strong>Adjustment</strong>
                                            <br/>
                                            <?= $adjustment->reason; ?>
                                        </td>
                                        <td style="width:30%;text-align: right;">
                                            <strong id="adjustment_values">
                                                <?= $adjustment->type . " " . $adjustment->adjustment; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:70%;text-align: left;"><strong>Total Payable</strong>
                                        </td>
                                        <td style="width:30%;text-align: right;">
                                            <strong>
                                                AED <span id="after_adjust_total">
                                                            <?= ($invoice <> null) ?$invoice->total : 0; ?>
                                                        </span>
                                            </strong>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                    <div id="adjustment_form" class="hidden"
                         style="width: 100%; margin-top: 30px; clear: both;">
                        <?php $form = ActiveForm::begin([
                            'id' => 'adjustmentForm'
                        ]); ?>
                        <div class="col-md-6">
                            <?= $form->field($adjustments, 'adjustment')->textInput([
                                'maxlength' => true,
                                'required' => 'required',
                                'class' => 'adjustment_amount'
                            ])->label("Amount"); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($adjustments, 'type')->dropDownList([
                                '-' => '-',
                                '+' => '+'
                            ], ['required' => 'required', 'class' => 'adjustment_type']); ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($adjustments, 'reason')->textarea([
                                'maxlength' => true,
                                'required' => 'required',
                                'class' => 'adjustment_note'
                            ]); ?>
                            <?= $form->field($adjustments, 'invoice_id')->hiddenInput()->label(false); ?>
                        </div><!---->
                        <button type="button" class="btn btn-default pull-right" id="save_adjustment">save
                        </button>
                        <?php
                        ActiveForm::end();
                        ?>
                    </div>

                </div>

                <div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                    <?php
                    if ($invoice <> null) {
                        if ($invoice->payment_status == 'unpaid') {
                            echo \yii\helpers\Html::a('Pay Online',
                           //     [\Yii::$app->params['appUrl'] . "/site/invoice-payment", 'invoice_id' => $invoice->invoice_id]);
                                "https://bbgdubai.org/site/invoice-payment?invoice_id=$invoice->invoice_id");
                            echo '<br/>';
                        }
                    }
                    ?>
                    <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->invoice_payment_info : ""; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="member_send_invoice_to_admin">Send to admin</button>
                <button type="button" class="btn btn-default" id="member_send_invoice">Send Invoice</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModalBox">Close</button>
            </div>
        </div>
    </div>
</section>

<style>
    .searchResult .view a {
        font-weight: bold;
    }

    #events, #news {
        background: #ffffff;
    }

    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
    #invoice_logo, #invoice_comapny_info {
        width: 60%;
        float: left;
    }

    #invoice_date_number {
        width: 35%;
        float: right;
    }

    #invoice_client_info{
        width: 40%;
        float: right;
    }

    @media (max-width: 767px) {

        #invoice_logo, #invoice_comapny_info {
            width: 100% !important;
            float: left;
        }

        #invoice_date_number {
            width: 100% !important;
            float: right;
        }

        #invoice_client_info{
            width:  100% !important;
            float: right;
        }

        .modal-dialog{
            width: 95% !important;
        }
    }
</style>
<!--// $this->registerJs('
-->
<script>
    $("body").on("click", "#member_send_invoice", function (e) {
        
        $.ajax({

            url: "/account/send-invoice-email/",
            type: "POST",
            data: {member: "<?= $member->id; ?>", invoice_id  : '<?= $_invoice_id; ?>' },
            beforeSend: function () {
                $("body #custom_bbg_loader").css("display", "block");
                $("html").css("opacity", ".5");
                $("html").css("background-color", "#000");
            },
            success: function (res) {
                $("body #custom_bbg_loader").css("display", "none");

                console.log(res);

                $("html").css("opacity", "1");
                if (res) {

                    $("#sendInvoice-<?= $member->id;?>").addClass("hidden");

                    swal({
                            title: "Great!",
                            text: "Invoice sent successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                        function () {
                            $("#closeModalBox").trigger("click");
                        });

                }
                $(this).attr("disabled", false);
            },
            error: function (data) {
                swal({
                        title: "Oooppss!",
                        text: data.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                    function () {
                        $("#closeModalBox").trigger("click");
                    });
            }
        });
    });
    $("body").on("click", "#member_send_invoice_to_admin", function (e) {

        $.ajax({

            url: "/account/send-invoice-emailren/",
            type: "POST",
            data: {member: "<?= $member->id; ?>", invoice_id  : '<?= $_invoice_id; ?>' },
            beforeSend: function () {
                $("body #custom_bbg_loader").css("display", "block");
                $("html").css("opacity", ".5");
                $("html").css("background-color", "#000");
            },
            success: function (res) {
                $("body #custom_bbg_loader").css("display", "none");

                console.log(res);

                $("html").css("opacity", "1");
                if (res) {

                   // $("#sendInvoice-<?= $member->id;?>").addClass("hidden");

                    swal({
                            title: "Great!",
                            text: "Invoice sent successfully.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                        function () {
                            $("#closeModalBox").trigger("click");
                        });

                }
                $(this).attr("disabled", false);
            },
            error: function (data) {
                swal({
                        title: "Oooppss!",
                        text: data.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                    function () {
                        $("#closeModalBox").trigger("click");
                    });
            }
        });
    });



    $("#edit_invoice_btn").click(function () {

        $("#adjustment_form").toggleClass("hidden");

    });


    $("#save_adjustment").click(function () {
        
        alert("");
        
        var $amount = parseFloat($(".adjustment_amount").val());
        var $type = $(".adjustment_type").val();
        var $note = $(".adjustment_note").val();
        var $actual_total = parseFloat($("#actual_total").val());

        if (typeof $amount == "") {

            swal({
                title: "Error",
                text: "Amount Cannot be empty or zero",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($amount == "" || $amount <= 0) {
            swal({
                title: "Error",
                text: "Amount Cannot be empty or zero",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($type == "") {

            swal({
                title: "Error",
                text: "Type Cannot be empty",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($note == "") {

            swal({
                title: "Error",
                text: "Reason Cannot be empty",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }

        if ($type == "-" && $amount > $actual_total) {
            swal({
                title: "Error",
                text: "Adjustment amount should be equal or less than actual amount",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            });

            return false;
        }


        var form = $("#adjustmentForm");
        $.ajax({
            url: "/event/event-invoice-adjustment/",
            type: "post",
            data: form.serialize(),
            success: function (res) {
                $("body #custom_bbg_loader").css("display", "none");
                $("html").css("opacity", "1");
                if (res) {

                    $("#adjustment_values").text($type + " " + $amount);

                    if($type == "-"){
                        $("#after_adjust_total").text(parseFloat($actual_total - $amount));
                    }

                    if($type=="+"){
                        $("#after_adjust_total").text(parseFloat($actual_total + $amount));
                    }

                    swal({
                        title: "Good job!",
                        text: "Invoice sent successfully.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                }
                $(this).attr("disabled", false);
            },
            error: function (data) {
                console.log(data);
            },

        });


    });

    function validationFloat() {

        var el = $(".adjustment_amount");

        el.prop("autocomplete", false); // remove autocomplete (optional)
        el.on("keydown", function (e) {
            var allowedKeyCodesArr = [9, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 37, 39, 109, 189, 46, 110, 190];  // allowed keys
            if ($.inArray(e.keyCode, allowedKeyCodesArr) === -1 && (e.keyCode != 17 && e.keyCode != 86)) {  // if event key is not in array and its not Ctrl+V (paste) return false;
                e.preventDefault();
            } else if ($.trim($(this).val()).indexOf('.') > -1 && $.inArray(e.keyCode, [110, 190]) != -1) {  // if float decimal exists and key is not backspace return fasle;
                e.preventDefault();
            } else {
                return true;
            }
            ;
        }).on("paste", function (e) {  // on paste
            var pastedTxt = e.originalEvent.clipboardData.getData("Text").replace(/[^0-9.]/g, "");  // get event text and filter out letter characters
            if ($.isNumeric(pastedTxt)) {  // if filtered value is numeric
                e.originalEvent.target.value = pastedTxt;
                e.preventDefault();
            } else {  // else
                e.originalEvent.target.value = ""; // replace input with blank (optional)
                e.preventDefault();  // retur false
            }
            ;
        });
    }

    validationFloat();

</script>
