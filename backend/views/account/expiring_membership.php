<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">

        <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    </div>

    <div class="row">
        <div>
            <div class="content table-responsive table-full-width">
                <div class="col-md-6">
                    <div class="header">
                        <h3 class="title">Members</h3>
                    </div>
                </div>
                <div class="card-box col-md-12">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'condensed' => true,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'first_name',
                                'headerOptions' => ['style' => 'width: 10%;'],
                            ],
                            [
                                'attribute' => 'last_name',
                                'headerOptions' => ['style' => 'width: 10%;'],
                            ],
                            [
                                'attribute' => 'company',
                                'value' => function ($model) {
                                    if ($model->accountCompany <> null) {
                                        return $model->accountCompany->name;
                                    }
                                    return '-';
                                },
                                'headerOptions' => ['style' => 'width: 15%;'],
                            ],
                            'registeration_date',
                            'expiry_date',
                            'last_renewal',
                            [
                                'attribute' => 'account_type',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->account_type <> null) {
                                        if ($model->account_type == "named_associate") {
                                            return '<span class="label label-info"> Named Associate </span>';
                                        } else {
                                            return '<span class="label label-info">' . ucwords($model->account_type) . '</span>';
                                        }
                                    }
                                    return '-';
                                },
                                'filter' => ['Additional' => 'additional', 'business' => 'Business', 'nominee' => 'Nominee', 'named_associate' => 'Named Associate'],
                                'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
                                'contentOptions' => ['class' => 'text-center'],
                                'headerOptions' => ['class' => 'text-center', 'style' => 'width: 12%;']
                            ],
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($model) {

                                    $status = $model->status;
                                    $class = "label-warning";

                                    if ($status == '1') {
                                        $class = "label-success";
                                    }

                                    if ($status == '2') {
                                        $class = "label-info";
                                    }

                                    if ($status == '3') {
                                        $class = "label-danger";
                                    }

                                    if ($status == '4') {
                                        $class = "label-primary";
                                    }

                                    return '<span id="_status_' . $model->id . '" class="label ' . $class . '">' . ucwords(Yii::$app->params['statusTitle'][$model->status]) . '</span>';
                                },
                                'filter' => ['In-active', 'Active', 'Invoiced', 'Deleted', 'Renewal required'],
                                'filterInputOptions' => ['prompt' => 'Select Status...', 'class' => 'form-control', 'id' => null],
                                'contentOptions' => ['class' => 'text-center'],
                                'headerOptions' => ['class' => 'text-center', 'style' => 'width: 12%;'],
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{gen_invoice} {message} {change_status} {view} ',
                                'buttons' => [

                                    'gen_invoice' => function ($url, $model) {

                                       // if (!$model->renw_invoice_gen) {

                                            return Html::a('<span class="label label-info"><span class="fa fa-money"></span></span>', '#', [
                                                'class' => 'showModalButton',
                                                'value' => Url::to(['/account/send-invoiceren/', 'member' => $model->id]),
                                                'title' => Yii::t('yii', 'Generate Renewal Invoice'),
                                                'id' => 'sendInvoice-' . $model->id
                                            ]);
                                       // }
                                    },
                                    'change_status' => function ($url, $model) {

                                        $label = '<span class="label label-danger"><span class="fa fa-check-square-o"></span></span>';
                                        $class = 'approve-true change_status';
                                        $title = 'Approve';

                                        if ($model->status <> 'approved') {
                                            $label = '<span class="label label-success"><span class="fa fa-check-square-o"></span></span>';
                                            $class = 'approve-false change_status';
                                            $title = 'UnApprove';
                                        }

                                        return Html::a($label, '#', ['class' => $class, 'title' => $title, 'id' => $model->id]);
                                    },
                                    'message' => function ($url) {
                                        return Html::a(
                                            '<span class="label label-info"><span class="fa fa-envelope"></span></span>',
                                            ['#'],
                                            [
                                                'class' => 'showModalButton',
                                                'value' => \yii\helpers\Url::to(['/account/send-message', 'event_id' => 1]),
                                                'title' => Yii::t('yii', 'Delete'),
                                                'title' => 'Send Message',
                                                'data-pjax' => '0',
                                            ]
                                        );
                                    },
                                    'view' => function ($url) {
                                        return Html::a(
                                            '<span class="label label-info"><span class="fa fa-search-plus"></span></span>',
                                            $url,
                                            [
                                                'title' => 'Register Member',
                                                'data-pjax' => '0',
                                            ]
                                        );
                                    },
                                    'update' => function ($url) {
                                        return Html::a(
                                            '<span class="label label-warning"><span class="fa fa-pencil"></span></span>',
                                            $url,
                                            [
                                                'title' => 'Register Member',
                                                'data-pjax' => '0',
                                            ]
                                        );
                                    },
                                    'delete' => function ($url) {
                                        return Html::a(
                                            '<span class="label label-danger"><span class="fa fa-trash"></span></span>',
                                            $url,
                                            [
                                                'title' => 'Register Member',
                                                'data-pjax' => '0',
                                            ]
                                        );
                                    }
                                ]
                            ]
                        ],
                        'emptyText' => ' - ',
                        'showFooter' => true,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ],
                        'toolbar' => [
                            '{export}',
                        ],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'exportConfig' => [
                            GridView::EXCEL => true,
                            GridView::PDF => true,
                        ],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => false,
                        ],
                    ]);
                    ?>

                </div>
            </div>
        </div>
    </div>
<?= $this->registerJs('
    
    $("body").on("click", "a.change_status", function(e) {
    
        // $(".change_status").click(function(e){
        
        e.preventDefault(); 
        $(this).attr("disabled",true);
        var subscription = $(this).attr("id"); 
        
        var status = 0;
        
        if($(this).hasClass("approve-true")){
            status = 1;
        }
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/account/update-status/",
            type: "POST",
            data: { status: status , id : subscription }, 
            success : function(res){
                 
                var text = "UnApproved"; 
                
                if(res == "1"){ 
                   
                    if(status == "1"){
                        
                        text = "Approved"; 
                        $(this).addClass("approve-false");
                        $(this).removeClass("approve-true");
                        
                        $("#" +subscription + " .label").removeClass("label-danger"); 
                        $("#" +subscription + " .label").addClass("label-success");
                        
                    } if(status == "0"){
                    
                        $(this).removeClass("approve-false");
                        $(this).addClass("approve-true");
                        
                        
                        $("#" +subscription + " .label").removeClass("label-success");
                        $("#" +subscription + " .label").addClass("label-danger");
                    } else{
                        
                    }
                    
                    swal({
                        title: "Success",
                        text: "Member " + text,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }     
                 
                $(this).attr("disabled",false);
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
'); ?>