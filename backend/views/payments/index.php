<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payments', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Payment Id',
                'format' => 'raw',
                'value' => function ($model) {
                    return str_pad($model->id, 7, "0", STR_PAD_LEFT);
                }
            ],
            [
                'label' => 'Invoice #',
                'format' => 'raw',
                'value' => function ($model) {

                    $link = "";

                    if (strpos($model->invoice_id, '|') !== false) {
                        $invoices = array_filter(explode('|', $model->invoice_id));

                        if (count($invoices) > 0) {
                            foreach ($invoices as $invoice) {

                                $link .= Html::a(str_pad($invoice, 7, "0", STR_PAD_LEFT) . "<br/>",
                                    ['/account/invoice-pdf', 'member' => $model->user_id, 'invoice' => $invoice],
                                    [
                                        'target' => '_blank',
                                        'data-pjax' => "0"
                                    ]);
                            }

                            return $link;
                        }
                    }

                    return Html::a(str_pad($model->invoice_id, 7, "0", STR_PAD_LEFT),
                        ['/account/invoice-pdf', 'member' => $model->user_id, 'invoice' => $model->invoice_id],
                        [
                            'target' => '_blank',
                            'data-pjax' => "0"
                        ]);
                }
            ],
            /*[
                'label' => 'Related to',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->paymentUser <> null) {
                        return Html::a($model->paymentUser->first_name . " " . $model->paymentUser->last_name,
                            ['/account/view', 'id' => $model->user_id],
                            [
                                'target' => '_blank',
                                'data-pjax' => "0"
                            ]);
                    }
                }
            ],*/
            /*[
                'label' => 'Company',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->paymentUser <> null) {
                        if ($model->paymentUser->accountCompany <> null) {
                            return Html::a($model->paymentUser->accountCompany->name,
                                ['/account/view', 'id' => $model->user_id],
                                [
                                    'target' => '_blank',
                                    'data-pjax' => "0"
                                ]);
                        }
                    }
                }
            ],*/
            [
                'attribute' => 'Amount',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->amount;
                }
            ],
            [
                'attribute' => 'transaction_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->transaction_id;
                }
            ],
            [
                'attribute' => 'Payment Method',
                'format' => 'raw',
                'value' => function ($model) {
                    return ucwords($model->payment_method);
                }
            ],
            [
                'attribute' => 'detail',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->detail;
                }
            ],
            [
                'attribute' => 'Payment Date',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->payment_date;
                }
            ],
            [
                'attribute' => 'Received By ',
                'format' => 'raw',
                'value' => function ($model) {

                    if ($model->payment_method != "online") {
                        return $model->received_by;
                    }

                    return '-';

                }
            ],
        ],
        'emptyText' => ' - ',
        'showFooter' => true,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'toolbar' => [
            '{export}',
        ],
        'export' => [
            'fontAwesome' => true,
        ],
        'exportConfig' => [
            GridView::EXCEL => true,
            GridView::PDF => true,
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => false,
        ],
    ]); ?>
</div>
