<?php
$member = null;
$invoice = null;

if ($model->paymentUser <> null) {
    $member = $model->paymentUser;
}

if ($model->invoice <> null) {
    $invoice = $model->invoice;
}
?>
<div class="events-create  card card-box">
    <section class="MainArea" style="font-family: 'Century Gothic'">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                    <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 60%; float: left;">
                        <a href="/"><img src="<?= Yii::$app->params['invoice_header_image']; ?>" alt="BBG-Dubai"></a>
                    </div>

                    <div class="col-3 col-sm-3 col-md-4 col-lg-4 col-xl-4" style="width: 35%; float: right;">
                        <h1 style="color: #000;"><b><?= Yii::$app->params['receipt_title']; ?></b></h1>
                        <strong><span>Date: <?= ($model <> null) ? formatDate($model->payment_date) : ""; ?></span></strong><br/>
                        <strong><span>Reference #: <?= ($model <> null) ? str_pad($model->id, 7, "0", STR_PAD_LEFT) : 0; ?></span></strong>
                    </div>

                    <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 100%; float: left;">
                        <strong><br/><?= (\Yii::$app->appSettings <> null) ? strip_tags(\Yii::$app->appSettings->settings->about, '<br>') : ""; ?>
                        </strong>
                        <strong><br/>Telephone: <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->telephone : ""; ?>
                            <br/></strong>
                        <strong>Email: <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->admin_email : ""; ?>
                            <br/></strong>
                        <strong>VAT Registration no:
                            <u><?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->app_vat : ""; ?></u><br/></strong>
                    </div>

                    <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 100%; float:left;">
                        <br/><br/>
                        <strong>Bill To:</strong><br/>
                        <strong>Name: <?= ($member <> null) ? $member->first_name . ' ' . $member->last_name : ""; ?>
                            <?= ($member <> null && $member->account_type == 'guest') ? '(Guest)' : '(' . ucwords($member->account_type) . ')'; ?></strong>
                        <br/>
                        <strong>Company: <?= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->name : ""; ?></strong>
                        <br/>
                        <strong>Address: <?= ($member <> null) ? $member->address : ""; ?><br/></strong>
                        <strong>P.O
                            Box: <?= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->postal_code : ""; ?>
                            <br/></strong>
                        <strong>Telephone: <?= ($member <> null) ? $member->phone_number : ""; ?></strong>
                        <br/>
                        <strong>VAT Registration no:
                            <u><?= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->vat_number : ""; ?></u><br/><br/></strong>
                    </div>

                    <div style="width: 100%;" class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                        <div style="width: 100%;min-height: 100px;">
                            <table class="table" style="width: 100%; float: left;" cellpadding="5">
                                <thead>
                                <tr>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: left;">
                                        Invoice #
                                    </th>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: left;">
                                        Description
                                    </th>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: left;">
                                        Payable Amount
                                    </th>
                                    <th class="col-md-4"
                                        style="width:25%; background: #1d355f; color: #ffffff; text-align: center">
                                        Amount
                                        Paid
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($invoice <> null) {

                                    ?>
                                    <tr>
                                        <td>
                                            <?= ($invoice <> null) ? \yii\helpers\Html::a(str_pad($invoice->invoice_id, 7, "0", STR_PAD_LEFT), '#', []) : 0; ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($invoice <> null) {
                                                if ($invoice->invoice_related_to == "member") {
                                                    echo $invoice->invoice_category;
                                                } else {
                                                    echo $invoice->invoice_category;
                                                }
                                            }
                                            ?></td>
                                        <td style="text-align: left;">AED <?= $model->total; ?></td>
                                        <td style="text-align: center;">AED <?= $model->amount_paid; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style="text-align:center; width: 100%; margin-top: 50px; clear: both;">
                        <span style="color: red; font-size: 10px;"><i>This is system generated receipt</i></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div style="text-align:center; width: 100%; margin-top: 50px; clear: both;">
        <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->app_name ."<br/>" : ""; ?>
    </div>
</div>
<style>
    .searchResult .view a {
        font-weight: bold;
    }

    #events, #news {
        background: #ffffff;
    }

    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
</style>