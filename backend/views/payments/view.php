<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Payments */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'invoice_id',
            'amount',
            'amount_payable',
            'transaction_id',
            'response_code',
            'detail:ntext',
            'payment_date',
            'status',
            'payment_method',
            'received_by',
            'shipping_address',
            'shipping_city',
            'shipping_country',
            'shipping_state',
            'shipping_postalcode',
            'currency',
            'phone_num',
            'customer_name',
            'email:email',
        ],
    ]) ?>

</div>
