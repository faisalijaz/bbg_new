<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Destinations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destinations-index card-box">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Destinations'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'tag',
            'capacity',
            [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->status == 0 ? 'In-Active' : 'Active' ;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
