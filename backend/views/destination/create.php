<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Destinations */

$this->title = Yii::t('app', 'Create Destinations');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Destinations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destinations-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
        'tags' => $tags,
    ]) ?>

</div>
