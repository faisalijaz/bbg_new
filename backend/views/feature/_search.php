<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeatureSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="features-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'status')->dropDownList(['0' => 'In-active', '1' => 'Active'], ['prompt' => 'Select status']) ?>
        </div>

        <div class="col-sm-4">
            <label>&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
