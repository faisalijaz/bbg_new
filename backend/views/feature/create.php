<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Features */

$this->title = Yii::t('app', 'Create Features');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Features'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="features-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
