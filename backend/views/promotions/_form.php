<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;

if($model->isNewRecord && !$model->code) {
    $model->code = Yii::$app->appSettings->getRandomNumber(8);
}

/* @var $this yii\web\View */
/* @var $model common\models\Promotions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promotions-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'value')->textInput() ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'type')->dropDownList([ 'fixed' => 'Fixed', 'percentage' => 'Percentage', ], ['prompt' => 'Select Type']) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'is_customer_specific')->dropDownList(['No', 'Yes'], ['prompt' => 'Customer Specific']) ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'redeem_type')->dropDownList([ 'single' => 'One time', 'multiple' => 'Multiple time'], ['prompt' => 'Select Redeem Type']) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
            ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?=
            $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'todayHighlight' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
            ?>
        </div>

    </div>


    <div class="row">

        <div class="col-lg-6 col-sm-12">

            <?= $form->field($model, 'start_time')->widget(TimePicker::classname(), [
                'options' => ['readonly' => true],
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'end_time')->widget(TimePicker::classname(), [
                'options' => ['readonly' => true],
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); ?>
        </div>

    </div>

    <?= $form->field($model, 'status')->dropDownList([ '-1' => 'Upcoming', '0' => 'Expired', '1' => 'Active'], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
