<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PromotionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promotions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="row">

        <div class="col-lg-3 col-sm-6">
            <?= $form->field($model, 'code') ?>
        </div>

        <div class="col-lg-3 col-sm-6">
            <?= $form->field($model, 'title') ?>
        </div>

        <div class="col-lg-3 col-sm-6">
            <?= $form->field($model, 'type')->dropDownList([ 'percentage' => 'Percentage', 'fixed' => 'Fixed'], ['prompt' => 'Select Type']) ?>
        </div>

        <div class="col-lg-3 col-sm-6">
            <?= $form->field($model, 'redeem_type')->dropDownList([ 'single' => 'One time', 'multiple' => 'Multiple time'], ['prompt' => 'Select Redeem Type']) ?>
        </div>

    </div>


    <div class="row">

        <div class="col-lg-3 col-sm-6">
            <?= $form->field($model, 'value') ?>
        </div>

        <div class="col-lg-3 col-sm-6">
            <?php echo $form->field($model, 'is_customer_specific')->dropDownList(['No', 'Yes'], ['prompt' => 'Customer Specific']) ?>

        </div>

        <div class="col-lg-3 col-sm-6">
            <?= $form->field($model, 'status')->dropDownList([ '-1' => 'Upcoming', '0' => 'Expired', '1' => 'Active'], ['prompt' => 'Select Status']) ?>

        </div>

        <div class="col-lg-3 col-sm-6">
            <label>&nbsp;</label>
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>

    </div>



    <?php // echo $form->field($model, 'start_date') ?>

    <?php // echo $form->field($model, 'end_date') ?>

    <?php // echo $form->field($model, 'start_time') ?>

    <?php // echo $form->field($model, 'end_time') ?>





    <?php ActiveForm::end(); ?>

</div>
