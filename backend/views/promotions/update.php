<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Promotions */

$this->title = 'Update Promotions: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Promotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promotions-update card-box">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
