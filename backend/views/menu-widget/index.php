<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchMenuWidget */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menu');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-widget-index card-box">

    <p>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Add Menu', ['create'], ['class' => 'btn btn-success btn-sm pull-right']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'url:url',
            'sort_order',
            [
                'attribute' => 'status',
                'filter' => true,
                'value' => function($data) {
                    return ($data->status == 'active' ? 'Atcive' : 'In-active');
                },
                'format' => 'raw'
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view-submenu} {view} {update} {delete}',
                'buttons' => [

                    'view-submenu' => function ($url) {
                        return Html::a(
                            '<span class="label label-info"><span class="fa fa-search-plus"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>


</div>
