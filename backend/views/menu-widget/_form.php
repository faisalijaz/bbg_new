<?php

use common\models\CmsPages;
use common\models\MenuWidget;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$extra_pages_list = Yii::$app->params['staticPageArray'];

$cms_pages = ArrayHelper::map(CmsPages::find()->all(), 'seo_url', 'seo_url');
$saved_cms_pages = ArrayHelper::map(MenuWidget::find()->where(['parent_id' => $model->id])->all(), 'id', 'title');
foreach ($extra_pages_list as $key => $saved_list) {
    foreach ($saved_cms_pages as $ky => $saved_cms) {
       if($saved_list == $saved_cms){
           $saved_cms_pages[$ky]= $key;
       }
    }
}

/* @var $this yii\web\View */
/* @var $model app\models\MenuWidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-widget-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>


    <?php
    echo '<label class="control-label">Submenu - Extra Pages</label>';
    echo Select2::widget([
        'name' => 'MenuWidget[extra_pages]',
        'value' => $saved_cms_pages,
        'data' => $extra_pages_list,
        'maintainOrder' => true,
        'options' => ['placeholder' => 'Select a submenu ...', 'multiple' => true],
        'pluginOptions' => [
            'maximumInputLength' => 10
        ],
    ]);
    echo '<br/>';
    ?>


    <?php
    echo '<label class="control-label">Submenu - CMS Pages</label>';
    echo Select2::widget([
        'name' => 'MenuWidget[cms_pages]',
        'value' => $saved_cms_pages,
        'data' => $cms_pages,
        'maintainOrder' => true,
        'options' => ['placeholder' => 'Select a submenu ...', 'multiple' => true],
        'pluginOptions' => [
            'maximumInputLength' => 10
        ],
    ]);
    echo '<br/>';
    ?>
    <div class="row">
        <div class="col-md-5">
    <?= $form->field($model, 'sort_order')->textInput() ?>
        </div>
        <div class="col-md-5">
    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select status']) ?>
        </div>
        <div class="form-group field-menuwidget-members_only" style="margin-top: 35px;">

        <?= $form->field($model, 'members_only')->widget(CheckboxX::classname(), [
                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                'options' => [
                    'class' => 'members_only',
                ],
                'pluginOptions' => [
                    'theme' => 'krajee-flatblue',
                    'enclosedLabel' => true,
                    'threeState' => false,
                ]
            ])->label(false); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
