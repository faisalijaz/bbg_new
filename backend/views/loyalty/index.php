<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LoyaltySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loyalties';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>


<div class="loyalty-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Loyalty', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Customer',
                'format' => 'raw',
                'value' => function ($model) {
                    $_customer_data = '';
                    if ($model->account <> null) {
                        $_customer_data .= $model->account->first_name . ' ' . $model->account->last_name . '<br/>';
                        $_customer_data .= $model->account->phone_number . '<br/>';
                        $_customer_data .= $model->account->email;
                    }
                    return $_customer_data;
                }
            ],
            [
                'attribute' => 'Earned Points',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getEarnedPoints($model->account_id);
                }
            ],
            [
                'attribute' => 'Redeemed Points',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getRedeemedPoints($model->account_id);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
