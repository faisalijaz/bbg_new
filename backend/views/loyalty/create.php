<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Loyalty */

$this->title = 'Create Loyalty';
$this->params['breadcrumbs'][] = ['label' => 'Loyalties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loyalty-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
