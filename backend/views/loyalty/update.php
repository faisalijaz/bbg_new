<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Loyalty */

$this->title = 'Update Loyalty: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Loyalties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loyalty-update card-box">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
