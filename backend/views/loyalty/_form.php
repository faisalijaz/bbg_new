<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Accounts;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Loyalty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loyalty-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'points')->textInput() ?>

    <?= $form->field($model, 'action')->dropDownList([ '+' => '+', '-' => '-', ], ['prompt' => 'Select Action']) ?>

    <?= $form->field($model, 'account_id')->dropDownList(ArrayHelper::map(Accounts::find()->all(), 'id', function($model) {
        return $model->first_name . ' '. $model->last_name;
    }), ['prompt' => 'Select Account']) ?>


    <?= $form->field($model, 'booking_id')->textInput() ?>

    <?= $form->field($model, 'point_type')->dropDownList([ 'preorder' => 'Preorder', 'referral' => 'Referral', 'sharing' => 'Sharing', ], ['prompt' => 'Select Point type']) ?>

    <?= $form->field($model, 'is_deleted')->dropDownList([ 'pending' => 'Pending', 'deleted' => 'Deleted', 'non-deleted' => 'Non Deleted', ], ['prompt' => 'Select status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
