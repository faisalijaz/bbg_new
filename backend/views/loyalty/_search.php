<?php

use common\models\Accounts;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LoyaltySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loyalty-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <!--<div class="col-lg-6 col-sm-12">
            <? /*= $form->field($model, 'points') */ ?>
        </div>-->

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'account_id')->dropDownList(ArrayHelper::map(Accounts::find()->all(), 'id', function ($model) {
                return $model->first_name . ' ' . $model->last_name;
            }), ['prompt' => 'Select Account']) ?>

        </div>


        <div class="col-lg-3 col-sm-12">
            <?= $form->field($model, 'action')->dropDownList(['+' => '+', '-' => '-',], ['prompt' => 'Select Action']) ?>
        </div>


        <div class="col-lg-3 col-sm-12">
            <?= $form->field($model, 'booking_id')->label('Booking Reference') ?>
        </div>
        <!--
        <div class="col-lg-6 col-sm-12">
            <? /*= $form->field($model, 'is_deleted')->dropDownList([ 'pending' => 'Pending', 'deleted' => 'Deleted', 'non-deleted' => 'Non Deleted', ], ['prompt' => 'Select status']) */ ?>
        </div>
        -->

        <div class="col-lg-2 col-sm-12">
            <label></label><br/><br/>
            <div class="form-group pull-right">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
