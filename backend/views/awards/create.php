<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Awards */

$this->title = 'Create Awards';
$this->params['breadcrumbs'][] = ['label' => 'Awards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="awards-create card-box">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
