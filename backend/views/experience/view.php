<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Experiences */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Experiences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="experiences-view card-box">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            [
                'attribute' => 'Image',
                'format' => ['image', ['width' => '100', 'height' => '100']],
                'value' => $model->icon,


            ],
            [
                'attribute' => 'pick_from',
                'value' => ($model->pick_from == 1 ? 'Yes' : 'No')
            ],
            [
                'attribute' => 'status',
                'value' => ($model->status == 1 ? 'Active' : 'In-active')
            ]
        ],
    ]) ?>

</div>
