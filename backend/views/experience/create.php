<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Experiences */

$this->title = Yii::t('app', 'Create Experiences');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Experiences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="experiences-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
