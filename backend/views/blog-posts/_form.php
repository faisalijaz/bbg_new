<?php

use kartik\widgets\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BlogPosts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-posts-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-6">
            <div id="images" class="form-group">
                <label class="control-label" for="input-image">Main Image</label>
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    <img src="<?= ($model->banner_image != "" && $model->banner_image <> null) ? $model->banner_image : Yii::$app->params['no_image']; ?>"
                         alt="" width="125" height="125" title=""
                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                </a>
                <?= $form->field($model, 'banner_image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'Select Status'])->label('Publish') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'show_until_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Show until'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
        </div>


        <div class="col-md-12">

            <?= $form->field($model, 'description')->textarea(['class' => 'tinyMCe', 'rows' => 6, 'id' => 'elm1']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
