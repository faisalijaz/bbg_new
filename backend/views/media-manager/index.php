<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MediaManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Media Managers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-manager-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Media Manager', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'name',
            'href',
            'parent_id',
            // 'created_at',
            // 'path',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
