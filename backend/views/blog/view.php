<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="card card-box">

        <p class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'title',
                'seo_url:url',
                'short_description:ntext',
                'description:ntext',
                'meta_title',
                'meta_description',
                'meta_keywords',
                'seo_keywords',
                'sort_order',
                [
                    'attribute' => 'banner_id',
                    'value' => ($model->banner_id ? $model->banner->name : '')
                ],
                'banner_image',
                'tags',
                'created_by',
                'created_at',
                'status',
            ],
        ]) ?>

    </div>
</div>


