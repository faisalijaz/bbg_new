<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ReferralSources */

$this->title = Yii::t('app', 'Create Referral Sources');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Referral Sources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="referral-sources-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
