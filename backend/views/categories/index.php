<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index card-box">
    <p class="pull-right">
        <?= Html::a('Create Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'condensed' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'label' => "Type",
                'attribute' => 'type',
                'value' => function ($model) {

                    return $model->type;
                },
                'filter' => ['general' => 'General', 'offers' => 'Offers'],
                'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
            ],
            'short_description',
            'description',
            /*
            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<img src="'. $model->image .'" width="100">';
                }
            ],
            */
            // 'baner_image',
            // 'parent_id',
            // 'meta_title',
            // 'meta_description',
            [
                'label' => "status",
                'attribute' => 'status',
                'value' => function ($model) {

                    return ($model->status) ? "Active" : "In-active";
                },
                'filter' => ['0' => 'In-active', '1' => 'active'],
                'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
