<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div id="images" class="form-group col-md-6">
        <label class="control-label" for="input-image">Category Image</label>
        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
            <img src="<?= $model->image ?>" alt="" width="125" height="125" title=""
                 data-placeholder="no_image.png"/>
        </a>
        <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>

    </div>


    <div id="images" class="form-group col-md-6">
        <label class="control-label" for="input-images">Banner Image</label>
        <a href="" id="thumb-images" data-toggle="image" class="img-thumbnail">
            <img src="<?= $model->image ?>" alt="" width="125" height="125" title=""
                 data-placeholder="no_image.png"/>
        </a>
        <?= $form->field($model, 'baner_image')->hiddenInput(['maxlength' => true, 'id' => 'input-images'])->label(false) ?>

    </div>

    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title'), ['prompt' => 'Select Parent Category'])->label(false); ?>
    <?= $form->field($model, 'type')->dropDownList([ 'general' => 'General', 'offers' => 'Offers', ], ['prompt' => 'Select Type']) ?>
    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([1 => 'Active', 0 => 'In-Acitve'])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
