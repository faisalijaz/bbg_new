<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'type',
            'short_description',
            'description',
            [
                'attribute' => 'Image',
                'format' => ['image', ['width' => '100', 'height' => '100']],
                'value' => $model->image,
            ],
            [
                'attribute' => 'Banner Image',
                'format' => ['image', ['width' => '100', 'height' => '100']],
                'value' => $model->baner_image,
            ],
            'parent_id',
            'meta_title',
            'meta_description',
            [
                'attribute' => 'status',
                'value'=> ($model->status == 0 ? 'In-Active' : 'Active')

            ]
        ],
    ]) ?>

</div>
