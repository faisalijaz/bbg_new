<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaxRates */

$this->title = 'Update Tax Rates: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tax Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tax-rates-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
