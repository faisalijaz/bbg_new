<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TaxratesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Tax Rates');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="tax-rates-index card-box">

    <p class="pull-right">
        <?= Html::a('Create Tax Rates', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'rate',
            'type',
            'created_at:date',
            // 'updated_at',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->status == 1 ? 'Active' : 'In-Active';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
