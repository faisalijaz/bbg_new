<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaxRates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tax-rates-form card-box">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rate')->textInput() ?>

        <?= $form->field($model, 'type')->dropDownList(['fixed' => 'Fixed', 'percentage' => 'Percentage',], ['prompt' => 'Select Tax type']) ?>

        <?= $form->field($model, 'status')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        
    </div>
