<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaxRates */

$this->title = 'Create Tax Rates';
$this->params['breadcrumbs'][] = ['label' => 'Tax Rates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-rates-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
