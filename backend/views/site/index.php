<?php

use backend\widgets\controls\DashboardWidget;
use backend\widgets\statistics\todaySales\TodaySalesWidget;

/* @var $this yii\web\View */
$this->title = 'Welcome, ' . yii::$app->user->identity->username;
$userDashboard = \common\models\UserWidget::find()->where(['user_id' => Yii::$app->user->id])->all();
$widgets = \common\models\Widgets::find()->where(['type' => 'backend', 'status' => 1])->orderBy('sorting_order', 'ASC')->all();
?>

<?= DashboardWidget::widget(['title' => 'Dashboard Widgets', 'position' => 'pull-right']); ?>

<div class="row">
    <?php foreach ($widgets as $widget) { ?>
        <?php if ($widget->code === 'total_members' || $widget->code === 'today_members' || $widget->code === 'total_events'
            || $widget->code === 'today_events' || $widget->code === 'pending_invoices' || $widget->code === 'paid_invoices'
            || $widget->code === "upcoming_events" || $widget->code === "past_events" || $widget->code === 'pending-members'
            || $widget->code === "active-members" || $widget->code === "invoiced-members" || $widget->code === 'deleted'
            || $widget->code === "renewal-required"

        ) { ?>
            <div class="col-lg-3 col-sm-12">
                <?= TodaySalesWidget::widget(['title' => $widget->name, 'code' => $widget->code]); ?>
            </div>
        <?php }
    } ?>
</div>



