<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

?>


<div class="site-error">

    <?php echo $this->render('errors/'.$exception->statusCode, ['message' => $message ]); ?>

</div>
