<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <div class="panel-heading">
        <h3 class="text-center"> <strong class="text-custom">BBG Admin</strong> </h3>
    </div>


    <div class="panel-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'form-horizontal m-t-20']); ?>

        <div class="form-group ">
            <div class="col-xs-12">
                <?= $form->field($model, 'username')->textInput(['placeholder' => 'User name'])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false) ?>
            </div>
        </div>


        <div class="form-group ">
            <div class="col-xs-12">
                <div class="checkbox checkbox-primary">
                    <input id="" type="checkbox">
                    <label for="checkbox-signup">
                        Remember me
                    </label>
                    <?= $form->field($model, 'rememberMe')->checkbox(['id' => 'checkbox-signup'])->label(false) ?>
                </div>
            </div>
        </div>

        <div class="form-group text-center m-t-40">
            <div class="col-xs-12">
                <?= Html::submitButton('Sign In', ['class' => 'btn btn-black btn-block btn-lg text-uppercase waves-effect waves-light', 'name' => 'login-button']) ?>
            </div>
        </div>

        <?php $form->end(); ?>
    </div>
</div>

