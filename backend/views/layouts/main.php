<?php
ini_set('memory_limit', '2048M');
use backend\assets\AppAsset;
use common\helpers\MenuHelper;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$expiringBookings =  [];

$site_settings = \common\models\Settings::findOne(1);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Wisdom IT team">
    <meta name="description" content="S'wich artificial dashboard, reporting dashboard, analytics, reporting">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?= Yii::getAlias('@web') . '/ubold/assets/js/modernizr.min.js' ?>"></script>

</head>

<?php
/*echo Menu::widget([
    'items' => Yii::$app->setting->getAdminMenu(),
    'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
    'encodeLabels' => false,
    'options' => ['class' => 'sidebar-menu'],
]);*/
?>

<body class="fixed-left">
<div id="custom_bbg_loader" style="z-index:9999; text-align:center;position: absolute;left: 39%; top: 39%; display: none;"><img src='/ring-loading.gif'></div>

<?php $this->beginBody() ?>

<!-- Begin page -->
<div id="wrapper">
    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <div class="text-center">
                <?= Html::a('BBG', ['/site/index'], ['class' => 'logo']) ?>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="">
                    <div class="pull-left">
                        <button class="button-menu-mobile open-left">
                            <i class="ion-navicon"></i>
                        </button>
                        <span class="clearfix"></span>
                    </div>

                    <ul class="nav navbar-nav navbar-right pull-right">
                        <li class="dropdown hidden-xs">
                            <?php
                            $count = 0;
                            if ($expiringBookings <> null) {
                                $count = count($expiringBookings);
                                ?>
                                <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light"
                                   data-toggle="dropdown" aria-expanded="true">
                                    <i class="icon-bell"></i> <span
                                            class="badge badge-xs badge-danger"><?= $count; ?></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-lg">
                                    <li class="notifi-title"><span
                                                class="label label-default pull-right">New <?= $count; ?></span>Tentative
                                        Bookings
                                    </li>
                                    <li class="list-group nicescroll notification-list"
                                        style="max-height: 400px; overflow: auto;">
                                        <?php
                                        foreach ($expiringBookings as $expiring) {
                                            ?> <!-- list item-->
                                            <div class="list-group-item ">
                                                <div class="media">
                                                    <div class="">
                                                        <h5 class="media-heading">
                                                            <a href="/tour-booking/update?id=<?= $expiring->id; ?>">Booking
                                                                # <?= $expiring->id; ?></a>
                                                            <?= Html::a('<span class="fa fa-trash"></span>', '#', [
                                                                'class' => 'showModalButton BookingsExpiring pull-right',
                                                                'style' => 'margin-left:12px',
                                                                'value' => \yii\helpers\Url::to(['/tour-booking/read-expiry', 'id' => $expiring->id]),
                                                                'title' => Yii::t('yii', '.'),
                                                            ]); ?>
                                                        </h5>
                                                        <p class="m-0">
                                                            <?php
                                                            if ($expiring->account <> null) { ?>
                                                                <small>
                                                                    <strong>Name: </strong><?= $expiring->account->first_name . ' ' . $expiring->account->last_name; ?>
                                                                </small>
                                                                <small>
                                                                    <strong>Phone: </strong><?= $expiring->account->phone_number; ?>
                                                                </small> <br/>
                                                            <?php } ?>
                                                            <small><strong>Amount
                                                                    due: </strong><?= $expiring->total_sum; ?>
                                                            </small>
                                                            <br/>
                                                            <small><strong>Cancellation
                                                                    Time:</strong> <?= $expiring->cancellation_time; ?>
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        } ?>
                                    </li>
                                </ul>
                            <?php }
                            ?>
                        </li>

                        <li class="dropdown">
                            <a href="" class="dropdown-toggle profile" data-toggle="dropdown"
                               aria-expanded="true"><img
                                        src="<?= Yii::getAlias('@web') . '/windows-8-user-account.jpg' ?>"
                                        alt="user-img" class="img-circle"> </a>
                            <ul class="dropdown-menu">
                                <?php if (!Yii::$app->user->isGuest) { ?>
                                    <li>
                                        <?= Html::a('<i class="ti-settings m-r-5"></i> Profile', ['user/update', 'id' => Yii::$app->user->identity->getId()]) ?>
                                    </li>

                                    <li><?= Html::a('<i class="ti-power-off m-r-5"></i> Logout', ['/site/logout'], ['data-method' => 'POST', 'class' => 'profile-link']) ?></li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                    <div class="pull-right">
                        <a href="<?= Yii::$app->appSettings->getByAttribute('app_url'); ?>" target="_blank"
                           class="btn btn-primary pull-right  btn-sm" style="margin-top: 15px; margin-left: 10px">
                            Visit Website</a>

                    </div>

                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->


    <div class="left side-menu">
        <div class="sidebar-inner">
            <!--- Divider -->
            <div id="sidebar-menu">

                <?php
                echo Menu::widget([
                    'items' => MenuHelper::getMenu(),
                    'submenuTemplate' => "\n<ul class='list-unstyled'>\n{items}\n</ul>\n",
                    'encodeLabels' => false,
                    'options' => [],
                ]);
                ?>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="container card-box">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- <h4 class="page-title"><? /*= $this->title */ ?></h4>-->
                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                    </div>
                </div>

                <?= \yii2mod\alert\Alert::widget() ?>

                <?= $content ?>

            </div>
            <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
           <?= date('Y'); ?>
        </footer>
    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

</div>
<!-- END wrapper -->


<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true, 'tabindex' => false],
    'options' => ['tabindex' => false]
]);
echo "<div id='modalContent'><div style='text-align:center'><img src='" . Yii::getAlias('@web') . '/loader_gif.gif' . "'></div></div>";
yii\bootstrap\Modal::end();
?>

<?php $this->endBody() ?>

</body>


<script>
    var resizefunc = [];
</script>

<script type="text/javascript">
    $(function () {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();
        $(".button-menu-mobile").trigger('click');
    });
</script>

<script type="text/javascript">

    $('form#ajaxSubmit').on('beforeSubmit', function () {
        alert('working');

        return false;
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }

        // dataGridId is the datagrid id that need to reload
        // refId is a custom form attribute that will allow to access datagrid
        var dataGridId = form.attr('refId');

        // ajaxSubmitBtn is a button that that used to trigger submit action

        $('#ajaxSubmitBtn').attr('disable', true);
        $('#ajaxSubmitBtn').val('Processing...');

        // submit form
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                if (response.status == 'OK') {
                    var html = "<div class='alert alert-success' role='alert'>" + response.message + "</div>";
                    html += "<div class='modal-footer'>";
                    html += "<a href='#' class='btn btn-success close_link' data-dismiss='modal'>Close</a>";
                    html += "</div>";
                    form.html(html);

                    $.pjax.reload({container: '#assignment-detail'});
                }

                if (response.status == 'ERROR') {
                    alert(response.message);
                }
            }
        });
        return false;
    });
</script>

</html>
<?php $this->endPage() ?>

<script type="text/javascript">

    var removeRow = function (rowId) {

        var deleteTitle = $('#' + rowId + ' .deletebtn').attr('data-title');
        var deleteUrl = $('#' + rowId + ' .deletebtn').attr('data-url');

        swal({
            title: "Are you sure?",
            text: deleteTitle,
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    url: deleteUrl,
                    type: "get",
                    success: function (response) {
                        swal("Deleted!", response.message, "success");
                        $("#" + rowId + ", .tooltip").remove();

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });

            } else {
                swal("Cancelled", "There is an error while deleting the row.", "error");
            }
        });

    }

</script>
<?php
$this->registerJs(' 
        $(document).ready(function(){ 
            $(".c_pagination ul li").on("click", function() { $("body").scrollTop(0); }); 
            $("body").scrollTop(0);
        });
    ');
?>
