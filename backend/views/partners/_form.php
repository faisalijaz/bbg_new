<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Partners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div id="images" class="form-group">
        <label class="control-label" for="input-image">Logo</label>
        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
            <img src="<?= ($model->logo != "" && $model->logo <> null) ? $model->logo : Yii::$app->params['no_image']; ?>" alt="" width="125" height="125" title=""
                 data-placeholder="<?= Yii::$app->params['no_image'];?>"/>
        </a>
        <?= $form->field($model, 'logo')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
