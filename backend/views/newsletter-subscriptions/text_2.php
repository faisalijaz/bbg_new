<?php

$string = "";

if (isset($params['page'])) {
    $page = $params['page'] + 1;
} else {
    $page = 1;
}


if (isset($params['all']) && $params['all'] == 1) {
    $string = "&all=1";
} else {

    if (isset($params['events']) && $params['events'] == 1) {
        $events = 1;
        $string .= "&events=1";
    } else {
        $events = 0;
    }

    if (isset($params['weekly_newsletter']) && $params['weekly_newsletter'] == 1) {
        $weekly_newsletter = 1;
        $string .= "&weekly_newsletter=1";
    } else {
        $weekly_newsletter = 0;
    }

    if (isset($params['special_offers']) && $params['special_offers'] == 1) {
        $special_offers = 1;
        $string .= "&special_offers=1";
    } else {
        $special_offers = 0;
    }
}


echo '<div class="alert alert-success">Total Sent : ' . $page * 5 .'</div>' ;

if ($total > (($page * 5) - 5)) {

    ?>


    <?= $this->registerJs('

setTimeout(function(){  window.location = "/newsletter-subscriptions/bulk-notify-gdpr/?page=' . $page . '&per-page=10' . $string . '"; }, 30000); 

'); ?>
    <?php
    /**/
}else{
    echo '<div class="alert alert-success">Finished sending to all users</div>';
}
