<?php

use kartik\checkbox\CheckboxX;
use kartik\form\ActiveForm;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsletterSubscriptions */

$this->title = 'Create Newsletter Subscriptions';
$this->params['breadcrumbs'][] = ['label' => 'Newsletter Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-subscriptions-create">

    <div class="newsletter-subscriptions-form">

        <?php $form = ActiveForm::begin(['method' => 'GET']); ?>

        <div class="col-md-12 form-group">
            <?php echo CheckboxX::widget([
                'name' => 'all',
                'value' => 0,
                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                'autoLabel' => true,
                'labelSettings' => [
                    'label' => 'All',
                    'position' => CheckboxX::LABEL_RIGHT
                ]
            ]); ?>

        </div>

        <div class="col-md-12 form-group">
            <?php echo CheckboxX::widget([
                'name' => 'events',
                'value' => 0,
                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                'autoLabel' => true,
                'labelSettings' => [
                    'label' => 'Events',
                    'position' => CheckboxX::LABEL_RIGHT
                ]
            ]); ?>
        </div>

        <div class="col-md-12 form-group">
            <?php echo CheckboxX::widget([
                'name' => 'weekly_newsletter',
                'value' => 0,
                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                'autoLabel' => true,
                'labelSettings' => [
                    'label' => 'Weekly E-Newsletter',
                    'position' => CheckboxX::LABEL_RIGHT
                ]
            ]); ?>
        </div>

        <div class="col-md-12 form-group">
            <?php echo CheckboxX::widget([
                'name' => 'special_offers',
                'value' => 0,
                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                'autoLabel' => true,
                'labelSettings' => [
                    'label' => ' Special Offers & Announcements',
                    'position' => CheckboxX::LABEL_RIGHT
                ]
            ]); ?>
        </div>

        <div class="modal-footer">
            <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
