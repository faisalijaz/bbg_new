<?php
ini_set('memory_limit', '2048M');

use common\models\NewsletterSubscriptions;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewsletterSubscriptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row card-box">
    <div class="col-md-6">
    <?= ExportMenu::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => NewsletterSubscriptions::find(),
        ]),
        'columns' => [
            [
                'label' => 'First Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name : "-";
                },
            ],
            [
                'label' => 'Last Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->last_name : "-";
                },
            ],
            [
                'label' => 'Company',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? ($model->member->accountCompany <> null) ? $model->member->accountCompany->name : "-" : "-";
                },
            ],
            [
                'label' => 'Type',
                'value' => function ($model) {
                    if($model->member <> null){
                        return 'Member';
                    }

                    return 'Non-Member';
                },
            ],
            [
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->active) ? "Active" : "In-active";
                },
                'filter' => [
                    '1' => 'Yes',
                    '0' => 'No',
                ],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
                'headerOptions' => ['class' => 'text-center', 'style' => 'width: 8%;']
            ],
            [
                'label' => 'Email',
                'attribute' => 'email',
                'value' => function ($model) {
                    return $model->email;
                }
            ],
            [
                'label' => 'Event News Subscription',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->events_news){
                        return 'Yes';
                    }

                    return 'No';
                },
            ],
            [
                'label' => 'Weekly Newsletter',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->weekly_newsletter){
                        return 'Yes';
                    }

                    return 'No';
                },
            ],
            [
                'label' => 'Special Offers',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->special_offers){
                        return 'Yes';
                    }

                    return 'No';
                },
            ],
            [
                'label' => 'Membership Subscription',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->membership_subscriptions){
                        return 'Yes';
                    }

                    return 'No';
                },
            ],
            [
                'label' => 'Date Created',
                'attribute' => 'date_created',
                'value' => function ($model) {
                    return $model->date_created;
                }
            ]
        ],
        "exportConfig" => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_PDF => true,
            ExportMenu::FORMAT_EXCEL => true,
        ],
        'target' => '_blank',
        'showColumnSelector' => false,
        "contentBefore" => [
            ["value" => "Manage Contacts"]
        ],
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-warning'
        ]

    ]); ?>
    </div>
    <div class="col-md-6">
        <p>
            <?= Html::a(
                '<span class="fa fa-envelope"> Send Bulk GDPR Notification</span>',
                ['/newsletter-subscriptions/bulk-notify-gdpr'],
                [
                    'class' => 'btn btn-sm btn-success pull-right',
                    'title' => Yii::t('yii', '<span class="fa fa-envelope">  GDPR Notification </span>'),
                ]
            ); ?>
        </p>
    </div>
</div>
<div class="row card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="newsletter-subscriptions-index">
    <p>
        <?= Html::a('Create New', ['create'], ['class' => 'btn btn-warning pull-right']) ?>
    </p>


    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'First Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name : $model->first_name;
                },
            ],
            [
                'label' => 'Last Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->last_name : $model->last_name;
                },
            ],
            [
                'label' => 'Company',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? ($model->member->accountCompany <> null) ? $model->member->accountCompany->name : "-" : "-";
                },
            ],
            'email:email',
            [
                'label' => 'Type',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->member <> null){
                        return '<span class="label label-success">Member</span>';
                    }

                    return '<span class="label label-warning">Non-Member</span>';
                },
            ],
            [
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->active){
                        return '<span class="label label-info">Active</span>';
                    }

                    return '<span class="label label-danger">In-active</span>';
                },
                'filter' => [
                    '1' => 'Yes',
                    '0' => 'No',
                ],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
                'headerOptions' => ['class' => 'text-center', 'style' => 'width: 8%;']
            ],
            [
                'attribute' => 'events_news',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->events_news){
                        return '<span class="label label-success">Yes</span>';
                    }

                    return '<span class="label label-warning">No</span>';
                },
            ],
            [
                'attribute' => 'weekly_newsletter',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->weekly_newsletter){
                        return '<span class="label label-success">Yes</span>';
                    }

                    return '<span class="label label-warning">No</span>';
                },
            ],
            [
                'attribute' => 'special_offers',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->special_offers){
                        return '<span class="label label-success">Yes</span>';
                    }

                    return '<span class="label label-warning">No</span>';
                },
            ],
            'date_created',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{message} {view} {update} {delete}',
                'buttons' => [
                    /*'message' => function ($url,$model) {
                        return Html::a(
                            '<span class="label label-info"><span class="fa fa-envelope"></span></span>',
                            ['#'],
                            [
                                'class' => 'showModalButton',
                                'value' => \yii\helpers\Url::to(['/newsletter-subscriptions/email-subscribers', 'id' => $model->id]),
                                'title' => Yii::t('yii', 'Delete'),
                                'title' => 'Send Message',
                                'data-pjax' => '0',
                            ]
                        );
                    },*/
                    'message' => function ($url,$model) {
                        return Html::a(
                            '<span class="label label-info"><span class="fa fa-envelope"></span></span>',
                            ['/newsletter-subscriptions/email-subscribers', 'id' => $model->id],
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'view' => function ($url) {
                        return Html::a(
                            '<span class="label label-info"><span class="fa fa-search-plus"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            '<span class="label label-warning"><span class="fa fa-pencil"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<span class="label label-danger"><span class="fa fa-trash"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                                'data' => [
                                    'confirm' => "Are you sure you want to delete?",
                                    'method' => 'post',
                                ]
                            ]
                        );
                    },
                ]
            ]
        ],
        'emptyText' => ' - ',
        'showFooter' => true,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'toolbar' => [
            '{export}',
        ],
        'export' => [
            'fontAwesome' => true,
        ],
        'exportConfig' => [
            GridView::EXCEL => true,
            GridView::PDF => true,
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => false,
        ],
    ]); ?>
</div>
