<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsletterSubscriptions */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Newsletter Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-subscriptions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'First Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name : $model->first_name;
                },
            ],
            [
                'label' => 'Last Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->last_name : $model->last_name;
                },
            ],
            [
                'label' => 'Company',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->member <> null) ? ($model->member->accountCompany <> null) ? $model->member->accountCompany->name : "-" : "-";
                },
            ],
            'email:email',
            [
                'label' => 'Type',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->member <> null){
                        return '<span class="label label-success">Member</span>';
                    }

                    return '<span class="label label-warning">Non-Member</span>';
                },
            ],
            [
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->active){
                        return '<span class="label label-info">Active</span>';
                    }

                    return '<span class="label label-danger">In-active</span>';
                },
                'filter' => [
                    '1' => 'Yes',
                    '0' => 'No',
                ],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
                'headerOptions' => ['class' => 'text-center', 'style' => 'width: 8%;']
            ],
            [
                'attribute' => 'events_news',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->events_news){
                        return '<span class="label label-success">Yes</span>';
                    }

                    return '<span class="label label-warning">No</span>';
                },
            ],
            [
                'attribute' => 'weekly_newsletter',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->weekly_newsletter){
                        return '<span class="label label-success">Yes</span>';
                    }

                    return '<span class="label label-warning">No</span>';
                },
            ],
            [
                'attribute' => 'special_offers',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->special_offers){
                        return '<span class="label label-success">Yes</span>';
                    }

                    return '<span class="label label-warning">No</span>';
                },
            ],
            'date_created',
        ],
    ]) ?>

</div>
