<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NewsletterSubscriptionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-subscriptions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="col-md-3">
        <?= $form->field($model, 'email') ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'events_news')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'select ....']) ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'weekly_newsletter')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'select ....']) ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'special_offers')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'select ....']) ?>
    </div>

    <div class="col-md-3">
        <?= $form->field($model, 'active')->dropDownList(['0' => 'In-active', '1' => 'Active'], ['prompt' => 'select ....']) ?>
    </div>

    <div class="col-md-12">
        <div class="form-group pull-right">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
