<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NewsletterSubscriptions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-subscriptions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_created')->textInput() ?>

    <?= $form->field($model, 'active')->dropDownList([ '0' => 'In-active', '1' =>'Active' ], ['prompt' => 'select ....']) ?>

    <?= $form->field($model, 'events_news')->dropDownList([ '0' => 'In-active', '1' =>'Active' ], ['prompt' => 'select ....']) ?>

    <?= $form->field($model, 'weekly_newsletter')->dropDownList([ '0' => 'In-active', '1' =>'Active' ], ['prompt' => 'select ....']) ?>

    <?= $form->field($model, 'special_offers')->dropDownList([ '0' => 'In-active', '1' =>'Active' ], ['prompt' => 'select ....']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
