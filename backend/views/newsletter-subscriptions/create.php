<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsletterSubscriptions */

$this->title = 'Create Newsletter Subscriptions';
$this->params['breadcrumbs'][] = ['label' => 'Newsletter Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-subscriptions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
