<style>
    body {
        font-family: "Century Gothic", Gadget, sans-serif;
        font-size: 13px;
        background: #ffffff;
        line-height: 1.5
    }

    h2 {
        font-size: 22px;
        font-weight: normal;
        color: #D11349;
        margin: 15px 0px 5px;
    }

    h3 {
        font-size: 16px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 0px;
    }

    p {
        margin: 20px 0px;
        font-size: 13px;
    }

    .eventLabel, .eventValue {
        color: #002D5D;
        margin: 5px 0px;
        line-height: 1.5;
        font-size: 13px;
        font-weight: bold;
    }

    .eventValue {
        color: #D11349;
        font-weight: normal;
    }

    h4 {
        font-size: 12px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 15px;
    }
</style>

<tr style="height:100px">
    <td valign="top" colspan="3">
        <table width="100%" min-height align="left" cellspacing="10">
            <tr>
                <td colspan="4"><strong>Please select the emails you wish to receive from us:</strong></td>
            </tr>
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4"><b>Title</b></td>
            </tr>
            <tr>
                <td colspan="2"><b>Event Date and Time:</b></td>
                <td colspan="2">
                    Date and time
                </td>
            </tr>
            <tr>
                <td colspan="2"><b>Registration Date:</b></td>
                <td colspan="2">
Date
                </td>
            </tr>
            <tr>
                <td colspan="2"><b>Venue</b></td>
                <td colspan="2">
                   Dubai
                </td>
            </tr>

            <tr>
                <td colspan="2"><b>Name:</b></td>
                <td colspan="2">

                </td>
            </tr>


            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>

            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2"><b>48 hour Cancellation policy</b></td>
            </tr>

            <tr>
                <td colspan="4">
                    If you wish to cancel your registration, please call us at
                    04-3970303 or email charmaine.dylanco@bbgdxb.com 48 hours prior to the event.

                </td>
            </tr>
        </table>

        <table width="94%" align="left" cellpadding="0" cellspacing="10">

            <tr>
                <td colspan="3"><h4>
                        British Business Group</br>
                        P.O. Box 9333 Dubai, UAE.</br>
                    </h4></td>
            </tr>
        </table>
        <br>
    </td>
</tr>
