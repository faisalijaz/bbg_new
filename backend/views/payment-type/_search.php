<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-types-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'code') ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'status') ?>
        </div>

    <div class="col-lg-12 col-sm-12">
        <div class="form-group pull-right">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
