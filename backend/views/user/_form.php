<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'last_name') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'username')->textInput(['disabled' => $model->isNewRecord ? false : true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'email')->textInput(['disabled' => $model->isNewRecord ? false : true]) ?>
        </div>
    </div>

    <div class="row">

    </div>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'address')->textarea(); ?>

    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'phone_number') ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?php echo $form->field($model, 'status')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    </div>



    <?php ActiveForm::end(); ?>

</div>
