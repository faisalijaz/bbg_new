<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="user-index card-box">

    <p class="pull-right">
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            'email:email',
            'type',
            [
                'attribute' => 'status',
                'filter' => true,
                'value' => function($data) {
                    return ($data->status == 1 ? 'Atcive' : 'In-Active');
                },
                'format' => 'raw'
            ],
            // 'created_at',
            // 'updated_at',

            ['class' => 'common\helpers\CustomActionColumn'],
        ],
    ]); ?>

</div>
