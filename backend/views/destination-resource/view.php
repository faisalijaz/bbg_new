<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DestinationResources */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Destination Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destination-resources-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'Destination',
                'value' => $model->destinationsName->title,
            ],
            'capacity',
            'description',
            [
                'attribute' => 'Image',
                'format' => ['image', ['width' => '100', 'height' => '100']],
                'value' => $model->image,
            ],
            'status',
        ],
    ]) ?>

</div>
