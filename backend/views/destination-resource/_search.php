<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DestinationResourcesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destination-resources-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="col-md-4">
        <?= $form->field($model, 'id') ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'name') ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'destination_assigned') ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'description') ?>
    </div>


    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
