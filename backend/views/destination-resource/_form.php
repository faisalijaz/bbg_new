<?php

use common\models\Destinations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DestinationResources */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destination-resources-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'destination_assigned')->dropDownList(ArrayHelper::map(Destinations::find()->all(), 'id', 'title'), ['prompt' => 'Select Destination']); ?>

    <?= $form->field($model, 'capacity')->textInput(['maxlength' => true,'type' =>'number']) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <div id="images" class="form-group">
        <label class="control-label" for="input-image">Image</label>
        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
            <img src="<?= $model->image ?>" alt="" width="125" height="125" title=""
                 data-placeholder="no_image.png"/>
        </a>
        <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>

    </div>

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Active', 'In-Active' => 'In-Active',], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
