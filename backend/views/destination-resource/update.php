<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DestinationResources */

$this->title = 'Update Destination Resources: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Destination Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="destination-resources-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
