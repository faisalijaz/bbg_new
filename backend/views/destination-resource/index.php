<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DestinationResourcesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Destination Resources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
</div>

<div class="destination-resources-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Destination Resources', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'Destination',
                'value' => function ($model) {
                    return $model->destinationsName->title;
                }
            ],
            'capacity',
            'description',
            [
                'attribute' => 'icon',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<img src="'. $model->image .'" width="100">';
                }
            ],
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
