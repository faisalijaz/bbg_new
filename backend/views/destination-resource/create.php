<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DestinationResources */

$this->title = 'Create Destination Resources';
$this->params['breadcrumbs'][] = ['label' => 'Destination Resources', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destination-resources-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
