<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PointOfContact */

$this->title = 'Create Point Of Contact';
$this->params['breadcrumbs'][] = ['label' => 'Point Of Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-of-contact-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
