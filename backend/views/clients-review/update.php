<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ClientsReviews */

$this->title = 'Update Clients Reviews: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clients Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clients-reviews-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
