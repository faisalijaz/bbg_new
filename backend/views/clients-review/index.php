<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClientsReviewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-reviews-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Clients Reviews', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'rating',
            'description',
            'client_name',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
