<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ClientsReviews */

$this->title = 'Create Clients Reviews';
$this->params['breadcrumbs'][] = ['label' => 'Clients Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-reviews-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
