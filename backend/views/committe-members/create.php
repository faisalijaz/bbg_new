<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CommitteMembers */

$this->title = 'Create Committe Members';
$this->params['breadcrumbs'][] = ['label' => 'Committe Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="committe-members-create card-box">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
