<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CommitteMembersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Committe Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="committe-members-index card-box">


    <p> <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Create Member', ['create'], ['class' => 'btn btn-success btn-sm pull-right']) ?></p>
    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'full_name',
            'designation',
            'email:email',
            [
                'attribute' => 'icon',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<img src="'. $model->image .'" width="150">';
                }
            ],
            // 'image',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
