<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CommitteMembers */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Committe Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="committe-members-view card-box">


    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
            'model' => $model,
        'attributes' => [
            'full_name',
            'designation',
            'email:email',
            'phonenumber',
            [
                'attribute' => 'Image',
                'format' => ['image', []],
                'value' => $model->image,
            ],
        ],
    ]) ?>

</div>



