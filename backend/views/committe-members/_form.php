<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CommitteMembers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="committe-members-form card-box">
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">

            <?= $form->field($model, 'type')->dropDownList(
                 \yii\helpers\ArrayHelper::map(\common\models\StaffCategories::find()->all(),'id', 'category_name')
            , ['prompt' => 'Select Type']); ?>

        </div>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
            <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
            <?= $form->field($model, 'phonenumber')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
            <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true, 'type' => 'number']) ?>
        </div>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
            <div id="images" class="form-group">
                <label class="control-label" for="input-image">Image</label>
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    <img src="<?= $model->image ?>" alt="" width="125" height="125" title=""
                         data-placeholder="no_image.png"/>
                </a>
                <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
            </div>
        </div>

        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'In-Active'], []); ?>
        </div>

        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6, 'id' => "elm1"]) ?>
        </div>

        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
