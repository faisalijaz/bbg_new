<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\ViewFromChair */
/* @var $form yii\widgets\ActiveForm */

$model->date = date('Y-m-d');
?>
<div class="view-from-chair-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <div id="images" class="form-group">
                <label class="control-label" for="input-image">Main Image</label>
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    <img src="<?= ($model->image != "" && $model->image <> null) ? $model->image : Yii::$app->params['no_image']; ?>"
                         alt="" width="125" height="125" title=""
                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                </a>
                <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
            </div>
        </div>

    </div>
    <div class="row">

        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-md-3">
            <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true, 'type' => 'number']) ?>
        </div>


        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(['0' => 'Un-Publish', '1' => 'Publish'], ['prompt' => 'Select Status'])->label('Publish') ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'display_on_home')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'Select Display on home'])->label('Show on Homepage') ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
        </div>


        <div class="col-md-12">

            <?= $form->field($model, 'description')->textarea(['class'=> 'tinyMCe','rows' => 6, 'id' => 'elm1']) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>