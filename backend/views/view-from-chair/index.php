<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ViewFromChairSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'View From Chairs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-from-chair-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create View From Chair'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'short_description:ntext',
             [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->status) ? "<span class='label label-success'>Published</span>" : "<span class='label label-danger'>Un-Published</span>";
                }
            ],
            'date:date',
            [
                'attribute' => 'display_on_home',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->display_on_home) ? "<span class='label label-success'>Published</span>" : "<span class='label label-danger'>Un-Published</span>";
                }
            ],
            'sort_order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
