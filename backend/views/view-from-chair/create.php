<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ViewFromChair */

$this->title = Yii::t('app', 'Create View From Chair');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'View From Chairs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-from-chair-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
