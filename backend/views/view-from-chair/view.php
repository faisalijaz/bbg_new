<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ViewFromChair */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'View From Chairs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-from-chair-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'Image',
                'format' => ['image', []],
                'value' => $model->image,


            ],
            'short_description:ntext',
            'description:ntext',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->status) ? "<span class='label label-success'>Published</span>" : "<span class='label label-danger'>Un-Published</span>";
                }
            ],
            'date:date',
            [
                'attribute' => 'display_on_home',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->display_on_home) ? "<span class='label label-success'>Published</span>" : "<span class='label label-danger'>Un-Published</span>";
                }
            ],
            'sort_order'
        ],
    ]) ?>

</div>
