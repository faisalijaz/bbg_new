<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MemberOffers */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Member Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="member-offers-update card-box card-box">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
