<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MemberOffers */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Member Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            ['attribute' => 'category_id',
                'format'=>'raw',
                'value' => function($model){
                return $model->memberCategories->title;
            }],
            'short_description',
            'description',
            'created_at',
            [
                'attribute' => 'Image',
                'format' => ['image', []],
                'value' => $model->image,
            ],
            [
                'attribute' => 'Related Type',
                'value' => function($model){
                    return ucwords($model->offer_rel);
                }
            ],
            [
                'attribute' => 'Related To',
                'value' => function($model){
                    if($model->offer_rel == 'member'){
                        $member = \common\models\Members::findOne($model->offer_rel_id);
                        return ($member <> null) ? $member->first_name . ' ' .$member->last_name : '-';
                    }

                    if($model->offer_rel == 'company'){
                        $member = \common\models\AccountCompany::findOne($model->offer_rel_id);
                        return ($member <> null) ? $member->name : '-';
                    }

                    return '-';
                }
            ],
        ],
    ]) ?>

</div>
