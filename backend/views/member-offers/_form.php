<?php

use dosamigos\ckeditor\CKEditor;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MemberOffers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-offers-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <div id="images" class="form-group">
                <label class="control-label" for="input-image">Member Offer Image</label>
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    <img src="<?= ($model->image != "" && $model->image <> null) ? $model->image : Yii::$app->params['no_image']; ?>"
                         alt="" width="125" height="125" title=""
                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                </a>
                <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'title')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(\common\models\Categories::find()->where(['type'=>'offers'])->all(), 'id', 'title'), ['prompt' => 'Select Category']); ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'is_active')->dropDownList(['0' => 'In-Active', '1' => 'Active'],['prompt' => 'Select ...']); ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'sort_order')->textInput(['type' => 'number']) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'offer_rel')
            ->dropDownList(['general' => 'General', 'member' => 'Member', 'company' => 'Company'],
                ['prompt' => 'Select ....', 'id' => 'offer_related_to'])->label('Related to'); ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'offer_rel_id')->widget(DepDrop::classname(), [
            'options' => ['id' => 'subcat-id'],
            'type'=>DepDrop::TYPE_SELECT2,
            'pluginOptions' => [
                'depends' => ['offer_related_to'],
                'placeholder' => 'Select...',
                'url' => \yii\helpers\Url::to(['/member-offers/find-list'])
            ]
        ])->label('Name');
        ?>
    </div>

    <div class="col-md-12">
    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 10],
        'preset' => 'basic',

    ]);
    ?></div>

    <div class="col-md-12">
        <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
