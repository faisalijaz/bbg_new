<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MemberOffersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Member Offers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-offers-index">

    <p class="pull-right">
        <?= Html::a('Create Member Offers', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            ['attribute' => 'category_id',
                'format'=>'raw',
                'value' => function($model){
                return ($model->memberCategories <> null) ? $model->memberCategories->title : "";
            }],
            'short_description',
            // 'description',
            // 'created_at',
            [
                'attribute' => 'Related Type',
                'value' => function($model){
                    return ucwords($model->offer_rel);
                }
            ],
            [
                'attribute' => 'Related To',
                'value' => function($model){
                    if($model->offer_rel == 'member'){
                        $member = \common\models\Members::findOne($model->offer_rel_id);
                        return ($member <> null) ? $member->first_name . ' ' .$member->last_name : '-';
                    }

                    if($model->offer_rel == 'company'){
                        $member = \common\models\AccountCompany::findOne($model->offer_rel_id);
                        return ($member <> null) ? $member->name : '-';
                    }

                    return '-';
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
