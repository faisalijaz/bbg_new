<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Areas */

$this->title = Yii::t('app', 'Create Areas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Areas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="areas-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
