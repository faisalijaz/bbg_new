<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AdminInvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-invoices-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Admin Invoices', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'invoice_id',
            'user_id',
            'invoice_related_to',
            'invoice_rel_id',
            'invoice_category',
            //'payment_status',
            //'subtotal',
            //'tax',
            //'total',
            //'invoice_date',
            //'invoice_sent',
            //'payment_id',
            //'paytabs_p_id',
            //'paytabs_payment_url:url',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
