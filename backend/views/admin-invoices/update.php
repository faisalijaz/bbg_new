<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdminInvoices */

$this->title = 'Update Admin Invoices: ' . $model->invoice_id;
$this->params['breadcrumbs'][] = ['label' => 'Admin Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->invoice_id, 'url' => ['view', 'id' => $model->invoice_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="admin-invoices-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
