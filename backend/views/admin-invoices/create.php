<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AdminInvoices */

$this->title = 'Create Admin Invoices';
$this->params['breadcrumbs'][] = ['label' => 'Admin Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-invoices-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
