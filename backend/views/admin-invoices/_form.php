<?php

use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AdminInvoices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-invoices-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4">
        <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\common\models\Members::find()->all(), 'id', function ($model) {
                return $model->id . ' - ' . $model->first_name . " " . $model->last_name;
            }),
            'language' => 'de',
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label("User"); ?>
    </div>
    <div class="col-md-4">

        <?= $form->field($model, 'invoice_related_to')->dropDownList([
            'member' => 'Member', 'event' => 'Event', 'renewal' => 'Renewal',
        ], ['prompt' => 'Select ...', 'id' => 'invoice_related']) ?>
    </div>
    <div class="col-md-4">

        <?= $form->field($model, 'invoice_rel_id')->widget(DepDrop::classname(), [
            'options' => ['id' => 'subcat-id'],
            'pluginOptions' => [
                'depends' => ['invoice_related'],
                'placeholder' => 'Select...',
                'url' => Url::to(['/admin-invoices/find-invoice-rel'])
            ]
        ]);
        ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'invoice_rel_id')->textInput() ?>
    </div>
    <div class="col-md-4">

        <?= $form->field($model, 'invoice_category')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'payment_status')->dropDownList(['paid' => 'Paid', 'unpaid' => 'Unpaid', 'partialy_paid' => 'Partialy paid',], ['prompt' => '']) ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'subtotal')->textInput() ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'tax')->textInput() ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'total')->textInput() ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'invoice_date')->textInput() ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'invoice_sent')->dropDownList(['0', '1',], ['prompt' => '']) ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'payment_id')->textInput() ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'paytabs_p_id')->textInput() ?>
    </div>

    <div class="col-md-4">

        <?= $form->field($model, 'paytabs_payment_url')->textInput(['maxlength' => true]) ?>
    </div>

    <table id="images" class="table table-striped table-bordered table-hover images-table upload-preview">
        <thead>
        <tr>
            <td class="text-left">Realted to</td>
            <td class="text-left">Related</td>
            <td class="text-left">Description</td>
            <td class="text-right">amount</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <?php $image_row = 0; ?>
        <?php
        if ($model->invoiceItems <> null) {
            foreach ($model->invoiceItems as $item) { ?>
                <tr id="image-row<?php echo $image_row; ?>">

                    <td class="">

                        <?= $form->field($item, 'invoice_related_to')->dropDownList([
                            'event' => 'Event', 'membership' => 'Membership'
                        ],['id' => 'item-related' . $image_row])->label(false); ?>

                    </td>

                    <td class="">
                        <?= $form->field($model, 'invoice_rel_id')->widget(DepDrop::classname(), [
                            'options' => ['id' => 'subcat-id'],
                            'pluginOptions' => [
                                'depends' => ['item-related' . $image_row],
                                'placeholder' => 'Select...',
                                'url' => Url::to(['/admin-invoices/find-invoice-item'])
                            ]
                        ])->label(false);

                        ?>

                    </td>

                    <td class="">

                        <?= $form->field($item, 'invoice_category')->textInput()->label(false); ?>

                    </td>

                    <td class="">

                        <?= $form->field($item, 'amount')->textInput()->label(false); ?>

                    </td>

                    <td class="text-left">
                        <button type="button" onclick="$('#image-row<?php echo $image_row; ?>, .tooltip').remove();"
                                data-toggle="tooltip" title="Remove" class="btn btn-danger"><i
                                    class="fa fa-minus-circle"></i></button>
                    </td>
                </tr>
                <?php $image_row++; ?>
            <?php }
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5"></td>
            <td class="text-left">
                <button type="button" onclick="addImage();" data-toggle="tooltip" title="Add" class="btn btn-primary"><i
                            class="fa fa-plus-circle"></i></button>
            </td>
        </tr>
        </tfoot>
    </table>


    <div class="col-md-12">

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
