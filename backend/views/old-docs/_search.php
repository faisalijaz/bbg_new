<?php

use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TblImageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-image-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5 col-sm-12">

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'user_id',
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\Members::find()->all(), 'id', function ($model) {
                        return $model->first_name . " " . $model->last_name;
                    }),
                    'options' => ['placeholder' => 'Search Member ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

            </div>
                <div class="col-lg-5 col-sm-12">

                    <?= Select2::widget([
                        'model' => $model,
                        'attribute' => 'company',
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\AccountCompany::find()->all(), 'id', 'name'),
                        'options' => ['placeholder' => 'Search Company ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                </div>

            <div class="form-group pull-right">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
