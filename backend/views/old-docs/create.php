<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblImage */

$this->title = 'Create Tbl Image';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
