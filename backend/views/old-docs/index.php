<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users Documents / Old System';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-image-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'condensed' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Name',
                'attribute' => 'user_id',
                'value' => function($model){
                    if($model->member <> null){
                        return $model->member->first_name . " " . $model->member->last_name;
                    }
                },
            ],
            [
                'label' => 'Company',
                'value' => function($model){
                    if($model->member <> null){
                        if($model->member->accountCompany <> null) {
                            return $model->member->accountCompany->name;
                        }
                    }
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Members::find()->all(),'id',function($model){
                    return $model->first_name . " " .$model->last_name;
                }),
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'label' => 'Document',
                'attribute' => 'filename',
                'format' => 'raw',
                'value' => function ($model) {

                    if (strpos($model->filename, '.png') ||
                        strpos($model->filename, '.jpg') ||
                        strpos($model->filename, '.jpeg') ||
                        strpos($model->filename, '.bmp') ||
                        strpos($model->filename, '.gif')) {

                        $img = Yii::$app->params['appUrl'] . '/uploads/user/' . $model->filename;
                        return '<img  src="' . $img . '" style="height:250px;">';

                    } else {
                        return Html::a($model->title, Yii::$app->params['appUrl'] . '/uploads/user/' . $model->filename, [
                            'target' => '_blank'
                        ]);
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>
</div>
