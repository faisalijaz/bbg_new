<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AccountCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-company-index">


    <?= ExportMenu::widget([
        'dataProvider' => $dataProviderfull,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            [
                'attribute' => 'category',
                'value' => function ($model) {
                    return ($model->companyCategory <> null) ? $model->companyCategory->title : "";
                }
            ],
            'vat_number',
            'url:url',
            'phonenumber',
            'fax',
            'postal_code',
            [
                'label' => 'Emirates',
                'attribute' => 'emirates_number'
            ],
            'address',
            'about_company:ntext',
            'logo',
            [
                'label' => 'Status',
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active <> null) ? "Active" : "In-active";
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
        "exportConfig" => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_PDF => true,
            ExportMenu::FORMAT_EXCEL => true,
        ],
        'target' => '_blank',
        'showColumnSelector' => false,
        "contentBefore" => [
            ["value" => "Companies List"]
        ],
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-warning'
        ]

    ]); ?>
</div>
    <p class="pull-right">
        <?= Html::a('Create Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'condensed' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'category',
                'value' => function ($model) {
                    return ($model->companyCategory <> null) ? $model->companyCategory->title : "";
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Categories::find()->all(),'id','title'),
                'filterInputOptions' => ['prompt' => 'Select category...', 'class' => 'form-control', 'id' => null],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center', 'style' => 'width: 12%;']
            ],
            'url:url',
            'phonenumber',
            'fax',
            'postal_code',
            [
                'label' => 'Emirates',
                'attribute' => 'emirates_number'
            ],
            'address',
            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
</div>
