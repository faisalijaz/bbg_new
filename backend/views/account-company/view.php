<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AccountCompany */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-company-view">


    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'vat_number',
            [
                'attribute' => 'category',
                'value' => function ($model) {
                    return ($model->companyCategory <> null) ? $model->companyCategory->title : "";
                },
            ],
            'url:url',
            'phonenumber',
            'fax',
            'postal_code',
            [
                'label' => 'Emirates',
                'attribute' => 'emirates_number'
            ],
            'address',
            'about_company:ntext',
            'logo',
            [
                'attribute' => 'is_active',
                'value' => function ($model) {
                    return ($model->is_active) ? "Yes" : "No";
                },
            ],
            [
                'attribute' => 'show_in_directory',
                'value' => function ($model) {
                    return ($model->show_in_directory) ? "Yes" : "No";
                },
            ],
        ],
    ]) ?>

</div>
<div class="row">

    <div class="col-lg-12">
        <h2>Offers & News</h2>
        <ul class="nav nav-tabs tabs">
            <li class="active tab border-right-list ">
                <a href="#eventsInfo" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">News</span>
                </a>
            </li>
            <li class="tab border-right-list ">
                <a href="#offersInfo" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-building"></i></span>
                    <span class="hidden-xs">Offers </span>
                </a>
            </li>
        </ul>
        <div class="tab-content" style="min-height: 1500px;">
            <div class="tab-pane" id="eventsInfo">
                <?= $this->render('_news', [
                    'news' => $memberNews,
                ]) ?>
            </div>
            <div class="tab-pane" id="offersInfo">
                <?= $this->render('_offers', [
                    'offers' => $memberOffers,
                ]) ?>
            </div>
        </div>
    </div>
</div>