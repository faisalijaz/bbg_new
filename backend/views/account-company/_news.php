<?php
/**
 * Created by PhpStorm.
 * User: Faisal Ijaz
 * Date: 05/12/2018
 * Time: 03:05 PM
 */

?>

<?= \kartik\grid\GridView::widget([
    'dataProvider' => $news,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'title',
        'short_description:ntext',
        [
            'label' => 'Status',
            'value' => function($model){
                return ($model->isPublished) ? "Published" : "UnPublished";
            }
        ],
        [
            'attribute' => 'Related To',
            'value' => function($model){
                if($model->related_to == 'member'){
                    $member = \common\models\Members::findOne($model->rel_id);
                    return ($member <> null) ? $member->first_name . ' ' .$member->last_name : '-';
                }

                if($model->related_to == 'company'){
                    $member = \common\models\AccountCompany::findOne($model->rel_id);
                    return ($member <> null) ? $member->name : '-';
                }

                return '-';
            }
        ],
    ],
    'emptyText' => ' - ',
    'showFooter' => true,
    'pjax' => true,
    'pjaxSettings' => [
        'neverTimeout' => true,
    ],
    'toolbar' => [
        '{export}',
    ],
    'export' => [
        'fontAwesome' => true,
    ],
    'exportConfig' => [
        \kartik\grid\GridView::EXCEL => true,
        \kartik\grid\GridView::PDF => true,
    ],
    'panel' => [
        'type' => \kartik\grid\GridView::TYPE_PRIMARY,
        'heading' => false,
    ],
]); ?>