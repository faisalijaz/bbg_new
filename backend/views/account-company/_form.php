<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AccountCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="account-company-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <div id="images" class="form-group">
            <label class="control-label" for="input-image">Company Logo</label>
            <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                <img src="<?= ($model->logo != "" && $model->logo <> null) ? $model->logo : Yii::$app->params['no_image']; ?>"
                     alt="" width="125" height="125" title=""
                     data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
            </a>
            <?= $form->field($model, 'logo')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
        </div>
    </div>

    <div class="col-md-6">
        <label class="control-label" for="membersearch-last_name">Sector</label>
        <?= Select2::widget([
            'model' => $model,
            'attribute' => 'category',
            'data' => ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title'),
            'options' => ['placeholder' => 'Select ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'phonenumber')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'emirates_number')->textInput(['maxlength' => true])->label('Emirates'); ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'vat_number')->textInput(['maxlength' => true]) ?>
    </div>


    <div class="col-md-6">
        <?= $form->field($model, 'is_active')->dropDownList(['1' => 'Active', '0' => 'In-active'],['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'show_in_directory')->dropDownList(['1' => 'Yes', '0' => 'No'],['prompt' => 'Please Select ...','maxlength' => true]) ?>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'about_company')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
