<?php
/**
 * Created by PhpStorm.
 * User: Faisal Ijaz
 * Date: 05/12/2018
 * Time: 03:05 PM
 */

?>

<?= \kartik\grid\GridView::widget([
    'dataProvider' => $offers,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'title',
        ['attribute' => 'category_id',
            'format'=>'raw',
            'value' => function($model){
                return $model->memberCategories->title;
            }],
        'short_description',
        [
            'attribute' => 'Related To',
            'value' => function($model){
                if($model->offer_rel == 'member'){
                    $member = \common\models\Members::findOne($model->offer_rel_id);
                    return ($member <> null) ? $member->first_name . ' ' .$member->last_name : '-';
                }

                if($model->offer_rel == 'company'){
                    $member = \common\models\AccountCompany::findOne($model->offer_rel_id);
                    return ($member <> null) ? $member->name : '-';
                }

                return '-';
            }
        ],
    ],
    'emptyText' => ' - ',
    'showFooter' => true,
    'pjax' => true,
    'pjaxSettings' => [
        'neverTimeout' => true,
    ],
    'toolbar' => [
        '{export}',
    ],
    'export' => [
        'fontAwesome' => true,
    ],
    'exportConfig' => [
        \kartik\grid\GridView::EXCEL => true,
        \kartik\grid\GridView::PDF => true,
    ],
    'panel' => [
        'type' => \kartik\grid\GridView::TYPE_PRIMARY,
        'heading' => false,
    ],
]); ?>