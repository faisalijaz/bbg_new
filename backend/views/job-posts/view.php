<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\JobPosts */

$this->title = $model->job_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Job Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-posts-view">


    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Job ID',
                'attribute' => 'id'
            ],
            [
                'label' => 'Member',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name . " " . $model->member->last_name : "-";
                }
            ],
            'user_type',
            'job_title',
            'position',
            'industry',
            'skills',
            'experience',
            'location',
            'posted_on',
            'apply_by',
            'contact_name',
            'contact_email:email',
            'contact_phone',
            'short_description:ntext',
            'description:ntext',
            [
                'attribute' => 'Published',
                'format' => 'raw',
                'value' => ($model->published == 1 ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>')
            ],
            'auto_remove_after',
        ],
    ]) ?>

</div>
