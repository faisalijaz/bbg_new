<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use common\models\Members;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\JobPosts */
/* @var $form yii\widgets\ActiveForm */

$model->user_type = "member";

?>

<div class="job-posts-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <label>Select Memebr</label>
            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'user_id',
                'data' => ArrayHelper::map(Members::find()->all(), 'id', function ($member){
                    return $member->first_name . " " .$member->last_name;
                }),
                'options' => ['placeholder' => 'Select a comapny ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
            <?= $form->field($model, 'user_type')->hiddenInput()->label(false); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'job_title')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">


        <div class="col-md-4">
            <?= $form->field($model, 'industry')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'skills')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'experience')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'posted_on')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Registration end date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'apply_by')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Registration end date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">



        <div class="col-md-4">
            <?= $form->field($model, 'auto_remove_after')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Registration end date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'published')->dropDownList(['0' => 'Un-Publish', '1' => 'Publish'], ['prompt' => '']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'contact_email')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>

    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
