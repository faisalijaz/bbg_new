<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\JobPostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Job Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="job-posts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Job Posts'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Member',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name . " " . $model->member->last_name : "-";
                }
            ],
            'job_title',
            'position',
            'industry',
            'skills',
            'experience',
            'location',
            'posted_on',
            'apply_by',
            //'contact_name',
            //'contact_email:email',
            //'contact_phone',
            //'short_description:ntext',
            //'description:ntext',
            //'published',
            //'auto_remove_after',

            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
</div>
