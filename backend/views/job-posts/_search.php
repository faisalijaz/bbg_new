<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\JobPostsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-posts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'user_type') ?>

    <?= $form->field($model, 'job_title') ?>

    <?= $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'industry') ?>

    <?php // echo $form->field($model, 'skills') ?>

    <?php // echo $form->field($model, 'experience') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'posted_on') ?>

    <?php // echo $form->field($model, 'apply_by') ?>

    <?php // echo $form->field($model, 'contact_name') ?>

    <?php // echo $form->field($model, 'contact_email') ?>

    <?php // echo $form->field($model, 'contact_phone') ?>

    <?php // echo $form->field($model, 'short_description') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'published') ?>

    <?php // echo $form->field($model, 'auto_remove_after') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
