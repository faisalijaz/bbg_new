<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-invoice-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?= ExportMenu::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => \common\models\TblInvoice::find()
                        ->where(['>=', 'FROM_UNIXTIME(invoice_date,"%Y-%m-%d")', '2018-01-01'])
            ]),
            'columns' =>[
                [
                    'label' => 'Invoice Number',
                    'attribute' => 'user_invoice_id',
                    'value' => function ($model) {
                        return $model->user_invoice_id;
                    }
                ],
            [
                'label' => 'Invoice Date',
                'attribute' => 'invoice_date',
                'value' => function ($model) {
                    return date('Y-m-d', $model->invoice_date);
                }
            ],
            [
                'label' => 'Member',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name . " " . $model->member->last_name : " - ";
                }
            ],
            [
                'label' => 'Company',
                'value' => function ($model) {
                    return ($model->member <> null) ? ($model->member->accountCompany <> null) ? $model->member->accountCompany->name : " - " : "";
                }
            ],

            'invoice_category',
            'payment_status',
            'amount',
            // 'pdf_name',
            'invoice_date',
            'created_date',

        ],
            "exportConfig" => [
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_PDF => true,
                ExportMenu::FORMAT_EXCEL => true,
            ],
            'target' => '_blank',
            'showColumnSelector' => false,
            "contentBefore" => [
                ["value" => "Old System Invoices"]
            ],
            'dropdownOptions' => [
                'label' => 'Export All from January 1st, 2018',
                'class' => 'btn btn-warning'
            ]

        ]); ?>
    </p>


    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Invoice Number',
                'attribute' => 'user_invoice_id',
                'value' => function ($model) {
                    return $model->user_invoice_id;
                }
            ],
            [
                'label' => 'Member',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name . " " . $model->member->last_name : " - ";
                }
            ],
            [
                'label' => 'Company',
                'value' => function ($model) {
                    return ($model->member <> null) ? ($model->member->accountCompany <> null) ? $model->member->accountCompany->name : " - " : "";
                }
            ],

            'invoice_category',
            'payment_status',
            //'amount',
            //'pdf_name',
            //'invoice_date',
            //'created_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
