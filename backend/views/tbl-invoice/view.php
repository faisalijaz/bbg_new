<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TblInvoice */

$this->title = $model->invoice_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-invoice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->invoice_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->invoice_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'invoice_id',
            [
                'label' => 'Member',
                'value' => function ($model) {
                    return ($model->member <> null) ? $model->member->first_name . " " . $model->member->last_name : " - ";
                }
            ],
            [
                'label' => 'Company',
                'value' => function ($model) {
                    return ($model->member <> null) ? ($model->member->accountCompany <> null) ? $model->member->accountCompany->name : " - " : "";
                }
            ],
            'user_invoice_id',
            'invoice_category',
            'payment_status',
            'note',
            'amount',
            'pdf_name',
            'invoice_date',
            'created_date',
        ],
    ]) ?>

</div>
