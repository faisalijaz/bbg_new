<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TblInvoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-invoice-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_user_id')->textInput() ?>

    <?= $form->field($model, 'user_invoice_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoice_category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_status')->dropDownList(['done' => 'paid','pending' => 'pending'],['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pdf_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoice_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
