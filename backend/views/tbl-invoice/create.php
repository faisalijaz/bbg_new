<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblInvoice */

$this->title = 'Create Tbl Invoice';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-invoice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
