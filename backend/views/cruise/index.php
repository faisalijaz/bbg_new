<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CruiseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cruises';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>


<div class="cruise-index card-box">
    <p class="pull-right">
        <?= Html::a('Create Cruise', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'seats_capacity',
            [
                'attribute' => 'status',
                'value' => function($model){
                                return $model->status == 0 ? 'In-Active' : 'Active' ;
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
