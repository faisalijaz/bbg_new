<?php

use kartik\widgets\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NewsCategories */
/* @var $form yii\widgets\ActiveForm */

$model->date = date('Y-m-d');

?>

<div class="news-categories-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

    </div>

    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'Select Status'])->label('Publish') ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'sort_order')->textInput(['type' => 'number']) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
        </div>


    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
