<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StaffCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="staff-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6">
        <?= $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'sort_order')->textInput(['type' => 'number']) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'status')->dropDownList([1 => 'Active', 0 => 'In-Acitve'])->label(false); ?>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success pull-right']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
