<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StaffCategories */

$this->title = Yii::t('app', 'Create Staff Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
