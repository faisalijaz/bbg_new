<?php
use common\models\Invoices;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;

$member = null;
$invoice = null;

if ($subscriber->member <> null) {
    $member = $subscriber->member;
}

if ($subscriber->invoice <> null) {
    $invoice = $subscriber->invoice;
} else {
    $invoice = Invoices::findOne(['invoice_id' => $subscriber->gen_invoice]);
}

$type = ($invoice <> null)  ? ($invoice->invoice_related_to == "event") ? "E-" : "M-" : "";
?>

<section class="MainArea" style="font-family: 'Century Gothic'">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" style="width: 60%; float: left;">
                    <a href="/"><img src="<?= Yii::$app->params['invoice_header_image']; ?>" alt="BBG-Dubai"></a>
                </div>

                <div class="col-3 col-sm-3 col-md-4 col-lg-4 col-xl-4" style="width: 35%; float: right;">
                    <h1 style="color: #000;"><b><?= Yii::$app->params['invoice_title']; ?></b></h1>
                    <strong><span>Invoice date: <?php echo date("d-M-Y", $subscriber->created_at); ?></span></strong><br/>
                    <strong><span>Invoice number: <?= ($invoice <> null) ? $type . str_pad($subscriber->gen_invoice, 7, "0", STR_PAD_LEFT) : 0; ?></span></strong>
                </div>

                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="width: 60%; float: left;">
                    <br/><br/>
                    <strong><br/><?= (\Yii::$app->appSettings <> null) ? strip_tags(\Yii::$app->appSettings->settings->about, '<br>')  : ""; ?></strong>
                    <strong><br/>Telephone: <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->telephone : ""; ?><br/></strong>
                    <strong>Email: <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->admin_email : ""; ?><br/></strong>
                    <strong>VAT Registration no:
                        <u><?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->app_vat : ""; ?></u><br/></strong>
                </div>

                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="width: 40%; float:right;">
                    <br/>
                    <strong>Bill To:</strong><br/>
                    <strong>Name: <?= ($subscriber <> null) ? $subscriber->firstname . ' ' . $subscriber->lastname : ""; ?>
                        <?= ($subscriber <> null && $subscriber->user_type == 'guest') ? '(Guest)' : '(' . ucwords($subscriber->user_type) . ')'; ?></strong>
                    <br/>
                    <strong>Company: <?= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->name : ""; ?></strong>
                    <br/>
                    <strong>Address: <?= ($member <> null) ? $member->address : ""; ?><br/></strong>
                    <strong>P.O
                        Box: <?= ($member <> null && $member->accountCompany <> null) ? $member->accountCompany->postal_code : ""; ?><br/></strong>
                    <strong>Telephone: <?= ($member <> null) ? $member->phone_number : ""; ?></strong>
                    <br/>
                    <strong>VAT Registration no:
                        <u><?= ($member <> null) ? $member->vat_number : ""; ?></u><br/><br/></strong>
                </div>

                <div style="width: 100%;" class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                    <div style="width: 100%;">
                        <table class="table" style="width: 100%; float: left;" cellpadding="5">
                            <thead>
                                <tr>
                                    <th class="col-md-8"
                                        style="width:70%; background: #1d355f; color: #ffffff; text-align: left;">Item
                                    </th>
                                    <th class="col-md-4"
                                        style="width:30%; background: #1d355f; color: #ffffff; text-align: center">Amount
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($invoice <> null) {
                                    if ($invoice->invoiceItems <> null) {
                                        foreach ($invoice->invoiceItems as $item) {

                                            ?>
                                            <tr>
                                                <td>
                                                    <?= ($item->eventSubscription <> null) ? $item->eventSubscription->firstname . ' ' . $item->eventSubscription->lastname : ""; ?>
                                                    <br/>  <?= ($item->eventSubscription <> null) ? $item->eventSubscription->eventData->title : ""; ?>
                                                </td>
                                                <td style="text-align: right;">AED <?= $item->subtotal; ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div style="clear:both;width: 100%;border-top: #000 solid 1px;margin-top: 30px;">
                        <table class="table" style="width: 100%; float: left;" cellpadding="5">
                            <tbody>

                            <tr>
                                <td style="width:70%;text-align: left;">
                                    <strong>Subtotal</strong>
                                    <br/><strong>VAT(5%)</strong>
                                </td>
                                <td style="width:30%;text-align: right;">
                                    <strong><?= ($invoice <> null) ? $invoice->subtotal : 0; ?></strong>
                                    <br/><strong><?= ($invoice <> null) ? $invoice->tax : 0; ?></strong>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:70%;text-align: left;">
                                    <br/><strong>Total</strong>
                                </td>
                                <td style="width:30%;text-align: right;">
                                    <br/>
                                    <strong>
                                        AED <?= ($invoice <> null) ? $invoice->subtotal + $invoice->tax : 0; ?>
                                        <input type="hidden"
                                               value="<?= ($invoice <> null) ? $invoice->subtotal + $invoice->tax : 0; ?>"
                                               id="actual_total"/>
                                    </strong>
                                </td>
                            </tr>

                            <?php
                            if ($invoice <> null) {
                                if ($invoice->adjustment <> null) {
                                    $adjustment = $invoice->adjustment;
                                    ?>
                                    <tr>
                                        <td style="width:70%;text-align: left;">
                                            <strong>Adjustment</strong>
                                            <br/>
                                            <?= $adjustment->reason; ?>
                                        </td>
                                        <td style="width:30%;text-align: right;">
                                            <strong>
                                                <?= $adjustment->type . " " . $adjustment->adjustment; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:70%;text-align: left;"><strong>Total Payable</strong>
                                        </td>
                                        <td style="width:30%;text-align: right;">
                                            <strong>AED <?= ($invoice <> null) ? $invoice->total : 0; ?></strong>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                    <?php
                    if ($invoice <> null) {
                        if ($invoice->payment_status == 'unpaid') {
                            echo \yii\helpers\Html::a('Pay Online',
                               // [\Yii::$app->params['appUrl'] . "/site/invoice-payment", 'invoice_id' => $invoice->invoice_id]);
                            "https://bbgdubai.org/site/invoice-payment?invoice_id=$invoice->invoice_id");
                            echo '<br/>';
                        }
                    }
                    ?>
                    <?= (\Yii::$app->appSettings <> null) ? \Yii::$app->appSettings->settings->invoice_payment_info : ""; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .searchResult .view a {
        font-weight: bold;
    }

    #events, #news {
        background: #ffffff;
    }

    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
</style>
