<style>
    body {
        font-family: "Century Gothic", Gadget, sans-serif;
        font-size: 13px;
        background: #ffffff;
        line-height: 1.5
    }

    h2 {
        font-size: 22px;
        font-weight: normal;
        color: #D11349;
        margin: 15px 0px 5px;
    }

    h3 {
        font-size: 16px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 0px;
    }

    p {
        margin: 20px 0px;
        font-size: 13px;
    }

    .eventLabel, .eventValue {
        color: #002D5D;
        margin: 5px 0px;
        line-height: 1.5;
        font-size: 13px;
        font-weight: bold;
    }

    .eventValue {
        color: #D11349;
        font-weight: normal;
    }

    h4 {
        font-size: 12px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 15px;
    }
</style>
<?php
$event_details = $emaildata->eventData;
?>
<table width="624" height="1250" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01"
       style="border: solid 1px #cccccc;">
    <tr>
        <td colspan="3">
            <img src="<?= Yii::$app->params['invoice_header_image']; ?>" width="624" height="268" alt="">
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="3">
            <table width="87%" height="100%" align="center">
                <tr>
                    <td colspan="4">You're registered for:</td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4"><b><?= ($event_details <> null) ? $event_details->title : ""; ?></b></td>
                </tr>
                <tr>
                    <td colspan="2"><b>Event Date and Time:</b></td>
                    <td colspan="2">
                        <?= ($event_details <> null) ? formatDate($event_details->event_startDate) : ''; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><b>Registration Date:</b></td>
                    <td colspan="2">
                        <?= ($emaildata <> null) ? formatDate($emaildata->created_at) : ''; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><b>Venue</b></td>
                    <td colspan="2">
                        <?= ($event_details <> null) ? $event_details->venue : ""; ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2"><b>Name:</b></td>
                    <td colspan="2">
                        <?= ($emaildata <> null && !empty($emaildata->firstname)) ? $emaildata->firstname . " " . $emaildata->lastname : "Guest"; ?>
                    </td>
                </tr>

                <?php if ($emaildata <> null && !empty($emaildata->email)) { ?>
                    <tr>
                        <td colspan="2"><b>Email:</b></td>
                        <td colspan="2"><?= $emaildata->email; ?></td>
                    </tr>
                <?php } ?>
                <!--                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr><td colspan="2"><b>Payment Status:</b></td><td colspan="2">Paid</td></tr>-->
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>

                <?php if ($emaildata->invoice <> null) {
                    ?>
                    <tr>
                        <td colspan="4"><b>Payment Details</b></td>
                    </tr>
                    <?php
                    $invoice = $emaildata->invoice;

                    $member = null;
                    if ($emaildata->member <> null) {
                        $member = $emaildata->member;
                    }

                    if ($invoice->amount != 0) {
                        ?>
                        <tr>
                            <td>
                                <?= $emaildata->firstname . ' ' . $emaildata->lastname; ?>
                            </td>
                            <td>
                                <?= ($member <> null && $member->group) ? $member->group->title : 'Guest'; ?>
                            </td>
                            <td>
                                <b>AED <?= $invoice->subtotal; ?></b>
                            </td>
                           <!-- <td>
                                <?/*= ($invoice->payment_status == 'paid') ? '<span style="padding: 5px;background: lawngreen;color: #ffffff;">' . ucwords($invoice->payment_status) . '</span>' : '<span  style="padding: 5px;background: red;color: #ffffff;">' . ucwords($invoice->payment_status) . '</span>'; */?>
                            </td>-->
                        </tr>
                        <tr>
                            <td colspan="4" style="font-size: 12px;" class="grid">
                                <br/>
                                <p style="float: right;font-size: 12px;">
                                    <b>Sub-Total:</b>
                                    <span id="subTotalAmount"> <?= $invoice->subtotal; ?></span>
                                    <br/><b>Total VAT:</b>
                                    <span id="totalAmountVAT"><?= $invoice->tax; ?></span>
                                    <br/><b>Total:</b> <span id="totalAmount"> <?= $invoice->amount; ?></span>
                                    AED
                                </p>
                            </td>
                        </tr>
                        <?php
                    }
                } ?>

                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"><b>48 hour Cancellation policy</b></td>
                </tr>

                <tr>
                    <td colspan="4">
                        If you wish to cancel your registration, please call us at
                        04-3970303 or email charmaine.dylanco@bbgdxb.com 48 hours prior to the event.

                    </td>
                </tr>
            </table>

            <table width="94%" align="center" cellpadding="0" cellspacing="10">

                <tr>
                    <td colspan="3"><h4>
                            British Business Group</br>
                            P.O. Box 9333 Dubai, UAE.</br>
                        </h4></td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
    </td>
    </tr>
    <tr>
        <td colspan="3">
            <img src="<?= Yii::$app->params['email_footer_image']; ?>" width="624" height="127" alt="">
        </td>
    </tr>
</table>

