<div id="invoice" style="border: 1px solid #dedede; padding: 5px;">
    <table align="center" cellpadding="0" cellspacing="10"
           style="background-color: #fff;width: 100%; font-size: 12px;">
        <tr>
            <td>
                <table width="100%" align="center" cellpadding="0" cellspacing="0">
                    <?php
                    if ($subscriber == null) {
                        ?>
                        <tr>
                            <td colspan="2">There is not any Invoice.</td>
                        </tr>
                        <?php
                    } else {
                        ?>
                        <tr>
                            <td width="" align="left" valign="top"><br>
                                <img src="http://bbgdubai.org/_assets/_images/logo.png" />
                                <br> <b>VAT #:</b>  <?= (Yii::$app->appSettings) ? Yii::$app->appSettings->settings->app_vat : ""; ?>
                            </td>
                            <td align="right" colspan="2">
                                <strong>Billed To:</strong>
                                <?= $subscriber->firstname . ' ' . $subscriber->lastname; ?>
                                <?= ($subscriber->user_type == 'guest') ? '(Guest)' : '(' . ucwords($subscriber->user_type) . ')'; ?>
                                <br/>
                                <b>Invoice #: <?= $subscriber->gen_invoice; ?></b>
                                <br/><b>Date:</b> <?php echo date("d-M-Y", $subscriber->created_at); ?>
                                <?php
                                if ($subscriber->member <> null) {
                                    echo '<br/><b>VAT #:</b> ' . $subscriber->member->vat_number;
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <h2 style="font-size:22px; font-weight: normal; color: #D11349; margin: 15px 0px 5px;">
                                    <?= trim("INVOICE"); ?>
                                </h2>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="left">
                                <table width="100%" height="100%" align="left" cellpadding="3" cellspacing="1">
                                    <tr>
                                        <th width="40%" align="left" bgcolor="#002D5D"
                                            style="width:40%;font-family:Century Gothic, Gadget, sans-serif ; font-size: 12px;  line-height: 2; color: #FFFFFF;">
                                            Item
                                        </th>
                                        <th width="15%" align="center" bgcolor="#002D5D"
                                            style="width:15%;text-align: center;font-family:  Century Gothic, Gadget, sans-serif ; font-size: 12px;  line-height: 2; color: #FFFFFF;">
                                            Amount
                                        </th>
                                        <th width="15%" align="center" bgcolor="#002D5D"
                                            style="width:15%;text-align: center;font-family:  Century Gothic, Gadget, sans-serif ; font-size: 12px;  line-height: 2; color: #FFFFFF;">
                                            VAT (5%)
                                        </th>
                                        <th width="30%" align="center" bgcolor="#002D5D"
                                            style="width:30%;text-align: center;font-family:  Century Gothic, Gadget, sans-serif ; font-size: 12px;  line-height: 2; color: #FFFFFF;">
                                            Total
                                        </th>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#DBDBDB" class="grid">
                                            <b>
                                                <?= $subscriber->firstname . "" . $subscriber->lastname; ?>
                                                <?= ($subscriber->user_type == "guest") ? " - Guest" : ' - ' . ucwords($subscriber->user_type); ?>
                                            </b>
                                        </td>
                                        <td bgcolor="#DBDBDB" class="grid" style="text-align: center;"><b>AED <?= $subscriber->subtotal; ?></b></td>
                                        <td bgcolor="#DBDBDB" class="grid" style="text-align: center;"><b>AED <?= $subscriber->tax; ?></b></td>
                                        <td bgcolor="#DBDBDB" class="grid" style="text-align: center;"><b>AED <?= $subscriber->fee_paid; ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="font-size: 12px;" class="grid"></td>
                                        <td style="font-size: 12px;" class="grid">
                                            <br/>
                                            <p style="margin-top: 50px;font-size: 12px;margin-left: 50px;">                                                <b>Sub-Total:</b>
                                                <span id="subTotalAmount"> <?= $subscriber->subtotal; ?></span>
                                                <br/><b>Total VAT:</b>
                                                <span id="totalAmountVAT"><?= $subscriber->tax; ?></span>
                                                <br/><b>Total:</b> <span id="totalAmount"> <?= $subscriber->fee_paid; ?></span> AED
                                            </p>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($subscriber->payment_status == "pending") { ?>
                                        <tr>
                                            <td colspan="4">BBG members can pay online.Please login your BBG Dubai
                                                account and click
                                                below
                                                <b>Pay online</b> link
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <a href="http://bbgdubai.org/event/InvoicePayment?id=12491&amp;eId=347&amp;d=420">
                                                    Pay online now
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <input type="hidden" value="#" id="payyyy"/>
                                            <td colspan="4">Below are the methods of payments acceptable by BBG:</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><p><b>CASH</b></p>
                                                <p><b>CHEQUE:</b> Payable to British Business Group</p></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><p><b>BANK TRANSFER-(NET OF BANK CHARGES)</b></p>
                                                <table width="98%" align="left" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="25%">In favor of</td>
                                                        <td width="1%">:</td>
                                                        <td>British Business Group</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">Bank</td>
                                                        <td width="1%" valign="top">:</td>
                                                        <td>HSBC Bank Middle East, P.O. Box 3766, Jumeirah 1, Dubai, UAE
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">Account No</td>
                                                        <td width="1%">:</td>
                                                        <td>030-123756-001</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">IBAN</td>
                                                        <td width="1%">:</td>
                                                        <td>AE880200000030123756001</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">Swift Code</td>
                                                        <td width="1%">:</td>
                                                        <td>BBMEAEAD</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="color: red; font-style: italic;">
                                System Generated Invoice
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="6"><h5> British Business Group, P.O. Box 9333 Dubai, UAE</h5>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </td>
        </tr>
    </table>
</div>