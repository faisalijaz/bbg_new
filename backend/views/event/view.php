<?php

use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Events */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-view  card card-box">

    <!--<p class="pull-right">
        <?/*= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) */?>
        <?/*= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>
    </p>-->
    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->


    <?php

    echo DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $model->title,
            'type' => DetailView::TYPE_PRIMARY,
        ],
        'attributes' => [
            'title',
           /* [
                'label' => 'Type',
                'value' => function ($model) {
                    if ($model->eventType <> null) {
                        return $model->eventType->name;
                    }
                    return '-';
                }
            ],*/
            'group_size',
          /*  [
                'label' => 'TBA',
                'value' => function ($model) {
                    if ($model->tba) {
                        return 'Yes';
                    }
                    return 'No';
                }
            ],*/
            'address:ntext',
            'registration_start',
            'registration_end',
            'cancellation_tillDate',
            'event_startDate',
            'event_endDate',
            'short_description:ntext',
            'fb_gallery',
            // 'condition',
            'description:ntext',
            'member_fee',
            'nonmember_fee',
            'committee_fee',
            'honourary_fee',
            'sponsor_fee',
            'focus_chair_fee',
            'currency',
            'display_price',
            'sticky',
            'userNote',
            'status',
            'active',
            'expiry_day',
            'create_date',
            'modify_date',
        ],
    ]);
    ?>

</div>
