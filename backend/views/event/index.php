<?php

use common\models\Events;
use common\models\EventSubscriptions;
use common\models\EventTypes;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Events';
$this->params['breadcrumbs'][] = $this->title;
$event_types = EventTypes::find()->all();

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    'title',
    [
        'attribute' => 'eventType',
        'format' => 'raw',
        'value' => function ($model) {
            if ($model->eventType <> null) {
                return '<span class="label label-info">' . $model->eventType->name . '</span>';
            }
            return '-';
        },
        'filter' => ArrayHelper::map(EventTypes::find()->all(), 'id', 'name'),
        'filterInputOptions' => ['prompt' => 'Select Type...', 'class' => 'form-control', 'id' => null],
    ],
    [
        'label' => 'Event Date',
        'value' => function ($model) {
            return $model->event_startDate;
        }
    ],
    [
        'label' => 'Venue',
        'value' => function ($model) {
            return $model->venue;
        }
    ],
    [
        'label' => 'Registerations',
        'value' => function ($model) {
            return EventSubscriptions::find()->where(['event_id' => $model->id])->count();
        }
    ],
    [
        'label' => 'Status',
        'format' => 'raw',
        'value' => function ($model) {

            $today = date('Y-m-d');

            if ($today == $model->event_startDate || $today == $model->event_endDate) {
                return '<span class="label label-warning">Now</span>';
            }

            if ($today <= $model->event_startDate) {
                return '<span class="label label-success">Upcoming</span>';
            }

            if ($today >= $model->event_endDate) {
                return '<span class="label label-danger">Past</span>';
            }
        }
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{register_list} {register_member} {status} {update} {delete}',
        'buttons' => [
            'register_list' => function ($url, $model) {
                return Html::a(
                    '<span class="label label-warning"><span class="fa fa-list-ol"></span></span>',
                    ['/event/event-registered-list', 'event' => $model->id],
                    [
                        'title' => 'Event Register list',
                        'data-pjax' => '0',
                    ]
                );
            },
            'register_member' => function ($url, $model) {
                return Html::a(
                    '<span class="label label-info"><span class="fa fa-user"></span></span>',
                    ['/event/subscribe-event', 'event' => $model->id],
                    [
                        'title' => 'Register Member',
                        'data-pjax' => '0',
                    ]
                );
            },

            'copy' => function ($url, $model) {
                return Html::a(
                    '<span class="label label-info"><span class="fa fa-copy bg-info"></span></span>',
                    ['/event/subscribe-event', 'event' => $model->id],
                    [
                        'title' => 'Copy Event',
                        'data-pjax' => '0',
                    ]
                );
            },
            'status' => function ($url, $model) {

                $label = '<span class="label label-danger"><span class="fa fa-eye-slash"></span></span>';
                $data = [
                    'title' => 'Publish',
                    'class' => 'publish change_event_status',
                    'data-pjax' => '0',
                    'id' => $model->id
                ];


                if ($model->active == "1") {

                    $label = '<span class="label label-success"><span class="fa fa-eye"></span></span>';
                    $data = [
                        'title' => 'Unpublish',
                        'class' => 'unpublish change_event_status',
                        'data-pjax' => '0',
                        'id' => $model->id
                    ];
                }

                return '<span id="status-link-' . $model->id . '">' . Html::a($label, $url, $data) . '</span>';
            },
            'update' => function ($url) {
                return Html::a(
                    '<span class="label label-primary"><span class="fa fa-pencil"></span></span>',
                    $url,
                    [
                        'title' => 'Update',
                        'data-pjax' => '0',
                    ]
                );
            },
            'delete' => function ($url, $model) {
                return Html::a(
                    '<span class="label label-danger"><span class="fa fa-trash"></span></span>',
                    ['event/delete', 'id' => $model->id],
                    [
                        'title' => 'Delete',
                        'data-pjax' => '0',
                        'data' => [
                            'confirm' => "Are you sure you want to delete?",
                            'method' => 'post',
                        ]
                    ]);
            }
        ],
    ],
];

?>
    <div class="events-index ">
        <div class="col-sm-12">
    <p>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Add Event', ['create'], ['class' => 'btn btn-success btn-sm pull-right']) ?>
    </p>
        </div>
    <div class="col-sm-12">
        <div class="col-md-6">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-md-6 text-right card-box">
            <span class="label label-warning"><span class="fa fa-list-ol"></span></span>&nbsp;Event Registrations&nbsp;
            <span class="label label-info"><span class="fa fa-user"></span></span>&nbsp;Register Event&nbsp;
            <span class="label label-success"><span class="fa fa-eye"></span></span>&nbsp;View&nbsp;
            <span class="label label-primary"><span class="fa fa-pencil"></span></span>&nbsp;Edit&nbsp;
            <span class="label label-danger"><span class="fa fa-trash"></span></span>&nbsp;Delete&nbsp;

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">
                <li class="active tab">
                    <a href="#current" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">Upcoming Events</span>
                    </a>
                </li>
                <li class="tab">
                    <a href="#past" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Past Events</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tab-content m-t-25">
        <div class="tab-pane active m-t-25" id="current">
            <div class="col-sm-12 card-box">
                <?php
                if ($event_types <> null && count($event_types)) {
                    foreach ($event_types as $type) {
                        ?>
                        <div class="col-md-2">
                            <span class="label label-info">
                                <i class="fa fa-chevron-circle-right"
                                   aria-hidden="true"></i> <?= $type->name; ?>
                                - <?= Events::find()->where(['type' => $type->id])
                                    ->andWhere(['>=', 'event_startDate', \Yii::$app->formatter->asDate('now', 'php:Y-m-d')])->count(); ?>
                            </span>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <hr/>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $columns,
                'condensed' => true,
                'emptyText' => '-',
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                ],
                'toolbar' => [
                    '{export}',
                ],
                'export' => [
                    'fontAwesome' => true,
                ],
                'exportConfig' => [
                    GridView::EXCEL => true,
                    GridView::PDF => true,
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => false,
                ],
            ]);
            ?>
        </div>

        <div class="tab-content m-t-25">
            <div class="tab-pane active m-t-25" id="past">
                <div class="col-sm-12 card-box">
                    <?php
                    if ($event_types <> null && count($event_types)) {
                        foreach ($event_types as $type) {
                            ?>
                            <div class="col-md-2">
                            <span class="label label-warning">
                                <i class="fa fa-chevron-circle-right"
                                   aria-hidden="true"></i> <?= $type->name; ?>
                                - <?= Events::find()->where(['type' => $type->id])
                                    ->andWhere(['<=', 'event_startDate', \Yii::$app->formatter->asDate('now', 'php:Y-m-d')])->count(); ?>
                            </span>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <hr/>
                <?= GridView::widget([
                    'dataProvider' => $dataProviderPast,
                    'filterModel' => $searchModelPast,
                    'columns' => $columns,
                    'condensed' => true,
                    'emptyText' => '-',
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                    ],
                    'toolbar' => [
                        '{export}',
                    ],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'exportConfig' => [
                        GridView::EXCEL => true,
                        GridView::PDF => true,
                    ],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => false,
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

<?= $this->registerJs('
 
    $("body").on("click", "a.change_event_status", function(e) {

        e.preventDefault();
        $(this).attr("disabled",true);
        var event_id = $(this).attr("id");

        var status = "0";

        if($(this).hasClass("publish")){
            status = "1";
        }

        $.ajax({

            url: "/event/change-event-status/",
            type: "POST", 
            data: { active : status , id : event_id },
            success : function(res){
                
                var text = "UnPublished";
                var res = JSON.parse(res);
                // console.log(res);
                if(res.status == "success"){ 
                    
                    $("#status-link-" + event_id).html(res.html);
                    
                    if(status == "1"){
                        text = "Published";
                    }
                    
                    swal({
                        title: "Success",
                        text: "Event " + text,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }
                $(this).attr("disabled",false);
            },
            error : function(res){
                console.log(res);
            }
        });
    });
        
'); ?>

        <style>
            .label {
                display: inline-block;
            }
        </style>
