<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\EventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'country_id') ?>

    <?= $form->field($model, 'city_id') ?>

    <?= $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'search_title') ?>

    <?php // echo $form->field($model, 'provience_state') ?>

    <?php // echo $form->field($model, 'other_region') ?>

    <?php // echo $form->field($model, 'postal_code') ?>

    <?php // echo $form->field($model, 'tba') ?>

    <?php // echo $form->field($model, 'map_cordinate') ?>

    <?php // echo $form->field($model, 'registration_start') ?>

    <?php // echo $form->field($model, 'registration_end') ?>

    <?php // echo $form->field($model, 'cancellation_tillDate') ?>

    <?php // echo $form->field($model, 'event_startDate') ?>

    <?php // echo $form->field($model, 'event_endDate') ?>

    <?php // echo $form->field($model, 'short_description') ?>

    <?php // echo $form->field($model, 'group_size') ?>

    <?php // echo $form->field($model, 'fb_gallery') ?>

    <?php // echo $form->field($model, 'condition') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'member_fee') ?>

    <?php // echo $form->field($model, 'nonmember_fee') ?>

    <?php // echo $form->field($model, 'committee_fee') ?>

    <?php // echo $form->field($model, 'honourary_fee') ?>

    <?php // echo $form->field($model, 'sponsor_fee') ?>

    <?php // echo $form->field($model, 'focus_chair_fee') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'display_price') ?>

    <?php // echo $form->field($model, 'sticky') ?>

    <?php // echo $form->field($model, 'userNote') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'quizNight') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'expiry_day') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'modify_date') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
