<?php

use common\models\Events;
use kartik\dialog\DialogAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

DialogAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Event Registrations';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .form-control {
        padding: 3px !important;
        height: 35px;
    }
</style>

<div class="row">

    <div class="col-sm-12">
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Add', ['/event/subscribe-event', 'event' => $eventId],
            [
                'class' => 'btn btn-success btn-sm pull-right',
                'style' => 'margin:5px;',
                'title' => 'Add Event Registration',
            ]); ?>

        <?= Html::a('<i class="fa fa-certificate" aria-hidden="true"></i> Download Badges', ['/event/event-subscribers-badges', 'event_id' => $eventId, 'mode' => 'download'], [
            'class' => 'btn  btn-sm btn-success pull-right',
            'target' => '_blank',
            'title' => 'Download Badges',
            'style' => 'margin:5px;'
        ]); ?>

        <?= Html::a('<i class="fa fa-id-badge" aria-hidden="true"></i> Badges', '#', [
            'class' => 'btn  btn-sm btn-success pull-right showModalButton',
            'style' => 'margin:5px',
            'value' => \yii\helpers\Url::to(['/event/event-subscribers-badges', 'event_id' => $eventId]),
            'title' => Yii::t('yii', 'Badges'),
        ]);
        ?>
    </div>

    <div class="col-md-12 col-sm-12">
        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i>  Go To Event Details',
            ['/event/update', 'id' => $eventId], ['class' => 'pull-right', 'style' => 'margin:5px;']); ?>
        <h3 class="col-sm-12">Event: <?= Html::encode(Events::findOne($eventId)->title) ?></h3>
    </div>

    <div class="col-sm-12 card-box">
        <div class="col-md-3">
            <span class="text-blue">
                <i class="fa fa-check-square" aria-hidden="true"></i> Registered - <?= $registered; ?>
            </span>
            <br/><br/>
            <span class="text-red">
                <i class="fa fa-window-close" aria-hidden="true"></i> Cancelled - <?= $non_registered; ?>
            </span>
            <br/><br/>
            <span class="text-blue">
                <i class="fa fa-user" aria-hidden="true"></i> Business Associates - <?= $businessAssociates; ?>
            </span>
        </div>
        <div class="col-md-3">
            <span class="text-blue"><i class="fa fa-address-card"
                                       aria-hidden="true"></i> Members - <?= $members; ?></span>
            <br/><br/>
            <span class="text-red">
                <i class="fa fa-user-o" aria-hidden="true"></i> Pre-Members - <?= $non_members; ?>
            </span>
        </div>
        <div class="col-md-3">
            <span class="text-blue">
                <i class="fa fa-check-square" aria-hidden="true"></i> Attended - <?= $attended; ?>
            </span>
            <br/><br/>
            <span class="text-red">
                <i class="fa fa-window-close" aria-hidden="true"></i> No Show - <?= $noShow; ?>
            </span>
        </div>
        <div class="col-md-3">
            <span class="text-blue">
                <i class="fa fa-money" aria-hidden="true"></i> Paid - <?= $paid; ?>
            </span>
            <br/><br/>
            <span class="text-red">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Unpaid - <?= $unpaid; ?>
            </span>
        </div>
    </div>
    <div class="col-md-4">
    <span class="pull-left">

    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'label' => 'Type',
                'attribute' => 'user_type_relation',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->params['UserTypesforEvent'][$model->user_type_relation];
                }
            ],
            [
                'attribute' => 'firstname',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->firstname;
                },
            ],
            [
                'attribute' => 'lastname',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->lastname;
                },
            ],
            [
                'attribute' => 'email',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->email;
                },
            ],
            [
                'attribute' => 'company',
                'format' => 'raw',
                'value' => function ($model) {

                    $company = "";
                    if ($model->member <> null) {
                        if ($model->member->accountCompany <> null) {
                            $company = $model->member->accountCompany->name;
                        }
                    }

                    if ($company == "") {
                        $company = $model->company;
                    }

                    return $company;
                },
            ],
            [
                'attribute' => 'mobile',
                'value' => function ($model) {
                    return $model->mobile;
                },
            ],
            [
                'attribute' => 'designation',
                'value' => function ($model) {
                    return $model->designation;
                },
            ],
            [
                'attribute' => 'diet_option_description',
                'format' => 'raw',
                'label' => 'Diet Option',
                'value' => function ($model) {
                    if ($model->diet_option) {
                        if ($model->diet_option_description == "other") {
                            return $model->other_diet_option_description;
                        }
                        return $model->diet_option_description;
                    }
                    return "-";
                }
            ],
            [
                'attribute' => 'fee_paid',
                'format' => 'raw',
                'label' => 'Fee',
                'value' => function ($model) {
                    $fee = $model->fee_paid;
                    if ($model->fee_paid <= 0) {
                        $model->fee_paid = 0;
                    } else {

                        $invoice = \common\models\Invoices::findOne($model->gen_invoice);

                      /*  if ($invoice <> null) {
                            $model->fee_paid = $invoice->total;
                        }*/
                        if ($invoice <> null) {
                            // $fee = $invoice->total;

                            $fee = $invoice->total;
                            $no_of_invoices  = count($invoice->invoiceItems);
                            if($no_of_invoices <= 0){
                                $no_of_invoices = 1;
                            }
                            $paid_required = $model->subtotal + $model->tax;
                            if($fee > $paid_required){
                                if(isset($invoice->adjustment->type) && $invoice->adjustment->type == '+'){
                                    $paid_required = $paid_required + ($invoice->adjustment->adjustment/ $no_of_invoices);
                                }else if(isset($invoice->adjustment->type) && $invoice->adjustment->type == '-'){
                                    $paid_required = $paid_required - ($invoice->adjustment->adjustment / $no_of_invoices);
                                }
                                $fee = $paid_required;
                            }
                        }
                        if ($fee == 0 && $model->fee_paid > 0) {
                            return $model->fee_paid ;
                        }

                    }
                    return $fee;

                },
                'headerOptions' => ['style' => 'width: 5%;'],
            ],
            [
                'attribute' => 'payment_status',
                'format' => 'raw',
                'value' => function ($model) {
                    $invoice = \common\models\Invoices::findOne($model->gen_invoice);

                    if($invoice <> null){
                        $model->payment_status =  ($invoice->payment_status == 'paid') ? "paid" : "pending";
                    }
                    return ucwords($model->payment_status);
                },
            ],
            [
                'attribute' => 'paymentMethod',
                'format' => 'raw',
                'value' => function ($model) {
                    return ucwords($model->paymentMethod);
                }
            ],
            [
                'label' => 'INVOICE NO.',
                'attribute' => 'gen_invoice',
                'format' => 'raw',
                'value' => function ($model) {
                    return str_pad($model->gen_invoice, 7, "0", STR_PAD_LEFT);
                }
            ],
            [
                'attribute' => 'walkin',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->walkin) ? "Yes" : "-";
                }
            ],
            [
                'attribute' => 'attended',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->attended) ? "Attended" : "No Show";
                }
            ],
            [
                'label' => 'Cancel Request',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->cancel_request && $model->status != "cancelled") ? 'Cancel Requested' : " - ";
                }
            ],
            [
                'label' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ucwords($model->status);
                }
            ],
            [
                'label' => 'Admin Note',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->userNote;
                }
            ]
        ],
        "exportConfig" => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_PDF => true,
            ExportMenu::FORMAT_EXCEL => true,
        ],
        'target' => '_blank',
        'showColumnSelector' => false,
        "contentBefore" => [
            ["value" => "Event Registration List"]
        ]

    ]);
    ?>
        </span>
    </div>
    <div class="col-md-3 pull-right">
        <?= Html::beginForm(['/event/bulk-attend'], 'post'); ?>
        <div class="col-md-9">
            <?= Html::dropDownList('bulk_attend', '', ['' => 'Mark as: ', '1' => 'Attended', '0' => 'No Show', 'print_badges' => 'Print Badges'],
                ['class' => 'dropdown form-control', 'required' => 'required']) ?>
            <?= Html::hiddenInput('event', $eventId, []); ?>
        </div>
        <div class="col-md-3">
            <?= Html::submitButton('Next', ['class' => 'btn btn-info',]); ?>
        </div>
    </div>
    <div class="col-md-12">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Type',
                    'attribute' => 'user_type_relation',
                    'value' => function ($model) {
                        return Yii::$app->params['UserTypesforEvent'][$model->user_type_relation];
                    },
                    'filter' => Yii::$app->params['UserTypesforEvent'],
                    'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
                    'headerOptions' => ['style' => 'width: 5%;'],
                ],
                [
                    'attribute' => 'firstname',
                    'value' => function ($model) {
                        return $model->firstname;
                    },
                    'headerOptions' => ['style' => 'width: 6%;'],
                ],
                [
                    'attribute' => 'lastname',
                    'value' => function ($model) {
                        return $model->lastname;
                    },
                    'headerOptions' => ['style' => 'width: 6%;'],
                ],
                [
                    'attribute' => 'email',
                    'value' => function ($model) {
                        return $model->email;
                    },
                    'headerOptions' => ['style' => 'width: 6%;'],
                ],
                [
                    'attribute' => 'company',
                    'value' => function ($model) {
                        $company = "";
                        if ($model->member <> null) {
                            if ($model->member->accountCompany <> null) {
                                $company = $model->member->accountCompany->name;
                            }
                        }

                        if ($company == "") {
                            $company = $model->company;
                        }

                        return $company;
                    },
                    'headerOptions' => ['style' => 'width: 6%;'],
                ],
                [
                    'attribute' => 'mobile',
                    'value' => function ($model) {
                        return $model->mobile;
                    },
                    'headerOptions' => ['style' => 'width: 6%;'],
                ],
                /*[
                    'attribute' => 'designation',
                    'value' => function ($model) {
                        return $model->designation;
                    },
                ],*/
                [
                    'attribute' => 'diet_option_description',
                    'label' => 'Diet Option',
                    'value' => function ($model) {
                        if ($model->diet_option) {
                            if ($model->diet_option_description == "other") {
                                return $model->other_diet_option_description;
                            }
                            return $model->diet_option_description;
                        }
                        return "-";
                    },
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width: 10%;'],
                    'filter' => ['Standard' => 'Standard', 'Vegetarian' => 'Vegetarian', 'other' => 'Other Dietary'],
                    'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null]
                ],
                [
                    'attribute' => 'fee_paid',
                    'format' => 'raw',
                    'label' => 'Fee',
                    'value' => function ($model) {

                        $fee = $model->fee_paid;

                        if ($model->fee_paid <= 0) {
                            $model->fee_paid = 0;
                        } else {

                            $invoice = \common\models\Invoices::findOne($model->gen_invoice);

                            if ($invoice <> null) {
                               // $fee = $invoice->total;

                                $fee = $invoice->total;
                                $no_of_invoices  = count($invoice->invoiceItems);
                                if($no_of_invoices <= 0){
                                    $no_of_invoices = 1;
                                }
                                $paid_required = $model->subtotal + $model->tax;
                                if($fee > $paid_required){
                                    if(isset($invoice->adjustment->type) && $invoice->adjustment->type == '+'){
                                            $paid_required = $paid_required + ($invoice->adjustment->adjustment/ $no_of_invoices);
                                    }else if(isset($invoice->adjustment->type) && $invoice->adjustment->type == '-'){
                                            $paid_required = $paid_required - ($invoice->adjustment->adjustment / $no_of_invoices);
                                    }
                                    $fee = $paid_required;
                                }
                            }

                            if ($fee == 0 && $model->fee_paid > 0) {
                                return $model->fee_paid . "<input type='hidden' id='fee-" . $model->id . "' value='$model->fee_paid' />";
                            }
                        }

                        return $fee . "<input type='hidden' id='fee-" . $model->id . "' value='$model->fee_paid' />";
                    },
                    'headerOptions' => ['style' => 'width: 5%;'],
                ],
                [
                    'attribute' => 'payment_status',
                    'format' => 'raw',
                    'value' => function ($model) {

                        $invoice = \common\models\Invoices::findOne($model->gen_invoice);

                        if($invoice <> null){
                            $model->payment_status =  ($invoice->payment_status == 'paid') ? "paid" : "pending";
                        }

                        return Html::dropDownList('payment_status', $model->payment_status, [
                            'paid' => 'Paid',
                            'pending' => 'Pending'
                        ], ['id' => $model->id, 'class' => 'change_payment_status form-control']);
                    },
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width: 5%;'],
                    'filter' => ['pending' => 'Unpaid', 'paid' => 'Paid'],
                    'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null]
                ],
                [
                    'attribute' => 'paymentMethod',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::dropDownList('paymentMethod', $model->paymentMethod, [
                            'online' => 'Online',
                            'cash' => 'Cash',
                            'cheque' => 'Cheque',
                            'bank transfer' => 'Bank Transfer',
                            'credit' => 'Credit',
                            'credit card' => 'Credit Card'
                        ], ['id' => $model->id, 'class' => 'change_payment_method form-control']);
                    },
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width: 5%;'],
                    'filter' => [
                        'o nline' => 'Online',
                        'cash' => 'Cash',
                        'cheque' => 'Cheque',
                        'bank transfer' => 'Bank Transfer',
                        'credit' => 'Credit',
                        'credit card' => 'Credit Card'
                    ],
                    'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null]
                ],
                [
                    'attribute' => 'attended',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::dropDownList('attended', $model->attended, [
                            '0' => 'No Show',
                            '1' => 'Attended'
                        ], ['id' => $model->id, 'class' => 'statusAttended form-control']);
                    },
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width: 8%;'],
                    'filter' => [
                        '0' => 'No Show',
                        '1' => 'Attended'
                    ],
                    'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null]
                ],

                ['class' => 'yii\grid\CheckboxColumn'],
                [
                    'label' => 'Cancel Request',
                    'attribute' => 'cancel_request',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return ($model->cancel_request && $model->status != "cancelled") ? '<span class="label label-danger">Cancel Requested</span>' : " - ";
                    },
                    'filter' => [
                        '1' => 'Cancel requested',
                        '0' => 'Not requested'
                    ],
                    'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width: 5%;']
                ],
                [
                    'label' => 'Registration Status',
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::dropDownList('status_change', $model->status, [
                            'approved' => 'Approved',
                            'pending' => 'Pending',
                            'cancelled' => 'Cancelled',
                            'deleted' => 'Deleted'
                        ], ['id' => $model->id, 'class' => 'change_reg_status form-control']);
                    },
                    'filter' => [
                        'approved' => 'Approved',
                        'pending' => 'Pending',
                        'cancelled' => 'Cancelled',
                        'deleted' => 'Deleted'
                    ],
                    'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width: 8%;']
                ],
                [
                    'label' => 'Registration Date',
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return \Yii::$app->formatter->asDate($model->created_at, 'php:D, d M, Y H:i:s');
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{attendance} {message} {print} {download} {single_badge} {update} {delete}',
                    'buttons' => [

                        'attendance' => function ($url, $model) {

                            $label = '<span class="label label-primary"><span class="fa fa-thumbs-up"></span></span>';
                            $class = 'attended-false change_attend_status';
                            $title = 'Change to No Show';

                            if (!$model->attended) {

                                $label = '<span class="label label-danger"><span class="fa fa-thumbs-down"></span></span>';
                                $class = 'attended-true change_attend_status';
                                $title = 'Change to Attended';

                            }

                            return '<span id="attend-link-' . $model->id . '">' . Html::a(
                                    $label,
                                    '#',
                                    [
                                        'class' => $class,
                                        'title' => $title,
                                        'id' => $model->id
                                    ]) . '</span>';

                        },

                        'print' => function ($url, $model) {

                            return Html::a('<span class="label label-warning"><span class="fa fa-file-pdf-o"></span></span>', '#', [
                                'class' => 'showModalButton',
                                'value' => Url::to(['/event/event-register-invoice/', 'subscriber' => $model->id]),
                                'title' => Yii::t('yii', 'View Invoice'),
                            ]);

                        },
                        'download' => function ($url, $model) {
                            return Html::a('<span class="label label-info"><span class="fa fa-download"></span></span>',
                                ['/event/event-register-invoice-pdf/', 'subscriber' => $model->id],
                                [
                                    'target' => '_blank',
                                    'title' => 'Download Invoice',
                                    'data-pjax' => '0',
                                ]
                            );
                        },

                        'single_badge' => function ($url, $model) {

                            return Html::a('<span class="label label-success"><span class="fa fa-id-badge"></span></span>',
                                ['/event/event-subscriber-badge', 'subscriber' => $model->id],
                                [
                                    'target' => '_blank',
                                    'title' => 'Download Badge',
                                    'data-pjax' => '0',
                                ]
                            );
                        },

                        'update' => function ($url, $model) {
                            return Html::a(
                                '<span class="label label-primary"><span class="fa fa-pencil"></span></span>',
                                ['/event/subscribe-event', 'event' => $model->event_id, 'subscriber' => $model->id],
                                [
                                    'title' => 'Register Member',
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<span class="label label-danger"><span class="fa fa-trash"></span></span>',
                                ['/event-subscriptions/delete', 'id' => $model->id, 'event_id' => $model->event_id],
                                [
                                    'title' => 'Register Member',
                                    'data-pjax' => '0',
                                    'data' => [
                                        'confirm' => "Are you sure you want to delete?",
                                        'method' => 'post',
                                    ]
                                ]
                            );
                        },
                    ],
                ],
            ],
            'condensed' => true,
            'emptyText' => '-',
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
            ],
            'toolbar' => [
                /*'{export}',*/
            ],
            'export' => [
                'fontAwesome' => true,
            ],
            'exportConfig' => [
                GridView::EXCEL => true,
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => false,
            ]
        ]);
        ?>
    </div>
</div>


<div id="credit_refund_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Credit / Refund amount on Cancellation</h4>
            </div>
            <div class="modal-body">
                <p>
                    This User has Already Paid for this Event. Do you want to make credit or refund the amount?
                </p>
                <p>
                    <label>Credit / Refund Note<span style="color: red;">*</span></label>
                    <textarea rows="5" id="credit_refund_note" class="credit_refund_note form-control"></textarea>
                <div class="hidden alert alert-danger" id="error_popver"></div>
                </p>
                <button type="button" class="btn btn-primary credit_refund_btn" id="credit">Credit</button>
                <button type="button" class="btn btn-info credit_refund_btn" id="refund">Refund</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<button type="button" class="hidden btn btn-info btn-lg" data-toggle="modal" id="modalBox" data-target="#myModal">Open
    Modal
</button>

<?php

if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 10000,
            type: "error",
            html: true,
            showConfirmButton: true
        });
    ');
}

if (\Yii::$app->session->getFlash('success')) {

    $success = Yii::$app->session->getFlash('success');

    $this->registerJs('
    swal({
            title: "Success!",
            text: "' . $success . '",
            timer: 10000,
            type: "success",
            html: true,
            showConfirmButton: true
        });
    ');
}

?>
<script>
    var $buttons = '<button type="button" role="button" id="refund" tabindex="0" class="creditRefund customSwalBtn">' + 'Refund' + '</button>' +
        '<button type="button" role="button" tabindex="0" id="credit" class="creditRefund customSwalBtn">' + 'Credit' + '</button>';

</script>
<?= $this->registerJs('
    $(".sendEventRegInvoice").click(function(e){
        
        e.preventDefault(); 
        $(this).attr("disabled",true);
        var eventReg = $(this).attr("id"); 
        
        $.ajax({
            url: "' . Yii::$app->request->baseUrl . '/event/send-reg-invoice/",
            type: "POST",
            data: { event_reg: eventReg }, 
            success : function(data){
                if(data){ 
                    swal({
                        title: "Success!",
                        text: "Invoice Sent!",
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }
                $(this).attr("disabled",false);
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
'); ?>

<?= $this->registerJs('
    
    $("body").on("click", "a.change_attend_status", function(e) {
    // $(".change_status").click(function(e){
        
        e.preventDefault(); 
        $(this).attr("disabled",true);
        var subscription = $(this).attr("id"); 
        
        var status = 0;
        
        if($(this).hasClass("attended-true")){
            status = 1;
        }
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/event/change-event-attendance/",
            type: "POST",
            data: { attended: status , id : subscription }, 
            success : function(res){
                 
                var text = "No Show";
                var res = JSON.parse(res);
                
                if(res.status == "success"){ 
                    
                    $("#attend-link-" + subscription).html(res.html);
                    
                    if(status == "1"){
                        text = "Attended";
                    }
                    
                    /*swal({
                        title: "Success",
                        text: "Event " + text,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });*/
                }     
                 
                $(this).attr("disabled",false);
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
 '); ?>
<?= $this->registerJs('
    
    $("body").on("change", ".change_reg_status", function(e) {
     
        e.preventDefault(); 
         
        var subscription = $(this).attr("id"); 
        var status = $(this).val();
        var cash_adjustments = false;    
        var paymentStatus = $(".change_payment_status#"+subscription+" option:selected").val();
        
        /*
        if(status == "cancelled" &&  paymentStatus == "paid") {
             
             $("#credit_refund_modal").modal("show");
             
             $(".credit_refund_btn").click(function(){
                
                var $type = $(this).attr("id");
                var $note = $("#credit_refund_note").val();
                
                if( $note == ""){
                     
                    $("#error_popver").removeClass("hidden");
                    $("#error_popver").html("Please enter note");
                    
                    setTimeout(function(){ 
                        $("#error_popver").addClass("hidden");
                    }, 3000);
                    
                } else {
                    
                    $.ajax({
            
                        url: "' . Yii::$app->request->baseUrl . '/event/change-reg-status/",
                        type: "POST",
                        data: { status : status, id : subscription, adjustment : true, type : $type, note : $note }, 
                        success : function(res){
                             
                            if(res){  
                                
                                swal({
                                    title: "Success",
                                    text: "Event Registration set to " + status,
                                    timer: 10000,
                                    type: "success",
                                    html: true,
                                    showConfirmButton: true
                                });
                                
                            } else{
                                swal({
                                    title: "Error!",
                                    text: "Status not changed!",
                                    timer: 10000,
                                    type: "error",
                                    html: true,
                                    showConfirmButton: true
                                });
                            }  
                            
                            $("#credit_refund_modal").modal("hide");   
                        },
                        error : function(data){
                        
                            swal({
                                title: "Error!",
                                text: data.responseText,
                                timer: 10000,
                                type: "error",
                                html: true,
                                showConfirmButton: true
                            });
                            
                        }
                    });
                }
             
             });
              
            
             
        } else { */
        
            $.ajax({
            
                url: "' . Yii::$app->request->baseUrl . '/event/change-reg-status/",
                type: "POST",
                data: { status : status, id : subscription, adjustment : false }, 
                success : function(res){
                     
                    if(res){  
                        
                        swal({
                            title: "Success",
                            text: "Event Registration set to " + status,
                            timer: 10000,
                            type: "success",
                            html: true,
                            showConfirmButton: true
                        });
                        
                    }       
                },
                error : function(data){
                    console.log(data);
                }
            });
        /* } */
    });
'); ?>
<?= $this->registerJs('    
    
    $("body").on("change", ".change_payment_status", function(e) {
     
        e.preventDefault(); 
         
        var subscription = $(this).attr("id"); 
        var payment_status = $(this).val();
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/event/change-payment-status/",
            type: "POST",
            data: { payment_status : payment_status , id : subscription }, 
            success : function(res){
                 
                if(res){  
                    
                    swal({
                        title: "Success",
                        text: "Payment status set to " + payment_status,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }     
                  
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });

 '); ?>
<?= $this->registerJs('    
    
    $("body").on("change", ".change_payment_method", function(e) {
     
        e.preventDefault(); 
         
        var subscription = $(this).attr("id"); 
        var payment_method = $(this).val();
        
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/event/change-payment-method/",
            type: "POST",
            data: { payment_method : payment_method , id : subscription }, 
            success : function(res){
                 
                if(res){  
                    
                    swal({
                        title: "Success",
                        text: "Payment Method is set to " + payment_method,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }      
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
    $("body").on("change", ".statusAttended", function(e) {
     
        e.preventDefault(); 
         
        var subscription = $(this).attr("id"); 
        var statusAttended = $(this).val();
        var Text = "No Show";
                
        $.ajax({
        
            url: "' . Yii::$app->request->baseUrl . '/event/change-attend-status/",
            type: "POST",
            data: { attended : statusAttended , id : subscription }, 
            success : function(res){
                 
                if(res){  
                    
                    if(statusAttended){
                        Text = "Attended";
                    }
                    
                    swal({
                        title: "Success",
                        text: "Payment Method is set to " + Text,
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }      
            },
            error : function(data){
                console.log(data);
            }
        }); 
    }); 
    
'); ?>
