<?php error_reporting(0); ?>
<div class="row">
    <?php foreach ($subscribers as $detail) {

        ?>
        <div class="col-md-6 pt10 pb10 mb15" <?= (isset($type) && ($type == 'pdf')) ? 'style="float: left;width: 45%;margin: 10;"' :''?> >
            <br>
            <div class="memberBadge" style="border: solid 1px #ccc;width: 340px; height:190px;text-align: center;" >
                <img src="<?= \Yii::$app->params['event_badge']; ?>" alt="BBG"/>
                <div class="row">
                    <div class="col-md-12 mt20 mb20">
                        <h3 class="text-blue pt20 pb20" <?= (isset($type) && ($type == 'pdf')) ? 'style="color: #1e3560 !important;"' : '' ?> >
                            <?php echo $detail->firstname . ' ' . $detail->lastname; ?></h3>
                        <?php
                        $companyName = '-';

                        if ($detail->member <> null) {
                            if ($detail->member->accountCompany <> null) {
                                $companyName = $detail->member->accountCompany->name;
                            }
                        }

                        if($companyName == "-"){
                            $companyName = $detail->company;
                        }

                        ?>
                        <h3 class="text-blue pt20 pb20" style="color: #1e3560 !important; font-size: 14px">
                            <?= $companyName; ?>
                        </h3>
                        <h3 style="color:  #be2030 !important;">
                            <?= ($detail->user_type == 'member') ? 'Member' : "Guest"; ?>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>