<?php

use kartik\checkbox\CheckboxX;
use kartik\widgets\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <div class="col-md-12">
        <h5 class="custom_color">Add Member / Guest</h5>
    </div>
    <div class="col-md-6">

        <div class="row form-group">

            <div class="col-md-12" style="margin-bottom: 10px;">
                <label class="control-label">Select Member</label>
                <?= Typeahead::widget([
                    'name' => 'first_name',
                    'options' => ['placeholder' => 'Filter as you type ...', 'class' => 'typeahead'] , 'id' => 'typo_member',
                    'scrollable' => true,
                    'pluginOptions' => ['highlight' => true],
                    'dataset' => [
                        [
                            'prefetch' => Url::to(['event/members-list']),
                            'remote' => [
                                'url' => Url::to(['event/members-list']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ],
                            'limit' => 10
                        ]
                    ]
                ]);
                ?>
            </div>

            <?php $form = ActiveForm::begin(['id' => 'AdminEventReg']); ?>
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true, 'id' => 'firstName']) ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true, 'id' => 'lastName']) ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'type' => 'numbers', 'id' => 'email']); ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true, 'id' => 'mobile']) ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'company')->textInput(['maxlength' => true, 'id' => 'company']) ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'designation')->textInput(['maxlength' => true, 'id' => 'designation']) ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <?php
                        if ($model->eventData->specialEventsFee <> null && $model->eventData->special_events == 1) {
                            echo $form->field($model, 'fee_paid')->dropDownList(ArrayHelper::map($model->eventData->specialEventsFee, 'amount', function ($data) {
                                return $data->fee_type . " - " . $data->amount . " AED";
                            }));
                        } else { echo $form->field($model, 'fee_paid')->textInput(['readonly' => 'readonly', 'maxlength' => true, 'id' => 'feePaid']); }
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'user_type_relation')->dropDownList(
                            Yii::$app->params['UserTypesforEvent'],[

                        ]);?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 checkbox-margin">
                        <?= $form->field($model, 'walkin')->widget(CheckboxX::classname(), [
                            'initInputType' => CheckboxX::INPUT_CHECKBOX,
                            'pluginOptions' => [
                                'theme' => 'krajee-flatblue',
                                'enclosedLabel' => true,
                                'threeState' => false,
                            ]
                        ])->label(false); ?>
                    </div>

                    <div class="col-md-3 checkbox-margin">

                        <?= $form->field($model, 'focGuest')->widget(CheckboxX::classname(), [
                            'initInputType' => CheckboxX::INPUT_CHECKBOX,
                            'pluginOptions' => [
                                'theme' => 'krajee-flatblue',
                                'enclosedLabel' => true,
                                'threeState' => false,
                            ]
                        ])->label(false); ?>

                    </div>

                    <?php
                    if (!$model->isNewRecord) {
                        ?>
                        <div class="col-md-3 checkbox-margin">
                            <?= $form->field($model, 'attended')->widget(CheckboxX::classname(), [
                                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                'pluginOptions' => [
                                    'theme' => 'krajee-flatblue',
                                    'enclosedLabel' => true,
                                    'threeState' => false,
                                ]
                            ])->label(false); ?>

                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'payment_status')->dropDownList(['paid' => 'Paid', 'unpaid' => 'Un-Paid'],[
                                    'prompt' => 'Select payment status ..'
                            ]); ?>
                        </div>

                        <div class="col-md-4">
                            <?=  $form->field($model, 'paymentMethod')->dropDownList( [
                                'online' => 'Online',
                                'cash' => 'Cash',
                                'cheque' => 'Cheque'
                            ]);
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'diet_option')->widget(CheckboxX::classname(), [
                            'initInputType' => CheckboxX::INPUT_CHECKBOX,
                            'options' => [
                                'id' => 'dietryPref',
                                'class' => 'HasDietryPrefrence',
                            ],
                            'pluginOptions' => [
                                'theme' => 'krajee-flatblue',
                                'enclosedLabel' => true,
                                'threeState' => false,
                            ]
                        ])->label(false); ?>
                    </div>
                    <div class="col-md-8">
                        <?= $form->field($model, 'diet_option_description')->dropDownList(
                            ['Standard' => 'Standard', 'Vegetarian' => 'Vegetarian', 'other' => 'Other Dietary'],
                            ['class' => 'hidden form-control', 'prompt' => 'Select Dietry Option...', 'id' => 'select_diet_pref'])
                            ->label(false); ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field($model, 'other_diet_option_description')->textarea(
                            [
                                'style' => 'width: 100%; height: 150px;;',
                                'class' => 'hidden extra_diet_instruction',

                            ])->label(false); ?>
                    </div>
                </div>


                <?= $form->field($model, 'user_type')->hiddenInput(['id' => 'userType'])->label(false); ?>
                <?= $form->field($model, 'event_id')->hiddenInput(['id' => 'eventId'])->label(false); ?>
                <?= $form->field($model, 'user_id')->hiddenInput(['id' => 'userId'])->label(false); ?>
                <?= $form->field($model, 'registered_by')->hiddenInput(['id' => 'registeredBy'])->label(false); ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'userNote')->textarea(['rows' => 6, 'id' => 'elm1']) ?>
        <?= $form->field($model, 'tax_group_id')->dropDownList(
            \yii\helpers\ArrayHelper::map(\common\models\TaxGroups::find()->all(), 'id', 'title'), ['class' => 'hidden'])->label(false);
        ?>
    </div>


    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Add & Send Email' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-success cus_btn_send_email' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <!-- This link to trigger popup invoice before sending -->

    <?= Html::a('<span class="label label-warning"><span class="fa fa-file-pdf-o"></span></span>', '#', [
                                'class' => 'showModalButton modelbox hidden',
                                'value' => Url::to(['/event/event-register-invoice/', 'subscriber' => 0]),
                                'title' => Yii::t('yii', 'View Invoice'),
                                'id' => 'invoice_view'
                            ]); ?>
</div>



<script>
    function getMember(email, event) {
        $.ajax({
            url: "/event/find-member",
            type: "post",
            data: {"email": email, "eventId": event},
            success: function (data) {

                var member = JSON.parse(data);
                // console.log(member);

                $(document).find("#firstName").val((member.first_name).trim());
                $(document).find("#lastName").val((member.last_name).trim());
                $(document).find("#email").val((member.email).trim());
                $(document).find("#company").val((member.company).trim());
                $(document).find("#designation").val((member.designation).trim());
                $(document).find("#mobile").val((member.phone_number).trim());
                $(document).find("#userId").val((member.id));
                $(document).find("#userType").val("member");
                $(document).find("#feePaid").val((member.member_fee).trim());
            }
        });
    }

    <?php
    $members_only_event = 0;
    if ($model->eventData <> null && $model->eventData->members_only) {
        $members_only_event = 1;
    }
    ?>

    var $members_only = <?= $members_only_event; ?>

</script>

<?= $this->registerJs('
    
    $(".typeahead").on("typeahead:selected", function(evt, item) {
          
        var email = "";
        var event =  $("#eventId").val();
        var data = $(this).val();
        data = data.split("|");
         
        email = $.trim(data[1]);
      
        // console.log($.trim(data[1]));  
        getMember(email, event);
        
    });
    
    
    $("#select_diet_pref").change(function(){ 
            if($(this).val() == "other"){
                $(".extra_diet_instruction").removeClass("hidden");
            }else{
                $(".extra_diet_instruction").val("");
                $(".extra_diet_instruction").addClass("hidden");
            }
      });
      
      $("#dietryPref").click(function(){ 
            if($(this).is(":checked")){
                $("#select_diet_pref").removeClass("hidden");
            }else{ 
                $("#select_diet_pref").addClass("hidden");
                 $(".extra_diet_instruction").addClass("hidden");
            }
      });
    
    $("#dietryPref").change(function(){
        
        if($("#dietryPref").is(":checked")){
                   
            $("#select_diet_pref").removeClass("hidden");
            
        } else {
            
            $(".extra_diet_instruction").val("");
            $("#select_diet_pref").val("");
            $("#select_diet_pref").addClass("hidden");
            $(".extra_diet_instruction").addClass("hidden");
        }
    
    });
    
     $("body").find("#select_diet_pref").change(function(){
            
        if($("#select_diet_pref option:selected").val() == "other"){
            $(".extra_diet_instruction").removeClass("hidden");
        } else{
            $(".extra_diet_instruction").val("");
            $(".extra_diet_instruction").addClass("hidden");
        }
        
    });
    
        
    
    $("body").on("beforeSubmit", "form#AdminEventReg", function () {
         
         var form = $(this);
         
         if($members_only){
             if(!$("#typo_member").val()){
                swal({
                        title: "Error",
                        text: "Please select Member",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-warning",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
               return false;
             }  
         }
         
         // alert("HI");
         // return false if form still have some validation errors
         
         if (form.find(".has-error").length) {
              return false;
         }
         
         // submit form
         $.ajax({
         
              url: form.attr("action"),
              type: "post",
              data: form.serialize(),
              beforeSend: function() {
                $(".cus_btn_send_email").attr( "disabled", "disabled" );
                $("body #custom_bbg_loader").css("display", "block");
                $("html").css("opacity", ".5");
                $("html").css("background-color", "#000");
              },
              success: function (response) {
                  
                  
                   if(!isNaN(response)){ 
                        
                        var link = "/event/event-register-invoice?subscriber=" + response;
                        
                        $("#invoice_view").attr("value" ,link );
                        $("#invoice_view").trigger("click");
                        
                        /* swal({
                            title: "Success",
                            text: "Event Registered!",
                            timer: 10000,
                            type: "success",
                            html: true,
                            showConfirmButton: true
                        },
                        function(){
                            window.location.reload(true);
                        }); */
                         
                  } else {
                        
                       response = JSON.parse(response); 
                       swal({
                            title: "Error",
                            text: response.error[0],
                            timer: 10000,
                            type: "error",
                            html: true,
                            showConfirmButton: true
                        },
                        function(){
                            window.location.reload(true);
                        });
                  } 
                  
                    $(".cus_btn_send_email").attr( "disabled", false);
                    $("body #custom_bbg_loader").css("display", "none");
                    $("html").css("opacity", "1");
              },
              error : function (response, error) {
                console.log(error);
                    $(".cus_btn_send_email").attr( "disabled", false);
                    $("body #custom_bbg_loader").css("display", "none");
                    $("html").css("opacity", "1");
                    
                  swal({
                        title: "Error",
                        text: error,
                        timer: 10000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                   },
                        function(){
                            window.location.reload(true);
                        });
              }
              
         });
         
         return false;
    });
 
');
?>

<?php

    $isNew = false;
    if ($model->isNewRecord) {
        $isNew = true;
    }

?>

<?= $this->registerJs('   

    $(document).ready(function(){
        
        var email = $("#email").val();
        var event = $("#eventId").val();
        
        if(email != ""){
            // getMember(email, event);
        }
        
        $(document).ready(function(){
           
           if($(".specialEventsCheck").is(":checked")){
                $(".normal_fee").addClass("hidden");
                $("#specialEventsFee").removeClass("hidden");
           } else { 
                $("#specialEventsFee").addClass("hidden");
                $(".normal_fee").removeClass("hidden");
           }
           
            if($("#dietryPref").is(":checked")){
                
                $("#select_diet_pref").removeClass("hidden");
                
                if($("#select_diet_pref option:selected").val() == "other"){
                    $(".extra_diet_instruction").removeClass("hidden");
                }
                
            } else { 
                $("#select_diet_pref").addClass("hidden");
                $(".extra_diet_instruction").addClass("hidden");
            }
            
       });
    });
');
?>

