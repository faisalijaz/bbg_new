<?php

use kartik\checkbox\CheckboxX;
use kartik\widgets\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <div class="col-md-12">
        <h5 class="custom_color">Cancel Registration</h5>
    </div>

    <div class="col-md-6">
        <div class="row form-group">

            <?php $form = ActiveForm::begin(['id' => 'AdminEventReg']); ?>
            <div class="col-md-12">
                <div class="form-group row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'invoice_id')->textInput(['maxlength' => true, 'id' => 'firstName']) ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'id' => 'lastName']) ?>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'amount')->textInput(['maxlength' => true, 'type' => 'numbers', 'id' => 'email']); ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'id' => 'mobile']) ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <?= $form->field($model, 'description')->textarea(['rows' => 6, 'id' => 'elm1']) ?>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add & Send Email' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-success cus_btn_send_email' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>