<div class="row">
    <div class="col-md-12 pt10 pb10 mb15" <?= (isset($type) && ($type == 'pdf')) ? 'style="float: left;width: 100%;margin: 10;"' : '' ?> >
        <br>
        <div class="memberBadge" style="margin: 5px auto;border: solid 1px #ccc;width: 446px;text-align: center;">
            <img src="<?= \Yii::$app->params['event_badge']; ?>" alt="BBG"/>
            <div class="row">
                <div class="col-md-12 mt20 mb20">
                    <h1 class="text-blue pt20 pb20" <?= (isset($type) && ($type == 'pdf')) ? 'style="color: #1e3560 !important;"' : '' ?> >
                        <?php echo $detail->firstname . ' ' . $detail->lastname; ?></h1>
                    <?php
                    $companyName = '-';
                    if ($detail->company != '') {
                        $companyName = $detail->company;
                    } ?>
                    <h3 class="text-blue pt20 pb20" style="color: #1e3560 !important;">
                        <?= $companyName; ?>
                    </h3>
                    <h2 style="color:  #be2030 !important;">
                        <?= ($detail->user_type == 'member') ? 'Member' : "Guest"; ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>