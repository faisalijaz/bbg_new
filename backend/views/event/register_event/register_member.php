<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Events */

$this->title = 'Event Registration';
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i>  Go To Event Registered List',
        ['/event/event-registered-list', 'event' => $model->event_id], ['class' => 'pull-right', 'style' => 'margin:5px;']); ?>


    <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i>  Go To Event Details',
        ['/event/update', 'id' => $model->event_id], ['class' => 'pull-right', 'style' => 'margin:5px;']); ?>

</div>
<h3><?= Html::encode($this->title) ?></h3>

<div class="events-create  card card-box">
    <?= $this->render('register_form', [
        'model' => $model,
    ]); ?>
</div>