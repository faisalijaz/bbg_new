<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CmsPages */
/* @var $form yii\widgets\ActiveForm */

$status = ['In-Active','Active'];
?>
<script src="/ubold/assets/js/text-editor.js"></script>

<div class="cms-pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">

                <li class="active tab">
                    <a href="#general" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">General</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#configuration" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Configuration</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#faq" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Tabs</span>
                    </a>
                </li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="general">

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'seo_url')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'id' => "elm1"]) ?>

                    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'meta_description')->textarea(['rows' => 6, 'maxlength' => true]) ?>

                    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>

                </div>

                <div class="tab-pane" id="configuration">

                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Banner Image</label>
                        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                            <img src="<?= ($model->banner_image != "" && $model->banner_image <> null) ? $model->banner_image : Yii::$app->params['no_image']; ?>" alt="" width="125" height="125" title="" data-placeholder="<?= Yii::$app->params['no_image']; ?>" />
                        </a>
                        <input type="hidden" name="CmsPages[banner_image]" value="<?php echo $model->banner_image; ?>" id="input-image" />

                    </div>

                    <?= $form->field($model, 'banner_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'banner_description')->textarea(['rows' => 4,'maxlength' => true]) ?>

                    <?= $form->field($model, 'bottom')->dropDownList(['1' => 'Yes', '0' => 'No'], ['prompt' => 'Bottom']) ?>

                    <?= $form->field($model, 'top')->dropDownList(['1' => 'Yes', '0' => 'No'], ['prompt' => 'Top']) ?>

                    <?= $form->field($model, 'sort_order')->textInput() ?>

                    <?= $form->field($model, 'banner_status')->dropDownList(['1' => 'Active', '0' => 'In-Active'], ['prompt' => 'Select Banner Status']) ?>

                    <?= $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'In-Active'], ['prompt' => 'Select Status']) ?>


                </div>

                <div class="tab-pane" id="faq">

                    <table id="tabs_table"
                           class="table table-striped table-bordered table-hover images-table upload-preview">
                        <thead>
                        <tr>
                            <td class="text-left">Title</td>
                            <td class="text-left">Description</td>
                            <td class="text-left">Status</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $tabs_row = 0; ?>
                        <?php foreach ($model->cmsTabs as $tab) { ?>

                            <tr id="feat-row<?php echo $tabs_row; ?>">
                                <td class="">
                                    <div class="form-group">
                                        <input type="text"
                                               name="CmsPages[tabs][<?php echo $tabs_row; ?>][title]"
                                               placeholder="Title" value="<?= $tab->title; ?>"
                                               class="form-control"/>
                                    </div>
                                </td>

                                <td class="">
                                    <div class="form-group">
                                        <textarea
                                                name="CmsPages[tabs][<?php echo $tabs_row; ?>][description]"
                                                placeholder="Description"
                                                class="form-control editor-box"><?= $tab->description; ?></textarea>
                                    </div>
                                </td>

                                <td class=""style="width: 10%;">
                                    <div class="form-group" >
                                        <?= $form->field($tab, 'status')->dropDownList(['active' => 'Active', 'In-Active' => 'In-Active'], ['prompt' => 'Select Status','name'=>'CmsPages[tabs]['.$tabs_row.'][status]'] ) ?>
                                    </div>
                                </td>



                                <td class="text-left">
                                    <button type="button"
                                            onclick="$('#feat-row<?php echo $tabs_row; ?>, .tooltip').remove();"
                                            data-toggle="tooltip" title="Remove" class="btn btn-danger"><i
                                                class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>
                            <?php $tabs_row++; ?>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-left">
                                <button type="button" onclick="tabs();" data-toggle="tooltip" title="Add"
                                        class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>

    //Start of Feature as tour_features.
    var tab_row = <?= $tabs_row ?>;

    function tabs() {
        html = '<tr id="feat-row' + tab_row + '">';

        html += '  <td class="">';
        html += '    <div class="form-group">';
        html += '      <input type="text" name="CmsPages[tabs][' + tab_row + '][title]" value="" placeholder="Title" class="form-control" />';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="">';
        html += '    <div class="form-group">';
        html += '       <textarea id="' + initEditor(tab_row) + '" class="form-control editor-box" name="CmsPages[tabs][' + tab_row + '][description]" placeholder="Description" rows="6" cols="100" ></textarea> ';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class="">';
        html += '    <div class="form-group">';
        html += '       <select class="form-control" name="CmsPages[tabs][' + tab_row + '][status]">';
        <?php foreach ($status as $st){ ?>
        html += '       <option value="<?= $st;?>"><?= $st;?></option>';
        <?php } ?>
        html += '       </<select>';
        html += '    </div>';
        html += '  </td>';

        html += '  <td class=""><button type="button" onclick="$(\'#feat-row' + tab_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

        html += '</tr>';



        $('#tabs_table tbody').append(html);



        tab_row++;
    }
</script>