<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EventSubscriptions */

$this->title = 'Create Event Subscriptions';
$this->params['breadcrumbs'][] = ['label' => 'Event Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-subscriptions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
