<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Groups */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
