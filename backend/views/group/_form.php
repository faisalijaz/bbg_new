<?php

use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Groups */
/* @var $form yii\widgets\ActiveForm */
?>


<div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Name') ?>

    <?= $form->field($model, 'status')->dropDownList(['In-Active' => 'In-active', 'active' => 'Active'], ['prompt' => 'Select status']) ?>

    <?= $form->field($model, 'joining_fee')->textInput(['maxlength' => true])->label('One Time Joining Fee'); ?>

    <?= $form->field($model, 'group_type')->dropDownList([
        'business' => 'Business',
        'individual' => 'individial',
        'nonprofit' => 'Non-Profit'
    ],
        ['class' => 'form-control'])->label('Type'); ?>
    <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true, 'type' => 'number'])->label(); ?>
    <?= $form->field($model, 'show_company')->widget(CheckboxX::classname(), [
        'initInputType' => CheckboxX::INPUT_CHECKBOX,
        'options' => [
            'class' => 'HasDietryPrefrence',
        ],
        'pluginOptions' => [
            'theme' => 'krajee-flatblue',
            'enclosedLabel' => true,
            'threeState' => false,
        ]
    ])->label(false); ?>

    <?= $form->field($model, 'show_industry_only')->widget(CheckboxX::classname(), [
        'initInputType' => CheckboxX::INPUT_CHECKBOX,
        'options' => [
            'class' => 'HasDietryPrefrence',
        ],
        'pluginOptions' => [
            'theme' => 'krajee-flatblue',
            'enclosedLabel' => true,
            'threeState' => false,
        ]
    ])->label(false); ?>

    <div class="col-md-3"><h4>Nominee</h4><hr/>
        <?= $form->field($model, 'add_nominee')->widget(CheckboxX::classname(), [
            'initInputType' => CheckboxX::INPUT_CHECKBOX,
            'options' => [
                'class' => 'HasDietryPrefrence',
            ],
            'pluginOptions' => [
                'theme' => 'krajee-flatblue',
                'enclosedLabel' => true,
                'threeState' => false,
            ]
        ])->label(false); ?>
            <?= $form->field($model, 'fee_nominee')->textInput(['maxlength' => true])->label('Fee') ?>
    </div>

    <div class="col-md-3"><h4>Alternate</h4><hr/>
        <?= $form->field($model, 'add_alternate')->widget(CheckboxX::classname(), [
            'initInputType' => CheckboxX::INPUT_CHECKBOX,
            'options' => [
                'class' => 'HasDietryPrefrence',
            ],
            'pluginOptions' => [
                'theme' => 'krajee-flatblue',
                'enclosedLabel' => true,
                'threeState' => false,
            ]
        ])->label(false); ?>
            <?= $form->field($model, 'fee_alternate')->textInput(['maxlength' => true])->label('Fee') ?>
    </div>

    <div class="col-md-3"><h4>Additional Members</h4><hr/>
        <?= $form->field($model, 'add_additional')->widget(CheckboxX::classname(), [
            'initInputType' => CheckboxX::INPUT_CHECKBOX,
            'options' => [
                'class' => 'HasDietryPrefrence',
            ],
            'pluginOptions' => [
                'theme' => 'krajee-flatblue',
                'enclosedLabel' => true,
                'threeState' => false,
            ]
        ])->label(false); ?>
            <?= $form->field($model, 'fee_additional')->textInput(['maxlength' => true])->label('Fee') ?>
    </div>

    <div class="col-md-3"><h4>Named Associates</h4><hr/>
        <?= $form->field($model, 'add_associate')->widget(CheckboxX::classname(), [
            'initInputType' => CheckboxX::INPUT_CHECKBOX,
            'options' => [
                'class' => 'HasDietryPrefrence',
            ],
            'pluginOptions' => [
                'theme' => 'krajee-flatblue',
                'enclosedLabel' => true,
                'threeState' => false,
            ]
        ])->label(false); ?>
        <?= $form->field($model, 'fee_associate')->textInput(['maxlength' => true])->label('Fee') ?>
    </div>


    <?= $form->field($model, 'description')->textarea([
        'rows' => 6, 'id' => 'elm1'
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>

