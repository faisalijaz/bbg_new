<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Membership Groups');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="experiences-index card-box">

    <p>
        <?= Html::a(Yii::t('app', 'Create Groups'), ['create'], ['class' => 'btn btn-sm btn-success']) ?>
    </p>

    <div class="row">
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'condensed' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'joining_fee',
            [
                'label' => 'Nominee',
                'value' => function ($model) {
                    return ($model->add_nominee) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Fee',
                'value' => function ($model) {
                    return $model->fee_nominee;
                }
            ],
            [
                'label' => 'Alternate',
                'value' => function ($model) {
                    return ($model->add_alternate) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Fee',
                'value' => function ($model) {
                    return $model->fee_alternate;
                }
            ],
            [
                'label' => 'Additional',
                'value' => function ($model) {
                    return ($model->add_additional) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Fee',
                'value' => function ($model) {
                    return $model->fee_additional;
                }
            ],
            [
                'label' => 'Named Associate',
                'value' => function ($model) {
                    return ($model->add_associate) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Fee',
                'value' => function ($model) {
                    return $model->fee_associate;
                }
            ],
            [
                'label' => 'Show Company',
                'value' => function ($model) {
                    return ($model->show_company) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Industry Only',
                'value' => function ($model) {
                    return ($model->show_industry_only) ? 'Yes' : 'No';
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->status;
                },
                'filter' => ['In-Active' => 'In-Active', 'active' => 'Active'],
                'filterInputOptions' => ['prompt' => 'Select...', 'class' => 'form-control', 'id' => null]],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>

</div>
