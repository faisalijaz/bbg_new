<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Groups */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membership Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-view card-box">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'joining_fee',
            [
                'label' => 'Nominee',
                'value' => function ($model) {
                    return ($model->add_nominee) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Nominee Fee',
                'value' => function ($model) {
                    return $model->fee_nominee;
                }
            ],
            [
                'label' => 'Alternate',
                'value' => function ($model) {
                    return ($model->add_alternate) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Alternate Fee',
                'value' => function ($model) {
                    return $model->fee_alternate;
                }
            ],
            [
                'label' => 'Additional',
                'value' => function ($model) {
                    return ($model->add_additional) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Additional Fee',
                'value' => function ($model) {
                    return $model->fee_additional;
                }
            ],
            [
                'label' => 'Named Associate',
                'value' => function ($model) {
                    return ($model->add_associate) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Named Associate Fee',
                'value' => function ($model) {
                    return $model->fee_associate;
                }
            ],
            [
                'label' => 'Show Company Details',
                'value' => function ($model) {
                    return ($model->show_company) ? 'Yes' : 'No';
                }
            ],
            [
                'label' => 'Show Industry Details Only',
                'value' => function ($model) {
                    return ($model->show_industry_only) ? 'Yes' : 'No';
                }
            ],
            'created_at:date',
            'updated_at:date',
            'status'
        ],
    ]) ?>

</div>
