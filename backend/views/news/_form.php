<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\NewsCategories;
use kartik\checkbox\CheckboxX;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
// File uploading in text editor


$lastmonth = date("Y-m-01", strtotime(date('Y-m-d')));
$created = date('Y-m-d',$model->created_at);

?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <div id="images" class="form-group">
                <label class="control-label" for="input-image">Main Image</label>
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    <img src="<?= ($model->image != "" && $model->image <> null) ? $model->image : Yii::$app->params['no_image']; ?>"
                         alt="" width="125" height="125" title=""
                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                </a>
                <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2" style="padding-top: 25px;">

            <?php
            if($created <= $lastmonth || !$model->isNewRecord) {

                echo $form->field($model, 're_post')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'class' => 're_post',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false);

            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'category')->dropDownList(ArrayHelper::map(NewsCategories::find()->Where(['status' => '1'])->all(),'id','title'), ['prompt' => 'Select Category']) ?>
            <?= $form->field($model, 'news_type')->hiddenInput(['value'=> 'member'])->label(false); ?>
        </div>
    </div>

    <div class="row">

        <div class="col-md-4">
            <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true, 'type' => 'number']) ?>
        </div>


        <div class="col-md-4">
            <?= $form->field($model, 'isPublished')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'Select Status'])->label('Publish') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'homepage_show')->dropDownList(['0' => 'No', '1' => 'Yes'], ['prompt' => 'Select Status'])->label('Show on Homepage') ?>
        </div>

    </div>

    <div class="row">

        <div class="col-md-6">
            <?= $form->field($model, 'related_to')->dropDownList([
                'general' => 'General',
                'company' => 'Company',
                'member' => 'Member'
            ],['maxlength' => true, 'id' => 'news_Related_to', 'prompt' => 'Select...'])->label('Related Type') ?>
        </div>

        <input type="hidden" value="<?= $model->rel_id; ?>" id="selected_rel">
        <div class="col-md-6">
            <?= $form->field($model, 'rel_id')->widget(DepDrop::classname(), [
                'options' => ['id'=>'rel_id'],
                'type'=>DepDrop::TYPE_SELECT2,
                'pluginOptions'=>[
                    'depends'=>['news_Related_to'],
                     'initialize'=>true,
                    'placeholder' => 'Select...',
                    'url' => Url::to(['/news/related-to']),
                    'params' => ['selected_rel']
                ]
            ])->label('Related To'); ?>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
        </div>


        <div class="col-md-12">

            <?= $form->field($model, 'description')->textarea(['class'=> 'tinyMCe','rows' => 6, 'id' => 'elm1']) ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>