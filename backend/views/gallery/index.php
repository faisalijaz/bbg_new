<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <p class="pull-right">
        <?= Html::a('Create Gallery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'icon',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<img src="'. $model->image .'" width="150">';
                }
            ],
            'size',
            'position',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->status) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">In-Active</span>';
                }
            ],// 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
