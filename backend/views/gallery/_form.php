<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
        <?= $form->field($model, 'name')->textInput() ?>
    </div><!---->

    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
        <div id="images" class="form-group">
            <label class="control-label" for="input-image">Image</label>
            <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                <img src="<?= $model->image ?>" alt="" width="125" height="125" title=""
                     data-placeholder="no_image.png"/>
            </a>
            <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
        </div>
    </div>
    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
        <?= $form->field($model, 'video_link')->textInput() ?>
    </div><!---->

    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
        <?= $form->field($model, 'size')->dropDownList(['small' => 'Small', 'large' => 'Large',], ['prompt' => 'Select Size']) ?>
    </div>

    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
        <?= $form->field($model, 'status')->dropDownList(['active' => 'Active', 'In-Active' => 'In-Active',], ['prompt' => 'Select Status']) ?>
    </div>

    <div class="col-md-12">

        <?= $form->field($model, 'description')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'basic',

        ]);
        ?>
    </div>

    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
