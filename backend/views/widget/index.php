<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\WidgetsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Widgets');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="widgets-index card-box">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
        <div class="col-sm-12">
            <p class="pull-right">
                <?= Html::a(Yii::t('app', 'Create Widgets'), ['create'], ['class' => 'btn btn-inverse btn-custom btn-rounded waves-effect waves-light']) ?>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">

            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
               // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'name',
                    'type',
                    'code',
                    'status',

                    ['class' => 'common\helpers\CustomActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>

        </div>
    </div>

</div>
