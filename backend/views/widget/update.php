<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Widgets */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Widgets',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Widgets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="widgets-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
