<?php

use yii\widgets\ActiveForm;
use common\models\Widgets;

/* @var $this yii\web\View */
/* @var $model app\models\Widgets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="widgets-form">

    <?php $form = ActiveForm::begin(['id' => 'dashboard-widgets']); ?>

    <div class="row">

        <div class="col-sm-12">
            <div class="form-group">
                <?php foreach(Widgets::find()->where(['status' => 1,'type'=>'backend'])->all() as $key => $widget) { ?>

                    <div class="checkbox checkbox-pink">

                        <input
                                id="checkbox<?= $key ?>"
                                name="UserWidget[widgets][]"
                                value="<?= $widget->id ?>"
                                <?= $widget->currentUserWidgets ? 'checked' : ''  ?>
                                type="checkbox">
                        <label for="checkbox<?= $key ?>"> <?= $widget->name ?> </label>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
