<?php
if (isset($sales['agetnsExpiredLimit']) && count($sales['agetnsExpiredLimit']) > 0) {
    ?>
    <div class="widget-panel widget-style-2 bg-white" style="border: #ddd 1px solid;">
        <i class="md md-account-child text-custom"></i>
        <h2 class="m-0 text-dark counter font-600"><?= count($sales['agetnsExpiredLimit']->keys); ?></h2>
        <div class="text-muted m-t-5"><?= $data['title'] ?></div>
    </div>
    <?php
} else {
    ?>
    <div class="widget-panel widget-style-2 bg-white" style="border: #ddd 1px solid;">
        <i class="md md-account-child text-custom"></i>
        <h2 class="m-0 text-dark counter font-600"><?= $data['data']; ?></h2>
        <div class="text-muted m-t-5"><?= $data['title'] ?></div>
    </div>
    <?php
}
?>

