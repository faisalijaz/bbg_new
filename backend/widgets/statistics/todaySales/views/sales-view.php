<div class="mini-stat clearfix card-box">
    <span class="mini-stat-icon bg-info"><i class="ion-social-usd text-white"></i></span>
    <div class="mini-stat-info text-right text-dark">
        <span class="counter text-dark"><?= $data['data']; ?></span>
        <?= $data['title'] ?>
    </div>
    <div class="tiles-progress">
        <div class="m-t-20">
            <h5 class="text-uppercase"> <span class="pull-right"></span></h5>
            <div class="progress progress-sm m-0">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                     aria-valuemax="100" style="width: 100%">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
    </div>
</div>