<?php

use common\widgets\Alert;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TourBookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'BBG Dubai');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Alert::widget() ?>
<div class="card-box dashboard_latest_bookings">
    <h4 class="m-t-0 header-title"><b>Memberships Registrations</b></h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $data['data'],
        'condensed' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Name',
                'format' => 'raw',
                'value' => function ($model) {
                    $html = $model->first_name . " " . $model->last_name . '<br/>';
                    $html .= Html::a($model->email, null, ['href' => $model->email]) . '<br/>';
                    $html .= $model->phone_number;
                    return $html;
                }
            ],
            [
                'attribute' => 'Type',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->account_type == 'named_associate') ? ucwords('Named Associate') : $model->account_type;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [

                    'view' => function ($url) {
                        return Html::a(
                            '<span class="label label-success"><span class="fa fa-eye"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            '<span class="label label-primary"><span class="fa fa-pencil"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                    'delete' => function ($url) {
                        return Html::a(
                            '<span class="label label-danger"><span class="fa fa-trash"></span></span>',
                            $url,
                            [
                                'title' => 'Register Member',
                                'data-pjax' => '0',
                            ]
                        );
                    },
                ]],
        ],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'toolbar' => [
            '{export}',
            '{toggleData}'
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => false,
        ],
    ]);
    ?>
</div>

