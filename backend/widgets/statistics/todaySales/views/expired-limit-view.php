<?php

use common\widgets\Alert;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TourBookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'BBG Dubai');
$this->params['breadcrumbs'][] = $this->title;


?>

<?= Alert::widget() ?>


<div class="card-box dashboard_latest_bookings">
    <h4 class="m-t-0 header-title"><b>Memberships Expiring Soon</b></h4>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $sales['agetnsExpiredLimitList'],
        'columns' => [['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Customer',
                'format' => 'raw',
                'value' => function ($model) {
                    $str = '';
                    if ($model  <> null) {
                        $account = $model;
                        $str .= $account->first_name . ' ' . $account->last_name . '<br/>';
                        $str .= $account->phone_number . '<br/>';
                        $str .= $account->email;
                    }
                    return $str;
                }
            ],
            [
                'attribute' => 'Expiry Date',
                'value' => function ($model) {
                    return  Yii::$app->formatter->asDate($model->expiry_date, 'yyyy-MM-dd'); // 2014-10-06;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{reset}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['/account/view', 'id' => $model->id], [
                            'class' => ''
                        ]);
                    },
                ]
            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>

