<?php

namespace backend\widgets\Statistics\todaySales;

use yii\web\AssetBundle;

/**
 * Class TodaySalesWidget
 * @package backend\widgets\statistics\todaySales
 */
class TodaySalesWidgetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [

    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

    /**
     * init function
     * @return void
     */
    public function init()
    {
        // Tell AssetBundle where the assets files are
        $this->sourcePath = __DIR__ . '/assets';

        parent::init();
    }
}
