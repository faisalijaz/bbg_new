<div class="row">
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-info">
                            <i class="icon-layers"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b>Customer Today</b></h4>
                        <h5 class="text-muted m-b-0 m-t-0"><?= $statistics['customersToday']; ?></h5>
                    </div>
                    <div class="table-detail text-right">
                        <span data-plugin="peity-bar" data-colors="#34d3eb,#ebeff2" data-width="120" data-height="45">5,3,9,6,5,9,7,3,5,2,9,7,2,1</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-custom">
                            <i class="icon-trophy"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b>Total Customers</b></h4>
                        <h5 class="text-muted m-b-0 m-t-0"><?= $statistics['allCustomer']; ?></h5>
                    </div>
                    <div class="table-detail text-right">
                        <span data-plugin="peity-pie" data-colors="#5fbeaa,#ebeff2" data-width="50"
                              data-height="45"><?= $statistics['allCustomer'] ;?>/<?= $statistics['allAccounts'];?></span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-danger">
                            <i class="icon-close"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b>Total Agents</b></h4>
                        <h5 class="text-muted m-b-0 m-t-0"><?= $statistics['allAgents'] ; ?></h5>
                    </div>
                    <div class="table-detail text-right">
                        <span data-plugin="peity-donut" data-colors="#f05050,#ebeff2" data-width="50" data-height="45"><?= $statistics['allAgents'] ;?>/<?= $statistics['allAccounts'];?></span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-info">
                            <i class="icon-layers"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b>Total sale till now</b></h4>
                        <h5 class="text-muted m-b-0 m-t-0"><?= $statistics['totalSales']; ?></h5>
                    </div>
                    <div class="table-detail text-right">
                        <span data-plugin="peity-bar" data-colors="#34d3eb,#ebeff2" data-width="120" data-height="45">5,3,9,6,5,9,7,3,5,2,9,7,2,1</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-custom">
                            <i class="icon-trophy"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b>Today Sales</b></h4>
                        <h5 class="text-muted m-b-0 m-t-0"><?= $statistics['todaySales']; ?></h5>
                    </div>
                    <div class="table-detail text-right">
                        <span data-plugin="peity-pie" data-colors="#5fbeaa,#ebeff2" data-width="50"
                              data-height="45">1/5</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-danger">
                            <i class="icon-close"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b>Total sales from Customers</b></h4>
                        <h5 class="text-muted m-b-0 m-t-0"><?= $statistics['totalSalesCustomer']; ?></h5>
                    </div>
                    <div class="table-detail text-right">
                        <span data-plugin="peity-donut" data-colors="#f05050,#ebeff2" data-width="50" data-height="45">1/5</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card-box">
            <div class="bar-widget">
                <div class="table-box">
                    <div class="table-detail">
                        <div class="iconbox bg-danger">
                            <i class="icon-close"></i>
                        </div>
                    </div>

                    <div class="table-detail">
                        <h4 class="m-t-0 m-b-5"><b>Total sales from Agents</b></h4>
                        <h5 class="text-muted m-b-0 m-t-0"><?= $statistics['totalSalesAgent']; ?></h5>
                    </div>
                    <div class="table-detail text-right">
                        <span data-plugin="peity-donut" data-colors="#f05050,#ebeff2" data-width="50" data-height="45">1/5</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0">Deals Analytics</h4>
            <div class="text-center">
                <ul class="list-inline chart-detail-list">
                    <li>
                        <h5><i class="fa fa-circle m-r-5" style="color: #81c868;"></i>Agents Deals</h5>
                    </li>
                    <li>
                        <h5><i class="fa fa-circle m-r-5" style="color: #ffbd4a;"></i>Customers Deals</h5>
                    </li>
                </ul>
            </div>
            <div id="morris-line-chart" style="height: 300px;"></div>
        </div>
        <!-- /Portlet -->
    </div>
</div>
<!-- end row -->


<div class="row">

    <div class="col-lg-6">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0 m-b-30">Average time for deal</h4>
            <div>
                <div id="morris-bar-chart" style="height: 320px;"></div>
            </div>
        </div>
    </div>
    <!-- col -->

    <div class="col-lg-6">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0 m-b-30">Sales by product group</h4>
            <div class="text-center">
                <ul class="list-inline chart-detail-list">
                    <li>
                        <h5><i class="fa fa-circle m-r-5" style="color: #ebeff2;"></i>Group 1</h5>
                    </li>
                    <li>
                        <h5><i class="fa fa-circle m-r-5" style="color: #5fbeaa;"></i>Group 2</h5>
                    </li>
                    <li>
                        <h5><i class="fa fa-circle m-r-5" style="color: #5d9cec;"></i>Group 3</h5>
                    </li>
                </ul>
            </div>
            <div id="donut-chart">
                <div id="morris-donut-example" style="height: 274px;">
                </div>
            </div>
        </div>
    </div>
    <!-- col -->


</div>
<!-- end row -->


<div class="row">
    <div class="col-lg-7">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0">Contacts</h4>
            <p class="text-muted m-b-30 font-13">
                Your awesome text goes here.
            </p>

            <div class="nicescroll" style="height: 300px;">
                <div class="table-responsive">
                    <table class="table table-actions-bar">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>John deo</td>
                            <td>Category one</td>
                            <td>johndeo@dummy.com</td>
                            <td><span class="label label-success">Delivered</span></td>
                            <td>
                                <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>John deo</td>
                            <td>Category one</td>
                            <td>johndeo@dummy.com</td>
                            <td><span class="label label-success">Delivered</span></td>
                            <td>
                                <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>John deo</td>
                            <td>Category one</td>
                            <td>johndeo@dummy.com</td>
                            <td><span class="label label-success">Delivered</span></td>
                            <td>
                                <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>John deo</td>
                            <td>Category one</td>
                            <td>johndeo@dummy.com</td>
                            <td><span class="label label-success">Delivered</span></td>
                            <td>
                                <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>John deo</td>
                            <td>Category one</td>
                            <td>johndeo@dummy.com</td>
                            <td><span class="label label-success">Delivered</span></td>
                            <td>
                                <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                            </td>
                        </tr>

                        <tr>
                            <td>John deo</td>
                            <td>Category one</td>
                            <td>johndeo@dummy.com</td>
                            <td><span class="label label-success">Delivered</span></td>
                            <td>
                                <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                            </td>
                        </tr>

                        <tr>
                            <td>John deo</td>
                            <td>Category one</td>
                            <td>johndeo@dummy.com</td>
                            <td><span class="label label-success">Delivered</span></td>
                            <td>
                                <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->

    <div class="col-lg-5">
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0">Activities</h4>
            <p class="text-muted font-13">
                Your awesome text goes here.
            </p>

            <div class="nicescroll p-20" style="height: 320px;">
                <div class="timeline-2">
                    <div class="time-item">
                        <div class="item-info">
                            <div class="text-muted">
                                <small>5 minutes ago</small>
                            </div>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong> Uploaded a photo <strong>"DSC000586.jpg"</strong>
                            </p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <div class="text-muted">
                                <small>30 minutes ago</small>
                            </div>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut
                                    tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <div class="text-muted">
                                <small>59 minutes ago</small>
                            </div>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#"
                                                                                                class="text-success">John
                                    Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut
                                    tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <div class="text-muted">
                                <small>5 minutes ago</small>
                            </div>
                            <p><strong><a href="#" class="text-info">John Doe</a></strong>Uploaded 2 new photos</p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <div class="text-muted">
                                <small>30 minutes ago</small>
                            </div>
                            <p><a href="" class="text-info">Lorem</a> commented your post.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut
                                    tincidunt euismod. "</em></p>
                        </div>
                    </div>

                    <div class="time-item">
                        <div class="item-info">
                            <div class="text-muted">
                                <small>59 minutes ago</small>
                            </div>
                            <p><a href="" class="text-info">Jessi</a> attended a meeting with<a href="#"
                                                                                                class="text-success">John
                                    Doe</a>.</p>
                            <p><em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut
                                    tincidunt euismod. "</em></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- end col -->
</div>


