<?php

namespace backend\widgets\statistics;

use common\models\Accounts;
use common\models\TourBookings;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class StatisticsWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'stats';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Dashboard Statistics';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        StatisticsWidgetAsset::register($this->getView());

        $accounts = new Accounts();
        $bookings = new TourBookings();

        $todayCustomer = 0; // $accounts->findTodaysCustomer();
        $allCustomer = 0;   // $accounts->findAllAccounts('customer');
        $allAgents = 0; // $accounts->findAllAccounts('seller');
        $totalSales = 0;    // $bookings->totalSales();
        $todaySales = 0;    // $bookings->todaySales();
        $totalSalesCustomer = 0;    // $bookings->totalSalesByAccount('agent');
        $totalSalesAgent = 0;   // $bookings->totalSalesByAccount('customer');
        $accounts = $allAgents + $allCustomer;

        $stats = [
            'customersToday' => $todayCustomer,
            'allCustomer' => $allCustomer,
            'allAgents' => $allAgents,
            'totalSales' => $totalSales,
            'todaySales' => $todaySales,
            'totalSalesCustomer' => $totalSalesCustomer,
            'totalSalesAgent' => $totalSalesAgent,
            'allAccounts' => $accounts
        ];

        return $this->render($this->view, ['statistics' => $stats]);
    }
}
