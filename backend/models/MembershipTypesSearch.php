<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MembershipTypes;

/**
 * MembershipTypesSearch represents the model behind the search form about `common\models\MembershipTypes`.
 */
class MembershipTypesSearch extends MembershipTypes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mt_id'], 'integer'],
            [['mt_name', 'mt_description'], 'safe'],
            [['mt_fee', 'mt_joining_fee'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MembershipTypes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mt_id' => $this->mt_id,
            'mt_fee' => $this->mt_fee,
            'mt_joining_fee' => $this->mt_joining_fee,
        ]);

        $query->andFilterWhere(['like', 'mt_name', $this->mt_name])
            ->andFilterWhere(['like', 'mt_description', $this->mt_description]);

        return $dataProvider;
    }
}
