<?php

namespace backend\models;

use common\models\Invoices;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * InvoicesSearch represents the model behind the search form about `common\models\Invoices`.
 */
class InvoicesSearch extends Invoices
{
    public $first_name;
    public $last_name;
    public $company;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'user_id', 'invoice_rel_id', 'payment_id'], 'integer'],
            [['first_name', 'last_name', 'invoice_related_to', 'invoice_category', 'payment_status', 'invoice_date', 'invoice_sent','company'], 'safe'],
            [['subtotal', 'tax', 'total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoices::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'invoice_id' => SORT_DESC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invoice_id' => $this->invoice_id,
            'user_id' => $this->user_id,
            'invoice_rel_id' => $this->invoice_rel_id,
            'subtotal' => $this->subtotal,
            'tax' => $this->tax,
            'total' => $this->total,
            'invoice_date' => $this->invoice_date,
            'payment_id' => $this->payment_id,
        ]);

        $query->andFilterWhere(['like', 'invoice_related_to', $this->invoice_related_to])
            ->andFilterWhere(['like', 'invoice_category', $this->invoice_category])
            ->andFilterWhere(['like', 'invoice_sent', $this->invoice_sent]);

        if($this->invoice_related_to == "event") {

            $query->join('LEFT JOIN', 'event_subscriptions subscription1', ' invoices.invoice_id = subscription1.gen_invoice');

            if ($this->first_name) {
                $query->andFilterWhere(['like', 'subscription1.firstname', $this->first_name]);
            }

            if ($this->last_name) {
                $query->andFilterWhere(['like', 'subscription1.lastname', $this->last_name]);
            }
        }

        if($this->company){

            $query->join('LEFT JOIN', 'event_subscriptions subscription', ' invoices.invoice_id = subscription.gen_invoice');
            $query->andFilterWhere(['like', 'subscription.company', $this->company]);
            $query->andFilterWhere(['like', 'invoices.payment_status', $this->payment_status]);

        }else{
            $query->andFilterWhere(['=', 'payment_status', $this->payment_status]);
        }

        return $dataProvider;
    }
}
