<?php

namespace backend\models;

use common\models\Members;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AccountSearch represents the model behind the search form about `common\models\Accounts`.
 */
class MemberSearch extends Members
{

    public $alphaSearch;
    public $searchExpert;
    public $find_exp_membership = 0;
    public $from_date;
    public $to_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'occasional_updates', 'created_at', 'updated_at','company'], 'integer'],
            [['purposeOfJoining','tellUsAboutYourself','howDidYouHear','approved', 'user_type_relation','account_type','expiry_date', 'registeration_date', 'email', 'address', 'group_id', 'searchExpert', 'alphaSearch', 'account_type', 'first_name', 'last_name', 'gender', 'country', 'status', 'findExpiredLimit', 'find_exp_membership'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Members::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $role = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());

        if (!isset($role['admin'])) {
            // $this->parent_id = \Yii::$app->user->getId();
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'occasional_updates' => $this->occasional_updates,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'account_type' => $this->account_type,
            'group_id' => $this->group_id,
        ]);


        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['=', 'company', $this->company])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'approved', $this->approved])
            ->andFilterWhere(['like', 'expiry_date', $this->expiry_date])
            ->andFilterWhere(['like', 'registeration_date', $this->registeration_date])
            ->andFilterWhere(['like', 'user_type_relation', $this->user_type_relation]);

        if ($this->from_date || $this->to_date) {

            $query->andFilterWhere(['between', 'expiry_date', $this->from_date, $this->to_date]);

        } else {
            if ($this->find_exp_membership) {
                $date = date('Y-m-d', strtotime('+2 month'));
                $query->andFilterWhere(['between', 'expiry_date', date('Y-m-d'), $date]);
                $query->andFilterWhere(['invoiced' => '1']);
            }
        }

       /*
            echo $query->createCommand()->getRawSql();
            die;
       */

        return $dataProvider;
    }
}
