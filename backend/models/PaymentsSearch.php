<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Payments;

/**
 * PaymentsSearch represents the model behind the search form of `\common\models\Payments`.
 */
class PaymentsSearch extends Payments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'invoice_id', 'transaction_id', 'received_by'], 'integer'],
            [['amount', 'amount_payable'], 'number'],
            [['response_code', 'detail', 'payment_date', 'status', 'payment_method', 'shipping_address', 'shipping_city', 'shipping_country', 'shipping_state', 'shipping_postalcode', 'currency', 'phone_num', 'customer_name', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'invoice_id' => $this->invoice_id,
            'amount' => $this->amount,
            'amount_payable' => $this->amount_payable,
            'transaction_id' => $this->transaction_id,
            'payment_date' => $this->payment_date,
            'received_by' => $this->received_by,
        ]);

        $query->andFilterWhere(['like', 'response_code', $this->response_code])
            ->andFilterWhere(['like', 'detail', $this->detail])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'payment_method', $this->payment_method])
            ->andFilterWhere(['like', 'shipping_address', $this->shipping_address])
            ->andFilterWhere(['like', 'shipping_city', $this->shipping_city])
            ->andFilterWhere(['like', 'shipping_country', $this->shipping_country])
            ->andFilterWhere(['like', 'shipping_state', $this->shipping_state])
            ->andFilterWhere(['like', 'shipping_postalcode', $this->shipping_postalcode])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'phone_num', $this->phone_num])
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
