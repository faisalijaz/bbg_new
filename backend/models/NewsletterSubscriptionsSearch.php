<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NewsletterSubscriptions;

/**
 * NewsletterSubscriptionsSearch represents the model behind the search form of `\common\models\NewsletterSubscriptions`.
 */
class NewsletterSubscriptionsSearch extends NewsletterSubscriptions
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['active','events_news','first_name','last_name','weekly_newsletter','special_offers','membership_subscriptions'], 'string'],
            [['email', 'date_created', 'active'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterSubscriptions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'active', $this->active])
            ->andFilterWhere(['like', 'events_news', $this->events_news])
            ->andFilterWhere(['like', 'weekly_newsletter', $this->weekly_newsletter])
            ->andFilterWhere(['like', 'special_offers', $this->special_offers])
            ->andFilterWhere(['like', 'membership_subscriptions', $this->membership_subscriptions]);

        return $dataProvider;
    }
}
