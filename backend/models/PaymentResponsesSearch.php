<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaymentResponses;

/**
 * PaymentResponsesSearch represents the model behind the search form of `app\models\PaymentResponses`.
 */
class PaymentResponsesSearch extends PaymentResponses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'payment_id','invoice_id'], 'integer'],
            [['payment_reference', 'response_code', 'response_text'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentResponses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'payment_id' => $this->payment_id,
            'invoice_id' => $this->payment_id,
        ]);

        $query->andFilterWhere(['like', 'payment_reference', $this->payment_reference])
            ->andFilterWhere(['like', 'response_code', $this->response_code])
            ->andFilterWhere(['like', 'response_text', $this->response_text]);

        return $dataProvider;
    }
}
