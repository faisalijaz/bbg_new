<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Events;

/**
 * EventsSearch represents the model behind the search form about `\common\models\Events`.
 */
class EventsSearch extends Events
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'create_by'], 'integer'],
            [['type', 'country_id', 'city_id', 'address', 'title', 'slug', 'search_title', 'provience_state', 'venue', 'postal_code', 'tba', 'map_cordinate', 'registration_start', 'registration_end', 'cancellation_tillDate', 'event_startDate', 'event_endDate', 'short_description', 'group_size', 'fb_gallery', 'condition', 'description', 'member_fee', 'nonmember_fee', 'committee_fee', 'honourary_fee', 'sponsor_fee', 'focus_chair_fee', 'currency', 'display_price', 'sticky', 'userNote', 'status', 'quizNight', 'active', 'expiry_day', 'create_date', 'modify_date', 'longitude', 'latitude'], 'safe'],
        ];
    }

    public $past;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Events::find();

        if ($this->past) {

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'event_startDate' => SORT_DESC,
                    ]
                ],
            ]);

            $query->andFilterWhere(['<=', 'event_startDate', \Yii::$app->formatter->asDate('now', 'php:Y-m-d')]);

        } else {

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'event_startDate' => SORT_ASC,
                    ]
                ],
            ]);

            $query->andFilterWhere(['>=', 'event_startDate', \Yii::$app->formatter->asDate('now', 'php:Y-m-d')]);

        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'create_date' => $this->create_date,
            'modify_date' => $this->modify_date,
            'create_by' => $this->create_by,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'country_id', $this->country_id])
            ->andFilterWhere(['like', 'city_id', $this->city_id])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'venue', $this->venue])
            ->andFilterWhere(['like', 'tba', $this->tba])
            ->andFilterWhere(['like', 'registration_start', $this->registration_start])
            ->andFilterWhere(['like', 'registration_end', $this->registration_end])
            ->andFilterWhere(['like', 'cancellation_tillDate', $this->cancellation_tillDate])
            ->andFilterWhere(['like', 'event_startDate', $this->event_startDate])
            ->andFilterWhere(['like', 'event_endDate', $this->event_endDate])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'group_size', $this->group_size])
            ->andFilterWhere(['like', 'fb_gallery', $this->fb_gallery])
            ->andFilterWhere(['like', 'condition', $this->condition])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'member_fee', $this->member_fee])
            ->andFilterWhere(['like', 'nonmember_fee', $this->nonmember_fee])
            ->andFilterWhere(['like', 'committee_fee', $this->committee_fee])
            ->andFilterWhere(['like', 'honourary_fee', $this->honourary_fee])
            ->andFilterWhere(['like', 'sponsor_fee', $this->sponsor_fee])
            ->andFilterWhere(['like', 'focus_chair_fee', $this->focus_chair_fee])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'display_price', $this->display_price])
            ->andFilterWhere(['like', 'sticky', $this->sticky])
            ->andFilterWhere(['like', 'userNote', $this->userNote])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'quizNight', $this->quizNight])
            ->andFilterWhere(['like', 'active', $this->active])
            ->andFilterWhere(['like', 'expiry_day', $this->expiry_day])
            ->andFilterWhere(['like', 'longitude', $this->longitude])
            ->andFilterWhere(['like', 'latitude', $this->latitude]);


        return $dataProvider;
    }
}
