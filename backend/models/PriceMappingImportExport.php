<?php

namespace backend\models;

use Yii;
use yii\base\Model;

use yii2tech\csvgrid\CsvGrid;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use common\models\TourPriceMapping;

/**
 * This is the model class for table "accounts".
 *
 * @property integer $id
 *
 */
class PriceMappingImportExport extends Model
{

    public $type;
    public $importFile;


    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['type', 'importFile'], 'safe'],
            [['importFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'csv'],
        ];
    }


    public function upload()
    {
        if ($this->validate()) {
            $this->importFile->saveAs('uploads/' . $this->importFile->baseName . '.' . $this->importFile->extension);
            return true;
        } else {
            return false;
        }
    }

    public function importResultDownload($updatedRecords) {

        $exporter = new CsvGrid([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $updatedRecords
            ]),
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function($data) {
                        return $data[0];
                    }
                ],
                [
                    'attribute' => 'tour_id',
                    'value' => function($data) {
                        return $data[1];
                    }
                ],
                [
                    'attribute' => 'group_id',
                    'value' => function($data) {
                        return $data[2];
                    }
                ],
                [
                    'attribute' => 'experience_id',
                    'value' => function($data) {
                        return $data[3];
                    }
                ],
                [
                    'attribute' => 'start_date',
                    'value' => function($data) {
                        return $data[4];
                    }
                ],
                [
                    'attribute' => 'end_date',
                    'value' => function($data) {
                        return $data[5];
                    }
                ],
                [
                    'attribute' => 'rate',
                    'value' => function($data) {
                        return $data[6];
                    }
                ],
                [
                    'attribute' => 'start_time',
                    'value' => function($data) {
                        return $data[7];
                    }
                ],
                [
                    'attribute' => 'end_time',
                    'value' => function($data) {
                        return $data[8];
                    }
                ],
                [
                    'attribute' => 'cutoff_time',
                    'value' => function($data) {
                        return $data[9];
                    }
                ],
                [
                    'attribute' => 'percent_increase',
                    'value' => function($data) {
                        return $data[10];
                    }
                ],
                [
                    'attribute' => 'child_price',
                    'value' => function($data) {
                        return $data[11];
                    }
                ],
                [
                    'attribute' => 'status',
                    'value' => function($data) {
                        return $data[12];
                    }
                ],
            ]
        ]);
        return $exporter->export()->send(Yii::$app->dateTime->getTime() . '-price-mapping-Import-results.csv');

    }

    public function exportSampleData() {

        $exporter = new CsvGrid([
            'dataProvider' => new ActiveDataProvider([
                'query' => TourPriceMapping::find()
                    ->with(['onlyTour', 'group', 'experience'])
                    ->where(['>', 'tour_id', 0])
                    ->join('INNER JOIN', 'tours t', 't.id = tour_id')
                    ->limit(10),
                'pagination' => false,
            ]),
            'columns' => [
                'id',
                [
                    'attribute' => 'tour_id',
                    'value' => function ($model) {
                        return $model->onlyTour->title;
                    }
                ],
                [
                    'attribute' => 'group_id',
                    'value' => function ($model) {
                        return $model->group->title;
                    }
                ],
                [
                    'attribute' => 'experience_id',
                    'value' => function ($model) {
                        return $model->experience->title;
                    }
                ],
                'start_date',
                'end_date',
                'rate',
                'start_time',
                'end_time',
                'cutoff_time',
                'percent_increase',
                'child_price'
            ]
        ]);
        return $exporter->export()->send(Yii::$app->dateTime->getTime() . '-price-mapping-sample.csv');

    }
}
