<?php

namespace backend\models;

use common\models\MemberOffers;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MemberOffersSearch represents the model behind the search form about `\common\models\MemberOffers`.
 */
class MemberOffersSearch extends MemberOffers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'category_id', 'sort_order'], 'integer'],
            [['image', 'short_description', 'description', 'created_at', 'offer_rel', 'offer_rel_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MemberOffers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'title' => $this->title,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'description', $this->description]);

        if($this->offer_rel){
            $query->andWhere(['offer_rel' => $this->offer_rel]);
        }

        if($this->offer_rel_id){
            $query->andWhere(['offer_rel_id' => $this->offer_rel_id]);
        }

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function membersOffers($params)
    {
        $query = MemberOffers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->offer_rel) {
            $query->where(['offer_rel' => $this->offer_rel]);
        }

        if ($this->offer_rel_id) {
            $query->where(['offer_rel_id' => $this->offer_rel_id]);
        }

        return $dataProvider;
    }
}
