<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Newslettertemplates;

/**
 * NewslettertemplatesSearch represents the model behind the search form of `\common\models\Newslettertemplates`.
 */
class NewslettertemplatesSearch extends Newslettertemplates
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'onlySubscription'], 'integer'],
            [['title', 'description', 'content', 'category', 'testemail', 'image', 'active', 'createDate', 'subject'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newslettertemplates::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'onlySubscription' => $this->onlySubscription,
            'createDate' => $this->createDate,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'testemail', $this->testemail])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'active', $this->active])
            ->andFilterWhere(['like', 'subject', $this->subject]);

        return $dataProvider;
    }
}
