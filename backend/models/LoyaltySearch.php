<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Loyalty;

/**
 * LoyaltySearch represents the model behind the search form about `common\models\Loyalty`.
 */
class LoyaltySearch extends Loyalty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'points', 'account_id', 'booking_id', 'created_at', 'updated_at'], 'integer'],
            [['action', 'point_type','is_deleted'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loyalty::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'points' => $this->points,
            'account_id' => $this->account_id,
            'booking_id' => $this->booking_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_deleted' => $this->is_deleted,
        ])->groupBy('loyalties.account_id');

        $query->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'point_type', $this->point_type]);

        return $dataProvider;
    }
}
