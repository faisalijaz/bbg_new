<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\JobPosts;

/**
 * JobPostsSearch represents the model behind the search form of `\common\models\JobPosts`.
 */
class JobPostsSearch extends JobPosts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['user_type', 'job_title', 'position', 'industry', 'skills', 'experience', 'location', 'posted_on', 'apply_by', 'contact_name', 'contact_email', 'contact_phone', 'short_description', 'description', 'published', 'auto_remove_after'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JobPosts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'posted_on' => $this->posted_on,
            'auto_remove_after' => $this->auto_remove_after,
        ]);

        $query->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'job_title', $this->job_title])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'industry', $this->industry])
            ->andFilterWhere(['like', 'skills', $this->skills])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'apply_by', $this->apply_by])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'published', $this->published]);

        return $dataProvider;
    }
}
