<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AccountCompany;

/**
 * AccountCompanySearch represents the model behind the search form of `\common\models\AccountCompany`.
 */
class AccountCompanySearch extends AccountCompany
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category'], 'integer'],
            [['name', 'show_in_directory','url', 'phonenumber', 'fax', 'postal_code', 'emirates_number', 'address', 'about_company', 'logo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccountCompany::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'phonenumber', $this->phonenumber])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'emirates_number', $this->emirates_number])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'about_company', $this->about_company])
            ->andFilterWhere(['like', 'show_in_directory', $this->show_in_directory])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
}
