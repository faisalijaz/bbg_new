<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TblInvoice;

/**
 * TblInvoiceSearch represents the model behind the search form of `\common\models\TblInvoice`.
 */
class TblInvoiceSearch extends TblInvoice
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'parent_user_id'], 'integer'],
            [['user_invoice_id', 'invoice_category', 'payment_status', 'amount', 'pdf_name', 'invoice_date', 'created_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblInvoice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invoice_id' => $this->invoice_id,
            'parent_user_id' => $this->parent_user_id,
            'created_date' => $this->created_date,
        ]);

        $query->andFilterWhere(['like', 'user_invoice_id', $this->user_invoice_id])
            ->andFilterWhere(['like', 'invoice_category', $this->invoice_category])
            ->andFilterWhere(['like', 'payment_status', $this->payment_status])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'pdf_name', $this->pdf_name])
            ->andFilterWhere(['like', 'invoice_date', $this->invoice_date]);

        return $dataProvider;
    }
}
