<?php

namespace backend\models;

use common\models\EventSubscriptions;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EventSubscriptionsSearch represents the model behind the search form about `\common\models\EventSubscriptions`.
 */
class EventSubscriptionsSearch extends EventSubscriptions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'registered_by', 'event_id', 'created_at', 'updated_at'], 'integer'],
            [['user_type', 'diet_option_description', 'firstname', 'lastname', 'email', 'userNote', 'mobile', 'company', 'walkin', 'attended', 'payment_status', 'status', 'gen_invoice', 'focGuest', 'paymentMethod'], 'safe'],
            [['fee_paid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventSubscriptions::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'registered_by' => $this->registered_by,
            'event_id' => $this->event_id,
            'fee_paid' => $this->fee_paid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'userNote', $this->userNote])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'walkin', $this->walkin])
            ->andFilterWhere(['like', 'attended', $this->attended])
            ->andFilterWhere(['like', 'payment_status', $this->payment_status])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'gen_invoice', $this->gen_invoice])
            ->andFilterWhere(['like', 'focGuest', $this->focGuest])
            ->andFilterWhere(['like', 'paymentMethod', $this->paymentMethod])
            ->andFilterWhere(['like', 'diet_option_description', $this->diet_option_description]);

        return $dataProvider;
    }
}
