<?php

namespace backend\controllers;

use backend\models\MemberOffersSearch;
use backend\models\NewsSearch;
use common\models\News;
use Yii;
use common\models\AccountCompany;
use backend\models\AccountCompanySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccountCompanyController implements the CRUD actions for AccountCompany model.
 */
class AccountCompanyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AccountCompany models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AccountCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $exportModel = new AccountCompanySearch();
        $dataProviderfull = $exportModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderfull' => $dataProviderfull
        ]);
    }

    /**
     * Displays a single AccountCompany model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $newsModel = new NewsSearch();
        $newsModel->related_to = "company";
        $newsModel->rel_id = $id;
        $newsDataProvider = $newsModel->memberNewsSearch(Yii::$app->request->queryParams);

        $offerModel = new MemberOffersSearch();
        $offerModel->offer_rel = "company";
        $offerModel->offer_rel_id = $id;
        $offerModelDataProvider = $offerModel->membersOffers(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'memberNews' => $newsDataProvider,
            'memberOffers' => $offerModelDataProvider,
        ]);
    }

    /**
     * Creates a new AccountCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AccountCompany();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AccountCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AccountCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AccountCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AccountCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AccountCompany::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
