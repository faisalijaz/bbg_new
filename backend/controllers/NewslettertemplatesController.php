<?php

namespace backend\controllers;

use backend\models\NewslettertemplatesSearch;
use common\helpers\EmailHelper;
use common\models\Categories;
use common\models\Events;
use common\models\News;
use common\models\NewsletterSubscriptions;
use common\models\Newslettertemplates;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * NewslettertemplatesController implements the CRUD actions for Newslettertemplates model.
 */
class NewslettertemplatesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Newslettertemplates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewslettertemplatesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Newslettertemplates model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Newslettertemplates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Newslettertemplates();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }


        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Newslettertemplates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Newslettertemplates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Newslettertemplates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Newslettertemplates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Newslettertemplates::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return string
     */
    public function actionGetEvent()
    {
        if (Yii::$app->request->post()) {

            $model = Events::findOne(Yii::$app->request->post('id'));

            return $this->renderPartial('event_template', [
                'event' => $model,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionGetNews()
    {
        if (Yii::$app->request->post()) {

            $news = News::findOne(Yii::$app->request->post('id'));

            return $this->renderPartial('news_template', [
                'news' => $news,
            ]);
        }
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionSendTestEmail()
    {
        if (Yii::$app->request->post()) {

            $model = $this->findModel(Yii::$app->request->post('id'));
            $model->testemail = Yii::$app->request->post('email');

            $testEmail = (new EmailHelper())->sendNewsLetterEmail($model->testemail, [], $model->subject, $model->description);

            if($testEmail){
                return true;
            }

            return false;
        }
    }

    public function actionSendNewsletter($id)
    {

        $model = Newslettertemplates::findOne($id);
        $subscribers = NewsletterSubscriptions::find()->where(['active' => '1'])->count();
        $categories = Categories::find()->all();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->subscribers) {

                $subscriptions = NewsletterSubscriptions::find()->where(['active' => '1'])->all();
                foreach ($subscriptions as $subscriber) {
                    (new EmailHelper())->sendNewsLetterEmail($subscriber->email, [], $model->subject, $model->description);
                }

            }

            if ($model->categories) {

                foreach ($model->categories as $cat) {
                    if ($cat) {

                        $category = Categories::findOne($cat);

                        if ($category <> null && $category->companies <> null) {
                            foreach ($category->companies as $company) {
                                if ($company->companyMembers <> null) {
                                    foreach ($company->companyMembers as $member) {

                                        (new EmailHelper())->sendNewsLetterEmail($member->email, [], $model->subject, $model->description);

                                    }
                                }
                            }
                        }

                    }
                }
            }

            $this->redirect('/newslettertemplates');
        }


        return $this->renderPartial('send_newsletter', [
            'model' => $model,
            'subscribers' => $subscribers,
            'categories' => $categories
        ]);
    }
}
