<?php

namespace backend\controllers;

use backend\models\MemberOffersSearch;
use common\models\AccountCompany;
use common\models\MemberOffers;
use common\models\Members;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MemberOffersController implements the CRUD actions for MemberOffers model.
 */
class MemberOffersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MemberOffers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MemberOffersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MemberOffers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MemberOffers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MemberOffers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MemberOffers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MemberOffers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MemberOffers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MemberOffers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MemberOffers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     *
     */
    public function actionFindList()
    {
        $out = [];
        $cat_id = "";

        if (isset($_POST['depdrop_parents'])) {

            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {

                $cat_id = $parents[0];

                if ($cat_id == "member") {

                    $members = Members::find()->where(['status' => '1'])->all();

                    if ($members <> null) {
                        foreach ($members as $member) {
                            $name = $member->first_name . " " . $member->last_name;
                            $out[] = ['id' => $member->id, 'name' => $name];
                        }
                    }
                }

                if ($cat_id == "company") {
                    $companies = AccountCompany::find()->all();
                    if ($companies <> null) {
                        foreach ($companies as $company) {
                            $out[] = ['id' => $company->id, 'name' => $company->name];
                        }
                    }
                }

                return Json::encode(['output' => $out, 'selected' => '']);
            }
        }

        return Json::encode(['output' => $out, 'selected' => '']);
    }
}
