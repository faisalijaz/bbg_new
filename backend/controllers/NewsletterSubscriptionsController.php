<?php

namespace backend\controllers;

use backend\models\NewsletterSubscriptionsSearch;
use common\helpers\EmailHelper;
use common\models\NewsletterSubscriptions;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * NewsletterSubscriptionsController implements the CRUD actions for NewsletterSubscriptions model.
 */
class NewsletterSubscriptionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsletterSubscriptions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsletterSubscriptionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NewsletterSubscriptions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsletterSubscriptions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterSubscriptions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NewsletterSubscriptions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NewsletterSubscriptions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NewsletterSubscriptions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsletterSubscriptions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterSubscriptions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionEmailSubscribers($id)
    {

        $model = NewsletterSubscriptions::findOne($id);
        $subject = "Please push our GDPR compliance button";

        if ($model->member <> null) {
            \Yii::$app->session->setFlash('success', 'Email Sent!.');
            (new EmailHelper())->sendEmail($model->email, [], $subject, 'member_subscription_settings', ['model' => $model]);
        } else {
            \Yii::$app->session->setFlash('success', 'Email Sent!.');
            (new EmailHelper())->sendEmail($model->email, [], $subject, 'non_member_subscription_settings', ['model' => $model]);
        }


        return $this->redirect(['view', 'id' => $model->id]);

    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionBulkNotifyGdpr()
    {

        $model = new NewsletterSubscriptions();
        $subject = "Please push our GDPR compliance button";


        if (Yii::$app->request->get()) {

            if (Yii::$app->request->get('all')) {
                $query = NewsletterSubscriptions::find()->where(['active' => '1']);
            } else {

                $query = NewsletterSubscriptions::find()->where(['active' => '1']);

                if (Yii::$app->request->get('weekly_newsletter')) {
                    $query->andWhere(['weekly_newsletter' => '1']);
                }

                if (Yii::$app->request->get('events')) {
                    $query->andWhere(['events_news' => '1']);
                }

                if (Yii::$app->request->get('special_offers')) {
                    $query->andWhere(['special_offers' => '1']);
                }

            }


            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => 5
            ]);

            $subscriptions = $query->offset($pages->offset)
                ->limit($pages->limit)->all();


            if($subscriptions <> null){

                foreach ($subscriptions as $model){

                    if ($model->member <> null && $model->gdpr_sent == 0) {

                        $model->gdpr_sent = 1;
                        $model->save();

                        (new EmailHelper())->sendEmail($model->email, [], $subject, 'member_subscription_settings', ['model' => $model]);
                    }
                    else {

                        if ($model->gdpr_sent == 0) {

                            $model->gdpr_sent = 1;
                            $model->is_member = 0;
                            $model->save();
                            (new EmailHelper())->sendEmail($model->email, [], $subject, 'non_member_subscription_settings', ['model' => $model]);
                        }
                    }
                }
            }

            return $this->render('text_2', ['total' => NewsletterSubscriptions::find()->count(),'subscriptions' => $subscriptions, 'params' => Yii::$app->request->get()]);

        }

        return $this->render('_send_gdpr_bulk', ['model' => $model]);

    }

    public function actionUpdateNonMembers()
    {

        $query = NewsletterSubscriptions::find();

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 100
        ]);

        $non_members = $query->offset($pages->offset)
            ->limit($pages->limit)->all();

        if ($non_members <> null) {
            foreach ($non_members as $non_member) {

                if ($non_member->member == null) {

                    $non_member->active = '0';
                    $non_member->events_news = "0";
                    $non_member->weekly_newsletter = "0";
                    $non_member->special_offers = "0";
                    $non_member->is_member = 0;
                    $non_member->save();
                }
            }
        }

        return $this->render('text', [
            'model' => $non_members,
            'page' =>  Yii::$app->request->get('page')
        ]);
    }
}
