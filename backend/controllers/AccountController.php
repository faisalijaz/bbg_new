<?php

namespace backend\controllers;

use backend\models\MemberOffersSearch;
use backend\models\MemberSearch;
use backend\models\NewsSearch;
use common\helpers\EmailHelper;
use common\models\AccountCompany;
use common\models\Accounts;
use common\models\InvoiceAdjustments;
use common\models\Invoices;
use common\models\Members;
use common\models\News;
use common\models\NewsletterSubscriptions;
use common\models\TblCity;
use common\models\TblProfile;
use common\models\TblUser;
use kartik\mpdf\Pdf;
use PharIo\Manifest\InvalidEmailException;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AccountController implements the CRUD actions for Accounts model.
 */
class AccountController extends Controller
{

    /**
     * @inheritgitdoc
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Accounts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MemberSearch();
        $exportModel = new MemberSearch();

        if (Yii::$app->request->get('account_type')) {
            $searchModel->account_type = Yii::$app->request->get('account_type');
        }

        if (Yii::$app->request->get('member_type')) {
            $searchModel->group_id = Yii::$app->request->get('member_type');
        }

        if (Yii::$app->request->get('status')) {

            $status = 1;

            if(Yii::$app->request->get('status') == "active"){
                $status = 1;
            }

            if(Yii::$app->request->get('status') == "pending"){
                $status = 0;
            }

            if(Yii::$app->request->get('status') == "invoiced"){
                $status = 2;
            }

            if(Yii::$app->request->get('status') == "deleted"){
                $status = 3;
            }

            if(Yii::$app->request->get('status') == "renewal-required"){
                $status = 4;
            }

            $searchModel->status = $status;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        $dataProviderfull = $exportModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'exportModel' => $exportModel,
            'dataProviderfull' => $dataProviderfull
        ]);
    }

    /**
     * Displays a single Accounts model.
     * @param integer $id to view data
     * @return mixed
     */
    public function actionView($id)
    {

        $searchModel = new MemberSearch();
        $searchModel->parent_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $newsModel = new NewsSearch();
        $newsModel->related_to = "member";
        $newsModel->rel_id = $id;
        $newsDataProvider = $newsModel->memberNewsSearch(Yii::$app->request->queryParams);

        $offerModel = new MemberOffersSearch();
        $offerModel->offer_rel = "member";
        $offerModel->offer_rel_id = $id;
        $offerModelDataProvider = $offerModel->membersOffers(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'contacts' => $dataProvider,
            'memberNews' => $newsDataProvider,
            'memberOffers' => $offerModelDataProvider,
        ]);
    }

    /**
     * Creates a new Accounts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Members();
        // Set default type to seller.
        $model->setPassword("admin123");
        $model->setVerificationCode();
        $model->generateAuthKey();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->generateAuthKey();
            $model->socialType = "facebook";
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return bool|string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->last_email = $model->email;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->generateAuthKey();
            $model->socialType = (string) "-";
            if ($model->save()) {

                if ($model->last_email <> $model->email) {

                    $newsletter = NewsletterSubscriptions::find()->where(['email' => $model->last_email])->one();

                    if($newsletter == null){
                        $newsletter = new NewsletterSubscriptions();
                    }

                    $newsletter->email = $model->email;
                    $newsletter->save();
                }

                return $this->redirect(['view', 'id' => $model->id]);

            }else{

                return false;
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     *
     */
    public function actionUpdateStatus()
    {
        if (Yii::$app->request->post()) {

            $model = Members::findOne(Yii::$app->request->post('id'));
            $model->status = Yii::$app->request->post('status');
            $model->approved = Yii::$app->request->post('status');

            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'active' => $model->getErrors()]));
            } else {
                if ($model->status) {

                    echo 1;

                    /*if ((new EmailHelper())->sendEmail(
                        $model->email,
                        [],
                        'BBG Application Approved',
                        'account/approve_user',
                        [
                            'member' => $model
                        ])) {

                        echo 1;
                    }*/

                } else {
                    echo 1;
                }
            }
        }
    }

    /**
     *
     */
    public function actionChangeRelationType()
    {
        if (Yii::$app->request->post()) {

            $model = Members::findOne(Yii::$app->request->post('user'));
            $model->user_type_relation = Yii::$app->request->post('type');

            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'active' => $model->getErrors()]));
            } else {
                    echo 1;
            }
        }
    }

    /**
     * Deletes an existing Accounts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id , to delete the data against specific ID
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     *
     */
    public function actionUpgradeMembership()
    {
        $errors = [];

        if (Yii::$app->request->post()) {

            $post = Yii::$app->request->post();

            $member = Members::findOne($post['member']);

            $invoice = $member->upgradeMembership($post['member'], $post['group_id'], $post['accountTypes']);


            return $this->render('send_invoice', [
                'member' => $member,
                'invoice' => Invoices::findOne($invoice),
                'adjustments' => InvoiceAdjustments::findOne(['invoice_id' => $invoice])
            ]);
        }

        return $this->render('upgrade_membership', []);
    }

    /**
     * @param $lastRenwal
     * @param $expiry
     * @return false|float|int|string
     */
    private function getMonths($lastRenwal, $expiry)
    {

        $ts1 = strtotime($lastRenwal);
        $ts2 = strtotime($expiry);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        return $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
    }

    /**
     * @param null $q query string
     * @param null $id id list of customers
     * @return array
     */
    public function actionAccountList($q = null, $id = null)
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = ['results' => ['id' => $q, 'name' => 'New Account']];

        if (!is_null($q)) {
            $accounts = Members::find()
                ->select(['id', 'first_name', 'last_name', 'phone_number'])
                ->where(['LIKE', 'first_name', $q])
                ->orWhere(['LIKE', 'last_name', $q])
                ->orWhere(['LIKE', 'phone_number', $q])
                ->limit(20)
                ->all();

            foreach ($accounts as $account) {
                $response[] = ['id' => $account->id, 'name' => $account->first_name . ' ' . $account->last_name . ' (' . $account->phone_number . ')'];
            }

            $response = ['results' => array_values($response)];
        } elseif ($id > 0) {
            $account = Accounts::findOne($id);
            $response['results'] = ['id' => $id, 'name' => $account->first_name . ' ' . $account->last_name . ' (' . $account->phone_number . ')'];
        }
        return $response;
    }

    /**
     * Finds the Accounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id to find something against the primary key
     * @return Accounts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Members::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist . ');
        }
    }

    /**
     * @return mixed
     */
    public function actionMemberCertificate($id, $mode = '', $type = "")
    {
        $model = $this->findModel($id);

        $pdf = Yii::$app->pdf; // or new Pdf('utf-8', 'Letter', 0, '', 0, 0, 0, 0, 0, 0);
        $mpdf = $pdf->api; // fetches mpdf api
        $mpdf->format = Pdf::FORMAT_A4;

        // $mpdf->setAutoTopMargin = "stretch";

        $mpdf->cssInline = '@media print { #cetificate_header { margin-top: 0px; }  }#cetificate_header { width: 100%;text-align: center;margin-top: 0px;}#cetificate_footer {border-right: 0.5px solid #ddd;border-bottom: 0.5px solid #ddd;}';

        if ($mode == 'download') {

            if ($type == "company") {

                $mpdf->WriteHtml($this->renderPartial('company_certificate_pdf', ['model' => $model])); // call mpdf write html
                return $mpdf->Output(($model->accountCompany <> null) ? $model->accountCompany->name : "" . ' (Company Certificate)', 'D'); // call the mpdf api output as needed

            } else {

                $mpdf->WriteHtml($this->renderPartial('certificate_pdf', ['model' => $model])); // call mpdf write html
                return $mpdf->Output($model->first_name . ' ' . $model->last_name . ' (Membership Certificate)', 'D'); // call the mpdf api output as needed
            }
        }

        if ($mode == 'view') {

            if ($type == "company") {

                $mpdf->WriteHtml($this->renderPartial('company_certificate_pdf', ['model' => $model])); // call mpdf write html
                return $mpdf->Output(($model->accountCompany <> null) ? $model->accountCompany->name : "" . ' (Membership Certificate)', 'I'); // call the mpdf api output as needed

            } else {

                $mpdf->WriteHtml($this->renderPartial('certificate_pdf', ['model' => $model])); // call mpdf write html
                return $mpdf->Output($model->first_name . ' ' . $model->last_name . ' (Membership Certificate)', 'I'); // call the mpdf api output as needed
            }
        }
    }

    /**
     * @param $id
     * @param string $mode
     * @param string $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPrintCertificate()
    {
        if (Yii::$app->request->post()) {

            $model = $this->findModel(Yii::$app->request->post('user'));

            if (Yii::$app->request->post('type') == "company") {

                return $this->renderPartial('company_certificate_pdf', [
                    'model' => $model
                ]);

            } else {

                return $this->renderPartial('certificate_pdf', [
                    'model' => $model
                ]);
            }
        }
    }

    /**
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionSendCertificate()
    {
        if (Yii::$app->request->post()) {

            $model = $this->findModel(Yii::$app->request->post('user'));

            try {

                if (Yii::$app->request->post('certificate_type') == "company") {

                    (new EmailHelper())->sendCertificateEmail(
                        $model->email, [],
                        'Company Certificate',
                        'account/company_certificate',
                        ['model' => $model]
                    );

                } else {

                    (new EmailHelper())->sendCertificateEmail(
                        $model->email, [],
                        'Membership Certificate',
                        'account/certificate',
                        ['model' => $model]
                    );

                }



            } catch (InvalidEmailException $e) {

                echo "<pre>";
                print_r($e->getMessage());
                die;

            }

            return 1;
        }
    }

    /**
     * @param $member
     * @return string
     */
    Public function actionSendInvoice($member){

        if($member){

            $member = Members::findOne($member);

            if ($member->mem_invoice_gen == 0 || $member->mem_invoice_gen == 1) {
                    if ($invoice = $member->createInvoice($member, "Membership Registration")) {;
                        $member->mem_invoice_gen = $invoice;
                        $member->save();
                    }
            }

            $invoices = Invoices::find()->where([
                'user_id' => $member->id,
                'invoice_related_to' => 'member',
            ])->orderBy(['invoice_id' => SORT_DESC])->one();

            if ($invoices <> null) {

                $adjustments = InvoiceAdjustments::findOne(['invoice_id' => $invoices->invoice_id]);

                if ($adjustments == null) {
                    $adjustments = new InvoiceAdjustments();
                }

                $adjustments->invoice_id = $invoices->invoice_id;

            } else {
                $adjustments = new InvoiceAdjustments();
            }


            return $this->renderPartial('send_invoice', [
                'member' => $member,
                'invoice' => $invoices,
                'adjustments' => $adjustments
            ]);
        }
    }

    /**
     * @param $member
     * @return string
     */
    Public function actionSendInvoiceren($member){

        if($member){

            $member = Members::findOne($member);
            if ($member->renw_invoice_gen == 0) {
                if ($invoice = $member->createInvoice($member, "Membership Renewal")) {
                    $member->mem_invoice_gen = $invoice;
                    $member->renw_invoice_gen = 1;
                    $member->invoice_renewal = $invoice;
                    $member->save();
                }
            }

            $invoices = Invoices::find()->where([
                'user_id' => $member->id,
                'invoice_related_to' => 'member',
            ])->orderBy(['invoice_id' => SORT_DESC])->one();

            if ($invoices <> null) {

                $adjustments = InvoiceAdjustments::findOne(['invoice_id' => $invoices->invoice_id]);

                if ($adjustments == null) {
                    $adjustments = new InvoiceAdjustments();
                }

                $adjustments->invoice_id = $invoices->invoice_id;

            } else {
                $adjustments = new InvoiceAdjustments();
            }


            return $this->renderPartial('send_invoice', [
                'member' => $member,
                'invoice' => $invoices,
                'adjustments' => $adjustments
            ]);
        }
    }
    /**
     * @param $member
     * @return string
     */
    Public function actionSendInvoiceEmailren()
    {

        if (Yii::$app->request->post()) {


            $member = Members::findOne(Yii::$app->request->post('member'));

            if ($member <> null) {

                $invoice_id = 0;

                if (Yii::$app->request->post('invoice_id')) {

                    $invoice_id = Yii::$app->request->post('invoice_id');

                    $invoices = Invoices::find()->where([
                        'invoice_rel_id' => $member->id,
                        'invoice_id' => $invoice_id,
                    ])->one();

                } else {

                    $invoices = Invoices::find()->where([
                        'user_id' => $member->id,
                        'invoice_related_to' => 'member',
                    ])->orderBy(['invoice_id' => SORT_DESC])->one();
                }


                if ((new EmailHelper())->sendInvoiceEmail('admin@bbgdubai.org', [], 'Membership Invoice', 'account/send_invoice', [
                    'member' => $member,
                    'invoice' => $invoices
                ]))

                {
                    if(!$member->invoiced) {

                    }

                    echo true;
                }
            }
        }
    }

    /**
     * @param $member
     * @return string
     */
    Public function actionSendInvoiceEmail()
    {

        if (Yii::$app->request->post()) {


            $member = Members::findOne(Yii::$app->request->post('member'));

            if ($member <> null) {

                $invoice_id = 0;

                if (Yii::$app->request->post('invoice_id')) {

                    $invoice_id = Yii::$app->request->post('invoice_id');

                    $invoices = Invoices::find()->where([
                        'invoice_rel_id' => $member->id,
                        'invoice_id' => $invoice_id,
                    ])->one();

                } else {

                    $invoices = Invoices::find()->where([
                        'user_id' => $member->id,
                        'invoice_related_to' => 'member',
                    ])->orderBy(['invoice_id' => SORT_DESC])->one();
                }


                if ((new EmailHelper())->sendInvoiceEmail($member->email, [], 'Membership Invoice', 'account/send_invoice', [
                    'member' => $member,
                    'invoice' => $invoices
                ]))

                {
                    if(!$member->invoiced) {

                        $member->invoiced = 1;
                        $member->status = 2;
                        $member->save();

                        $invoices->invoice_sent = 1;
                        $invoices->save();
                    }

                    echo true;
                }
            }
        }
    }

    /**
     * @param $member
     * @return string
     */
    Public function actionMemberOffers($member){

        if($member){

            $data = Members::findOne($member);

            return $this->render('send_invoice', [
                'model' => $data,
            ]);
        }
    }

    /**
     * @return bool
     */
    public function actionApproveMember()
    {

        if (Yii::$app->request->post()) {

            $member = Members::findOne(Yii::$app->request->post('user'));
            $member->status = Yii::$app->request->post('status');
            $member->approved = Yii::$app->request->post('status');

            if ($member->save()) {


                if ($member->company <> null && $member->company) {
                    $show = ($member->approved) ? '1' : '0';
                    $company = AccountCompany::findOne($member->company);
                    if ($company <> null) {
                        $company->show_in_directory = $show;
                        $company->save();
                    }
                }

                if ($member->approved) {

                    (new EmailHelper())->sendEmail(
                        $member->email,
                        [],
                        'BBG Application Approved',
                        'account/approve_user',
                        ['member' => $member]
                    );

                    echo 1;
                    return;
                }

                echo 0;
                return;
            }

        }

        return false;

    }

    /**
     * @param $amount
     * @return float|int
     */
    public function calculateVat($amount)
    {
        return ((5 / 100) * $amount);
    }

    /**
     * @param $subscriber
     * @return mixed
     */
    public function actionInvoicePdf($member, $invoice)
    {
        $member = Members::findOne($member);
        $invoices = Invoices::findOne($invoice);

        if ($invoices <> null) {

            $adjustments = InvoiceAdjustments::findOne(['invoice_id' => $invoice]);

            if ($adjustments == null) {
                $adjustments = new InvoiceAdjustments();
            }
        } else {
            $adjustments = new InvoiceAdjustments();
        }

        $adjustments->invoice_id = $invoice;


        $pdf = \Yii::$app->pdf;
        $mpdf = $pdf->api;
        $mpdf->format = Pdf::FORMAT_A4;
        $mpdf->orientation = Pdf::ORIENT_PORTRAIT;
        $mpdf->WriteHtml($this->renderPartial('invoice_pdf', [
            'member' => $member,
            'invoice' => $invoices,
            'adjustments' => $adjustments
        ]));
        return $mpdf->Output('Invoice-' . str_pad($invoices->invoice_id, "7", '0', STR_PAD_LEFT), 'I');
    }

    /**
     * @return bool
     */
    public function actionSpecialMemberStatus()
    {
        if (Yii::$app->request->post()) {

            $status = Yii::$app->request->post("member_status");
            $status_type = Yii::$app->request->post("status_type");
            $member = Members::findOne(Yii::$app->request->post('user'));

            if ($status == "honorary") {
                $member->honourary = $status_type;
            }

            if ($status == "sponsor") {
                $member->sponsor = $status_type;
            }

            if ($status == "committee_member") {
                $member->committee = $status_type;
            }

            if ($status == "focus_chair") {
                $member->focus_chair = $status_type;
            }

            if ($member->save()) {

                /* (new EmailHelper())->sendEmail(
                     $member->email,
                     [],
                     'BBG Application Approved',
                     'account/approve_user',
                     ['member' => $member]
                 );*/

                return true;
            }

        }

        return false;
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionResetPassword()
    {
        if (Yii::$app->request->post()) {

            $member = $this->findModel(Yii::$app->request->post('user'));
            $member->socialType = (string) "-";
            $password = $member->genRandomPassword();

            $member->setPassword($password);

            if($member->save()){
                return $password;
            } else {
                if (count($member->getErrors()) > 0) {
                    echo $member->getErrors()[0];
                    return;
                }
            }

            return false;
        }
    }

    /**
     * @return string
     */
    public function actionSendMessage(){

        return $this->renderPartial('send_message', []);
    }

    /**
     * @return string
     */
    public function actionUpcomingRenewals()
    {

        $searchModel = new MemberSearch();
        $searchModel->find_exp_membership = true;

        if (Yii::$app->request->get('from_date')) {
            $searchModel->account_type = Yii::$app->request->get('from_date');
        }

        if (Yii::$app->request->get('to_date')) {
            $searchModel->group_id = Yii::$app->request->get('to_date');
        }

        if (Yii::$app->request->get('group_id')) {
            $searchModel->account_type = Yii::$app->request->get('group_id');
        }

        if (Yii::$app->request->get('account_type')) {
            $searchModel->group_id = Yii::$app->request->get('account_type');
        }

        if (Yii::$app->request->get('status')) {
            $searchModel->account_type = Yii::$app->request->get('status');
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('expiring_membership', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $member
     * @return string
     */
    public function actionSendRenewalInvoice($member)
    {
        $member = Members::findOne($member);
        $group = null;

        $invoice_data = new Members();
        $data = $invoice_data->createRenewalInvoice($member);
 /*
        if ($member <> null && $member->group <> null) {
            $group = $member->group;
        }

        if ($member <> null) {

            $invoice = Invoices::findOne($member->invoice_renewal);

            if ($invoice == null) {
                $invoice = new Invoices();
            }

            $invoice->invoice_related_to = 'renewal';
            $invoice->invoice_rel_id = $member->id;
            $invoice->invoice_category = ($group <> null) ? $group->title . " Membership Renewal" : "";
            $invoice->payment_status = 'unpaid';

            if ($invoice->save()) {

                $items = InvoiceItems::findOne(['invoice_id' =>  $invoice->invoice_id]);

                if($items == null){
                    $items = new InvoiceItems();
                }

                $items->invoice_id = $invoice->invoice_id;
                $items->user_id = $member->id;
                $items->invoice_related_to = 'member';
                $items->invoice_rel_id = $member->id;
                $items->invoice_category = ($group <> null) ? $group->title . " Membership Renewal" : "";

                if ($member->account_type == "nominee" || $member->account_type == "member") {
                    $items->subtotal = ($group <> null) ? $group->fee_nominee : 0;
                }

                if ($member->account_type == "alternate") {
                    $items->subtotal = ($group <> null) ? $group->fee_alternate : 0;
                }

                if ($member->account_type == "additional") {
                    $items->subtotal = ($group <> null) ? $group->fee_additional : 0;
                }

                if ($member->account_type == "named_associate") {
                    $items->subtotal = ($group <> null) ? $group->fee_associate : 0;
                }

                $items->tax = $this->calculateVat($items->subtotal);
                $items->amount = $items->subtotal + $items->tax;
                $items->save();
                $member->renw_invoice_gen = 1;
                $member->invoice_renewal = $invoice->invoice_id;
                $member->save();
            }
        }

        if ($invoice <> null) {

            $adjustments = InvoiceAdjustments::findOne(['invoice_id' => $invoice->invoice_id]);

            if ($adjustments == null) {
                $adjustments = new InvoiceAdjustments();
            }

            $adjustments->invoice_id = $invoice->invoice_id;

        } else {
            $adjustments = new InvoiceAdjustments();
        }*/

        return $this->renderPartial('send_invoice', [
            'member' => $member,
            'invoice' => ($data <> null)? $data['invoice'] : null,
            'adjustments' => ($data <> null)? $data['adjustments'] : null
        ]);
    }

    public function actionImport()
    {
        //
        $count = 0;
        $query = TblUser::find();

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $users = $query->offset($pages->offset)
            ->limit($pages->limit)->all();

        $path = '/uploads/user/';
        // $event_sub = TblEventSubscription::find()->all();
        if (count($users) > 0) {

            $data = [];

            foreach ($users as $user) {

                $user_data = Members::findOne(['id' => $user->id]);

                if ($user_data == null) {
                    $user_data = Members::findOne(['email' => $user->profile->email]);
                    if ($user_data == null) {
                        $user_data = new Members();
                    }
                }

                $user_data->id = $user->id;
                $user_data->bbg_membershipid = $user->bbg_membershipid;
                $user_data->old_membershipid = $user->old_membershipid;
                $user_data->parent_id = $user->parent_id;

                $user_data->setPassword($user->profile <> null && $user->profile->email <> "") ? $user->profile->email : ($user->profile <> null && $user->profile->mobile_number <> "") ? $user->profile->mobile_number : "zxcvbnm";

                $user_data->auth_key = $user->salt;
                /* if($user->salt == ""){
                     $user_data->auth_key = "asdfjaiqwiiuhakjfhiq7y3iuhrbkjbar";
                 }*/

                $user_data->user_name = $user->username;
                if ($user_data->user_name == "") {
                    $user_data->user_name = $user->profile->email;
                }


                $user_data->title = ($user->profile <> null) ? $user->profile->title : 0;
                $user_data->first_name = ($user->profile <> null) ? $user->profile->firstname : 0;
                $user_data->last_name = ($user->profile <> null) ? $user->profile->lastname : 0;
                $user_data->designation = ($user->profile <> null) ? $user->profile->position : 0;
                $user_data->email = ($user->profile <> null) ? $user->profile->email : 0;
                $user_data->secondry_email = ($user->profile <> null) ? $user->profile->secondery_email : 0;
                $user_data->phone_number = ($user->profile <> null) ? $user->profile->mobile_number : 0;
                $user_data->picture = $path . $user->avatar;

                $user_data->city = "";
                if ($user->profile->city <> null) {
                    $city = TblCity::findOne($user->profile->city);
                    if ($city <> null) {
                        $user_data->city = $city->Name;
                    }
                }

                if ($user->profile->country <> null) {

                    if ($user->profile->country == "ARE") {
                        $user_data->country_code = 228;
                    }

                    if ($user->profile->country == 'GBR') {
                        $user_data->country_code = 229;
                    }
                }

                $user_data->nationality = ($user->profile <> null) ? $user->profile->nationality : 0;
                $user_data->address = ($user->profile <> null) ? $user->profile->secondery_email : 0;

                if ($user->category == "business") {
                    $user_data->group_id = 1;
                } else if ($user->category == "individual") {
                    $user_data->group_id = 2;
                } else if ($user->category == "notforprofit") {
                    $user_data->group_id = 3;
                }
                $user_data->account_type = $user->type;
                if ($user->type == "notforprofit") {
                    $user_data->account_type = "nominee";
                }
                if ($user->type == "individual") {
                    $user_data->account_type = "nominee";
                }
                if ($user->type == "business") {
                    $user_data->account_type = "nominee";
                }

                $status = 0;
                $user_data->status = $user->status;

                if ($user->status == "-1") {
                    $user_data->status = 2;
                }

                if ($user->status == "-2") {
                    $user_data->status = 3;
                }

                if ($user->status == "-3") {
                    $user_data->status = 4;
                }

                $user_data->approved = $user->approved;
                $user_data->vat_number = ($user->profile <> null) ? $user->profile->vat_number : "";
                $user_data->linkedin = ($user->profile <> null) ? $user->profile->linkedinurl : "";
                $user_data->twitter = ($user->profile <> null) ? $user->profile->twitterurl : "";
                $user_data->user_industry = ($user->profile <> null) ? $user->profile->companytype : 0;

                $user_data->invoiced = $user->id;
                $user_data->registeration_date = date('Y-m-d', ($user->createtime));;


                if ($user->lastrenewal <> "") {
                    $user_data->last_renewal = date('Y-m-d ', ($user->lastrenewal));
                }

                $user_data->created_at = $user->createtime;
                $exp_date = date('Y-m-d', ($user->lastrenewal));
                $user_data->expiry_date = date("Y-m-d", strtotime($exp_date . " +1 year"));

                $user_data->newsletter = 0;
                if ($user->profile <> null) {
                    if ($user->profile->subscribe_newsletter == 'yes') {
                        $user_data->newsletter = 1;
                    }
                }

                /*      $user_data->inforamtion_source      = $user->id;
                        $user_data->mem_invoice_gen         = $user->id;
                        $user_data->renw_invoice_gen        = $user->id;
                */

                $user_data->honourary = "0";
                if ($user->honourary == 'true') {
                    $user_data->honourary = "1";
                }

                $user_data->charity = "0";
                if ($user->charity == 'true') {
                    $user_data->charity = "1";
                }


                $user_data->focus_chair = "0";
                if ($user->focus_chair == 'true') {
                    $user_data->focus_chair = "1";
                }


                $user_data->committee = "0";
                if ($user->committee == 'true') {
                    $user_data->committee = "1";
                }

                $user_data->sponsor = "0";
                if ($user->sponsor == 'true') {
                    $user_data->sponsor = "1";
                }

                $user_data->invoice_renewal = $user->request_renewal;
                $user_data->lastvisit = $user->lastvisit;
                $user_data->lastpasswordchange = $user->lastpasswordchange;
                $user_data->delete_request = $user->delete_request;

                if (!$user_data->save()) {
                    $data[] =  '<span style="color: red;">' . $user_data->id  . '</span><br/>';
                    $data[] =  $user_data->getErrors();

                } else {
                    $data[] =  '<span style="color: green;">' . $user_data->id . '</span><br/>';
                }

            }

            return $this->render('text',[
                'data' => $data,
                'page' =>  Yii::$app->request->get('page')
            ]);


        } else {
            echo "no record";
        }

    }

    public function actionImportCompany()
    {

        $count = 0;
        $query = TblProfile::find();

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 1000
        ]);

        $users = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('id', 'DESC')->all();

        foreach ($users as $profile) {

            $company = AccountCompany::findOne(['name' => $profile->companyname]);

            if ($company == null) {

                $company = AccountCompany::findOne(['url' => $profile->companyurl]);

                if ($company == null) {

                    $company = new AccountCompany();

                    $company->name = $profile->companyname;
                    if ($profile->companyname == "") {
                        $company->name = $profile->companyurl;
                    }
                    $cat = ($profile->companytype <> null) ? $profile->companytype : 0;
                    $company->category = (int) $cat;
                    $company->url = ($profile->companyurl <> null) ? $profile->companyurl : "#";
                    $company->phonenumber = ($profile->phone <> null) ? $profile->phone : "-";;
                    $company->fax = ($profile->fax <> null) ? $profile->fax : "-";
                    $company->postal_code = ($profile->pobox <> null) ? $profile->pobox : "-";
                    $company->emirates_number = ($profile->emirate <> null) ? $profile->emirate : "-";
                    $company->address = ($profile->address <> null) ? $profile->address : "-";
                    $company->about_company = ($profile->about <> null) ? $profile->about : "-";

                    if (!$company->save()) {
                        echo '<pre>';
                        print_r($company->getErrors());
                        echo '</pre>';
                    } else {
                        echo '<span style="color: green;">' . $company->id . '</span><br/>';
                    }
                }
            }

            if ($company <> null && $company->id <> null) {
                $account = Members::findOne($profile->user_id);
                if ($account <> null) {
                    $account->company = $company->id;
                    $account->save();
                }
            }
        }


    }

    public function actionImprotRenewals(){

    }

    public function invoiceAdjustment($invoice_id, $amount, $type, $reason)
    {

        $model = InvoiceAdjustments::findOne(['invoice_id' => $invoice_id]);

        if ($model == null) {
            $model = new InvoiceAdjustments();
        }

        $model->adjusted_by = Yii::$app->user->id;
        $model->adjustment = $amount;
        $model->type = $type;
        $model->reason = $reason;
        $model->invoice_id = $invoice_id;

        if ($model->save()) {

            $invoice = Invoices::findOne($invoice_id);

            if ($model->type == '-') {
                $invoice->total = ($invoice->subtotal + $invoice->tax) - $model->adjustment;
            }

            if ($model->type == '+') {
                $invoice->total = ($invoice->subtotal + $invoice->tax) + $model->adjustment;
            }

            $invoice->save();

            return $model;
        }

        return false;

    }


}
