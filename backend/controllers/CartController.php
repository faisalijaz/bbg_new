<?php

namespace backend\controllers;

use app\models\CheckoutModel;
use backend\models\BookingItem;
use backend\models\PromoCode;
use backend\models\TouristInformation;
use common\models\Accounts;
use common\models\TourBookings;
use Yii;

/**
 * Class CartController
 * @package frontend\controllers
 */
class CartController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     * @return mixed
     *
     */
    public function actionIndex()
    {
        $items = Yii::$app->session->get('cartItems');
        return $this->render('index', [
            'items' => $items,
        ]);

        $this->cart = new BookingItem();
    }


    public function actionCheckout()
    {

        if (!\Yii::$app->session->has('adminCartItems')) {
            return $this->redirect(['/tour-booking/tours-list']);
        }


        $model = new CheckoutModel([
            'scenario' => 'customer',
            'account_id' => \Yii::$app->session->get('customerSessionId'),
            'payment_type' => 'COD'
        ]);

        // If request is post then try to create booking
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $email = "";

            if (\Yii::$app->session->get('customerSessionId')) {
                $acc = Accounts::findOne(\Yii::$app->session->get('customerSessionId'));
                if ($acc <> null) {
                    $email = $acc->email;
                }
            }
            if (!$model->saveBooking()) {
                /*echo '<pre>';
                print_r($model->getErrors());
                die();*/
            }
            $booking = $model->saveBooking();

            if (count(\Yii::$app->request->post('adult')) > 0 && Yii::$app->request->post('adult') <> null) {
                foreach (Yii::$app->request->post('adult') as $adults) {
                    $save_information = new TouristInformation([
                        'name' => $adults['name'],
                        'adult_or_child' => 'adult',
                        'age' => $adults['age'],
                        'gender' => $adults['gender'],
                        'booking_id' => $booking->id,
                        'nationality' => $adults['nationality'],
                        'pick_from_address' => (isset($adults['pick_from_address'])) ? $adults['pick_from_address'] : '',
                    ]);
                    $save_information->save();
                }
            }

            if (count(\Yii::$app->request->post('child')) > 0 && Yii::$app->request->post('child') <> null) {
                foreach (Yii::$app->request->post('child') as $childs) {
                    $save_information = new TouristInformation([
                        'name' => $childs['name'],
                        'adult_or_child' => 'child',
                        'age' => $childs['age'],
                        'gender' => $childs['gender'],
                        'booking_id' => $booking->id,
                        'nationality' => $childs['nationality'],
                        'pick_from_address' => (isset($childs['pick_from_address'])) ? $childs['pick_from_address'] : '',
                    ]);
                    $save_information->save();
                }
            }


            $model->sendEmail(
                $email,
                $booking,
                $model->cart->totalCartAmount()
            );
            // Flush cart Session
            $model->cart->flushCart();
            return $this->redirect(['booking-confirmation', 'booking_id' => $booking->id]);
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * @param integer $booking_id id
     * @return string
     */
    public function actionBookingConfirmation($booking_id)
    {
        $booking = TourBookings::findOne($booking_id);

        return $this->render('thank-you', [
            'model' => '',
            'booking' => $booking
        ]);
    }

    /**
     * @return CartModel|\yii\web\Response
     */
    public function actionDeleteCartItem()
    {

        $cart = new BookingItem();

        $key = Yii::$app->request->post('key');
        $cart->deleteCartItem($key);

        if ($cart->getItems()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['status' => 'OK', 'message' => 'Item deleted from cart.'];
        } else {
            return $this->redirect(['/tour']);
        }
    }


    /**
     * Apply Promo discount
     * */
    public function actionApplyPromo()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $promo = new PromoCode();

        if ($promo->load(Yii::$app->request->post(), '') && $promo->validate()) {
            $promo->applyAdminCode();

            return [
                'status' => 'OK',
                'message' => 'Promo code has been applied'
            ];
        } else {
            $errors = '';
            foreach ($promo->getErrors() as $error) {
                $errors .= '<br />' . $error[0];
            }

            return [
                'status' => 'ERROR',
                'message' => $errors
            ];
        }
    }
}
