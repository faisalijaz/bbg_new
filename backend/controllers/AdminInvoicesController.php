<?php

namespace backend\controllers;

use backend\models\AdminInvoicesSearch;
use common\models\AdminInvoices;
use common\models\Events;
use common\models\EventSubscriptions;
use common\models\Members;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use kartik\widgets\DepDrop;

/**
 * AdminInvoicesController implements the CRUD actions for AdminInvoices model.
 */
class AdminInvoicesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdminInvoices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdminInvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdminInvoices model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AdminInvoices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AdminInvoices();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->invoice_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AdminInvoices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->invoice_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AdminInvoices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdminInvoices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminInvoices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AdminInvoices::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return bool
     */
    public function actionFindInvoiceRel()
    {
        ob_clean();
        $out = [];

        if (isset($_POST['depdrop_parents'])) {

            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {

                $cat_id = $parents[0];

                if ($cat_id == "event") {
                    $res = ArrayHelper::map(Events::find()->all(), 'id', 'title');
                } else {
                    $res = ArrayHelper::map(Members::find()->all(), 'id', function ($model) {
                        return $model->id . ' - ' . $model->first_name . " " . $model->last_name;
                    });
                }

                foreach ($res as $k => $v) {
                    $out[] = ['id' => $k, 'name' => $v];
                }

                echo Json::encode(['output' => $out, 'selected' => 'select ...']);
                return true;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * @return bool
     */
    public function actionFindInvoiceitem()
    {
        ob_clean();
        $out = [];

        if (isset($_POST['depdrop_parents'])) {

            $parents = $_POST['depdrop_parents'];

            if ($parents != null) {

                $cat_id = $parents[0];

                if ($cat_id == "event") {
                    $res = ArrayHelper::map(EventSubscriptions::find()->all());
                } else {
                    $res = ArrayHelper::map(Members::find()->all(), 'id', function ($model) {
                        return $model->id . ' - ' . $model->first_name . " " . $model->last_name;
                    });
                }

                foreach ($res as $k => $v) {
                    $out[] = ['id' => $k, 'name' => $v];
                }

                echo Json::encode(['output' => $out, 'selected' => 'select ...']);
                return true;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}
