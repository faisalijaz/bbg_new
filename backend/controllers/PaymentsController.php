<?php

namespace backend\controllers;

use Yii;
use common\models\Payments;
use backend\models\PaymentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payments();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionReceipt($payment)
    {

        if ($payment) {

            $payment = Payments::findOne($payment);

            return $this->renderPartial('recipts', [
                'model' => $payment,
            ]);
        }
    }

    /**
     * Send Invoice Email
     **/
    public function actionSendReceiptEmail()
    {

        if (Yii::$app->request->post()) {

            $payment = Payments::findOne(Yii::$app->request->post('paymentId'));

            $email = "";
            $send = false;

            $invoice = null;
            if ($payment->invoice <> null) {
                $invoice = $payment->invoice;
            }

            if ($invoice <> null) {
                if ($invoice <> null) {
                    if ($invoice->invoice_related_to == "member") {

                        if ($payment->paymentUser <> null) {

                            $email = $payment->paymentUser->email;

                            $send = (new EmailHelper())->sendInvoiceEmail($email, [], 'Payment Receipt', 'payments/receipt_pdf', [
                                'model' => $payment,
                            ]);
                        } else {

                            if ($invoice <> null && $invoice->invoiceItems <> null) {

                                foreach ($invoice->invoiceItems as $item) {

                                    $subscription = EventSubscriptions::findOne($item->invoice_rel_id);

                                    if ($subscription <> null) {
                                        $send = (new EmailHelper())->sendInvoiceEmail($email, [], 'Payment Receipt', 'payments/receipt_pdf', [
                                            'model' => $payment,
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if ($send) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    /**
     * Send Invoice Email
     **/
    public function actionPaymentReceiptPdf($id)
    {

        if ($id) {

            $payment = Payments::findOne($id);
            $pdf = \Yii::$app->pdf;
            $mpdf = $pdf->api;
            $mpdf->format = Pdf::FORMAT_A4;
            $mpdf->orientation = Pdf::ORIENT_PORTRAIT;
            $mpdf->WriteHtml($this->renderPartial('receipt_pdf', [
                'model' => $payment
            ]));
            return $mpdf->Output('Payment-receipt-', 'I');

        }
    }
}
