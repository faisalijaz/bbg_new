<?php

namespace backend\controllers;

use backend\models\EventsSearch;
use backend\models\EventSubscriptionsSearch;
use common\helpers\EmailHelper;
use common\models\Events;
use common\models\EventSubscriptions;
use common\models\Groups;
use common\models\InvoiceAdjustments;
use common\models\Invoices;
use common\models\Members;
use common\models\MembershipEventFee;
use common\models\TblEvent;
use common\models\UserFinancials;
use common\helpers\AppSetting;
use kartik\mpdf\Pdf;
use Yii;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * EventController implements the CRUD actions for Events model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        $searchModelPast = new EventsSearch();
        $searchModelPast->past = true;
        $dataProviderPast = $searchModelPast->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelPast' => $searchModelPast,
            'dataProviderPast' => $dataProviderPast
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Events();
        $membershipGroup = Groups::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'member_groups' => $membershipGroup
            ]);
        }
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $membershipGroup = Groups::find()->all();
        $membershipGroupFee = MembershipEventFee::findAll(['event_id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'member_groups' => $membershipGroup,
                'member_groups_fee' => $membershipGroupFee
            ]);
        }
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public function actionSubscribeEvent($event = "", $subscriber = "")
    {
        $isNew = true;
        $e_data = null;

        if($event) {
            $e_data = Events::findOne($event);
        }

        if ($subscriber) {

            $model = EventSubscriptions::findOne($subscriber);
            $isNew = false;

            $fee = $model->fee_paid;
            $invoice = Invoices::findOne($model->gen_invoice);

            if($invoice <> null){

                $fee = $invoice->total;

                // $adjustment = InvoiceAdjustments::findOne(['invoice_id' => $invoice->invoice_id]);

                /*if($adjustment <> null){
                    if($adjustment->type == "-"){
                        $fee  = $model->fee_paid - $adjustment->adjustment;
                    }else{
                        $fee  = $model->fee_paid + $adjustment->adjustment;
                    }
                }*/
            }

            $model->fee_paid = $fee;

        } else {

            $model = new EventSubscriptions();
            $model->event_id = $event;
            $model->registered_by = \Yii::$app->user->identity;
            $model->user_type = 'guest';
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {

            if($model->payment_status == ""){
                $model->payment_status = $model->status;
            }

            if(!$model->gen_invoice){

                if(!$model->invoice){

                    $invoice = new Invoices();
                    $invoice->user_id = $model->user_id;
                    $invoice->invoice_related_to = 'event';
                    $invoice->invoice_prefix = 'E';
                    $invoice->invoice_rel_id = $model->event_id;
                    $invoice->invoice_category = ($e_data <> null) ? $e_data->title : "";
                    $invoice->payment_status = 'unpaid';

                    if (!$invoice->save()) {
                        echo \GuzzleHttp\json_encode($invoice->getErrors());
                        return;
                    }

                    $model->gen_invoice = $invoice->invoice_id;
                    $model->invoice = $invoice->invoice_id;
                }
            }

            if (!$model->save()) {
                echo \GuzzleHttp\json_encode($model->getErrors());
            } else {

                if($isNew) {
                    $ccEmail = (new AppSetting())->getByAttribute('admin_email');

                    $subjectLine = "Event Registration for " . ($model->eventData <> null) ? $model->eventData->title : "";

                    (new EmailHelper())->sendEmail(trim($model->email), [], $subjectLine, 'events/foc-email', [
                        'emaildata' => $model
                    ]);
                }

                echo $model->id;
                return;
            }

        } else {
            return $this->render('register_event/register_member', [
                'model' => $model,
            ]);
        }
    }


    /**
     * @return string
     */
    public function actionEventRegisteredList($event = "")
    {
        $searchModel = new EventSubscriptionsSearch();
        $searchModel->event_id = $event;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = 100;

        $registered = EventSubscriptions::find()->where(['event_id' => $event])->count();
        $non_registered = EventSubscriptions::find()->where(['status' => 'cancelled', 'event_id' => $event])->count();

        $paid = EventSubscriptions::find()->where(['payment_status' => 'paid', 'event_id' => $event])->count();
        $pending = EventSubscriptions::find()->where(['payment_status' => 'pending', 'event_id' => $event])->count();

        // $unpaid = $registered - $paid;
        $unpaid = $pending;

        $attended = EventSubscriptions::find()->where(['attended' => '1', 'event_id' => $event])->count();
        $noShow = $registered - $attended;

        $members = EventSubscriptions::find()->where(['user_type' => 'member', 'event_id' => $event])->count();
        $businessAssociates = EventSubscriptions::find()->where(['user_type_relation' => 'business-associate', 'event_id' => $event])->count();
        $non_members = $registered - $members - $businessAssociates;


        return $this->render('register_event/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'eventId' => $event,
            'unpaid' => $unpaid,
            'paid' => $paid,
            'attended' => $attended,
            'noShow' => $noShow,
            'members' => $members,
            'non_members' => $non_members,
            'businessAssociates' => $businessAssociates,
            'registered' => $registered,
            'non_registered' => $non_registered
        ]);
    }

    /**
     * @param null $q
     * @throws \yii\db\Exception
     */
    public function actionMembersList($q = null)
    {
        $query = new Query;

        $query->select('id,first_name, last_name, email')
            ->from('accounts')
            ->where('first_name LIKE "%' . $q . '%"')
            ->orWhere('last_name LIKE "%' . $q . '%"')
            ->orWhere('email LIKE "%' . $q . '%"')
            ->orWhere('company LIKE "%' . $q . '%"')
            ->orderBy(['first_name' => SORT_ASC])
            ->orderBy(['last_name' => SORT_ASC]);

        $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];

        foreach ($data as $d) {
            $out[$d['id']] = $d['first_name'] . ' ' . $d['last_name'] . " | " . $d['email'];
        }
        echo Json::encode($out);
    }

    /**
     *
     */
    public function actionFindMember()
    {
        $data = [];

        if (Yii::$app->request->post()) {

            $member = Members::findOne(['email' => Yii::$app->request->post('email')]);
            $event = Events::findOne(['id' => Yii::$app->request->post('eventId')]);

            if ($member <> null) {

                $data['first_name'] = $member->first_name;
                $data['last_name'] = $member->last_name;
                $data['email'] = $member->email;
                $data['phone_number'] = $member->phone_number;
                $data['member_fee'] = $event->member_fee;
                $data['designation'] = $member->designation;

                $event_fee = MembershipEventFee::findOne([
                    'member_group' => $member->group_id,
                    'member_type' => $member->account_type,
                    'event_id' => Yii::$app->request->post('eventId')
                ]);

                if ($event_fee <> null) {
                    $data['member_fee'] = $event_fee->fee;
                }

                if ($member->accountCompany <> null) {
                    $data['company'] = $member->accountCompany->name;
                } else {
                    $data['company'] = "";
                }

                $data['id'] = $member->id;
            }

            print_r(\GuzzleHttp\json_encode($data));
        }
    }

    /**
     * @param $event
     * @param $event
     * @return string
     */
    public function actionEventRegisterInvoice($subscriber)
    {
        $data = EventSubscriptions::findOne(['id' => $subscriber]);
        $adjustments = null;

        if ($data->gen_invoice) {
            $adjustments = InvoiceAdjustments::findOne(['invoice_id' => $data->gen_invoice]);
        }

        if($adjustments == null){
            $adjustments = new InvoiceAdjustments();
        }

        $adjustments->invoice_id = $data->gen_invoice;

        return $this->renderPartial('invoices/view_registered_event', [
            'subscriber' => $data,
            'adjustments' => $adjustments
        ]);

    }

    /**
     * @param $event
     * @param $event
     * @return string
     */
    public function actionEventInvoiceAdjustment()
    {
        if(Yii::$app->request->post()){

            $data = Yii::$app->request->post();
            $invoiceId =  $data['InvoiceAdjustments']['invoice_id'];

            $model = InvoiceAdjustments::findOne(['invoice_id' =>$invoiceId]);

            if($model == null){
                $model = new InvoiceAdjustments();
            }

            $model->adjusted_by = Yii::$app->user->id;

            if($model->load(Yii::$app->request->post()) && $model->save()){

                $invoice = Invoices::findOne($invoiceId);

                if ($model->type == '-') {
                    $invoice->total = ($invoice->subtotal + $invoice->tax) - $model->adjustment;
                }

                if ($model->type == '+') {
                    $invoice->total = ($invoice->subtotal + $invoice->tax) + $model->adjustment;
                }

                if($invoice->total > 0 && $invoice->payment_status == "paid"){
                    $invoice->payment_status = 'unpaid';
                }

                $invoice->save();

                return true;
            }

            return false;
        }
    }

    /**
     * @param $event
     * @param $event
     * @return string
     */
    public function actionEventRegisterInvoicePdf($subscriber)
    {
        $data = EventSubscriptions::findOne(['id' => $subscriber]);

        $pdf = \Yii::$app->pdf;
        $mpdf = $pdf->api;
        $mpdf->format = Pdf::FORMAT_A4;
        $mpdf->orientation = Pdf::ORIENT_PORTRAIT;
        $mpdf->WriteHtml($this->renderPartial('invoices/event_reg_invoice', [
            'subscriber' => $data
        ]));
        return $mpdf->Output('Event-Registeration-Invoice-', 'I');
    }

    /**
     * Send Invoice Email
     **/
    public function actionSendInvoiceEmail(){

        if (Yii::$app->request->post()) {

            $event_red_id = Yii::$app->request->post('event_reg');
            $subscriber = EventSubscriptions::findOne(['id' => $event_red_id]);
            $invoice = null;

            if($subscriber <> null){
                $invoice = Invoices::findOne($subscriber->gen_invoice);
            }
            $ccEmail = (new AppSetting())->getByAttribute('admin_email');
            $event_title = ($subscriber->eventData <> null) ? $subscriber->eventData->title : "";

            $send = (new EmailHelper())->sendInvoiceEmail(trim($subscriber->email), "info@bbgdxb.com", 'Invoice for' . $event_title, 'invoices/event_registartion', [
                'subscriber' => $subscriber,
                'invoice' => $invoice
            ]);
            if ($send) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }


    /**
     * @param $event_id
     * @param string $mode
     * @return string
     */
    public function actionEventSubscribersBadges($event_id, $mode = "")
    {
        if ($event_id) {

            $subscribers = EventSubscriptions::find()->orderBy([
                'lastname' => SORT_ASC,
            ])->where(['event_id' => $event_id])->all();

            if ($mode == 'download') {
                $pdf = Yii::$app->pdf; // or new Pdf();
                $mpdf = $pdf->api; // fetches mpdf api
                $mpdf->format = Pdf::FORMAT_A4;
                $mpdf->orientation = Pdf::ORIENT_PORTRAIT;
                $mpdf->cssFile = '/ubold/assets/css/custom.css';

                $mpdf->WriteHtml($this->renderPartial('register_event/download_badge', ['subscribers' => $subscribers, 'type' => 'pdf'])); // call mpdf write html

                return $mpdf->Output('Badges', 'I'); // call the mpdf api output as needed

            } else {

                return $this->renderPartial('register_event/badge', [
                    'subscribers' => $subscribers
                ]);
            }
        }
    }

    /**
     * @param $subscriber
     * @return mixed
     */
    public function actionEventSubscriberBadge($subscriber)
    {
        if ($subscriber) {

            $data = EventSubscriptions::findOne(['id' => $subscriber]);

            $pdf = Yii::$app->pdf; // or new Pdf();
            $mpdf = $pdf->api; // fetches mpdf api
            $mpdf->format = Pdf::FORMAT_A4;
            $mpdf->orientation = Pdf::ORIENT_PORTRAIT;
            $mpdf->cssFile = '/ubold/assets/css/custom.css';

            $mpdf->WriteHtml($this->renderPartial('register_event/single-badge',
                ['detail' => $data, 'type' => 'pdf'])); // call mpdf write html

            return $mpdf->Output('Badges', 'I'); // call the mpdf api output as needed

        }
    }

    /**
     *
     */
    public function actionSendRegInvoice()
    {
        if (Yii::$app->request->post()) {

            $data = EventSubscriptions::findOne(['id' => Yii::$app->request->post('event_reg')]);
            $send = (new EmailHelper())->sendEmail($data->email, [], 'Event Registration Invoice', 'invoices/event_registartion', ['subscriber' => $data]);

            if ($send) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    /**
     * @param string $subscription
     * @param string $status
     */
    public function actionChangeEventAttendance()
    {
        if (Yii::$app->request->post()) {

            $model = EventSubscriptions::findOne(Yii::$app->request->post('id'));
            $model->attended = Yii::$app->request->post('attended');

            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'active' => $model->getErrors()]));
            } else {

                $label = '<span class="label label-primary"><span class="fa fa-thumbs-up"></span></span>';
                $class = 'attended-false change_attend_status';
                $title = 'Change to No Show';

                if (!$model->attended) {

                    $label = '<span class="label label-danger"><span class="fa fa-thumbs-down"></span></span>';
                    $class = 'attended-true change_attend_status';
                    $title = 'Change to Attended';

                }

                $html = Html::a($label, '#', ['class' => $class, 'title' => $title, 'id' => $model->id]);

                print_r(Json::encode([
                    'status' => 'success',
                    'html' => $html
                ]));
            }
        }
    }

    /**
     *
     */
    public function actionChangeRegStatus()
    {
        if (Yii::$app->request->post()) {

            $model = EventSubscriptions::findOne(Yii::$app->request->post('id'));
            $model->status = Yii::$app->request->post('status');

            if (Yii::$app->request->post('adjustment')) {

                $userFinance = new UserFinancials();

                $userFinance->user_id = ($model->user_id) ? $model->user_id : $model->registered_by;
                $userFinance->invoice_id = $model->gen_invoice;
                $userFinance->amount = $model->fee_paid;
                $userFinance->description = Yii::$app->request->post("note");

                // TODO: if type is refund, make request to payment gateway for refund
                $userFinance->type = Yii::$app->request->post('type');

                if (!$userFinance->save()) {
                    print_r($userFinance->getErrors());
                } else {
                    echo 1;
                    return true;
                }
            }

            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'errors' => $model->getErrors()]));
            } else {
                echo 1;
                return true;
            }
        }
    }

    /**
     *
     */
    public function actionChangePaymentStatus()
    {
        if (Yii::$app->request->post()) {

            $model = EventSubscriptions::findOne(Yii::$app->request->post('id'));
            $model->payment_status = Yii::$app->request->post('payment_status');

            if($model->payment_status == "paid") {
                $model->status = "approved";
            }else{
                $model->status = "pending";
            }


            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'active' => $model->getErrors()]));
            } else {

                $invoice = Invoices::findOne($model->gen_invoice);
                if($model->payment_status == "paid") {
                    $invoice->payment_status = "paid";
                }else{
                    $invoice->payment_status = "unpaid";
                }
                $invoice->save();
                echo 1;
            }
        }
    }

    /**
     *
     */
    public function actionChangeAttendStatus()
    {
        if (Yii::$app->request->post()) {

            $model = EventSubscriptions::findOne(Yii::$app->request->post('id'));
            $model->attended = Yii::$app->request->post('attended');

            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'active' => $model->getErrors()]));
            } else {
                echo 1;
            }

        }
    }

    /**
     *
     */
    public function actionChangePaymentMethod()
    {
        if (Yii::$app->request->post()) {

            $model = EventSubscriptions::findOne(Yii::$app->request->post('id'));
            $model->paymentMethod = Yii::$app->request->post('payment_method');

            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'active' => $model->getErrors()]));
            } else {
                echo 1;
            }
        }
    }

    /**
     * @param string $subscription
     * @param string $status
     */
    public function actionChangeEventStatus()
    {
        if (\Yii::$app->request->post()) {

            $model = Events::findOne(['id' => \Yii::$app->request->post('id')]);

            $model->active = \Yii::$app->request->post('active');

            if (!$model->save()) {
                print_r(Json::encode(['status' => 'error', 'active' => $model->getErrors()]));
            } else {

                $label = '<span class="label label-danger"><span class="fa fa-eye-slash"></span></span>';

                $data = [
                    'title' => 'Publish',
                    'class' => 'publish change_event_status',
                    'data-pjax' => '0',
                    'id' => $model->id
                ];


                if ($model->active == "1") {

                    $label = '<span class="label label-success"><span class="fa fa-eye"></span></span>';
                    $data = [
                        'title' => 'Unpublish',
                        'class' => 'unpublish change_event_status',
                        'data-pjax' => '0',
                        'id' => $model->id
                    ];
                }

                print_r(Json::encode([
                    'status' => 'success',
                    'html' => Html::a($label, "#", $data)
                ]));
            }
        }
    }

    /**
     *
     */
    public function actionBulkAttend()
    {

        if (Yii::$app->request->post()) {


            if (Yii::$app->request->post('bulk_attend') == 'print_badges') {

                if (Yii::$app->request->post('selection')) {

                    $subscribers = EventSubscriptions::find()->orderBy([
                        'lastname' => SORT_ASC,
                    ])->where(['IN', 'id', Yii::$app->request->post('selection')])->all();


                    $pdf = Yii::$app->pdf; // or new Pdf();
                    $mpdf = $pdf->api; // fetches mpdf api
                    $mpdf->format = Pdf::FORMAT_A4;
                    $mpdf->orientation = Pdf::ORIENT_PORTRAIT;
                    $mpdf->cssFile = '/ubold/assets/css/custom.css';

                    $mpdf->WriteHtml($this->renderPartial('register_event/download_badge', ['subscribers' => $subscribers, 'type' => 'pdf'])); // call mpdf write html
                    return $mpdf->Output('Badges', 'I'); // call the mpdf api output as needed

                }

            } else {

                $event = Yii::$app->request->post('event');
                if (Yii::$app->request->post('selection')) {

                    foreach (Yii::$app->request->post('selection') as $id) {

                        $eventSub = EventSubscriptions::findOne($id);
                        $eventSub->attended = Yii::$app->request->post('bulk_attend');
                        $eventSub->save();
                    }

                }

                return $this->redirect('/event/event-registered-list?event=' . $event);
            }
        }
        return $this->redirect('/event/');
    }

    public function actionImportEvent()
    {
        $events = TblEvent::find()->where(['>','id',443])->all();

        foreach ($events as $result ){

            $new_event = Events::findOne($result->id);

            if($new_event == null){

                $data = new Events();

                $startTime = "";
                $endTime = "";

                if ($result->event_endDate <> "") {

                    $r = explode('to', $result->event_endDate);

                    if (count($r) > 0) {

                        //$new_time = DateTime::createFromFormat('h:i A', $r[0]);
                        $startTime = date('H:i:s', strtotime($r[0]));
                        $endTime = date('H:i:s', strtotime($r[1]));
                    }

                }

                $committee_fee = 0;
                $honourary_fee = 0;
                $sponsor_fee = 0;
                $focus_chair_fee = 0;

                if ($result->committee_fee == "member_fee") {
                    $committee_fee = $result->member_fee;
                }

                if ($result->honourary_fee == "member_fee") {
                    $honourary_fee = $result->member_fee;
                }

                if ($result->sponsor_fee == "member_fee") {
                    $sponsor_fee = $result->member_fee;
                }

                if ($result->focus_chair_fee == "member_fee") {
                    $focus_chair_fee = $result->member_fee;
                }

                $active = 0;

                if ($result->active == "true") {
                    $active = 1;
                }

                $data->id = $result->id;
                $data->type = $result->type;
                $data->country_id = $result->country_id;
                $data->city_id = $result->city_id;
                $data->address = $result->address;
                $data->title = $result->title;
                $data->venue = $result->other_region;
                $data->tba = $result->tba;
                $data->registration_start = $result->registration_start;
                $data->registration_end = $result->registration_end;
                $data->cancellation_tillDate = $result->registration_end;
                $data->event_startDate = $result->event_startDate;
                $data->event_endDate = $result->event_startDate;
                $data->event_startTime = $startTime;
                $data->event_endTime = $endTime;
                $data->short_description = $result->short_description;
                $data->group_size = $result->group_size;
                $data->fb_gallery = $result->fb_gallery;
                $data->description = $result->description;
                $data->member_fee = $result->member_fee;
                $data->nonmember_fee = $result->nonmember_fee;
                $data->committee_fee = (string) $committee_fee;
                $data->honourary_fee = (string) $honourary_fee;
                $data->sponsor_fee = (string) $sponsor_fee;
                $data->focus_chair_fee = (string) $focus_chair_fee;
                $data->display_price = $result->display_price;
                $data->userNote = $result->userNote;
                $data->status = $result->status;
                $data->quizNight = $result->quizNight;
                $data->active = (string) $active;
                $data->create_date	 = $result->create_date;
                $data->longitude = $result->longitude;
                $data->latitude = $result->latitude;
                $data->currency = $result->currency;
                if(!$data->save()){
                    echo '<pre>';
                    print_r($data->getErrors());
                    die;
                }

            }
        }
    }
}

