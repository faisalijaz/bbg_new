<?php

namespace backend\controllers;

use backend\models\NewsSearch;
use common\models\AccountCompany;
use common\models\Members;
use common\models\News;
use common\models\TblNews;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->re_post) {
                $model->created_at = strtotime(date('Y-m-d'));
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionImportNews()
    {

        $News = TblNews::find()->all();
        $path = '/uploads/news/';
        foreach ($News as $news) {

            $_news = News::findOne(['id' => $news->id]);

            if ($_news == null) {
                $_news = new News();
            }

            $_news->id = $news->id;
            $_news->title = $news->title;
            $_news->image = $path . $news->image;
            $_news->description = $news->description;
            $_news->news_type = "member";
            if ($news->isMember <> "1") {
                $_news->news_type = "topical";
            }
            $_news->link = $news->link;
            $_news->created_at = strtotime($news->createdDate);
            $_news->updated_at = strtotime($news->modifiedDate);
            $_news->isPublished = $news->isActive;

            if (!$_news->save()) {
                echo '<pre>';
                print_r($_news->getErrors());
                echo '</pre>';
            } else {
                echo '<span style="color: green;">' . $_news->id . '</span><br/>';
            }

        }
    }

    public function actionRelatedTo()
    {
        $out = [];
        $selected = '';

        $selected_rel = 0;
        if (isset($_POST['depdrop_all_params']['selected_rel']) && $_POST['depdrop_all_params']['selected_rel'] > 0 ) {
            $selected_rel = $_POST['depdrop_all_params']['selected_rel'];
        }


        if (isset($_POST['depdrop_parents'])) {

            $ids = $countryId = $_POST['depdrop_parents'][0];

            if ($ids == "company") {

                $companies = AccountCompany::find()->all();
                $i = 1;


                if ($companies <> null) {
                    foreach ($companies as $company) {
                        if ($i) {
                            if($selected_rel == $company->id) {
                                $selected = $company->id;
                                $i = 0;
                            }

                        }
                        $out[] = ['id' => $company->id, 'name' => $company->name];
                    }
                }
            }

            if ($ids == "member") {

                $members = Members::find()->all();
                $i = 1;
                $selected = '';

                if ($members <> null) {
                    foreach ($members as $member) {
                        if ($i) {
                            $selected = $member->id;
                            $i = 0;
                        }
                        $out[] = ['id' => $member->id, 'name' => $member->first_name . " " . $member->last_name];
                    }
                }
            }

            return Json::encode(['output' => $out, 'selected' => $selected]);
        }

        return Json::encode(['output' => $_POST, 'selected' => '']);
        return;
    }
}
