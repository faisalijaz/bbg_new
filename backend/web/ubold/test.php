
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="user-scalable=no, width=device-width">
    <link href='http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css' rel='stylesheet'/>
    <link href='style.css' rel='stylesheet'/>
    <!-- jQuery Plugin - upload multiple images ajax -->
    <script src='http://code.jquery.com/jquery.js'></script>
    <script src='http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
    <!-- jQuery Plugin - upload multiple images ajax -->
    <script src='js/uploadImages.js'></script>
    <script>
        $(function () {

            /* Check File API compatibility */
            if (!$.fileReader()) {
                alert("File API is not supported on your browser");
            }
            else {
                console.log("File API is supported on your browser");
            }

            $("#form").removeClass('hidden');
            $("#message-div").addClass('hidden');

            /* createImage Event */
            $(document).on("createImage", function (e) {
                console.log(e.file.name);
                console.log(e.file.size);
                console.log(e.file.type);
            });

            /* deleteImage Event */
            $(document).on("deleteImage", function (e) {
                console.log(e.file.name);
                console.log(e.file.size);
                console.log(e.file.type);
                /* if not there are images, the button is disabled */
                if ($("#upload-preview").countImages() == 0) {
                    $("#btn").attr("disabled", "disabled");
                }
            });

            /* Prevent form submit */
            $("#form").on("submit", function (e) {
                e.preventDefault();
            });

            /* Preview and Validate */
            $("#form input[type='file']").on("change", function () {

                $("#upload-preview").uploadImagesPreview("#form",
                    {
                        image_type: "jpg|jpeg|png|gif",
                        min_size: 24,
                        max_size: (1024 * 1024 * 3), /* 3 Mb */
                        max_files: 10
                    }, function () {
                        switch (__errors__upload__) /* Check the possibles erros */
                        {
                            case 'ERROR_CONTENT_TYPE':
                                alert("Error content type");
                                break;
                            case 'ERROR_MIN_SIZE':
                                alert("Error min size");
                                break;
                            case 'ERROR_MAX_SIZE':
                                alert("Error max size");
                                break;
                            case 'ERROR_MAX_FILES':
                                alert("Error max files");
                                break;
                            default:
                                $("#btn").removeAttr("disabled");
                                break; /* Activate the button Form */
                        }
                    });
            });

            /* Send form */
            $("#SubmitBtn").on("click", function (e) {
                e.preventDefault();

                /*images are required */

                var name = $("#name").val();
                var phone = $("#phone").val();
                var email = $("#email").val();
                var usermessage = $("#usermessage").val();

                if (!ValidateEmail(email)) {
                    $("#message").html('<p class="alert alert-danger">Email format is not correct! </p>');

                    return false;
                }

                if (name == "") {
                    $("#message").html('<p class="alert alert-danger">Please provide your name! </p>');

                    return false;
                }
                if (phone == "") {
                    $("#message").html('<p class="alert alert-danger">Please provide your Phone Number! </p>');
                    return false;
                }
                if (email == "") {
                    $("#message").html('<p class="alert alert-danger">Please provide your Email Address! </p>');
                    return false;
                }

                if (usermessage == "") {
                    $("#message").html('<p class="alert alert-danger">Message cannot be empty! </p>');
                    return false;
                }


                $("#SubmitBtn").attr("disabled", true);

                $("#upload-preview").uploadImagesAjax("ajax.php", {
                    params: {data: $("#form").serialize()}, /* Set the extra parameters here */
                    beforeSend: function () {
                        console.log("Sending ...");
                    },
                    success: function (data) {
                        $("#form").addClass('hidden');
                        $("#message-div").removeClass('hidden');
                        $("#message-div").html('<span class="success-msg">Thanks for Your Request, One of Our Consultant will be in touch with you shortly!</span>');
                    },
                    error: function (e) {

                        $("#form").addClass('hidden');
                        $("#message-div").removeClass('hidden');
                        $("#message-div").html(e.statusText);
                        $("#message").html('<p class="error-msg">' + e.statusText + '</p>');

                        setTimeout(function () {
                            $("#form").removeClass('hidden');
                            $("#message-div").addClass('hidden');
                            $("#SubmitBtn").attr("disabled", false);
                        }, 5000);

                    },
                    complete: function () {
                        console.log("Completed");
                        setTimeout(function () {
                            $("#message").html('');
                            $("#SubmitBtn").attr("disabled", false);
                        }, 5000);


                    }
                });
            });
        });

        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        }

    </script>

    <style>
        .img-responsive {
            max-width: 150px;
        }
    </style>

</head>
<body>

<div class="container" style="padding-top: 10px;">
    <div id="message-div" class="hidden"></div>
    <form method="post" id="form" class="form-inline" enctype="multipart/form-data">

        <div class="form-group col-md-12 col-lg-12 col-xs-12 col-sm-12"><h2>Share your ideas with us</h2></div>

        <div class="form-group col-md-12 col-lg-12  col-xs-12 col-sm-12">
            <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
        </div>
        <div class="form-group col-lg-12 col-md-6 col-xs-12 col-sm-12">
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
        </div>
        <div class="form-group col-lg-12 col-md-6 col-xs-12 col-sm-12">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
        </div>
        <div class="form-group col-md-12 col-xs-12 col-sm-12">
            <textarea class="form-control" name="message" id="usermessage" placeholder="Message"></textarea>
        </div>
        <div class="image-upload-input col-lg-12 col-md-6 col-xs-12 col-sm-12">
            <div id="upload-preview"></div>
            <div class="form-group ">
                <label for="upload-image">
                    <input type="file" id="upload-image" style="opacity: 0;" multiple
                           class="btn btn-primary form-group col-lg-12 col-md-6 col-xs-12 col-sm-12"/>
                    <span id="btn" class="btn btn-primary glyphicon glyphicon-upload"></span>
                    Upload Files: <span class="badge count-images">0</span>
                </label>
            </div>
        </div>
        <div class="form-group col-md-12">
            <a href="#" id="SubmitBtn" class="col-md-12 form-control btn btn-primary">Submit </a>
        </div>
        <div id="message"></div>
    </form>

</div>
<!-- Show the images preview here -->


</body>

</html>