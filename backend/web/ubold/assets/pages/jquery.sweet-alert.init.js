
/**
* Theme: Ubold Admin Template
* Author: Coderthemes
* SweetAlert
*/

!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        
    //Basic
    $('#sa-basic').click(function(){
        swal("Here's a message!");
    });

    //A title with a text under
    $('#sa-title').click(function(){
        swal("Here's a message!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis")
    });

    //Success Message
    $('#sa-success').click(function(){
        swal("Good job!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis", "success")
    });

    //Warning Message
    $('#sa-warning').click(function(){
        var url = $(this).attr('value');
        var title = $(this).attr('title');
        var dataGridId = $(this).attr('gridId');
        swal({
            title: "Are you sure?",
            text: title,
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                // submit form
                setTimeout(function(){
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {},
                        success: function (response) {
                            if(response.status == 'OK') {
                                swal("Deleted!", response.message, "success");

                                if(dataGridId != 'undefined') {
                                    $.pjax.reload({container:'#'+dataGridId});
                                }
                            }

                            if(response.status == 'ERROR') {
                                swal("Cancelled", response.message, "error");
                            }
                        }
                    });

                }, 2000);

            } else {
                swal("Cancelled", "Address not deleted :)", "error");
            }
        });
    });

    //Parameter
    $('#sa-params').click(function(){
        var url = $(this).attr('value');
        var title = $(this).attr('label');
        swal({
            title: "Are you sure?",   
            text: title,
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
    });

    //Custom Image
    $('#sa-image').click(function(){
        swal({   
            title: "Sweet!",   
            text: "Here's a custom image.",   
            imageUrl: "assets/plugins/sweetalert/thumbs-up.jpg" 
        });
    });

    //Auto Close Timer
    $('#sa-close').click(function(){
         swal({   
            title: "Auto close alert!",   
            text: "I will close in 2 seconds.",   
            timer: 2000,   
            showConfirmButton: false 
        });
    });


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);

var alertBox = {
    'delete' : function (xId) {
        //Warning Message
        var url = $('#'+xId).attr('value');
        var title = $('#'+xId).attr('title');
        var dataGridId = $('#'+xId).attr('gridId');
        swal({
            title: "Are you sure?",
            text: title,
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                // submit form
                setTimeout(function(){
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {},
                        success: function (response) {
                            if(response.status == 'OK') {
                                swal("Deleted!", response.message, "success");

                                if(dataGridId != 'undefined') {
                                    $.pjax.reload({container:'#'+dataGridId});
                                }
                            }

                            if(response.status == 'ERROR') {
                                swal("Cancelled", response.message, "error");
                            }
                        }
                    });

                }, 2000);

            } else {
                swal("Cancelled", "Address not deleted :)", "error");
            }
        });
    }
}
