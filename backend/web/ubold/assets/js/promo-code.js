var promo = {

    'apply': function (url) {

        var promoCode = $('#promo-code').val();

        if(promoCode === '') {
            promo.alert('Please enter promo code', 'error');
            return;
        }

        // submit form
        $.ajax({
            url: url,
            type: 'post',
            data: { promoCode: promoCode },
            success: function (response) {
                if(response.status == 'OK') {
                    promo.alert(response.message, 'success');
                    $.pjax.reload({container:'#cart-widget'});
                }

                if(response.status == 'ERROR') {
                    promo.alert(response.message, 'error');
                }
            }
        });
    },

    'cancel': function (url) {

        // submit form

        $.ajax({
            url: url,
            type: 'post',
            data: {},
            success: function (response) {
                if(response.status == 'OK') {

                    promo.alert(response.message, 'success');
                    $.pjax.reload({container:'#cart-widget'});
                }

                if(response.status == 'ERROR') {
                    promo.alert(response.message, 'error');
                }
            }
        });

    },

    'alert': function(message, type) {
        swal({
            title: type,
            text: message,
            timer: 5000,
            type: type,
            html: true,
            showConfirmButton: true
        });

    }
};