$(document).ready(function () {

    $("#add_special_event").click(function (e) {
        e.preventDefault();
        var index = $('.special_events_list').length;
        var html = '<div class="form-group special_events_list" style="margin: 10px;" >' +
            '           <div class="col-md-5">' +
            '               <input type="text" class="form-control" placeholder="Enter Fee Type" name="Events[specialEvents][' + index + '][fee_type]" value="">' +
            '           </div>' +
            '           <div class="col-md-5">' +
            '               <input type="text" class="validate_float_number form-control" placeholder="Enter Fee Amount" name="Events[specialEvents][' + index + '][amount]" value="">' +
            '           </div>' +
            '           <div class="col-md-2"><a href="#" class="remove_special_event btn btn-sm btn-danger pull-left" style="margin: 10px;" >' +
            '               <i class="fa fa-minus-circle" aria-hidden="true"></i> Remove' +
            '           </a></div>' +
            '       </div><div class="clearfix"></div> ';

        $("#special_events_form").append(html);

    });

    $('body').on('click', 'a.remove_special_event', function(e) {
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
    });
});



