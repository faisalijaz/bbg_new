(function() {
  'use strict';

  angular
    .module('tourApp')
    .factory('categoryService', categories);

  /** @ngInject */
  function categories($log, $http) {

    var apiHost = '/categories/';

    var service = {
      apiHost: apiHost,
      getCategories: getCategories
    };

    return service;

    function getCategories() {

      return $http.get(apiHost + 'list')
        .then(getContributorsComplete)
        .catch(getContributorsFailed);

      function getContributorsComplete(response) {
        return response.data;
      }

      function getContributorsFailed(error) {
        $log.error('XHR Failed for getContributors.\n' + angular.toJson(error.data, true));
      }
    }
  }
})();
