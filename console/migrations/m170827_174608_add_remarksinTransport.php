<?php

use yii\db\Migration;

class m170827_174608_add_remarksinTransport extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%assign_transport}}', 'remarks', 'TEXT');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%assign_transport}}', 'remarks');
    }


}
