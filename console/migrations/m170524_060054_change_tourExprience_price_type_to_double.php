<?php

use yii\db\Migration;

class m170524_060054_change_tourExprience_price_type_to_double extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%tour_experiences}}', 'price', 'DOUBLE DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_experiences}}', 'price');
    }

}
