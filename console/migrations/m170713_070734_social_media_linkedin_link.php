<?php

use yii\db\Migration;

class m170713_070734_social_media_linkedin_link extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%settings}}', 'linkedin','VARCHAR(255)');
        $this->addColumn('{{%settings}}', 'pinterest','VARCHAR(255)');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%settings}}', 'linkedin');
        $this->dropColumn('{{%settings}}', 'pinterest');
    }
}
