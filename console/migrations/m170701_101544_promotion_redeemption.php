<?php

use yii\db\Migration;

class m170701_101544_promotion_redeemption extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%promotion_redemption}}', [
            'id' => $this->primaryKey(),
            'promotion_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->null(),
        ], $tableOptions);

        $this->createIndex('promotion_redeem', '{{%promotion_redemption}}', 'promotion_id');
        $this->addForeignKey('promotion_redeem_fk', '{{%promotion_redemption}}', 'promotion_id', '{{%promotions}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('promotion_redeem_account', '{{%promotion_redemption}}', 'account_id');
        $this->addForeignKey('promotion_redeem_account_fk', '{{%promotion_redemption}}', 'account_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable('{{%promotion_redemption}}');
    }
}
