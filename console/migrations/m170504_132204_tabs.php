<?php

use yii\db\Migration;

class m170504_132204_tabs extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tabs}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'description' => $this->text(),
            'status' => "ENUM('active', 'In-Active') DEFAULT 'In-Active'",

        ], $tableOptions);

        $this->createIndex('tabs_page', '{{%tabs}}', 'page_id');
        $this->addForeignKey('tabs_page_fk', '{{%tabs}}', 'page_id', '{{%cms_pages}}', 'id', 'CASCADE', 'CASCADE');
    }
    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tabs}}');
    }
}
