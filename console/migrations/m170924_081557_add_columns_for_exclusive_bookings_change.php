<?php

use yii\db\Migration;

class m170924_081557_add_columns_for_exclusive_bookings_change extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%tour_bookings}}', 'isExclusive');
        $this->addColumn('{{%tour_booking_details}}', 'isExclusive', 'ENUM("0","1") DEFAULT "0"');
    }

    public function safeDown()
    {

    }
 
}
