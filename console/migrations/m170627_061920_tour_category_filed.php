<?php

use yii\db\Migration;

class m170627_061920_tour_category_filed extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tours}}', 'category_id', 'INT(11)');

        $this->createIndex('category_id', '{{%tours}}', 'category_id');
        $this->addForeignKey('category_id_fk', '{{%tours}}', 'category_id', '{{%categories}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropColumn('{{%tours}}', 'category_id');
    }

}
