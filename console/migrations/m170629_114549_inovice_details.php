<?php

use yii\db\Migration;

class m170629_114549_inovice_details extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%invoice_details}}', [
            'id' => $this->primaryKey(),
            'invoice_id' => $this->integer()->notNull(),
            'booking_ref' => $this->integer()->notNull(),
            'amount' => $this->double()->notNull(),
        ], $tableOptions);

        $this->createIndex('booking_ref', '{{%invoice_details}}', 'booking_ref');
        $this->addForeignKey('booking_ref_fk', '{{%invoice_details}}', 'booking_ref', '{{%tour_bookings}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable('{{%invoice_details}}');
    }
}
