<?php

use yii\db\Migration;

/**
 * Class m170521_103021_add_seat_limits_in_bookings
 */
class m170521_103021_add_seat_limits_in_tours extends Migration
{
    /**
     * Create a new column for seat limits in bookings table
     */
    public function safeUp()
    {
        $this->addColumn('{{%tours}}', 'seats', 'INT(11) DEFAULT 0');
    }

    /**
     * Drop column for seat limits in bookings table
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tours}}', 'seats');
    }
}
