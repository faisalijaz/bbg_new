<?php

use yii\db\Migration;

class m170711_052726_awards_certificates_model extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%awards}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'short_description' => $this->string(150),
            'description' => $this->string(255),
            'image' => $this->string(255),
            'sort_order' => $this->string(15)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%awards}}');
    }
}
