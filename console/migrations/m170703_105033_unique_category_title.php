<?php

use yii\db\Migration;

class m170703_105033_unique_category_title extends Migration
{
    public function safeUp()
    {
        $this->createIndex('uniqueCategory', '{{%categories}}', 'title', 'title');
    }

    public function safeDown()
    {
        $this->dropIndex('uniqueCategory', '{{%categories}}');
    }

}
