<?php

use yii\db\Migration;

/**
 * Class m170506_080731_tour_bookings
 */
class m170506_080731_tour_bookings extends Migration
{
    /**
     * Crate a new table tour bookings
     * @return mixed
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_bookings}}', [
            'id' => $this->primaryKey(),
            'booking_by' => "ENUM('agent', 'customer') DEFAULT 'customer'",
            'price' => $this->float()->notNull(),
            'discount' => $this->float()->defaultValue(0),
            'total_sum' => $this->float()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'payment_type' => "ENUM('cach_on_deliver', 'credit_card_online','credit_card_on_deliver','reward_points') DEFAULT 'cach_on_deliver'",
            'payment_status' => "ENUM('pending', 'recived') DEFAULT 'pending'",
            'account_id' => $this->integer()->notNull(),
            'status' => "ENUM('created', 'cancelled', 'deleted', 'confirmed', 'saved') DEFAULT 'created'",
            'special_instruction' => $this->string()
        ], $tableOptions);

        $this->createIndex('account_id', '{{%tour_bookings}}', 'account_id');
        $this->addForeignKey('account_id_fk', '{{%tour_bookings}}', 'account_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * drop table tour bookings
     * @return string
     */
    public function safeDown()
    {
        $this->dropTable('{{%tour_bookings}}');
    }
}
