<?php

use yii\db\Migration;

class m170426_155000_tour_price_mapping extends Migration
{
    /**
     * Up function will add migraiton availabilities into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_price_mapping}}', [
            'id' => $this->primaryKey(),
            'tour_id' => $this->integer()->notNull(),
            'price' => $this->double()->notNull(),
            'date' => $this->date()->notNull(),
        ], $tableOptions);

        $this->createIndex('tour_mapping', '{{%tour_price_mapping}}', 'tour_id');
        $this->addForeignKey( 'tour_mapping_fk', '{{%tour_price_mapping}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tour_price_mapping}}');
    }
}
