<?php

use yii\db\Migration;

/**
 * Class m170521_083722_change_agnent_parent_name
 */
class m170521_083722_change_agnent_parent_name extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        // Drop Column agent_parent
        $this->dropColumn('{{%accounts}}', 'agent_parent');

        // Add column parent_id
        $this->addColumn('{{%accounts}}', 'parent_id', 'INT(11) DEFAULT 0');

    }

    public function safeDown()
    {
        $this->dropColumn('{{%accounts}}', 'parent_id');
    }
}
