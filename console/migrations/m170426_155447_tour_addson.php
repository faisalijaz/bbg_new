<?php

use yii\db\Migration;

class m170426_155447_tour_addson extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_addons}}', [
            'id' => $this->primaryKey(),
            'addon_id' => $this->integer()->notNull(),
            'tour_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'image' => $this->string(),
            'description' => $this->string(),
            'sort_order' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('addon', '{{%tour_addons}}', 'addon_id');
        $this->addForeignKey( 'addon_fk', '{{%tour_addons}}', 'addon_id', '{{%addons}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('tour_addon', '{{%tour_addons}}', 'tour_id');
        $this->addForeignKey( 'tour_addon_fk', '{{%tour_addons}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tour_addons}}');
    }
}
