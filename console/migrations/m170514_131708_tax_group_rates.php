<?php

use yii\db\Migration;

class m170514_131708_tax_group_rates extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tax_group_rates}}', [
            'id' => $this->primaryKey(),
            'tax_group_id' => $this->integer()->notNull(),
            'tax_id' => $this->integer()->notNull(),
            'sort_order' => $this->smallInteger()->defaultValue(0),

        ], $tableOptions);


        $this->createIndex('tax_group', '{{%tax_group_rates}}', 'tax_group_id');
        $this->addForeignKey('tax_group_FK', '{{%tax_group_rates}}', 'tax_group_id', 'tax_groups', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('tax_rates', '{{%tax_group_rates}}', 'tax_id');
        $this->addForeignKey('tax_rates_FK', '{{%tax_group_rates}}', 'tax_id', 'tax_rates', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tax_group_rates}}');
    }
}
