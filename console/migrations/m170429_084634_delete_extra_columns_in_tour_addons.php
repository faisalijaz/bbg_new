<?php

use yii\db\Migration;

class m170429_084634_delete_extra_columns_in_tour_addons extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->dropColumn('{{%tour_addons}}', 'title');
        $this->dropColumn('{{%tour_addons}}', 'description');
        $this->dropColumn('{{%tour_addons}}', 'image');
    }


    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->addColumn('{{%tour_addons}}', 'title', 'VARCHAR(255)');
        $this->addColumn('{{%tour_addons}}', 'description', 'VARCHAR(255)');
        $this->addColumn('{{%tour_addons}}', 'image', 'VARCHAR(255)');
    }
}
