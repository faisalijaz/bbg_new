<?php

use yii\db\Migration;

class m170629_143222_promotions extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%promotions}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull(),
            'title' => $this->string()->null(),
            'type' => "ENUM('fixed', 'percentage') DEFAULT 'fixed'",
            'value' => $this->double()->notNull(),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
            'start_time' => $this->time(),
            'end_time' => $this->time(),
            'is_customer_specific' => $this->integer(1)->defaultValue(0),
            'status' => $this->integer(1)->defaultValue(0),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%promotions}}');
    }
}
