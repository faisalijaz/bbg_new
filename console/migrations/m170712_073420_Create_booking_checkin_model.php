<?php

use yii\db\Migration;

class m170712_073420_Create_booking_checkin_model extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_checkin}}', [
            'id' => $this->primaryKey(),
            'booking_ref' => $this->integer()->null(),
            'tour_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->null(),
            'person' => $this->integer()->null(),
            'checked_in' => 'ENUM("yes", "no")',
            'date' => $this->date(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%tour_checkin}}');
    }
}
