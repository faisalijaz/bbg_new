<?php

use yii\db\Migration;

/**
 * Class m170615_071432_setFlag_isMapped_to_bookingsDetails
 */
class m170615_071432_setFlag_isMapped_to_bookingsDetails extends Migration
{
    /**
     * @return mixed
     */
    public function up()
    {
        $this->addColumn('{{%tour_booking_details}}', 'isMapped', 'INT(11) DEFAULT 0 ');
    }

    /**
     * @return mixed
     */
    public function down()
    {
        $this->dropColumn('{{%tour_booking_details}}', 'isMapped');
    }
}
