<?php

use yii\db\Migration;

class m170515_120536_add_groupid_inTour extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tours}}', 'tax_group_id', 'INT(11) NOT NULL DEFAULT 0');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%tours}}', 'tax_group_id');
    }
}
