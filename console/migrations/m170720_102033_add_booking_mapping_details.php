<?php

use yii\db\Migration;

class m170720_102033_add_booking_mapping_details extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_booking_mappings}}', 'persons', 'INT(11) DEFAULT 0');
        $this->addColumn('{{%tour_booking_mappings}}', 'checkin_date', 'DATE');
        $this->addColumn('{{%tour_booking_mappings}}', 'date_created', 'DATETIME');
    }

    public function safeDown()
    {
        $this->addColumn('{{%tour_booking_mappings}}', 'persons');
        $this->addColumn('{{%tour_booking_mappings}}', 'checkin_date');
        $this->addColumn('{{%tour_booking_mappings}}', 'date_created');
    }

}
