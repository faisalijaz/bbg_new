<?php

use yii\db\Migration;

class m170805_081234_change_payment_date_type_in_payments extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%payments}}', 'payment_date', 'DATE');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%payments}}', 'payment_date');
    }

}
