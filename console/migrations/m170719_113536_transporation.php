<?php

use yii\db\Migration;

class m170719_113536_transporation extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%destination_resources}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'destination_assigned' => $this->integer()->notNull(),
            'description' => $this->string()->null(),
            'image' => $this->string()->null(),
            'status' => "ENUM('active', 'In-Active')",
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->createTable('{{%destination_resource}}');
    }
}
