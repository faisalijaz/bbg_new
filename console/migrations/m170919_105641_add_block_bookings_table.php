<?php

use yii\db\Migration;

class m170919_105641_add_block_bookings_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%block_bookings_dates}}', [
            'id' => $this->primaryKey(),
            'tour_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
        ], $tableOptions);

        $this->createIndex('disable_booking_tour_id', '{{%block_bookings_dates}}', 'tour_id');
        $this->addForeignKey('disable_booking_tour_id_fk', '{{%block_bookings_dates}}', 'tour_id', '{{%tours}}', 'id');


    }

    public function safeDown()
    {

    }
}
