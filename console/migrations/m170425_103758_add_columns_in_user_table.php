<?php

use yii\db\Migration;

class m170425_103758_add_columns_in_user_table extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function safeUp()
    {
        $this->addColumn("user", "first_name", "VARCHAR(50) NOT NULL AFTER `email` ");
        $this->addColumn("user", "last_name", "VARCHAR(50) NOT NULL AFTER `first_name` ");
        $this->addColumn("user", "address", "VARCHAR(255) NOT NULL AFTER `last_name` ");
        $this->addColumn("user", "phone_number", "VARCHAR(15) NOT NULL AFTER `address` ");
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function safeDown()
    {
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'last_name');
        $this->dropColumn('user', 'address');
        $this->dropColumn('user', 'phone_number');
    }
}
