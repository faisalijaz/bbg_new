<?php

use yii\db\Migration;

class m170517_094344_update_tour_bookings_details_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_booking_details}}', 'check_in', 'DATE');
        $this->addColumn('{{%tour_booking_details}}', 'adults', 'INT(11) NOT NULL DEFAULT 1');
        $this->addColumn('{{%tour_booking_details}}', 'childern', 'INT(11) NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_booking_details}}', 'check_in');
        $this->dropColumn('{{%tour_booking_details}}', 'adults');
        $this->dropColumn('{{%tour_booking_details}}', 'childern');
    }
}
