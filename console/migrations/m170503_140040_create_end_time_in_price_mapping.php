<?php

use yii\db\Migration;

class m170503_140040_create_end_time_in_price_mapping extends Migration
{
    /* @up
     * @migration that will create a new column in price mapping table
     * @attributes {end_time}
     * @engine is InnoDB
     * */
    // Use safeUp/safeDown to run migration code within a transaction

    /**
     * Cahnges in tour price mapping model
     * @return mixed
     */
    public function safeUp()
    {
        $this->addColumn('{{%tour_price_mapping}}', 'end_time', 'time');
    }

    /**
     * Cahnges in tour price mapping model
     * @return mixed
     */
    public function safeDown()
    {
        $this->addColumn('{{%tour_price_mapping}}', 'end_time');
    }
}
