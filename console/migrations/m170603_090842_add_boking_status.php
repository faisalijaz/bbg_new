<?php

use yii\db\Migration;

class m170603_090842_add_boking_status extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%tour_bookings}}', 'status', "enum('created','partially-cancelled', 'cancelled','deleted','confirmed','saved') DEFAULT 'created'");
    }

    public function down()
    {
        $this->alterColumn('{{%tour_bookings}}', 'status', "enum('created','cancelled','deleted','confirmed','saved') DEFAULT 'created'");
    }
}
