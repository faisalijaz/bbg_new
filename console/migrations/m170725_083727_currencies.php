<?php

use yii\db\Migration;

class m170725_083727_currencies extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%currencies}}', [
            'id' => $this->primaryKey(),
            'currency' => $this->string()->null(),
            'code' => $this->string()->null(),
            'symbol' => $this->string()->null(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%clients_reviews}}');
    }
}
