<?php

use yii\db\Migration;

class m170517_152440_update_booking_addons_table extends Migration
{
    /**
     * Crate a new table tour booking addons
     * @return string
     */
    public function up()
    {
        // Drop table and create new one

        $this->dropTable('{{%tour_booking_addons}}');

        //Create new table
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_booking_addons}}', [
            'id' => $this->primaryKey(),
            'booking_tour_id' => $this->integer()->notNull(),
            'addon_id' => $this->integer()->notNull(),
            'price' => $this->float()->notNull()
        ], $tableOptions);

        $this->createIndex('booking_tour_id', '{{%tour_booking_addons}}', 'booking_tour_id');
        $this->addForeignKey('booking_tour_id_fk', '{{%tour_booking_addons}}', 'booking_tour_id', '{{%tour_booking_details}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('addon_id', '{{%tour_booking_addons}}', 'addon_id');
        $this->addForeignKey('addon_id_fk', '{{%tour_booking_addons}}', 'addon_id', '{{%addons}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * drop table tour booking addons
     * @return string
     */
    public function down()
    {
        $this->dropTable('{{%tour_booking_addons}}');
    }
}
