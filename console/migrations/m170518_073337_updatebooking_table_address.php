<?php

use yii\db\Migration;

class m170518_073337_updatebooking_table_address extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_bookings}}', 'booking_address', 'INT(11) NULL');

        $this->createIndex('booking_address', '{{%tour_bookings}}', 'booking_address');
        $this->addForeignKey('booking_address_fk', '{{%tour_bookings}}', 'booking_address', '{{%address}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_booking_details}}', 'booking_address');
    }

}
