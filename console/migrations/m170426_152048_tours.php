<?php

use yii\db\Migration;

class m170426_152048_tours extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%tours}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'seo_url' => $this->string()->notNull(),
            'short_description' => $this->string(),
            'sort_order' => $this->integer()->defaultValue(0),
            'description' => $this->text(),
            'price' => $this->double()->notNull(),
            'banner' => $this->string(),
            'location' => $this->string()->notNull(),
            'start_from' => $this->date()->notNull(),
            'end_on' => $this->date()->notNull()
        ], $tableOptions);

        $this->createIndex('tour_title', '{{%tours}}', 'title');
    }
    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tours}}');
    }
}
