<?php

use yii\db\Migration;

class m170721_135917_insert_data_into_destination_resources extends Migration
{
    public function safeUp()
    {
        $this->batchInsert(
            '{{%destination_resources}}',
            ['name', 'destination_assigned', 'description', 'image', 'status', 'capacity'],
            [
                ['Tour Dubai 1', 5, 'Tour Dubai 1', '', 'active', 350],
                ['Tour Dubai 3', 5, 'Tour Dubai 3', '', 'active', 100],
                ['Shark', 5, 'Shark', '', 'active', 80],
                ['Arabian Discoverer', 7, 'Arabian Discoverer', '', 'active', 160],
                ['Tour Dubai 2', 7, 'Tour Dubai 2', '', 'active', 140],
                ['Sheikha ', 7, 'Sheikha ', '', 'active', 115],
                ['Taj', 9, 'Taj', '', 'active', 280],
                ['Tour Dubai Yascht', 12, 'Tour Dubai Yascht', '', 'active', 32]
            ]
        );
    }

    public function safeDown()
    {

    }

}
