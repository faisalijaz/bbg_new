<?php

use yii\db\Migration;

class m170503_193050_groups extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%groups}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => "ENUM('active', 'In-Active') DEFAULT 'Active'",

        ], $tableOptions);

        $this->insert('{{%groups}}', [
            'title' => 'Default Group',
            'created_at' => time(),
            'updated_at' => time(),
            'status' => 'Active'
        ]);
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%groups}}');
    }
}
