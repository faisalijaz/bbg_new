<?php

use yii\db\Migration;

class m170806_104622_tour_pickup_rates extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_pickup_rates}}', [
            'id' => $this->primaryKey(),
            'tour_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'pickup_time' => $this->string()->notNull(),
            'price' => $this->double()->notNull()->defaultValue(0),
            'status' => "ENUM('active', 'In-Active')",
        ], $tableOptions);

        $this->createIndex('pick_tour_id', '{{%tour_pickup_rates}}', 'tour_id');
        $this->createIndex('pick_group_id', '{{%tour_pickup_rates}}', 'group_id');
        $this->createIndex('pick_city_id', '{{%tour_pickup_rates}}', 'city_id');

        $this->addForeignKey('pick_tours_id_fk', '{{%tour_pickup_rates}}', 'tour_id', '{{%tours}}', 'id');
        $this->addForeignKey('pick_city_id_fk', '{{%tour_pickup_rates}}', 'city_id', '{{%cities}}', 'id');
        $this->addForeignKey('pick_group_id_fk', '{{%tour_pickup_rates}}', 'group_id', '{{%groups}}', 'id');

    }

    public function safeDown()
    {
        $this->dropTable('{{%tour_pickup_rates}}');
    }
}
