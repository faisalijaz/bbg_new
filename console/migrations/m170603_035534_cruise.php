<?php

use yii\db\Migration;

class m170603_035534_cruise extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%cruise}}', [
            'id'              => $this->primaryKey(),
            'title'           => $this->string(50)->notNull(),
            'seats_capacity'  => $this->integer()->notNull(),
            'status'          => $this->smallInteger(1)->defaultValue(1),
        ], $tableOptions);
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%cruise}}');
    }
}
