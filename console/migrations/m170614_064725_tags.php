<?php

use yii\db\Migration;

class m170614_064725_tags extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tags}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50),
            'description' => $this->string(100),
        ], $tableOptions);
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tags}}');
    }
}