<?php

use yii\db\Migration;

class m170429_131452_add_experience_id_in_tour extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tours}}', 'experience_id', 'INT(11) NOT NULL DEFAULT 0');
    }


    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%tours}}', 'experience_id');
    }
}
