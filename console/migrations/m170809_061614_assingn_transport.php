<?php

use yii\db\Migration;

class m170809_061614_assingn_transport extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%assign_transport}}', [
            'id' => $this->primaryKey(),
            'booking_detail_id' => $this->integer()->notNull(),
            'destination_id' => $this->integer()->notNull(),
            'vehicle_id' => $this->integer()->notNull(),
            'driver_id' => $this->integer()->notNull(),
            'pickup_time' => $this->time()->notNull(),
            'pickup_date' => $this->date()->notNull(),
            'assigned_by' => $this->integer()->notNull(),
            'status' => "ENUM('active', 'In-Active') DEFAULT 'active'",
            'date_created' => $this->date()->notNull(),
        ], $tableOptions);

        $this->createIndex('t_booking_detail_id', '{{%assign_transport}}', 'booking_detail_id');
        $this->createIndex('t_destination_id', '{{%assign_transport}}', 'destination_id');
        $this->createIndex('t_vehicle_id', '{{%assign_transport}}', 'vehicle_id');
        $this->createIndex('t_driver_id', '{{%assign_transport}}', 'driver_id');

        $this->addForeignKey('t_booking_detail_id_fk', '{{%assign_transport}}', 'booking_detail_id', '{{%tour_booking_details}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('t_destination_id_fk', '{{%assign_transport}}', 'destination_id', '{{%destinations}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('t_vehicle_id_fk', '{{%assign_transport}}', 'vehicle_id', '{{%vehicles}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('t_driver_id_fk', '{{%assign_transport}}', 'driver_id', '{{%drivers}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%assign_transport}}');
    }
}
