<?php

use yii\db\Migration;

class m170713_124547_add_checkedin_by_field_checkin extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_checkin}}', 'checkedin_by', 'INT(11)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_checkin}}', 'checkedin_by');
    }
}