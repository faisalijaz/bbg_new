<?php

use yii\db\Migration;

class m170817_120410_addBookingRefinInvoices extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoices}}', 'booking_id', 'INT(11)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%invoices}}', 'booking_id');
    }

}
