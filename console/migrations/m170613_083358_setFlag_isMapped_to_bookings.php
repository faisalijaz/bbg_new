<?php

use yii\db\Migration;

class m170613_083358_setFlag_isMapped_to_bookings extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tour_bookings}}', 'isMapped', 'INT(11) DEFAULT 0 ');
    }

    public function down()
    {
        $this->dropColumn('{{%tour_bookings}}', 'isMapped');
    }
}
