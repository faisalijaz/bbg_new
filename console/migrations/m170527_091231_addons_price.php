<?php

use yii\db\Migration;

class m170527_091231_addons_price extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tour_booking_details}}', 'addons_price', 'DECIMAL(10, 2) DEFAULT 0');
        $this->addColumn('{{%tour_booking_details}}', 'adult_price', 'DECIMAL(10, 2) DEFAULT 0');
        $this->addColumn('{{%tour_booking_details}}', 'child_price', 'DECIMAL(10, 2) DEFAULT 0');
        $this->addColumn('{{%tour_booking_details}}', 'taxes', 'DECIMAL(10, 2) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%tour_booking_details}}', 'addons_price');
        $this->dropColumn('{{%tour_booking_details}}', 'adult_price');
        $this->dropColumn('{{%tour_booking_details}}', 'child_price');
        $this->dropColumn('{{%tour_booking_details}}', 'taxes');
    }

}
