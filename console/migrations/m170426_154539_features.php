<?php

use yii\db\Migration;

class m170426_154539_features extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%features}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'icon' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
        ], $tableOptions);
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%features}}');
    }
}
