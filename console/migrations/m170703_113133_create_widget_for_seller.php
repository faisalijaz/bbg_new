<?php

use yii\db\Migration;

class m170703_113133_create_widget_for_seller extends Migration
{
    public function safeUp()
    {
        $this->batchInsert(
            '{{%widgets}}',
            ['name', 'code', 'status', 'type','sorting_order'],
            [
                ['Expired Selling Limit', 'findExpiredLimit', 1, 'backend', '1'],
                ['List of agents Expired limit', 'list_agents_expired_limits', 1, 'backend', '3']

            ]
        );
    }

    public function safeDown()
    {

    }
}
