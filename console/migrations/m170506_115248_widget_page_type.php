<?php

use yii\db\Migration;

class m170506_115248_widget_page_type extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%widgets}}', 'type', "ENUM('frontend', 'backend') DEFAULT 'frontend'");
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%widgets}}', 'type');
    }
}
