<?php

use yii\db\Migration;

class m170807_050300_add_tour_pickup_booking extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_booking_pickup}}', [
            'id' => $this->primaryKey(),
            'tour_booking_id' => $this->integer()->notNull(),
            'pickup_city_id' => $this->integer()->notNull(),
            'pickup_hotel_id' => $this->string()->null(),
            'pickup_room_number' => $this->string()->null(),
            'pickup_phone_number' => $this->string()->null(),
            'pickup_time' => $this->string()->notNull(),
            'pickup_price' => $this->double()->notNull()->defaultValue(0),
            'pickup_date' => $this->date()->notNull()
        ], $tableOptions);

        $this->createIndex('pk_tour_booking_id', '{{%tour_booking_pickup}}', 'tour_booking_id');
        $this->createIndex('pk_city_id', '{{%tour_booking_pickup}}', 'pickup_city_id');
        $this->createIndex('pk_hotel_id', '{{%tour_booking_pickup}}', 'pickup_hotel_id');
        $this->addForeignKey('pk_pick_tours_id_fk', '{{%tour_booking_pickup}}', 'tour_booking_id', '{{%tour_bookings}}', 'id');
        $this->addForeignKey('pk_pick_city_id_fk', '{{%tour_booking_pickup}}', 'pickup_hotel_id', '{{%cities}}', 'id');
        $this->addForeignKey('pk_hotel_id_fk', '{{%tour_booking_pickup}}', 'pickup_hotel_id', '{{%hotels}}', 'id');

    }

    public function safeDown()
    {
        $this->dropTable('{{%tour_booking_pickup}}');
    }
}
