<?php

use yii\db\Migration;

class m170529_064518_add_addons_max_quantity extends Migration
{
    /**
     * change column qty name to max_qty
     */
    public function up()
    {
        $this->renameColumn('{{%addons}}', 'quantity', 'max_qty');
        $this->alterColumn('{{%addons}}', 'max_qty', 'INT(11) DEFAULT 1');
    }

    /**
     * drop column max_qty
     */
    public function down()
    {
        $this->dropColumn('{{%addons}}', 'max_qty');
    }
}
