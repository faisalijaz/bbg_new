<?php

use yii\db\Migration;

class m170224_202714_add_settings extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('settings', 'app_url', 'VARCHAR(255) DEFAULT NULL');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('settings', 'app_url');
    }

}
