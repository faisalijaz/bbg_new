<?php

use yii\db\Migration;

/**
 * Class m170506_082343_tour_booking_details
 */
class m170506_082343_tour_booking_details extends Migration
{
    /**
     * Crate a new table tour booking details
     * @return string
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_booking_details}}', [
            'id' => $this->primaryKey(),
            'booking_id' => $this->integer()->notNull(),
            'tour_id' => $this->integer()->notNull(),
            'price' => $this->float()->notNull(),
            'qty' => $this->float()->defaultValue(0),
            'comments' => $this->string()
        ], $tableOptions);

        $this->createIndex('booking_id', '{{%tour_booking_details}}', 'booking_id');
        $this->addForeignKey('booking_id_fk', '{{%tour_booking_details}}', 'booking_id', '{{%tour_bookings}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('tour_id', '{{%tour_booking_details}}', 'tour_id');
        $this->addForeignKey('tour_id_fk', '{{%tour_booking_details}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * drop table tour booking details
     * @return string
     */
    public function safeDown()
    {
        $this->dropTable('{{%tour_booking_details}}');
    }
}
