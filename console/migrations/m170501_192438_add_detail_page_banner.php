<?php

use yii\db\Migration;

class m170501_192438_add_detail_page_banner extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tours}}', 'detail_banner', 'VARCHAR(255) DEFAULT NULL');
    }


    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%tours}}', 'detail_banner');
    }
}
