<?php

use yii\db\Migration;

class m170514_075644_agent_transaction_limit extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%accounts}}', '', 'FLOAT(11) NOT NULL DEFAULT 0');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%accounts}}', '');
    }
}
