<?php

use yii\db\Migration;

class m170722_073829_cilents_reviews extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%clients_reviews}}', [
            'id' => $this->primaryKey(),
            'rating' => $this->string()->null(),
            'description' => $this->text(),
            'client_name' => $this->string()->null(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
            'status' => "ENUM('active', 'In-Active')",
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->createTable('{{%clients_reviews}}');
    }
}
