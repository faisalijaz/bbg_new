<?php

use yii\db\Migration;

class m170807_071714_add_tour_details_id_payments extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%payments}}', 'booking_detail_ref', 'INT(11) NULL');
    }

    public function safeDown()
    {
        $this->addColumn('{{%payments}}', 'booking_detail_ref');
    }
}
