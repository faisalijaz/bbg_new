<?php

use yii\db\Migration;

class m170503_081408_Child_price_mapping_tour_model extends Migration
{
    /**
     * Cahnges in tour model create type and percentage fields for child Price
     * @return mixed
     */
    public function safeUp()
    {
        $this->addColumn('{{%tours}}', 'child_price_type', "ENUM('+', '-') DEFAULT '-'");
        $this->addColumn('{{%tours}}', 'child_price_percentage', 'int(11) DEFAULT 0');
    }

    /**
     * Cahnges in tour model create type and percentage fields for child Price
     * @return mixed
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tours}}', 'child_price_type');
        $this->dropColumn('{{%tours}}', 'child_price_percentage');
    }
}
