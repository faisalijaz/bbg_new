<?php

use yii\db\Migration;

class m170426_170242_seo_content_for_tour extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tours}}', 'meta_title', 'VARCHAR(255)');
        $this->addColumn('{{%tours}}', 'meta_description', 'VARCHAR(255)');
        $this->addColumn('{{%tours}}', 'meta_keywords', 'VARCHAR(255)');


    }


    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%tours}}', 'meta_title');
        $this->dropColumn('{{%tours}}', 'meta_description');
        $this->dropColumn('{{%tours}}', 'meta_keywords');
    }
}
