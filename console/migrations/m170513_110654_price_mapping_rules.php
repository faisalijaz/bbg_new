<?php

use yii\db\Migration;

class m170513_110654_price_mapping_rules extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tour_addons}}', 'group_id', 'INT(11) NOT NULL DEFAULT 1');
        $this->addColumn('{{%tour_addons}}', 'rate', 'INT(11) NOT NULL DEFAULT 0');

        $this->createIndex('group_addon', '{{%tour_addons}}', 'group_id');
        $this->addForeignKey('group_addon_fk', '{{%tour_addons}}', 'group_id', '{{%groups}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%tour_addons}}', 'group_id');
        $this->dropColumn('{{%tour_addons}}', 'rate');

    }
}
