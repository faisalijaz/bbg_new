<?php

use yii\db\Migration;

class m170522_105102_add_paymentCycle_inAccounts extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        // Add column new column in accounts table.
        $this->addColumn('{{%accounts}}', 'paymentCycle', 'INT(11) NOT NULL DEFAULT 0');

    }

    public function safeDown()
    {
        //Drop column from Accounts Table
        $this->dropColumn('{{%accounts}}', 'paymentCycle');
    }
}