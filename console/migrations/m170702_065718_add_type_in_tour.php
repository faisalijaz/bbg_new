<?php

use yii\db\Migration;

class m170702_065718_add_type_in_tour extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tours}}', 'type', "enum('shared','exclusive') DEFAULT 'shared'");
    }

    public function down()
    {
        $this->dropColumn('{{%tours}}', 'type');
    }
}
