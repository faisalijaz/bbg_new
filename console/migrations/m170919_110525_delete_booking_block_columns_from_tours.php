<?php

use yii\db\Migration;

class m170919_110525_delete_booking_block_columns_from_tours extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%tours}}', 'block_booking');
    }

    public function safeDown()
    {
    }
}
