<?php

use yii\db\Migration;

class m170806_094924_drivers extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%drivers}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'address' => $this->string()->null(),
            'phone_number' => $this->string()->notNull(),
            'license_number' => $this->string()->null(),
            'id_card_number' => $this->string()->null(),
            'status' => "ENUM('active', 'In-Active')",
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%drivers}}');
    }
 
}
