<?php

use yii\db\Migration;

class m170908_065814_pickup_from_experience extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%experiences}}', 'pick_from', "enum('0','1') DEFAULT '0'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%experiences}}', 'pick_from');
    }
}
