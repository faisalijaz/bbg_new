<?php

use yii\db\Migration;

class m170909_024039_add_pickfrom_tourist_information extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tourist_information}}', 'pick_from_address', 'VARCHAR(512)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tourist_information}}', 'pick_from_address');
    }


}
