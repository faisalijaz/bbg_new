<?php

use yii\db\Migration;

class m170426_152309_addons extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%addons}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'price' => $this->integer()->notNull(),
            'short_description' => $this->string(150),
            'description' => $this->string(255),
            'image' => $this->string(255),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'quantity' => $this->integer()->notNull(),
            'sort_order' => $this->string(15)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%addons}}');
    }
}
