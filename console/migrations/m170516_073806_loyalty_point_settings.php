<?php

use yii\db\Migration;

class m170516_073806_loyalty_point_settings extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%settings}}', 'points_on_spent', 'INT(11) DEFAULT 0');
        $this->addColumn('{{%settings}}', 'points_of_spent', 'INT(11) DEFAULT 0');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%settings}}', 'points_on_spent');
        $this->dropColumn('{{%settings}}', 'points_of_spent');
    }
}
