<?php

use yii\db\Migration;

class m170606_065604_update_loylties extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%loyalties}}', 'is_deleted', "enum('deleted','pending','active') DEFAULT 'active'");
    }

    public function down()
    {
        $this->alterColumn('{{%loyalties}}', 'is_deleted', 'SMALLINT(1) DEFAULT 0');
    }
}
