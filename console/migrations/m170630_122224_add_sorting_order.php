<?php

use yii\db\Migration;

class m170630_122224_add_sorting_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%widgets}}', 'sorting_order', 'INT(11)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%widgets}}', 'sorting_order');
    }
}
