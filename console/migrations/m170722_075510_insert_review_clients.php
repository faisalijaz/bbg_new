<?php

use yii\db\Migration;

class m170722_075510_insert_review_clients extends Migration
{
    public function safeUp()
    {
        $this->batchInsert(
            '{{%clients_reviews}}',
            ['rating', 'description', 'client_name', 'created_at', 'updated_at', 'status'],
            [
                ['5', 'Absolutely amazing! Friendly staff, great show, highly recommended! ', 'Tena', NULL, NULL, 'active'],
                ['5', 'It’s here to thank you and your team (Reshma, Lany, Ashiq, JR, Mohammad from Al Sahara) so much for the efforts applied for the event to go smooth and great! My guests were so happy and excited every moment throughout the day and at the end of the day I got lots of very positive feedback from almost all the participants. Guys it’s such a pleasure to work wit you and I’ll be looking forward for our new arrangements! ', 'Shukhrat Toirov (Senior Protocol Officer, LUKOIL Overseas Baltic Ltd)', NULL, NULL, 'active'],
                ['5', 'Dear Jacky, John, Dexter, Rey, Rustam, Oscar and Joe,   I just wanted to say a BIG THANK YOU on behalf of Arabian Adventures for a great job done on Friday!   Please extend our thank you to your to everyone involved in this project.   We look forward to working with you closely in the near future.', 'Ishrat Jahan  (Senior Project Executive – Meetings, Incentives & Events – Arabian Adventures)', NULL, NULL, 'active'],
                ['5', 'Thank you to all for making their trip once again memorable. Each and every one of you made it possible to achieve this result. This clearly shows what we as a team are capable of doing. Please pass this appreciation across to your teams.', 'Best Regards Kamaal Ahmed, Logistics Manager | Gulf Ventures', NULL, NULL, 'active'],
                ['5', 'Thank you for your cooperation and for this unforgettable experience. Everything was very well organized, the restaurant and the dinner were just perfect! Many thanks and for our professional driver Mohamed. ', '-Mihail Panayotov-', NULL, NULL, 'active']
            ]
        );
    }

    public function safeDown()
    {

    }
}
