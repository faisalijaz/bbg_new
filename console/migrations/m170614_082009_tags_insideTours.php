<?php

use yii\db\Migration;

class m170614_082009_tags_insideTours extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tours}}', 'tag_id', 'INT(11) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%tours}}', 'tag_id');
    }


}
