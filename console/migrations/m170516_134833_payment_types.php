<?php

use yii\db\Migration;

class m170516_134833_payment_types extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%payment_types}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->string(255)->notNull(),
            'code'              => $this->string(10)->notNull(),
            'status'            => $this->smallInteger(1)->defaultValue(0),

        ], $tableOptions);

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%payment_types}}');
    }
}
