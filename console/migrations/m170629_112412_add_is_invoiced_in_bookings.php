<?php

use yii\db\Migration;

class m170629_112412_add_is_invoiced_in_bookings extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tour_bookings}}', 'invoiced', "enum('0','1') DEFAULT '0'");
    }

    public function down()
    {
        $this->dropColumn('{{%tour_bookings}}', 'invoiced');
    }
}
