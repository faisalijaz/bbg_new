<?php

use yii\db\Migration;

class m170529_085137_add_quantity_tour_booking_addons extends Migration
{
    /**
     * Add qty in tour booking addons
     */
    public function up()
    {
        $this->addColumn('{{%tour_booking_addons}}', 'quantity', 'INT(11) DEFAULT 1');
    }

    /**
     * Drop qty in tour booking addons
     */
    public function down()
    {
        $this->dropColumn('{{%tour_booking_addons}}', 'quantity');
    }
}
