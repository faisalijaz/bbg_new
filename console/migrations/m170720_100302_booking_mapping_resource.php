<?php

use yii\db\Migration;

class m170720_100302_booking_mapping_resource extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_booking_mappings}}', 'resource_id', 'INT(11)');

        $this->createIndex('resource_id', '{{%tour_booking_mappings}}', 'resource_id');
        $this->addForeignKey('resource_id_fk', '{{%tour_booking_mappings}}', 'resource_id', '{{%destination_resources}}', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_booking_mappings}}', 'resource_id');
    }
}
