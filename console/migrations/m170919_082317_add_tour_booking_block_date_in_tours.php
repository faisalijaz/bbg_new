<?php

use yii\db\Migration;

class m170919_082317_add_tour_booking_block_date_in_tours extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tours}}', 'block_booking', 'DATE');
    }

    public function safeDown()
    {

    }
}
