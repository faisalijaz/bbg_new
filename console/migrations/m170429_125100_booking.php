<?php

use yii\db\Migration;

class m170429_125100_booking extends Migration
{
    /*
     * id, booking_by, price, discount, total_sum, created_at, updated_at
     * payment_type, payment_status, account_id, status[created, cancelled
     * , deleted........], special_instruction
     * */

    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bookings}}', [
            'id' => $this->primaryKey(),
            'booking_by' => "ENUM('agent', 'customer', 'admin') DEFAULT 'customer'",
            'total' => $this->float()->notNull(),
            'discount' => $this->float()->defaultValue(0),
            'net_total' => $this->float()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'payment_type' => $this->integer()->notNull(),
            'payment_status' => $this->smallInteger(1),
            'account_id' => $this->integer()->notNull(),
            'status' => "ENUM('created', 'cancelled', 'deleted', 'confirmed', 'saved') DEFAULT 'created'",
            'special_instruction' => $this->string()
        ], $tableOptions);

        $this->createIndex('booking_account', '{{%bookings}}', 'account_id');
        $this->addForeignKey('booking_account_fk', '{{%bookings}}', 'account_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%bookings}}');
    }
}
