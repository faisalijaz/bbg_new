<?php

use yii\db\Migration;

class m170722_082802_change_description_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%clients_reviews}}', 'description', 'TEXT');
    }

    public function safeDown()
    {
    }
}
