<?php

use yii\db\Migration;

class m170917_140739_change_type_account_to_varchar_payment_cycle extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%accounts}}','paymentCycle', 'VARCHAR(10)');
    }

    public function safeDown()
    {
    }

}
