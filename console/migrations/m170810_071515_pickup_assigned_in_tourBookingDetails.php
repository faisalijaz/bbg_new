<?php

use yii\db\Migration;

class m170810_071515_pickup_assigned_in_tourBookingDetails extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_booking_details}}', 'pickupAssigned', 'INT(11) DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_booking_details}}', 'pickupAssigned');
    }
}
