<?php

use yii\db\Migration;

class m170806_091807_manage_hotels extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%hotels}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->null(),
            'address' => $this->string()->null(),
            'area' => $this->integer()->null(),
            'city' => $this->integer()->null(),
            'status' => "ENUM('active', 'In-Active')",
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%hotels}}');
    }
}
