<?php

use yii\db\Migration;

/**
 * Class m170502_060735_update_tour_price_mapping_priceType
 */
class m170502_060735_update_tour_price_mapping_priceType extends Migration
{
    /**
     *  create columns
     * @return string
     */
    public function safeUp()
    {
        $this->alterColumn('{{%tour_price_mapping}}', 'rate', 'double');
    }

    /**
     *  drop columns
     * @return string
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tour_price_mapping}}', 'rate');
    }
}
