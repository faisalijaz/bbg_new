<?php

use yii\db\Migration;

class m170907_144521_tourist_information_save extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tourist_information}}', [
            'id' => $this->primaryKey(),
            'booking_id' => $this->integer()->notNull(),
            'adult_or_child' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'gender' => $this->string()->null(),
            'age' => $this->string()->notNull(),
            'nationality' => $this->string()->null(),
        ], $tableOptions);

        $this->createIndex('tourist_booking_id', '{{%tourist_information}}', 'booking_id');
        $this->addForeignKey('tourist_booking_id_fk', '{{%tourist_information}}', 'booking_id', '{{%tour_bookings}}', 'id');

    }

    public function safeDown()
    {
        $this->dropTable('{{%tourist_information}}');
    }
}
