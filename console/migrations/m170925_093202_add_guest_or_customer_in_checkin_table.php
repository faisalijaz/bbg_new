<?php

use yii\db\Migration;

class m170925_093202_add_guest_or_customer_in_checkin_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_checkin}}', 'user_type', 'ENUM("guest","customer") DEFAULT "customer" ');
    }

    public function safeDown()
    {

    }
}
