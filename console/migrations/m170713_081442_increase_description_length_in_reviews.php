<?php

use yii\db\Migration;

class m170713_081442_increase_description_length_in_reviews extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%reviews}}', 'description', 'TEXT');
    }

    public function safeDown()
    {
        $this->alterColumn('{{%reviews}}', 'description', 'VARCHAR(255)');
    }

}
