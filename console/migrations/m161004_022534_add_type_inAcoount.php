<?php

use yii\db\Migration;

class m161004_022534_add_type_inAcoount extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%accounts}}', 'account_type', "ENUM('seller', 'customer') DEFAULT 'customer'");
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%accounts}}', 'account_type');
    }

}
