<?php

use yii\db\Migration;

class m170729_080012_assign_category_to_destination extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%destinations}}', 'category', 'INT(11)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%destinations}}', 'category');
    }
}
