<?php

use yii\db\Migration;

class m170629_144129_promotions_for_customers extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%promotions_for_users}}', [
            'id' => $this->primaryKey(),
            'promotion_code_id' => $this->integer()->notNull(),
            'user_account_id' => $this->integer()->null(),
        ], $tableOptions);

        $this->createIndex('promotions_code_id', '{{%promotions_for_users}}', 'promotion_code_id');
        $this->addForeignKey('promotions_code_id_fk', '{{%promotions_for_users}}', 'promotion_code_id', '{{%promotions}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('users_account_id', '{{%promotions_for_users}}', 'user_account_id');
        $this->addForeignKey('users_account_id_fk', '{{%promotions_for_users}}', 'user_account_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable('{{%promotions_for_users}}');
    }
}
