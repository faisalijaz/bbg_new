<?php

use yii\db\Migration;

/**
 * Class m170615_090209_change_data_type_of_tags_in_destinations
 */
class m170615_090209_change_data_type_of_tags_in_destinations extends Migration
{
    /**
     * @return mixed
     */
    public function up()
    {
        // Remove column tags and then make it foreign key
        $this->dropColumn('{{%destinations}}', 'tag');

        $this->addColumn('{{%destinations}}', 'tag', 'INT(11) DEFAULT NULL');

        $this->createIndex('tag', '{{%destinations}}', 'tag');
        $this->addForeignKey('tag_fk', '{{%destinations}}', 'tag', '{{%tags}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @return mixed
     */
    public function down()
    {
        $this->dropColumn('{{%destinations}}', 'tag');
    }
}
