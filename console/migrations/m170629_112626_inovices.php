<?php

use yii\db\Migration;

class m170629_112626_inovices extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%invoices}}', [
            'id' => $this->primaryKey(),
            'user_account' => $this->integer()->notNull(),
            'amount' => $this->double()->notNull(),
            'url' => $this->string(),
            'invoice_note' => $this->string(),
            'date' => $this->dateTime(),
            'status' => "ENUM('paid', 'partiallyPaid','cancelled','pending') DEFAULT 'pending'",
        ], $tableOptions);

        $this->createIndex('user_account', '{{%invoices}}', 'user_account');
        $this->addForeignKey('user_account_fk', '{{%invoices}}', 'user_account', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable('{{%invoices}}');
    }
}
