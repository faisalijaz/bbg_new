<?php

use yii\db\Migration;

class m170503_193719_group_id_in_price_mapping extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tour_price_mapping}}', 'group_id', 'INT(11) NOT NULL DEFAULT 1');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%tour_price_mapping}}', 'group_id');
    }
}
