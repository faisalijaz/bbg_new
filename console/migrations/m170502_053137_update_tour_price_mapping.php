<?php

use yii\db\Migration;

/**
 * Class m170502_053137_update_tour_price_mapping
 */
class m170502_053137_update_tour_price_mapping extends Migration
{
    /**
     *  create columns
     * @return string
     */
    public function safeUp()
    {
        // Dropping columns price & date
        $this->dropColumn('{{%tour_price_mapping}}', 'price');
        $this->dropColumn('{{%tour_price_mapping}}', 'date');

        // Adding columns date, end_date, rate, start_time, end_time, cut..........
        $this->addColumn('{{%tour_price_mapping}}', 'start_date', 'date');
        $this->addColumn('{{%tour_price_mapping}}', 'end_date', 'date');
        $this->addColumn('{{%tour_price_mapping}}', 'rate', 'INT(11) NOT NULL DEFAULT 0');
        $this->addColumn('{{%tour_price_mapping}}', 'start_time', 'time');
        $this->addColumn('{{%tour_price_mapping}}', 'cutoff_time', 'time');
        $this->addColumn('{{%tour_price_mapping}}', 'percent_increase', 'INT(11) NOT NULL DEFAULT 0');
    }

    /**
     *  drop columns
     * @return string
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tour_price_mapping}}', 'price');
        $this->dropColumn('{{%tour_price_mapping}}', 'date');
        $this->dropColumn('{{%tour_price_mapping}}', 'start_date');
        $this->dropColumn('{{%tour_price_mapping}}', 'end_date');
        $this->dropColumn('{{%tour_price_mapping}}', 'rate');
        $this->dropColumn('{{%tour_price_mapping}}', 'start_time');
        $this->dropColumn('{{%tour_price_mapping}}', 'cutoff_time');
        $this->dropColumn('{{%tour_price_mapping}}', 'percent_increase');
    }
}
