<?php

use yii\db\Migration;

class m170911_120858_change_pickup_area_default_value_ extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%tour_booking_pickup}}', 'pickup_area', 'INT(11) null');
    }

    public function safeDown()
    {
        echo "m170911_120858_change_pickup_area_default_value_ cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170911_120858_change_pickup_area_default_value_ cannot be reverted.\n";

        return false;
    }
    */
}
