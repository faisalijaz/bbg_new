<?php

use yii\db\Migration;

/**
 * Class m170615_060258_change_bookingId_to_bookingDetailsId
 */
class m170615_060258_change_bookingId_to_bookingDetailsId extends Migration
{

    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {

        $this->dropTable('{{%tour_booking_mappings}}');

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tour_booking_mappings}}', [
            'id' => $this->primaryKey(),
            'destination_id' => $this->integer()->notNull(),
            'booking_details_Id' => $this->integer()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('destination_id', '{{%tour_booking_mappings}}', 'destination_id');
        $this->addForeignKey('destination_id_fk', '{{%tour_booking_mappings}}', 'destination_id', '{{%destinations}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('booking_details_Id', '{{%tour_booking_mappings}}', 'booking_details_Id');
        $this->addForeignKey('booking_details_Id_fk', '{{%tour_booking_mappings}}', 'booking_details_Id', '{{%tour_booking_details}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tour_booking_mappings}}');
    }
}
