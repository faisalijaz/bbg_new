<?php

use yii\db\Migration;

class m170501_194006_reviews extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%reviews}}', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer()->notNull(),
            'tour_id' => $this->integer()->notNull(),
            'rating' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => "ENUM('active', 'In-Active') DEFAULT 'In-Active'",

        ], $tableOptions);

        $this->createIndex('review_account', '{{%reviews}}', 'account_id');
        $this->addForeignKey('review_account_fk', '{{%reviews}}', 'account_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('review_tour', '{{%reviews}}', 'tour_id');
        $this->addForeignKey( 'review_tour_fk', '{{%reviews}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%reviews}}');
    }
}
