<?php

use yii\db\Migration;

class m170627_055242_category_model extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'short_description' => $this->string()->null(),
            'description' => $this->string()->null(),
            'image' => $this->string()->null(),
            'baner_image' => $this->string()->null(),
            'parent_id' => $this->integer()->null(),
            'meta_title' => $this->string()->null(),
            'meta_description' => $this->string()->null(),
            'status' => $this->smallInteger(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('parent_id', '{{%categories}}', 'parent_id');
        $this->addForeignKey('parent_id_fk', '{{%categories}}', 'parent_id', '{{%categories}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%categories}}');
    }


}
