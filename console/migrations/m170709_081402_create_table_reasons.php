<?php

use yii\db\Migration;

class m170709_081402_create_table_reasons extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payment_reasons}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->null(),
            'description' => $this->string()->null(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%payment_reasons}}');
    }
}
