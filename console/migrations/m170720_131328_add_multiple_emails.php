<?php

use yii\db\Migration;

class m170720_131328_add_multiple_emails extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%accounts}}', 'cc_emails', 'VARCHAR(255)');
    }

    public function safeDown()
    {
        $this->addColumn('{{%accounts}}', 'cc_emails', 'VARCHAR(255)');
    }
}
