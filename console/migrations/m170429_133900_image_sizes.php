<?php

use yii\db\Migration;

class m170429_133900_image_sizes extends Migration
{
    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {

        $this->addColumn('{{%settings}}', 'grid_size_width', 'Integer(11)');
        $this->addColumn('{{%settings}}', 'grid_size_height', 'Integer(11)');

        $this->addColumn('{{%settings}}', 'listing_size_width', 'Integer(11)');
        $this->addColumn('{{%settings}}', 'listing_size_height', 'Integer(11)');

        $this->addColumn('{{%settings}}', 'additional_image_size_width', 'Integer(11)');
        $this->addColumn('{{%settings}}', 'additional_image_size_height', 'Integer(11)');

        $this->addColumn('{{%settings}}', 'popup_size_width', 'Integer(11)');
        $this->addColumn('{{%settings}}', 'popup_size_height', 'Integer(11)');

        $this->addColumn('{{%settings}}', 'page_banner_size_width', 'Integer(11)');
        $this->addColumn('{{%settings}}', 'page_banner_size_height', 'Integer(11)');

        $this->addColumn('{{%settings}}', 'home_banner_size_width', 'Integer(11)');
        $this->addColumn('{{%settings}}', 'home_banner_size_height', 'Integer(11)');
    }


    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {

        $this->dropColumn('{{%settings}}', 'grid_size_width', 'Integer(11)');
        $this->dropColumn('{{%settings}}', 'grid_size_height', 'Integer(11)');

        $this->dropColumn('{{%settings}}', 'listing_size_width', 'Integer(11)');
        $this->dropColumn('{{%settings}}', 'listing_size_height', 'Integer(11)');

        $this->dropColumn('{{%settings}}', 'additional_image_size_width', 'Integer(11)');
        $this->dropColumn('{{%settings}}', 'additional_image_size_height', 'Integer(11)');

        $this->dropColumn('{{%settings}}', 'popup_size_width', 'Integer(11)');
        $this->dropColumn('{{%settings}}', 'popup_size_height', 'Integer(11)');

        $this->dropColumn('{{%settings}}', 'page_banner_size_width', 'Integer(11)');
        $this->dropColumn('{{%settings}}', 'page_banner_size_height', 'Integer(11)');

        $this->dropColumn('{{%settings}}', 'home_banner_size_width', 'Integer(11)');
        $this->dropColumn('{{%settings}}', 'home_banner_size_height', 'Integer(11)');
    }
}
