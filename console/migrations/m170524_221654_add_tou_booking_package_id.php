<?php

use yii\db\Migration;

class m170524_221654_add_tou_booking_package_id extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tour_booking_details}}', 'package_id', 'INT(11) NOT NULL DEFAULT 1');
        $this->createIndex('booking_package', '{{%tour_booking_details}}', 'package_id');
        $this->addForeignKey('booking_package_fk', '{{%tour_booking_details}}', 'package_id', 'experiences', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropForeignKey('booking_package_fk', '{{%tour_booking_details}}');
        $this->dropIndex('booking_package', '{{%tour_booking_details}}');
        $this->dropColumn('package_id', '{{%tour_booking_details}}');
    }
}
