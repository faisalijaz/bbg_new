<?php

use yii\db\Migration;

class m170809_122734_assign_driver_to_vehicle extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%vehicles}}', 'driver_id', 'INT(11)');

        $this->createIndex('vehicle_driver', '{{%vehicles}}', 'driver_id');
        $this->addForeignKey('vehicle_driver_fk', '{{%vehicles}}', 'driver_id', 'drivers', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->addColumn('{{%vehicles}}', 'driver_id');
    }
}
