<?php

use yii\db\Migration;

class m170912_110023_add_amount_to_be_collected_in_tour_bookings extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_bookings}}', 'amount_to_collect', 'FLOAT DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_bookings}}', 'amount_to_collect');
    }
}
