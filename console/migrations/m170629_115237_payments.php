<?php

use yii\db\Migration;

class m170629_115237_payments extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%payments}}', [
            'id' => $this->primaryKey(),
            'invoice_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->null(),
            'amount' => $this->double()->notNull(),
            'transaction_no' => $this->smallInteger(1)->defaultValue(1),
            'payment_note' => $this->string(),
            'payment_date' => $this->integer()->notNull(),
            'status' => "ENUM('done', 'cancelled','pending') DEFAULT 'pending'",
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%payments}}');
    }
}
