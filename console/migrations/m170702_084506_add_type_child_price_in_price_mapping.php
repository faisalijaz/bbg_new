<?php

use yii\db\Migration;

class m170702_084506_add_type_child_price_in_price_mapping extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tour_price_mapping}}', 'child_price', "DOUBLE DEFAULT '0'");
    }

    public function down()
    {
        $this->dropColumn('{{%tour_price_mapping}}', 'child_price');
    }
}
