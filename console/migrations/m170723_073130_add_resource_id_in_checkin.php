<?php

use yii\db\Migration;

class m170723_073130_add_resource_id_in_checkin extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_checkin}}', 'resource_id', 'INT(11)');
    }

    public function safeDown()
    {
        $this->addColumn('{{%tour_checkin}}', 'resource_id');
    }
}
