<?php

use yii\db\Migration;

class m170506_152643_update_contat_type extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->alterColumn('{{%settings}}', 'location', 'text');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->alterColumn('{{%settings}}', 'location', 'VARCHAR(255)');
    }
}
