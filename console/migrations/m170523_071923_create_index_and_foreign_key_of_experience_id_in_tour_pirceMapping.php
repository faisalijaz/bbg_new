<?php

use yii\db\Migration;

class m170523_071923_create_index_and_foreign_key_of_experience_id_in_tour_pirceMapping extends Migration
{
    public function safeUp()
    {
        $this->createIndex('experience_id', '{{%tour_price_mapping}}', 'experience_id');
        $this->addForeignKey('experience_id_fk', '{{%tour_price_mapping}}', 'experience_id', '{{%experiences}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('experience_id_fk', '{{%tour_price_mapping}}');
        $this->dropIndex('experience_id', '{{%tour_price_mapping}}');
    }
}
