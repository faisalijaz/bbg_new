<?php

namespace console\controllers;

use common\helpers\EmailHelper;
use common\models\Accounts;
use common\models\TourBookings;
use yii\console\Controller;

/**
 * Test controller
 */
class CronController extends Controller
{

    public function actionIndex()
    {
        mail('se.faisalijaz@gmail.com','Cron Running','Cron');
        $this->getTentativeBookings();
    }

    public function getTentativeBookings()
    {
        $bookings = TourBookings::find()->where(['payment_status' => 'pending'])
            ->andWhere(['status' => 'created'])
            ->andWhere(['IS NOT', 'cancellation_time', null])
            ->andWhere(['<=', 'FROM_UNIXTIME(tour_bookings.created_at,"%Y-%m-%d")', date('Y-m-d')])
            ->all();

        foreach ($bookings as $b) {
            $cancelBookings = TourBookings::findOne($b->id);
            $cancelBookings->status = 'cancelled';
            $cancelBookings->save();
            // $this->sendEmail($cancelBookings);
        }
    }

    /**
     * Send order confirmation to user
     *
     */
    public function sendEmail($booking)
    {
        $cc = [];
        $account = Accounts::findOne($booking->account_id);

        if (!empty($account->cc_emails) && strpos($account->cc_emails, ',') != False) {
            $cc = explode(',', $account->cc_emails);
        }

        (new EmailHelper())->sendBookingEmail($account->email, $cc, 'Booking Cancellation', 'booking/booking-cancel', ['booking' => $booking]);
    }

}