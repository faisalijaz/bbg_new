<?php
return [
    'sourceLanguage' => 'en',
    'language' => 'en',
    'timeZone' => 'Asia/Dubai',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '/' => 'site/index',
                'members' => 'account',
            ],

        ],
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => '/a/frontend/web',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        's3bucket' => [
            'class' => \frostealth\yii2\aws\s3\Storage::className(),
            'region' => 'ap-southeast-1',
            'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
                'key' => 'AKIAJ7QHALPYNWCH54IQ',
                'secret' => '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',
            ],
            'bucket' => 'bbg-assets',
            'cdnHostname' => 'https://d1l2trc10ppjt4.cloudfront.net/',
            'defaultAcl' => \frostealth\yii2\aws\s3\Storage::ACL_PUBLIC_READ,
            'debug' => false, // bool|array
        ],
        'pdf' => [
            'class' => \kartik\mpdf\Pdf::classname(),
            // set to use core fonts only
            'mode' => \kartik\mpdf\Pdf::MODE_UTF8,
            // A4 paper format
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // refer settings section for all configuration options
        ],
        'html2pdf' => [
            'class' => 'yii2tech\html2pdf\Manager',
            'viewPath' => '@app/pdf',
            'converter' => [
                'class' => 'yii2tech\html2pdf\converters\Wkhtmltopdf',
                'defaultOptions' => [
                    'pageSize' => 'A4'
                ],
            ]
        ],
        'dateTime' => [
            'class' => 'common\helpers\DateTimeHelper',
        ],
        'appSettings' => [
            'class' => 'common\helpers\AppSetting',
        ],
        'commonHelper' => [
            'class' => 'common\helpers\CommonHelper',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'imagemanager' => [
            'class' => 'noam148\imagemanager\components\ImageManagerGetPath',
            'mediaPath' =>  '/uploads',
            'cachePath' => '/uploads',
            'useFilename' => true,
            'absoluteUrl' => false,
            'databaseComponent' => 'db' // The used database component by the image manager, this defaults to the Yii::$app->db component
        ],
    ],
    'modules' => [
        'imagemanager' => [
            'class' => 'noam148\imagemanager\Module',
            //set accces rules ()
            'canUploadImage' => true,
            'canRemoveImage' => function () {
                return true;
            },
            'deleteOriginalAfterEdit' => true, // false: keep original image after edit. true: delete original image after edit
            // Set if blameable behavior is used, if it is, callable function can also be used
            'setBlameableBehavior' => true,
            //add css files (to use in media manage selector iframe)
            'cssFiles' => [
                'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',
            ],
        ],
        'social' => [
            // the module class
            'class' => 'kartik\social\Module',

            // the global settings for the twitter widget
            'twitter' => [
                'screenName' => 'TWITTER_SCREEN_NAME',
            ]
        ],
        'newsletter' => [
            'class' => 'yiimodules\newsletter\Module',
        ],
        'redactor' => 'yii\redactor\RedactorModule',
    ]
];