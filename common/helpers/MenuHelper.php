<?php

namespace common\helpers;

use yii;


/**
 * MenuHelper for left side menu render
 *
 * @author Nadeem AKhtar <nadeem@myswich.com>
 * @since 1.0
 */
class MenuHelper
{

    /**
     * get Admin menu
     * this function will create admin menu with permission and return it back,
     * @return array $menuItems this is left side menu
     * */
    public static function getMenu()
    {
        return $menuItems = [

            [
                'label' => '<i class="fa fa-desktop"></i> <span class="hidden-sm"> Dashboard </span>',
                'url' => ['/site/index'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ],
            [
                'label' => '<i class="fa fa-vcard"></i> <span class="hidden-sm"> Memberships </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Members',
                        'url' => ['/account'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Active Members',
                        'url' => ['/account/','status' => 'active'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Pending Requests',
                        'url' => ['/account/','status' => 'pending'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Invoiced',
                        'url' => ['/account/','status' => 'invoiced'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Renewal Required',
                        'url' => ['/account/','status' => 'renewal-required'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Deleted Members',
                        'url' => ['/account/','status' => 'deleted'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Business',
                        'url' => ['/account', 'member_type' => '1'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Individual',
                        'url' => ['/account', 'member_type' => '2'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Non-Profit',
                        'url' => ['/account', 'member_type' => '3'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Upcoming Renewals',
                        'url' => ['/account/upcoming-renewals'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Membership Upgrade',
                        'url' => ['/account/upgrade-membership'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Membership Types',
                        'url' => ['/group'],
                        'visible' => \Yii::$app->user->can('groups')
                    ],

                    [
                        'label' => 'Old Docs',
                        'url' => ['/old-docs'],
                        'visible' => \Yii::$app->user->can('categories')
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-building"></i> <span class="hidden-sm"> Companies </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Companies',
                        'url' => ['/account-company'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    [
                        'label' => 'Categories',
                        'url' => ['/categories'],
                        'visible' => \Yii::$app->user->can('categories')
                    ],
                ]
            ],
            [
                'label' => '<i class="fa fa-calendar"></i> <span class="hidden-sm"> Events </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Events',
                        'url' => ['/event'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    [
                        'label' => 'Past Events',
                        'url' => ['/event/','type' => 'past'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    [
                        'label' => 'Event Types',
                        'url' => ['/event-types'],
                        'visible' => \Yii::$app->user->can('event_types')
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-money"></i> <span class="hidden-sm"> Financials </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Invoices',
                        'url' => ['/invoices'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    [
                        'label' => 'Payments',
                        'url' => ['/payments'],
                        'visible' => \Yii::$app->user->can('event_types')
                    ],
                    [
                        'label' => 'Old System Invoices',
                        'url' => ['/tbl-invoice'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                ]
            ],
            [
                'label' => '<i class="fa fa-newspaper-o"></i> <span class="hidden-sm"> News </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'News',
                        'url' => ['/news'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    [
                        'label' => 'News Categories',
                        'url' => ['/news-categories'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                ]
            ],
            [
                'label' => '<i class="fa fa-user-o"></i> <span class="hidden-sm"> Manage Contacts </span>',
                'url' => ['/newsletter-subscriptions'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ],
            [
                'label' => '<i class="fa fa-envelope-o"></i> <span class="hidden-sm"> Newsletters </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Newsletters',
                        'url' => ['/newslettertemplates'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    /*[
                        'label' => 'Newsletter Subscriptions',
                        'url' => ['/newsletter-subscriptions'],
                        'visible' => \Yii::$app->user->can('events')
                    ],*/
                ]
            ],
            [
                'label' => '<i class="fa fa-briefcase"></i> <span class="hidden-sm"> Job Posts </span>',
                'url' => ['/job-posts'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ],
            [
                'label' => '<i class="fa fa-sitemap"></i> <span class="hidden-sm"> Manage Content </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('cms_page_menu'),
                'items' => [
                    [
                        'label' => 'Committee Members & Staff',
                        'url' => ['/committe-members'],
                        'visible' => \Yii::$app->user->can('CommitteMembers')
                    ],
                    [
                        'label' => 'Payment Responses',
                        'url' => ['/payment-responses'],
                        'visible' => \Yii::$app->user->can('PaymentResponses'),
                    ],
                    [
                        'label' => 'Categories for Staff',
                        'url' => ['/staff-categories'],
                        'visible' => \Yii::$app->user->can('CommitteMembers')
                    ],
                    [
                        'label' => 'View from Chair',
                        'url' => ['/view-from-chair/index'],
                        'visible' => \Yii::$app->user->can('CommitteMembers')
                    ],
                    [
                        'label' => ' CMS pages',
                        'url' => ['/cms-page'],
                        'visible' => \Yii::$app->user->can('cms-pages')
                    ],
                    [
                        'label' => 'Sponsors',
                        'url' => ['/sponsors'],
                        'visible' => \Yii::$app->user->can('event_types')
                    ],
                    [
                        'label' => 'Member Offers',
                        'url' => ['/member-offers'],
                        'visible' => \Yii::$app->user->can('categories')
                    ],
                    [
                        'label' => 'Blog Posts',
                        'url' => ['/committe-members'],
                        'visible' => \Yii::$app->user->can('CommitteMembers')
                    ],
                    [
                        'label' => 'Referral Sources',
                        'url' => ['/referral-sources'],
                        'visible' => \Yii::$app->user->can('CommitteMembers')
                    ],
                    [
                        'label' => 'Main Menu',
                        'url' => ['/menu-widget'],
                        'visible' => \Yii::$app->user->can('settings')
                    ],
                    [
                        'label' => 'Categories',
                        'url' => ['/categories'],
                        'visible' => \Yii::$app->user->can('categories')
                    ],
                    [
                        'label' => 'Gallery',
                        'url' => ['/gallery'],
                        'visible' => \Yii::$app->user->can('gallery')
                    ],
                    [
                        'label' => 'Banners',
                        'url' => ['/banner'],
                        'visible' => \Yii::$app->user->can('banners')

                    ],
                    [
                        'label' => 'Settings',
                        'url' => ['/settings'],
                        'visible' => \Yii::$app->user->can('settings')
                    ],

                    [
                        'label' => 'Dashboard Widgets',
                        'url' => ['/widget'],
                        'visible' => \Yii::$app->user->can('widgets')
                    ],
                    [
                        'label' => 'Partners',
                        'url' => ['/partners'],
                        'visible' => \Yii::$app->user->can('partners')

                    ],
                    /*[
                        'label' => 'Tags',
                        'url' => ['/tag'],
                        'visible' => \Yii::$app->user->can('tags')
                    ]*/
                ]
            ],

            [
                'label' => ' <i class="fa fa-user-circle"></i><span class="hidden-sm">Permissions</span>',
                'url' => '#',
                'visible' => yii::$app->user->can('UserManagement'),
                'items' => [
                    [
                        'label' => 'Users',
                        'url' => ['/user']
                    ],
                    [
                        'label' => 'Rule Assignment',
                        'url' => ['/admin/assignment']
                    ],
                    [
                        'label' => 'Routes',
                        'url' => ['/admin/route']
                    ],
                    [
                        'label' => 'Permission',
                        'url' => ['/admin/permission']
                    ],
                    [
                        'label' => 'Roles',
                        'url' => ['/admin/role']
                    ],
                    [
                        'label' => 'Settings',
                        'url' => ['/settings']
                    ]
                ],
                'options' => ['class' => 'has_sub'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ]

        ];
    }

    private function getMemberTypes(){

    }
}
