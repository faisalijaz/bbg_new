<?php

namespace common\helpers;

use yii;
use yii\helpers\ArrayHelper;

/**
 * Email Helper component is the class that will take care of all system emails
 * Like Order Confirmation, Reset Password, Forgot Password, Failed Order email ETC.
 *
 * Example by using emailHelper Component in common.php
 *
 * ~~~~
 * yii::$app->emailHelper->sendEmail($to, $cc, $subject, $template, $data)
 * ~~~~
 * @property string $to
 * @property array $cc
 * @property string $subject
 * @property string $template
 * @property mixed $data
 *
 * */
class EmailHelper
{
    /**
     * @param $to
     * @param array $cc
     * @param $subject
     * @param $template
     * @param $data
     * @return mixed
     */
    public function sendEmail($to, $cc = [], $subject, $template, $data)
    {

        Yii::$app->mail->htmlLayout = "@common/mail/layouts/html";

        $setting = (new AppSetting())->getSettings();

        return Yii::$app->mail->compose(['html' => $template], ArrayHelper::merge($data, ['title' => $subject, 'setting' => $setting]))
            ->setFrom([$setting->from_email => $setting->app_name])
            ->setReplyTo($setting->from_email)
            ->setTo($to)
            ->setCc($cc)
            ->setSubject($subject)
            ->send();

    }

    /**
     * @param $to
     * @param array $cc
     * @param $subject
     * @param $template
     * @param $data
     * @return mixed
     */
    public function sendNewsLetterEmail($to, $cc = [], $subject, $template, $headers = "")
    {

        Yii::$app->mail->htmlLayout = "";

        $setting = (new AppSetting())->getSettings();

        return Yii::$app->mail->compose()
            ->setFrom([$setting->from_email => $setting->app_name])
            ->setReplyTo($setting->from_email)
            ->setTo($to)
            ->setCc($cc)
            ->setSubject($subject)
            ->setHtmlBody($template)
            ->send();

    }

    /**
     * @param $to
     * @param array $cc
     * @param $subject
     * @param $template
     * @param $data
     * @return mixed
     */
    public function sendIcalInvote($to, $cc = [], $subject, $template, $attach)
    {

        Yii::$app->mail->htmlLayout = "";

        $setting = (new AppSetting())->getSettings();

        return Yii::$app->mail->compose()
            ->setFrom([$setting->from_email => $setting->app_name])
            ->setReplyTo($setting->from_email)
            ->setTo($to)
            ->setCc($cc)
            ->setSubject($subject)
            ->setHtmlBody($template)
            ->attachContent($attach, [
                'fileName' => 'invite.ics',
                'contentType' => 'text/x-vCalendar'
            ])->send();

        /*->setHeaders([
                'Content-Type' => 'text/x-vCalendar',
                'Content-Disposition' => 'inline',
                'charset' => 'utf-8'
            ])*/
    }

    /**
     * @param $to
     * @param array $cc
     * @param $subject
     * @param $template
     * @param $data
     * @return mixed
     */
    public function sendInvoiceEmail($to, $cc = [], $subject, $template, $data)
    {

        Yii::$app->mail->htmlLayout = "@common/mail/layouts/html_invoice";
        $setting = (new AppSetting())->getSettings();

        return Yii::$app->mail->compose(['html' => $template], ArrayHelper::merge($data, ['title' => $subject, 'setting' => $setting]))
            ->setFrom([$setting->from_email => $setting->app_name])
            ->setReplyTo($setting->from_email)
            ->setTo($to)
            ->setCc($cc)
            ->setSubject($subject)
            ->send();
    }

    /**
     * @param $to
     * @param array $cc
     * @param $subject
     * @param $template
     * @param $data
     * @return mixed
     */
    public function sendCertificateEmail($to, $cc = [], $subject, $template, $data)
    {

        Yii::$app->mail->htmlLayout = "@common/mail/layouts/sendCertificate";
        $setting = (new AppSetting())->getSettings();

        return Yii::$app->mail->compose(['html' => $template], ArrayHelper::merge($data, ['title' => $subject, 'setting' => $setting]))
            ->setFrom([$setting->from_email => $setting->app_name])
            ->setReplyTo($setting->from_email)
            ->setTo($to)
            ->setCc($cc)
            ->setSubject($subject)
            ->send();
    }

    /**
     * @param $to
     * @param array $cc
     * @param $subject
     * @param $template
     * @param $data
     * @return mixed
     */
    public function sendEventEmail($to, $cc = [], $subject, $template, $data)
    {

        Yii::$app->mail->htmlLayout = "@common/mail/layouts/html";

        $setting = (new AppSetting())->getSettings();

        if ($template === 'event/event-registration') {

            if (count($cc) > 0) {
                $cc = ArrayHelper::merge($cc, [
                    'se.faisalijaz@gmail.com' => 'Admin BBG'
                ]);
            } else {
                $cc = [
                    'se.faisalijaz@gmail.com' => 'Admin BBG'
                ];
            }

        }

        return Yii::$app->mail->compose(['html' => $template], ArrayHelper::merge($data, ['title' => $subject, 'setting' => $setting]))
            ->setFrom([$setting->from_email => $setting->app_name])
            ->setReplyTo($setting->from_email)
            ->setTo($to)
            ->setCc($cc)
            ->setSubject($subject)
            ->send();
    }

}