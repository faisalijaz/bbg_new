<?php

namespace common\helpers;

use yii\db\Expression;
use DateTime;
use DateInterval;
use DatePeriod;

class DateTimeHelper
{
    /**
     * Constructor
     */
    public function __construct()
    {
        date_default_timezone_set('Asia/Dubai');
    }

    /**
     * @return Expression
     */
    public function getCurrentDateTime()
    {
        return new Expression('NOW()');
    }

    /**
     * @return string
     */
    public static function getTime()
    {
        $time = new \DateTime('now');
        return $time->format('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public static function getDate()
    {
        $time = new \DateTime('now');
        return $time->format('Y-m-d');
    }

    /**
     * @param $minute
     * @return bool|string
     */
    public function getPastMinutes($minute)
    {
        return date('H:i:s', strtotime($minute . ' minutes'));
    }

    /**
     * @param $date
     * @return bool|string
     */
    public function getReadableTime($date)
    {
        return date('H', strtotime($date));
    }

    /**
     * @param $time
     * @return int
     */
    public function getTimeStamp($time)
    {
        return strtotime($time);
    }

    /**
     * @param $seconds
     * @return string
     */
    public function getSecondToHoursSecond($seconds)
    {
        $s = $seconds % 60;
        $m = floor(($seconds % 3600) / 60);
        return "$m min, $s sec";
    }

    /**
     * @param $from
     * @param $to
     * @return bool
     */
    public function validateInBetween($from, $to)
    {

        $time = new \DateTime('now');
        $cur_time = $time->format('H:i:s');

        $st_time = strtotime($from);
        $end_time = strtotime($to);
        $cur_time = strtotime($cur_time);

        if ($cur_time >= $st_time && $cur_time <= $end_time) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $created_time
     * @param $driver_time
     * @return string
     */
    public function getTimeDiffrence($created_time, $driver_time)
    {

        if (!empty($driver_time)) {
            $to_time = strtotime($created_time);
            $from_time = strtotime($driver_time);

            $etime = abs($to_time - $from_time);

            if ($etime < 1) {
                return '0 seconds';
            }

            $a = [365 * 24 * 60 * 60 => 'year',
                30 * 24 * 60 * 60 => 'month',
                24 * 60 * 60 => 'day',
                60 * 60 => 'hour',
                60 => 'minute',
                1 => 'second'
            ];
            $a_plural = ['year' => 'years',
                'month' => 'months',
                'day' => 'days',
                'hour' => 'hours',
                'minute' => 'minutes',
                'second' => 'seconds'
            ];

            foreach ($a as $secs => $str) {
                $d = $etime / $secs;
                if ($d >= 1) {
                    $r = round($d);
                    return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str);
                }
            }
        } else {
            return 'N/A';
        }
    }

    /**
     * @param           $date
     * @param bool|true $override
     * @return bool|string
     */
    public function getReadableFormat($date, $override = true)
    {

        if ($override) {
            return date('F jS, Y', strtotime($date));
        } else {
            return date('F jS, Y', $date);
        }
    }

    /**
     * @param $date
     * @return bool|string
     */
    public function getMonth($date)
    {
        return date('F', strtotime($date));
    }

    /**
     * @param        $dateTime
     * @param string $format
     * @return bool|string
     */
    public function getReadableDateAndTimeFormat($dateTime, $format = 'M j, Y G:i:s')
    {
        return date($format, strtotime($dateTime));
    }

    /**
     * @param $time
     * @return bool|string
     */
    public function getTimeFormat($time)
    {

        return date('g:i a', strtotime($time));
    }

    /**
     * @param $dateTime
     * @return string
     */
    public function getAgoTime($dateTime)
    {
        $etime = time() - strtotime($dateTime);

        if ($etime < 1) {
            return '0 seconds';
        }

        $a = [365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        ];
        $a_plural = ['year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        ];

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

    /**
     * @return bool|string
     */
    public function getCurrentDay()
    {
        return date('l');
    }

    /**
     * @param $time
     * @return bool|string
     */
    public function getTimeWithMonth($time)
    {
        return date('F j, Y, g:i a', $time);
    }

    /**
     * Add days or week to date
     * @param integer $duration      value
     * @param string  $duration_type type
     * @return string
     * */
    public static function addDaysInDate($duration, $duration_type)
    {

        $type = 'day';
        if ($duration_type == 'Days') {
            $type = 'days';
        } else if ($duration_type == 'Months') {
            $type = 'months';
        } else if ($duration_type == 'Years') {
            $type = 'years';
        } else if ($duration_type == 'UnLimted') {
            $duration = 10;
            $type = 'years';
        }

        return date('Y-m-d', strtotime('+' . $duration . ' ' . $type));
    }

    /**
     * Returns every date between two dates as an array
     * @param string $startDate the start of the date range
     * @param string $endDate   the end of the date range
     * @param string $format    DateTime format, default is Y-m-d
     * @return array returns every date between $startDate and $endDate, formatted as "Y-m-d"
     */
    public function createDaysInDateRange($startDate, $endDate, $format = 'Y-m-d')
    {
        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $interval = new DateInterval('P1D'); // 1 Day
        $dateRange = new DatePeriod($begin, $interval, $end);

        $range = [];
        foreach ($dateRange as $date) {
            $range[] = $date->format($format);
        }

        return $range;
    }

    /**
     * @param $timestamp
     * @param $days
     * @return int
     */
    public function pastDate($timestamp, $days, $operator = '+') {
        return strtotime($operator.$days.' day', strtotime($timestamp) );
    }
}
