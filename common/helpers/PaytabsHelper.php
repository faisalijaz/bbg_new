<?php

namespace common\helpers;

@session_start();
define("TESTING", "https://localhost:8888/paytabs/apiv2/index");
define("AUTHENTICATION", "https://www.paytabs.com/apiv2/validate_secret_key");
define("PAYPAGE_URL", "https://www.paytabs.com/apiv2/create_pay_page");
define("VERIFY_URL", "https://www.paytabs.com/apiv2/verify_payment");

class PaytabsHelper
{

    private $merchant_id = "10025833";
    private $merchant_email = "info@bbgdxb.com";
    private $merchant_secretKey = "kSBNQh08Gv0em0ZLgf7qQQUuQZjJYnHjfi66z6fgCfuZftLvJTWAJR4zQoiPL081jf7Tlm9YCEXS8JoRClrlcwU3VdSHd1IwgQpC";
    public $api_key;

    /**
     * paytabs constructor.
     * @param $merchant_email
     * @param $merchant_secretKey
     */
    function paytabs()
    {
        $this->merchant_email = "";
        $this->merchant_secretKey = "";
        $this->api_key = "";
    }

    /**
     * @return string
     */
    function authentication()
    {
        $obj = json_decode($this->runPost(AUTHENTICATION, array("merchant_email" => $this->merchant_email, "merchant_secretKey" => $this->merchant_secretKey)));
        if ($obj->access == "granted")
            $this->api_key = $obj->api_key;
        else
            $this->api_key = "";
        return $this->api_key;
    }

    /**
     * @param $values
     * @return mixed
     */
    function create_pay_page($invoice, $cardData)
    {
        $user = null;
        if ($invoice <> null) {

            if ($invoice->invoice_related_to == 'event' && $invoice->user_id == '0') {

                $event_data ="" ;
                $name = "";

            } else {
                $user = $invoice->member;
                $name = ($user <> null) ? $user->first_name . " " . $user->last_name : "";
            }
        }

        $cc_name = explode(" ", $cardData);

        $values = [

            // Customer's Name on the invoice
            'title' => $name,

            // This will be prefilled as Credit Card First Name & Last Name
            'cc_first_name' => (isset($cc_name[0])) ? $cc_name[0] : "",
            'cc_last_name' => (isset($cc_name[1])) ? $cc_name[1] : "",
            'email' => ($user <> null) ? $user->email : "",
            'cc_phone_number' => "+971",
            'phone_number' => ($user <> null) ? $user->phone_number : "",

            //Customer's Billing Address (All fields are mandatory)
            // When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected.
            // For other countries, the state can be a string of up to 32 characters.
            'billing_address' => ($user <> null) ? $user->address : "",
            'city' => ($user <> null) ? $user->address : "",
            'state' => ($user <> null) ? $user->city : "",
            'postal_code' => ($user <> null && $user->accountCompany <> null) ? $user->accountCompany->postal_code : "",
            'country' => "ARE",

            //Customer's Shipping Address (All fields are mandatory)
            'address_shipping' => ($user <> null) ? $user->address : "",
            'city_shipping' => ($user <> null) ? $user->address : "",
            'state_shipping' => ($user <> null) ? $user->address : "",
            'postal_code_shipping' => ($user <> null && $user->accountCompany <> null) ? $user->accountCompany->postal_code : "",
            'country_shipping' => "ARE",


            //Product title of the product. If multiple products then add “||” separator
            "products_per_title" => ($invoice <> null) ? $invoice->invoice_rel_id . "/" . $invoice->invoice_category : "",

            //Currency of the amount stated. 3 character ISO currency code
            'currency' => "AED",
            "unit_price" => ($invoice <> null) ? $invoice->total : "",                    // Unit price of the product. If multiple products then add “||” separator.
            'quantity' => "1",                    //Quantity of products. If multiple products then add “||” separator
            'other_charges' => "0",            //Additional charges. e.g.: shipping charges, taxes, VAT, etc.
            'amount' => ($invoice <> null) ? $invoice->total : "",                //Amount of the products and other charges, it should be equal to: amount = (sum of all products’ (unit_price * quantity)) + other_charges
            //This field will be displayed in the invoice as the sub total field

            'discount' => "0",                    //Discount of the transaction. The Total amount of the invoice will be= amount - discount
            "msg_lang" => "english",            //Language of the PayPage to be created. Invalid or blank entries will default to English.(Englsh/Arabic)


            "reference_no" => ($invoice <> null) ? $invoice->invoice_id : "",        // Invoice reference number in your system
            "site_url" => "https://www.bbgdubai.org", //The requesting website be exactly the same as the website/URL associated with your PayTabs Merchant Account
            'return_url' => "http://bbg.manal.ae/site/verify-payment/",
            "cms_with_version" => "Website API 3.1"
        ];

        $values['merchant_email'] = $this->merchant_email;
        $values['merchant_secretKey'] = $this->merchant_secretKey;
        $values['secret_key'] = $this->merchant_secretKey;
        $values['ip_customer'] = $_SERVER['REMOTE_ADDR'];
        $values['ip_merchant'] = $_SERVER['SERVER_ADDR'];

        return json_decode($this->runPost(PAYPAGE_URL, $values));
    }

    /**
     * @return mixed
     */
    function send_request()
    {
        $values['ip_customer'] = $_SERVER['REMOTE_ADDR'];
        $values['ip_merchant'] = $_SERVER['SERVER_ADDR'];
        return json_decode($this->runPost(TESTING, $values));
    }

    /***
     * @param $payment_reference
     * @return mixed
     */
    function verify_payment($payment_reference)
    {
        $values['merchant_email'] = $this->merchant_email;
        $values['merchant_secretKey'] = $this->merchant_secretKey;
        $values['secret_key'] = $this->merchant_secretKey;
        $values['payment_reference'] = $payment_reference;
        return json_decode($this->runPost(VERIFY_URL, $values));
    }

    /**
     * @param $url
     * @param $fields
     * @return mixed
     */
    function runPost($url, $fields)
    {

        $fields_string = "";

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        rtrim($fields_string, '&');

        $ch = curl_init();
        $ip = $_SERVER['REMOTE_ADDR'];

        $ipaddress = array(
            "REMOTE_ADDR" => $ip,
            "HTTP_X_FORWARDED_FOR" => $ip
        );
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $ipaddress);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, 1);

        $result = curl_exec($ch);
        print_r($result);
        curl_close($ch);

        return $result;
    }
}

?>