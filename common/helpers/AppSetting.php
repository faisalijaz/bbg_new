<?php

namespace common\helpers;

use common\models\MenuWidget;
use common\models\Settings;
use yii\helpers\ArrayHelper;

/**
 * Country Model
 *
 * @author Nadeem Akhtar <nadeemakhtar.se@gmail.com>
 */
class AppSetting
{

    public $settings;

    public function __construct() {
        $this->settings = Settings::find()->one();
        //$this->MenuWidget = MenuWidget::find()->one();
        $this->MenuWidget = ArrayHelper::map(MenuWidget::find()->where(['parent_id' => 0, 'status' => "active"])->orderBy(['sort_order' => SORT_ASC])->all(), 'title', 'url');
    }

    public function getSettings() {
        return $this->settings;
    }

    public function getByAttribute($attribute) {
        return $this->settings->{$attribute};
    }

    public function getMenuItems() {

        // return $this->MenuWidget;

        $menu = MenuWidget::find()->where(['parent_id' => 0, 'status' => "active"])->orderBy(['sort_order' => SORT_ASC])->all();
        $array = [];

        foreach ($menu as $item) {

            if ($item->members_only) {

                if (!\Yii::$app->user->isGuest) {

                    if ($item->url == "/apply/membership") {

                    } else {
                        $array[$item->title] = $item->url;
                    }
                }

            } else {
                $array[$item->title] = $item->url;
            }

        }

        return $array;
    }

    public function getSubMenuItems($attribute) {

        $usr_data = MenuWidget::find()->where(['title' => $attribute, 'status' => 'active'])
            ->orderBy(['sort_order' => SORT_ASC])->all();
        $for_members = false;

        foreach ($usr_data as $data){
            $id =  $data->id;

        }

        if ($usr_data <> null) {


            $menu_chlids = MenuWidget::find()->where(['parent_id' => $id])->orderBy(['sort_order' => SORT_ASC])->all();
            $array = [];

            foreach ($menu_chlids as $child) {

                $memberOnly = MenuWidget::find()->where(['url' => $child->url, 'members_only' => '1'])
                    ->orderBy(['sort_order' => SORT_ASC])->all();

                if ($memberOnly <> null) {

                    if (!\Yii::$app->user->isGuest) {

                        if ($child->url == "/apply/membership") {

                        } else {
                            $array[$child->title] = $child->url;
                        }
                    }

                } else {
                    $array[$child->title] = $child->url;
                }

            }
            return $array;
        }

    }

    /**
     * Get Random unique number with length
     * @param int $length string length
     * @param int $domd5  string
     * @return string
     */
    public function getRandomNumber($length = 6, $domd5 = 0)
    {

        $conso = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '*', '#'];
        $vocal = ['A', 'E', 'I', 'O', 'U'];
        $password = '';
        srand((double)microtime() * 1000000);
        $max = $length / 2;
        for ($i = 1; $i <= $max; $i++) {
            $password .= $conso[rand(0, 19)];
            $password .= $vocal[rand(0, 4)];
        }
        if ($domd5) {
            return md5($password);
        } else {
            return $password;
        }
    }
}
