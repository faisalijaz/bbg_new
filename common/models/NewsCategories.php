<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_categories".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $status
 * @property string $date
 * @property int $created_by
 */
class NewsCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['short_description', 'status'], 'string'],
            [['date','sort_order'], 'safe'],
            [['created_by'], 'integer'],
            [['title'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'short_description' => Yii::t('app', 'Short Description'),
            'status' => Yii::t('app', 'Status'),
            'date' => Yii::t('app', 'Date'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
