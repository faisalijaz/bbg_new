<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widgets".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $code
 * @property integer $status
 *
 * @property UserWidget[] $userWidgets
 */
class Widgets extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $items = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widgets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'type'], 'required'],
            [['status','sorting_order'], 'integer'],
            [['items'], 'safe'],
            [['name', 'code', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWidgets()
    {
        return $this->hasMany(UserWidget::className(), ['widget_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentUserWidgets()
    {
        return $this->hasOne(UserWidget::className(), ['widget_id' => 'id'])->where(['user_id' => Yii::$app->user->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetItems()
    {
        return $this->hasMany(WidgetItems::className(), ['widget_id' => 'id']);
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        WidgetItems::deleteAll(['widget_id' => $this->id]);

        foreach ($this->items as $item) {
            $widgetItem = new WidgetItems();
            $widgetItem->attributes = $item;
            $widgetItem->widget_id = $this->id;
            $widgetItem->save();
        }
    }
}
