<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "promotion_redemption".
 *
 * @property integer $id
 * @property integer $promotion_id
 * @property integer $account_id
 *
 * @property Accounts $account
 * @property Promotions $promotion
 */
class PromotionRedemption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotion_redemption';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promotion_id'], 'required'],
            [['promotion_id', 'account_id'], 'integer'],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::className(), 'targetAttribute' => ['account_id' => 'id']],
            [['promotion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Promotions::className(), 'targetAttribute' => ['promotion_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'promotion_id' => Yii::t('app', 'Promotion ID'),
            'account_id' => Yii::t('app', 'Account ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(Promotions::className(), ['id' => 'promotion_id']);
    }
}
