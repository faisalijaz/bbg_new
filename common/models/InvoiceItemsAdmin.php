<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoice_items".
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $user_id
 * @property string $invoice_related_to
 * @property int $invoice_rel_id
 * @property string $invoice_category
 * @property string $payment_status
 * @property int $status
 * @property string $amount
 * @property string $pdf_name
 * @property string $invoice_date
 * @property string $created_date
 * @property string $subtotal
 * @property string $tax
 * @property int $payment_id
 */
class InvoiceItemsAdmin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'status', 'payment_id'], 'required'],
            [['invoice_id', 'user_id', 'invoice_rel_id', 'status', 'payment_id'], 'integer'],
            [['invoice_related_to', 'payment_status'], 'string'],
            [['amount', 'subtotal', 'tax'], 'number'],
            [['created_date'], 'safe'],
            [['invoice_category'], 'string', 'max' => 500],
            [['pdf_name', 'invoice_date'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'invoice_related_to' => Yii::t('app', 'Invoice Related To'),
            'invoice_rel_id' => Yii::t('app', 'Invoice Rel ID'),
            'invoice_category' => Yii::t('app', 'Invoice Category'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'status' => Yii::t('app', 'Status'),
            'amount' => Yii::t('app', 'Amount'),
            'pdf_name' => Yii::t('app', 'Pdf Name'),
            'invoice_date' => Yii::t('app', 'Invoice Date'),
            'created_date' => Yii::t('app', 'Created Date'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'tax' => Yii::t('app', 'Tax'),
            'payment_id' => Yii::t('app', 'Payment ID'),
        ];
    }
}
