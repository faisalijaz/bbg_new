<?php

namespace common\models;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $address
 * @property string $street
 * @property string $area
 * @property string $city
 * @property string $country
 *
 * @property Accounts $account
 * @property AddressCity $addressCity
 * @property AddressArea $addressArea
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return string
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            // [['address', 'area', 'city'], 'required'],
            [['account_id'], 'integer'],
            [['address', 'street', 'area', 'city', 'country'], 'string', 'max' => 255],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::className(), 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'address' => 'Address',
            'street' => 'Street',
            'area' => 'Area',
            'city' => 'City',
            'country' => 'Country',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressArea()
    {
        return $this->hasOne(Areas::className(), ['id' => 'area']);
    }
}
