<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $status
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'show_industry_only', 'show_company', 'joining_fee', 'add_nominee', 'fee_nominee', 'add_alternate', 'fee_alternate', 'add_additional', 'fee_additional', 'add_associate', 'fee_associate'], 'required'],
            [['created_at', 'updated_at','sort_order'], 'integer'],
            [['status','description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'joining_fee' => Yii::t('app', 'Joining Fee'),
            'fee_nominee' => Yii::t('app', 'Membership Fee'),
            'add_nominee' => Yii::t('app', 'Can Add'),
            'fee_alternate' => Yii::t('app', 'Membership Fee'),
            'add_alternate' => Yii::t('app', 'Can Add'),
            'fee_additional' => Yii::t('app', 'Membership Fee'),
            'add_additional' => Yii::t('app', 'Can Add'),
            'fee_associate' => Yii::t('app', 'Membership Fee'),
            'add_associate' => Yii::t('app', 'Can Add'),
            'show_industry_only' => Yii::t('app', 'Show Industry Only'),
            'show_company' => Yii::t('app', 'Show Company'),
            'description' => Yii::t('app', 'Description'),

        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

}
