<?php

namespace common\models;

use backend\models\BookingItem;
use Yii;
use yii\base\Model;

/**
 * Validate promo code
 * @property integer $promoCode
 */
class PromoCode extends Model
{
    public $promoCode;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [

            [['promoCode'], 'required'],
            ['promoCode', 'exist', 'targetClass' => '\common\models\Promotions', 'targetAttribute' => 'code', 'message' => 'Invalid promo code'],
        ];
    }

    /* beforeValidate function that will validate if customer exists, customer user promo already or not
     * @params
     * */

    public function afterValidate()
    {


        if ($this->promoCode && !count($this->getErrors()) > 0) {
            $promo = Promotions::find()->where(['code' => $this->promoCode])->one();
            $account = \Yii::$app->user->getIdentity();

            // Check if promo code still available and not yet expired
            $todayDate = strtotime(\Yii::$app->dateTime->getTime());
            $promoDateBegin = strtotime($promo->start_date);
            $promoDateEnd = strtotime($promo->end_date);

            if (!($todayDate > $promoDateBegin) || !($todayDate < $promoDateEnd)) {
                $this->addError('Expire', 'The promo code you have entered has unfortunately already expired.');
                return false;
            }

            // check if code is active and not disable
            if ($promo->status == 0) {
                $this->addError('InActive', 'The promo code is now inactive.');
                return false;
            }

            // check if customer have already used promo or not
            $promotionRedemption = PromotionRedemption::find()->where(['account_id' => $account->id, 'promotion_id' => $promo->id])->one();


            if ($promo->redeem_type == 'single' && $promotionRedemption !== null) {
                $this->addError('AlreadyUsed', 'The promo code you have entered has already been redeemed.');
                return false;
            }

            // check if promo code is only for specific customers
            $promoForCustomer = PromotionsForUsers::find()->where(['user_account_id' => $account->id, 'promotion_code_id' => $promo->id])->one();

            if ($promo->is_customer_specific && $promoForCustomer === null) {
                $this->addError('inValidRedeem', 'Oh snap. It seems like you are not entitled to use this promo code.');
                return false;
            }

            // if promo code is valid from time and to time then check is time in between from & time
            if ((!empty($promo->start_time) && !empty($promo->end_time)) && !(Yii::$app->dateTime->validateInBetween($promo->start_time, $promo->end_time))) {
                $this->addError('deliveryTime', 'The promo code you have entered is only valid from ' . Yii::$app->dateTime->getTimeFormat($promo->start_time) . '  until ' . Yii::$app->dateTime->getTimeFormat($promo->end_time));
                return false;
            }
        }
    }

    /**
     * Apply Promo Code
     * */
    public function applyCode()
    {

        Yii::$app->session->set('promoCode', $this->promoCode);
        (new CartModel())->applyPromo();
    }

    /**
     * Apply Promo Code
     * */
    public function applyAdminCode()
    {

        Yii::$app->session->set('promoCode', $this->promoCode);
        (new BookingItem())->applyPromo();
    }


    /**
     * Apply Promo Code
     * */
    public function cancelAdminCode()
    {
        (new BookingItem())->cancelPromo();
    }

    /**
     * Cancel Promo Code
     * */
    public function cancelCode()
    {
        (new CartModel())->cancelPromo();
    }
}
