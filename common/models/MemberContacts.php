<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "member_contacts".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $contact_id
 * @property string $date_added
 */
class MemberContacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'contact_id', 'date_added'], 'required'],
            [['member_id', 'contact_id'], 'integer'],
            [['date_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'contact_id' => 'Contact ID',
            'date_added' => 'Date Added',
        ];
    }

    public function getMemberInfo(){
        return $this->hasOne(Members::className(), ['id' => 'contact_id']);
    }
}
