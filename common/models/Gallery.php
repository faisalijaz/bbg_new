<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $image
 * @property string $size
 * @property string $position
 * @property string $status
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'required'],
            [['name', 'image','video_link'], 'string'],
            [['size', 'position', 'status'], 'string'],
            [['description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'size' => 'Size',
            'position' => 'Position',
            'status' => 'Status',
            'video_link' => 'Video Link',
        ];
    }
}
