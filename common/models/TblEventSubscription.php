<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_event_subscription".
 *
 * @property int $id
 * @property int $user_id
 * @property string $user_type
 * @property int $parent_id
 * @property int $user_parent_id the person registered
 * @property int $event_id
 * @property string $session_data
 * @property string $payment_session_data
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $userNote
 * @property string $mobile
 * @property string $company
 * @property string $walkin
 * @property string $attended
 * @property double $fee_paid
 * @property string $payment_status
 * @property string $status
 * @property string $create_date
 * @property int $gen_invoice
 * @property int $focGuest
 * @property string $paymentMethod
 * @property double $sub_total
 * @property double $vat
 */
class TblEventSubscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_event_subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'parent_id', 'user_parent_id', 'event_id', 'gen_invoice', 'focGuest'], 'integer'],
            [['user_parent_id', 'event_id', 'payment_session_data', 'userNote', 'focGuest', 'paymentMethod', 'sub_total', 'vat'], 'required'],
            [['userNote', 'walkin', 'attended', 'status'], 'string'],
            [['fee_paid', 'sub_total', 'vat'], 'number'],
            [['create_date'], 'safe'],
            [['user_type', 'firstname', 'lastname', 'mobile', 'payment_status'], 'string', 'max' => 25],
            [['session_data', 'payment_session_data'], 'string', 'max' => 100],
            [['email'], 'string', 'max' => 250],
            [['company'], 'string', 'max' => 150],
            [['paymentMethod'], 'string', 'max' => 240],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'user_type' => Yii::t('app', 'User Type'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'user_parent_id' => Yii::t('app', 'User Parent ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'session_data' => Yii::t('app', 'Session Data'),
            'payment_session_data' => Yii::t('app', 'Payment Session Data'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'email' => Yii::t('app', 'Email'),
            'userNote' => Yii::t('app', 'User Note'),
            'mobile' => Yii::t('app', 'Mobile'),
            'company' => Yii::t('app', 'Company'),
            'walkin' => Yii::t('app', 'Walkin'),
            'attended' => Yii::t('app', 'Attended'),
            'fee_paid' => Yii::t('app', 'Fee Paid'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'status' => Yii::t('app', 'Status'),
            'create_date' => Yii::t('app', 'Create Date'),
            'gen_invoice' => Yii::t('app', 'Gen Invoice'),
            'focGuest' => Yii::t('app', 'Foc Guest'),
            'paymentMethod' => Yii::t('app', 'Payment Method'),
            'sub_total' => Yii::t('app', 'Sub Total'),
            'vat' => Yii::t('app', 'Vat'),
        ];
    }
}
