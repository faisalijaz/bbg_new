<?php

namespace common\models;

/**
 * This is the model class for table "event_images".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $image
 * @property integer $sort_order
 */
class EventImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'image'], 'required'],
            [['event_id', 'sort_order', 'user_id'], 'integer'],
            [['image', 'title', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }
}
