<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "membership_event_fee".
 *
 * @property integer $id
 * @property integer $member_type
 * @property integer $member_group
 * @property integer $event_id
 * @property string $fee
 */
class MembershipEventFee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membership_event_fee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_type', 'member_group', 'event_id', 'fee'], 'required'],
            [['member_group', 'event_id'], 'integer'],
            [['member_type', ],'string'],
            [['fee'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_type' => 'Member Type',
            'member_group' => 'Member Group',
            'event_id' => 'Event ID',
            'fee' => 'Fee',
        ];
    }
}
