<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_news".
 *
 * @property int $id
 * @property string $related
 * @property string $category_id
 * @property string $title
 * @property string $image
 * @property string $description
 * @property int $user_id
 * @property int $member_id
 * @property string $link
 * @property string $isMember
 * @property string $modifiedDate
 * @property string $createdDate
 * @property string $isActive
 */
class TblNews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'description', 'link', 'isActive'], 'string'],
            [['user_id', 'member_id'], 'integer'],
            [['member_id', 'link', 'isMember'], 'required'],
            [['modifiedDate', 'createdDate'], 'safe'],
            [['related', 'category_id'], 'string', 'max' => 25],
            [['title'], 'string', 'max' => 250],
            [['isMember'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'related' => Yii::t('app', 'Related'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title' => Yii::t('app', 'Title'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'link' => Yii::t('app', 'Link'),
            'isMember' => Yii::t('app', 'Is Member'),
            'modifiedDate' => Yii::t('app', 'Modified Date'),
            'createdDate' => Yii::t('app', 'Created Date'),
            'isActive' => Yii::t('app', 'Is Active'),
        ];
    }
}
