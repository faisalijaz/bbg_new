<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_invoice".
 *
 * @property int $invoice_id
 * @property int $parent_user_id
 * @property string $user_invoice_id
 * @property string $invoice_category
 * @property string $payment_status
 * @property string $amount
 * @property string $pdf_name
 * @property string $invoice_date
 * @property string $created_date
 */
class TblInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_user_id'], 'integer'],
            [['created_date','note'], 'safe'],
            [['user_invoice_id', 'amount'], 'string', 'max' => 20],
            [['invoice_category'], 'string', 'max' => 100],
            [['payment_status', 'pdf_name', 'invoice_date'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'parent_user_id' => Yii::t('app', 'Parent User ID'),
            'user_invoice_id' => Yii::t('app', 'User Invoice ID'),
            'invoice_category' => Yii::t('app', 'Invoice Category'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'amount' => Yii::t('app', 'Amount'),
            'pdf_name' => Yii::t('app', 'Pdf Name'),
            'invoice_date' => Yii::t('app', 'Invoice Date'),
            'created_date' => Yii::t('app', 'Created Date'),
        ];
    }

    public function getMember(){
        return $this->hasOne(Members::className(),['id' => 'parent_user_id']);
    }
}
