<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tax_group_rates".
 *
 * @property integer $id
 * @property integer $tax_group_id
 * @property integer $tax_id
 * @property integer $sort_order
 *
 * @property TaxGroups $taxGroup
 * @property TaxRates $tax
 */
class TaxGroupRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_group_rates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_group_id', 'tax_id'], 'required'],
            [['tax_group_id', 'tax_id', 'sort_order'], 'integer'],
            [['tax_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxGroups::className(), 'targetAttribute' => ['tax_group_id' => 'id']],
            [['tax_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxRates::className(), 'targetAttribute' => ['tax_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tax_group_id' => 'Tax Group ID',
            'tax_id' => 'Tax ID',
            'sort_order' => 'Sort Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxGroup()
    {
        return $this->hasOne(TaxGroups::className(), ['id' => 'tax_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTax()
    {
        return $this->hasOne(TaxRates::className(), ['id' => 'tax_id']);
    }
}
