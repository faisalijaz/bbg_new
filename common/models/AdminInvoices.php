<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoices".
 *
 * @property int $invoice_id
 * @property int $user_id
 * @property string $invoice_related_to
 * @property int $invoice_rel_id
 * @property string $invoice_category
 * @property string $payment_status
 * @property double $subtotal
 * @property double $tax
 * @property double $total
 * @property string $invoice_date
 * @property string $invoice_sent
 * @property int $payment_id
 * @property int $paytabs_p_id
 * @property string $paytabs_payment_url
 */
class AdminInvoices extends \yii\db\ActiveRecord
{
    public $items;
    public $adjustment;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'invoice_rel_id', 'payment_id', 'paytabs_p_id'], 'integer'],
            [['invoice_related_to', 'invoice_rel_id', 'invoice_category', 'payment_id', 'paytabs_p_id', 'paytabs_payment_url'], 'required'],
            [['invoice_related_to', 'payment_status', 'invoice_sent'], 'string'],
            [['subtotal', 'tax', 'total'], 'number'],
            [['invoice_date','items','adjustment'], 'safe'],
            [['invoice_category'], 'string', 'max' => 500],
            [['paytabs_payment_url'], 'string', 'max' => 1200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'invoice_related_to' => Yii::t('app', 'Invoice Related To'),
            'invoice_rel_id' => Yii::t('app', 'Invoice Rel ID'),
            'invoice_category' => Yii::t('app', 'Invoice Category'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'tax' => Yii::t('app', 'Tax'),
            'total' => Yii::t('app', 'Total'),
            'invoice_date' => Yii::t('app', 'Invoice Date'),
            'invoice_sent' => Yii::t('app', 'Invoice Sent'),
            'payment_id' => Yii::t('app', 'Payment ID'),
            'paytabs_p_id' => Yii::t('app', 'Paytabs P ID'),
            'paytabs_payment_url' => Yii::t('app', 'Paytabs Payment Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceItems(){
        return $this->hasMany(InvoiceItemsAdmin::className(),['invoice_id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceAdjustment(){
        return $this->hasMany(InvoiceAdjustments::className(),['invoice_id' => 'invoice_id']);
    }
}
