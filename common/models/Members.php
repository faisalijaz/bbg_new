<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "accounts".
 *
 * @property integer $id
 * @property string  $email
 * @property string  $auth_key
 * @property string  $password_hash
 * @property string  $password_reset_token
 * @property integer $status
 * @property integer $amazing_offers
 * @property integer $occasional_updates
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $account_type
 * @property string  $first_name
 * @property string  $last_name
 * @property string  $dob
 * @property integer $gender
 * @property integer $country
 * @property string  $socialType
 * @property string  $socialID
 * @property string  $verification_code
 * @property string  $phone_verification
 * @property string  $picture
 * @property string  $phone_number
 * @property string  $country_code
 * @property string  $group_id
 *
 */
class Members extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;


    /**
     * @var
     */
    public $password;
    public $companyData = [];
    public $interested_category;
    public $terms;
    public $newsletter = 0;
    public $user_docs = [];
    public $sync_events_calander;
    public $last_email;

    /**
     * @inheritdoc
     * @return string
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'auth_key', 'first_name', 'password_hash'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'on' => 'insert', 'targetClass' => '\common\models\Members', 'message' => 'This email address has already been taken.'],

            [['invoiced', 'approved', 'status', 'newsletter', 'occasional_updates', 'created_at', 'updated_at', 'gender',  'group_id', 'parent_id'], 'integer'],
            [['account_type', 'socialType', 'verification_code', 'phone_verification',], 'string'],
            [['socialType', 'mem_invoice_gen', 'renw_invoice_gen', 'honourary', 'focus_chair', 'committee', 'sponsor', 'charity', 'sticky', 'company'], 'default', 'value' => 0],

            // [['last_renewal', 'expiry_date', 'registeration_date'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],

            [['delete_request', 'login_first', 'dob', 'country_code', 'country','compnayData', 'user_docs', 'bbg_membershipid', 'old_membershipid', 'lastvisit', 'lastpasswordchange'], 'safe'],
            [['email', 'designation', 'password_hash', 'password_reset_token', 'first_name', 'last_name', 'socialID', 'vat_number', 'user_name'], 'string', 'max' => 255],
            [['phone_number', 'auth_key', 'secondry_email', 'last_renewal', 'expiry_date', 'registeration_date','referred_by'], 'string',/* 'max' => 64*/],

            [['password_reset_token'], 'unique'],
            [['company_type', 'purposeOfJoining', 'tellUsAboutYourself', 'howDidYouHear', 'nationality', 'linkedin', 'twitter', 'last_email', 'address', 'sync_events_calander', 'created_at', 'invoice_renewal', 'updated_at', 'picture', 'summary', 'newsletter', 'terms', 'companyData', 'interested_category', 'user_docs', 'city', 'title', 'user_industry'], 'safe'],
            ['approved', 'default', 'value' => self::STATUS_INACTIVE],
            ['approved', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }


    /**
     * Set password if not empty
     * @param boolean $insert yes or not
     * @return boolean
     * */

    public function beforeSave($insert)
    {
        $company = AccountCompany::findOne($this->company);

        if ($company == null) {
            $company = new AccountCompany();
        }

        if ($this->companyData <> null && $this->group_id <> 2) {

            $company->attributes = $this->companyData;

            if (!$company->save()) {
                $this->addError('company_error', $company->getErrors());
                return false;
            }

            $this->company = $company->id;
            // $company->save();
            // return true;
        }

        if ($this->interested_category <> null) {

            InterestedCategories::deleteAll('member_id = :id ', [':id' => $this->id]);

            $new_interest = new InterestedCategories();

            foreach ($this->interested_category as $key => $row) {
                $new_interest->setIsNewRecord(true);
                $new_interest->id = null;
                $new_interest->member_id = $this->id;
                $new_interest->category_id = $row;
                $new_interest->save();
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->user_docs <> null) {

            $mem_docs = new MembershipDocuments();
            $mem_docs->attributes = $this->user_docs;
            $mem_docs->account_id = $this->id;

            if (!$mem_docs->save()) {
                $this->addError('documents_error', $mem_docs->getErrors());
                return false;
            }

        }

        if ($this->invoice_renewal <> null && $this->invoice_renewal > 0) {

            $membership_renewals = new MembershipRenewals();

            $membership_renewals->invoice_id = $this->invoice_renewal;
            $membership_renewals->member_id = $this->id;
            $membership_renewals->renewed_by = Yii::$app->user->id;
            $membership_renewals->description = "Membership Renewal";
            $membership_renewals->save();
        }
    }

    /**
     * @param int|string $id to find
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @param mixed $token token
     * @param null  $type type of user
     * @return static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * @param string $username username
     * @return static
     */
    public static function findByUsername($username)
    {
        return Members::find()->where(['email' => $username])->andWhere(['<>', 'status', 0])->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return Members::find()->where(['password_reset_token' => $token])->andWhere(['<>', 'status', 0])->one();
    }

    /**
     * @param string $token user authToken
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     * @return string
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new password reset token
     * @return string
     */
    public function setVerificationCode()
    {
        $this->verification_code = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * @inheritdoc
     * @return integer
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     * @return integer
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     * @param  string $authKey authentication key for user
     * @return string
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password password of user
     * @return string
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return string
     */
    public function genRandomPassword()
    {
        $length = 10;
        $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        shuffle($chars);
        return implode(array_slice($chars, 0, $length));
    }

    /**
     * Generates "remember me" authentication key
     * @return string
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }


    /**
     * Removes password reset token
     * @return string
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param string $username username
     * @return array
     */
    public function getUserDetails($username)
    {
        $user = static::findByEmail($username);

        return ['user_id' => $user->getId()];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserIndustry()
    {
        return $this->hasOne(Categories::className(), ['id' => 'user_industry']);
    }

    /**
     * Finds user by username
     *
     * @param string $email email
     *
     * @return null|static
     *
     * @author Nadeem Akhtar <nadeem@myswich.com>
     *
     */
    public static function findByEmail($email)
    {
        return static::findOne(['user_name' => $email]);
    }

    /**
     * Check User Credentials
     * @param string $username username
     * @param string $password Password of user
     * @return string
     * */
    public function checkUserCredentials($username, $password)
    {
        $user = static::findByEmail($username);
        if (empty($user)) {
            return false;
        }

        return $user->validatePassword($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountAddress()
    {
        return $this->hasMany(Address::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalties()
    {
        return $this->hasMany(Loyalty::className(), ['account_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return integer
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return integer
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoices::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountCompany()
    {
        return $this->hasOne(AccountCompany::className(), ['id' => 'company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterestedCategories()
    {
        return $this->hasMany(InterestedCategories::className(), ['member_id' => 'id']);
    }

    /**
     * @param $id
     * @return \yii\db\ActiveQuery
     */
    public function getContacts($id)
    {
        return $this->hasMany(Accounts::className(), ['parent_id' => $id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointOfContact()
    {
        return $this->hasMany(PointOfContact::className(), ['user_id' => 'id']);
    }

    /**
     * @param $id
     * @return \yii\db\ActiveQuery
     */
    public function getMemberContacts()
    {
        return $this->hasMany(MemberContacts::className(), ['member_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckMemberContacts()
    {
        return $this->hasOne(MemberContacts::className(), ['contact_id' => 'id', 'member_id' => Yii::$app->user->id]);
    }

    /**
     * @param $member
     * @param $contact
     * @return null|static
     */
    public function checkMemberContact($member, $contact)
    {
        return MemberContacts::findOne(['member_id' => $member, 'contact_id' => $contact]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberDocs()
    {
        return $this->hasOne(MembershipDocuments::className(), ['account_id' => 'id']);
    }

    /**
     * @param $id
     * @return \yii\db\ActiveQuery
     */
    public function getMemberEventsReg()
    {
        return $this->hasMany(EventSubscriptions::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberReferral()
    {
        return $this->hasOne(ReferralSources::className(), ['id' => 'howDidYouHear']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    Public function createInvoice($member, $type)
    {

        if ($member <> null) {

            $transaction = \Yii::$app->db->beginTransaction();

            try {

                $invoice = new Invoices();

                $invoice->user_id = $member->id;
                $invoice->invoice_related_to = "member";
                $invoice->invoice_prefix = 'M';
                $invoice->invoice_rel_id = $member->id;
                $invoice->invoice_category = $type;

                if ($invoice->save()) {
                    if (!$this->createInvoiceItems($member, $invoice)) {
                        return false;
                    }
                } else {
                    return false;
                }

                $transaction->commit();
                return $invoice->invoice_id;

            } catch (\Exception $exception) {
                $transaction->rollback();
                throw $exception;
            }
        }
    }

    /**
     * @param $member
     * @param $invoice
     * @return array
     */
    public function createInvoiceItems($member, $invoice)
    {
        $errors = [];

        $group = ($member->group <> null) ? $member->group : null;


        $invoicesitems = InvoiceItems::find($invoice->invoice_id);

        if(!empty($invoicesitems)) {
            InvoiceItems::deleteAll(['invoice_id' => $invoice->invoice_id]);
        }
        $membership = new InvoiceItems();

        $membership->invoice_id = $invoice->invoice_id;
        $membership->user_id = $invoice->user_id;
        $membership->invoice_related_to = "membership";
        $membership->invoice_rel_id = $member->group_id;
        $membership->invoice_category = "Membership (" . ($group <> null) ? $group->title : "" . ")";
        $membership->payment_status = "unpaid";

        if ($member->account_type == "nominee" || $member->account_type == "member") {
            $membership->subtotal = ($group <> null) ? $group->fee_nominee : 0;
        }

        if ($member->account_type == "alternate") {
            $membership->subtotal = ($group <> null) ? $group->fee_alternate : 0;
        }

        if ($member->account_type == "additional") {
            $membership->subtotal = ($group <> null) ? $group->fee_additional : 0;
        }

        if ($member->account_type == "named_associate") {
            $membership->subtotal = ($group <> null) ? $group->fee_associate : 0;
        }

        $membership->tax = $this->calculateVat($membership->subtotal);
        $membership->amount = $membership->tax + $membership->subtotal;

        if (!$membership->save()) {
            $errors [] = $membership->getErrors();
        }

        // Create Item for Joining Fee for new Members
        if ($invoice->invoice_category == "Membership Upgrade" || $invoice->invoice_category == "Membership Renewal") {
            return true;
        } else {
            if ($membership->status == 0) {

                $membershipJoining = new InvoiceItems();

                $membershipJoining->invoice_id = $invoice->invoice_id;
                $membershipJoining->user_id = $invoice->user_id;
                $membershipJoining->invoice_related_to = "membership";
                $membershipJoining->invoice_rel_id = $member->group_id;
                $membershipJoining->invoice_category = "Membership Joining Fee";
                $membershipJoining->payment_status = "unpaid";
                $membershipJoining->subtotal = ($group <> null) ? $group->joining_fee : 0;
                $membershipJoining->tax = $this->calculateVat($membershipJoining->subtotal);
                $membershipJoining->amount = $membershipJoining->tax + $membershipJoining->subtotal;

                if (!$membershipJoining->save()) {
                    $errors [] = $membershipJoining->getErrors();
                }

                if (count($errors) > 0) {
                    return false;
                }

                return true;
            }
        }
    }

    /***
     * @param $member
     * @return array|bool
     */
    public function createRenewalInvoice($member)
    {
        $group = null;
        if ($member <> null) {
            $group = $member->group;
        }

        if ($member <> null) {

            if ($member->invoice_renewal) {

                $invoice = Invoices::findOne(['invoice_id' => $member->invoice_renewal]);

                if ($invoice == null) {
                    $invoice = new Invoices();
                } else {

                    $adjustments = InvoiceAdjustments::findOne(['invoice_id' => $invoice->invoice_id]);

                    return ['invoice_num' => $invoice->invoice_id, 'invoice' => $invoice, 'adjustments' => $adjustments];

                }

            } else {
                $invoice = new Invoices();
            }

            $invoice->invoice_related_to = 'renewal';
            $invoice->invoice_prefix = 'R';
            $invoice->invoice_rel_id = $member->id;
            $invoice->invoice_category = ($group <> null) ? $group->title . " Membership Renewal" : "";
            $invoice->payment_status = 'unpaid';
            $invoice->invoice_date = date("Y-m-d");


            if ($invoice->save()) {

                $items = InvoiceItems::findOne(['invoice_id' => $invoice->invoice_id]);

                if ($items == null) {
                    $items = new InvoiceItems();
                }

                $items->invoice_id = $invoice->invoice_id;
                $items->user_id = $member->id;
                $items->invoice_related_to = 'member';
                $items->invoice_rel_id = $member->id;
                $items->invoice_category = ($group <> null) ? $group->title . " Membership Renewal" : "";

                if ($member->account_type == "nominee" || $member->account_type == "member") {
                    $items->subtotal = round(($group <> null) ? $group->fee_nominee : 0, 2);
                }

                if ($member->account_type == "alternate") {
                    $items->subtotal = round(($group <> null) ? $group->fee_alternate : 0, 2);
                }

                if ($member->account_type == "additional") {
                    $items->subtotal = round(($group <> null) ? $group->fee_additional : 0, 2);
                }

                if ($member->account_type == "named_associate") {
                    $items->subtotal = round(($group <> null) ? $group->fee_associate : 0, 2);
                }

                $items->tax = round($this->calculateVat($items->subtotal), 2);
                $items->amount = round($items->subtotal + $items->tax, 2);
                $items->save();

                $member->renw_invoice_gen = 1;
                $member->invoice_renewal = $invoice->invoice_id;
                $member->save();

                if ($invoice <> null) {

                    $adjustments = InvoiceAdjustments::findOne(['invoice_id' => $invoice->invoice_id]);

                    if ($adjustments == null) {
                        $adjustments = new InvoiceAdjustments();
                    }

                    $adjustments->invoice_id = $invoice->invoice_id;

                } else {

                    $adjustments = new InvoiceAdjustments();
                }

                return ['invoice_num' => $member->invoice_renewal, 'invoice' => $invoice, 'adjustments' => $adjustments];
            }

        }
        return false;
    }

    /***
     * @param $id
     * @param $group_id
     * @param $account_type
     * @return \yii\db\ActiveQuery
     * @throws \Exception
     */
    public function upgradeMembership($id, $group_id, $account_type)
    {
        $member = Members::findOne($id);
        $group = Groups::findOne($member->group_id);

        if ($member->group_id == $group_id) {
            $errors[] = "This Member alreay has same membership type";
        }

        $lastRenwal = $member->last_renewal;
        $expiry = $member->expiry_date;

        $total_months = $this->getMonths($lastRenwal, $expiry);
        $remaining_months = $this->getMonths(date('Y-m-d'), $expiry);

        $fee_paid = 0;

        if ($group <> null) {

            if ($member->account_type == "nominee" || $member->account_type == "member") {
                $fee_paid = round(($group <> null) ? $group->fee_nominee : 0, 2);
            }

            if ($member->account_type == "alternate") {
                $fee_paid = round(($group <> null) ? $group->fee_alternate : 0, 2);
            }

            if ($member->account_type == "additional") {
                $fee_paid = round(($group <> null) ? $group->fee_additional : 0, 2);
            }

            if ($member->account_type == "named_associate") {
                $fee_paid = round(($group <> null) ? $group->fee_associate : 0, 2);
            }
        }

        $discount = 0;
        if ($total_months > 0 && $remaining_months > 0) {
            $discount = round(($fee_paid / $total_months) * $remaining_months, 2);
        }

        $member->group_id = $group_id;
        $member->account_type = $account_type;

        if ($member->save()) {

            if ($invoice = $this->createInvoice($member, "Membership Upgrade")) {

                $member->mem_invoice_gen = $invoice;
                $member->save();

                $this->invoiceAdjustment($invoice, $discount, "-", "Membership Upgrade Adjustment");

                return $invoice;
            }
        }
    }

    /**
     * @param $invoice_id
     * @param $amount
     * @param $type
     * @param $reason
     * @return bool|InvoiceAdjustments|null|static
     */
    public function invoiceAdjustment($invoice_id, $amount, $type, $reason)
    {

        $model = InvoiceAdjustments::findOne(['invoice_id' => $invoice_id]);

        if ($model == null) {
            $model = new InvoiceAdjustments();
        }

        $model->adjusted_by = Yii::$app->user->id;
        $model->adjustment = $amount;
        $model->type = $type;
        $model->reason = $reason;
        $model->invoice_id = $invoice_id;

        if ($model->save()) {

            $invoice = Invoices::findOne($invoice_id);

            if ($model->type == '-') {
                $invoice->total = round((($invoice->subtotal + $invoice->tax) - $model->adjustment), 2);
            }

            if ($model->type == '+') {
                $invoice->total = round((($invoice->subtotal + $invoice->tax) + $model->adjustment), 2);
            }

            $invoice->save();

            return $model;
        }

        return false;

    }

    /**
     * @param $amount
     * @return float|int
     */
    public function calculateVat($amount)
    {
        return ((5 / 100) * $amount);
    }

    /**
     * @param $lastRenwal
     * @param $expiry
     * @return false|float|int|string
     */
    private function getMonths($lastRenwal, $expiry)
    {

        $ts1 = strtotime($lastRenwal);
        $ts2 = strtotime($expiry);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        return $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
    }

}
