<?php

namespace common\models;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $type
 * @property string $country_id
 * @property string $city_id
 * @property string $address
 * @property string $title
 * @property string $venue
 * @property string $tba
 * @property string $registration_start
 * @property string $registration_end
 * @property string $cancellation_tillDate
 * @property string $event_startDate
 * @property string $event_endDate
 * @property string $short_description
 * @property string $group_size
 * @property string $fb_gallery
 * @property string $condition
 * @property string $description
 * @property string $member_fee
 * @property resource $nonmember_fee
 * @property string $committee_fee
 * @property string $honourary_fee
 * @property string $sponsor_fee
 * @property string $focus_chair_fee
 * @property string $currency
 * @property string $display_price
 * @property string $sticky
 * @property string $userNote
 * @property string $status
 * @property string $quizNight
 * @property string $active
 * @property string $expiry_day
 * @property string $create_date
 * @property string $modify_date
 * @property string $longitude
 * @property string $latitude
 * @property integer $create_by
 *
 * @property EventCity[] $eventCity
 * @property EventType[] $eventType
 * @property MembershipTypesFee[] $membershipTypesFee
 *
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    public $MemberTypeFee = [];
    public $specialEvents = [];
    public $hour;
    public $minute;
    public $meridian;
    public $images;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'venue', 'type', 'group_size', 'short_description', 'description'], 'required'],
            [['registration_start', 'registration_end', 'cancellation_tillDate', 'event_startDate', 'event_endDate', 'group_size', 'short_description', 'description'], 'required'],
            [['id', 'create_by'], 'integer'],
            [['address', 'short_description', 'description', 'currency', 'display_price', 'sticky', 'userNote', 'status','members_only', 'active'], 'string'],
            [['create_date', 'modify_date','map_address'], 'safe'],
            [['type', 'quizNight', 'event_startTime', 'event_endTime', 'registration_start', 'registration_end', 'cancellation_tillDate', 'event_startDate', 'event_endDate', 'condition', 'member_fee', 'nonmember_fee', 'committee_fee', 'honourary_fee', 'sponsor_fee', 'focus_chair_fee', 'expiry_day'], 'string', 'max' => 25],
            [['country_id', 'city_id', 'group_size'], 'string', 'max' => 11],
            [['title'], 'string', 'max' => 150],
            [['venue', 'image',], 'string', 'max' => 500],
            [['tba'], 'string', 'max' => 5],
            [['fb_gallery'], 'string', 'max' => 250],
            [['MemberTypeFee', 'event_id', 'diet_option', 'diet_option_description', 'other_diet_option_description', 'specialEvents', 'special_events', 'hour', 'minute', 'meridian', 'images'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'country_id' => 'Country',
            'city_id' => 'City',
            'address' => 'Address',
            'title' => 'Title',
            'venue' => 'Venue',
            'tba' => 'Tba',
            'registration_start' => 'Registration Start',
            'registration_end' => 'Registration End',
            'cancellation_tillDate' => 'Cancellation Date',
            'event_startDate' => 'Event Start Date',
            'event_endDate' => 'Event End Date',
            'short_description' => 'Short Description',
            'group_size' => 'Group Size',
            'fb_gallery' => 'Fb Gallery',
            'image' => 'Image',
            'condition' => 'Condition',
            'description' => 'Description',
            'member_fee' => 'Member Fee',
            'nonmember_fee' => 'Nonmember Fee',
            'committee_fee' => 'Committee Fee',
            'honourary_fee' => 'Honourary Fee',
            'sponsor_fee' => 'Sponsor Fee',
            'focus_chair_fee' => 'Focus Chair Fee',
            'currency' => 'Currency',
            'display_price' => 'Display Price',
            'sticky' => 'Sticky',
            'userNote' => 'Dietary Preference',
            'status' => 'Status',
            'quizNight' => 'Quiz Night',
            'active' => 'Published',
            'expiry_day' => 'Expiry Day',
            'create_date' => 'Create Date',
            'modify_date' => 'Modify Date',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'create_by' => 'Create By',
            'dietry_pref' => 'Select Diet Preference',
            'members_only' => 'Members Only'
        ];
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        if ($this->images <> null) {
            EventImages::deleteAll(['event_id' => $this->id]);
            foreach ($this->images as $image) {
                $imgEvent = new EventImages();
                $imgEvent->attributes = $image;
                $imgEvent->event_id = $this->id;

                if (!$imgEvent->save()) {
                    $this->addError('images_error', $imgEvent->getErrors());
                    return false;
                }

            }
        }

        if ($this->specialEvents <> null) {
            if ($this->specialEvents && $this->specialEvents <> null) {
                SpecialEventsFee::deleteAll(['event_id' => $this->id]);
                foreach ($this->specialEvents as $data) {

                    $spEvent = new SpecialEventsFee();
                    $spEvent->attributes = $data;
                    $spEvent->event_id = $this->id;

                    if (!$spEvent->save()) {
                        $this->addError('images_error', $spEvent->getErrors());
                        return false;
                    }
                }
            }
        }


        if ($this->MemberTypeFee <> null) {
            MembershipEventFee::deleteAll(['event_id' => $this->id]);
            foreach ($this->MemberTypeFee as $key => $value) {

                foreach ($value as $k => $v) {

                    $memTypeFee = new MembershipEventFee();

                    $memTypeFee->event_id = $this->id;
                    $memTypeFee->member_group = $key;
                    $memTypeFee->member_type = $k;
                    $memTypeFee->fee = $v;

                    if (!$memTypeFee->save()) {
                        $this->addError('images_error', $memTypeFee->getErrors());
                        return false;
                    }
                }
            }
        }


    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventType()
    {
        return $this->hasOne(EventTypes::className(), ['id' => 'type']);
    }

    /**
     * This function will return all event fee stored for different membership groups and types
     * @return \yii\db\ActiveQuery
     */
    public function getMembershipTypesFee()
    {
        return $this->hasMany(MembershipEventFee::className(), ['event_id' => 'id']);
    }

    /**
     * This function will return all event fee stored for different membership groups and types
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialEventsFee()
    {
        return $this->hasMany(SpecialEventsFee::className(), ['event_id' => 'id']);
    }

    public function getEventImages()
    {
        return $this->hasMany(EventImages::className(), ['event_id' => 'id']);
    }

    /**
     * ################################################################
     */

    public function SendEmail()
    {

    }
}
