<?php

namespace common\models;

/**
 * This is the model class for table "promotions".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property string $type
 * @property double $value
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property integer $is_customer_specific
 * @property integer $status
 * @property integer $redeem_type
 */
class Promotions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_customer_specific', 'start_date', 'end_date', 'status', 'code', 'value'], 'required'],
            [['type', 'redeem_type'], 'string'],
            [['value'], 'number'],
            [['start_date', 'end_date', 'start_time', 'end_time'], 'safe'],
            [['is_customer_specific', 'status'], 'integer'],
            [['code', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'title' => 'Title',
            'type' => 'Type',
            'redeem_type' => 'Redeem Type',
            'value' => 'Value',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'is_customer_specific' => 'Is Customer Specific',
            'status' => 'Status',
        ];
    }
}
