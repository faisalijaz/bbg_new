<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment_responses".
 *
 * @property int $id
 * @property string $payment_reference
 * @property int $payment_id
 * @property string $response_code
 * @property string $response_text
 */
class PaymentResponses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_responses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_id','invoice_id'], 'integer'],
            [['response_text'], 'string'],
            [['payment_reference', 'response_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_reference' => 'Payment Reference',
            'payment_id' => 'Payment ID',
            'response_code' => 'Response Code',
            'response_text' => 'Response Text',
        ];
    }
}
