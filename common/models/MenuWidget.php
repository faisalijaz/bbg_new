<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu_widget".
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property int $sort_order
 * @property string $status
 */
class MenuWidget extends \yii\db\ActiveRecord
{

    public $cms_pages = [];
    public $extra_pages = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_widget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'url', 'sort_order', 'status'], 'required'],
            [['sort_order' , 'parent_id','members_only'], 'integer'],
            [['status'], 'string'],
            [['cms_pages' , 'extra_pages' , 'parent_id'], 'safe'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'sort_order' => 'Sort Order',
            'status' => 'Status',
        ];
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes){
        $sort_order = 1;
        $sort_order_db = '';

        MenuWidget::deleteAll('parent_id ='.$this->id );

       $sql = 'SELECT sort_order FROM menu_widget WHERE parent_id = '.$this->id.' ORDER BY id DESC LIMIT 1';
       $model = MenuWidget::findBySql($sql)->all();
        foreach ($model as $row){
            $sort_order_db = $row->sort_order;
        }
        if(!empty($sort_order_db)){
            $sort_order = $sort_order_db+1;
        }

        if ($this->extra_pages <> null) {
            foreach ($this->extra_pages as $cms_page){


                $title = Yii::$app->params['staticPageArray'][$cms_page];


                $submenu_cms = new MenuWidget();
                $submenu_cms->title = $title;
                $submenu_cms->url = $cms_page;
                $submenu_cms->sort_order = $sort_order;
                $submenu_cms->parent_id = $this->id;
                $submenu_cms->status = $this->status;
                $submenu_cms->save();
                $sort_order++;

            }
        }


        if ($this->cms_pages <> null) {

            foreach ($this->cms_pages as $cms_page){
                $submenu_cms = new MenuWidget();
                $submenu_cms->title = $cms_page;
                $submenu_cms->url = '/pages/view-by-seo-url?seo_url='.$cms_page;
                $submenu_cms->sort_order = $sort_order;
                $submenu_cms->parent_id = $this->id;
                $submenu_cms->status = $this->status;
                $submenu_cms->save();
                $sort_order++;
            }
        }

    }


}
