<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "membership_renewals".
 *
 * @property integer $id
 * @property integer $member_id
 * @property string $expiry_date
 * @property string $renwal_created
 * @property string $renwal_started_on
 * @property integer $renewed_by
 * @property integer $invoice_id
 * @property string $description
 */
class MembershipRenewals extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membership_renewals';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'renwal_started_on', 'renewed_by', 'invoice_id', 'description'], 'required'],
            [['member_id', 'renewed_by', 'invoice_id'], 'integer'],
            [['expiry_date', 'renwal_created', 'renwal_started_on'], 'safe'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'expiry_date' => Yii::t('app', 'Expiry Date'),
            'renwal_created' => Yii::t('app', 'Renwal Created'),
            'renwal_started_on' => Yii::t('app', 'Renwal Started On'),
            'renewed_by' => Yii::t('app', 'Renewed By'),
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
