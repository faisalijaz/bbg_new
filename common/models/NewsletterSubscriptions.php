<?php

namespace common\models;

/**
 * This is the model class for table "newsletter_subscriptions".
 *
 * @property int $id
 * @property string $email
 * @property string $date_created
 * @property string $active
 */
class NewsletterSubscriptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'newsletter_subscriptions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['date_created','gdpr_sent','is_member'], 'safe'],
            [['active','events_news','weekly_newsletter','special_offers','membership_subscriptions','first_name', 'last_name'], 'string'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            // ['email', 'unique', 'targetClass' => '\common\models\NewsletterSubscriptions', 'message' => 'You are already subscribed!'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'date_created' => 'Date Created',
            'active' => 'Active',
            'events_news' => 'Events',
            'weekly_newsletter' => 'Weekly E-Newsletter',
            'special_offers' => 'Special Offers & Announcements',
            'membership_subscriptions' => 'Membership',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember(){
        return $this->hasOne(Members::className(),['email' => 'email']);
    }
}
