<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "member_offers".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property integer $category_id
 * @property string $short_description
 * @property string $description
 * @property string $created_at
 */
class MemberOffers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_offers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'image', 'category_id', 'short_description', 'description'], 'required'],
            [['category_id','sort_order'], 'integer'],
            [['title','description','image'], 'string'],
            [['created_at','is_active','offer_rel','offer_rel_id'], 'safe'],
            [['short_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'category_id' => 'Category',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberCategories()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

}
