<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_image".
 *
 * @property int $id
 * @property string $title
 * @property string $filename
 * @property int $product_id
 * @property int $user_id
 * @property int $userDelete
 * @property string $type
 * @property string $create_date
 */
class TblImage extends \yii\db\ActiveRecord
{

    public $company;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'filename', 'product_id', 'userDelete'], 'required'],
            [['product_id', 'userDelete'], 'integer'],
            [['create_date','user_id','company'], 'safe'],
            [['title'], 'string', 'max' => 45],
            [['filename'], 'string', 'max' => 250],
            [['type'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'filename' => Yii::t('app', 'Filename'),
            'product_id' => Yii::t('app', 'Product ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'userDelete' => Yii::t('app', 'User Delete'),
            'type' => Yii::t('app', 'Type'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }

    public function getMember(){
        return $this->hasOne(Members::className(),['id' => 'user_id']);
    }
}
