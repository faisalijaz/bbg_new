<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_eventfee".
 *
 * @property int $feeId
 * @property int $eventId
 * @property string $feeName
 * @property double $feeAmount
 */
class TblEventfee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_eventfee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eventId', 'feeName', 'feeAmount'], 'required'],
            [['eventId'], 'integer'],
            [['feeAmount'], 'number'],
            [['feeName'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'feeId' => Yii::t('app', 'Fee ID'),
            'eventId' => Yii::t('app', 'Event ID'),
            'feeName' => Yii::t('app', 'Fee Name'),
            'feeAmount' => Yii::t('app', 'Fee Amount'),
        ];
    }
}
