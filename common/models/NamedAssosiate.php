<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "named_assosiate".
 *
 * @property int $assosiate_id
 * @property int $parent_user_id
 * @property string $assosiate_title
 * @property string $assosiate_firstname
 * @property string $assosiate_lastname
 * @property string $assosiate_email
 * @property string $assosiate_mobile
 * @property string $assosiate_country
 * @property string $assosiate_city
 * @property string $created_on
 * @property string $assosiate_status
 */
class NamedAssosiate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'named_assosiate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_user_id'], 'integer'],
            [['created_on'], 'safe'],
            [['assosiate_title'], 'string', 'max' => 20],
            [['assosiate_firstname', 'assosiate_lastname', 'assosiate_email'], 'string', 'max' => 100],
            [['assosiate_mobile'], 'string', 'max' => 15],
            [['assosiate_country', 'assosiate_city'], 'string', 'max' => 50],
            [['assosiate_status'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'assosiate_id' => Yii::t('app', 'Assosiate ID'),
            'parent_user_id' => Yii::t('app', 'Parent User ID'),
            'assosiate_title' => Yii::t('app', 'Assosiate Title'),
            'assosiate_firstname' => Yii::t('app', 'Assosiate Firstname'),
            'assosiate_lastname' => Yii::t('app', 'Assosiate Lastname'),
            'assosiate_email' => Yii::t('app', 'Assosiate Email'),
            'assosiate_mobile' => Yii::t('app', 'Assosiate Mobile'),
            'assosiate_country' => Yii::t('app', 'Assosiate Country'),
            'assosiate_city' => Yii::t('app', 'Assosiate City'),
            'created_on' => Yii::t('app', 'Created On'),
            'assosiate_status' => Yii::t('app', 'Assosiate Status'),
        ];
    }
}
