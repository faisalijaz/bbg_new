<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "loyalties".
 *
 * @property integer $id
 * @property integer $points
 * @property string $action
 * @property integer $account_id
 * @property integer $booking_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $point_type
 * @property integer $is_deleted
 */
class Loyalty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loyalties';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['points', 'account_id', 'booking_id'], 'required'],
            [['points', 'account_id', 'booking_id', 'created_at', 'updated_at'], 'integer'],
            [['action', 'point_type', 'is_deleted'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'points' => 'Points',
            'action' => 'Action',
            'account_id' => 'Account ID',
            'booking_id' => 'Booking ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'point_type' => 'Point Type',
            'is_deleted' => 'Is Deleted',
        ];

    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEarnedPoints($account_id)
    {
        $earned_points = Loyalty::find()->where(['action' => '+'])
            ->andWhere(['account_id' => $account_id])->sum('points');
        if($earned_points > 0){
            return $earned_points;
        }else{
            return 0;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRedeemedPoints($account_id)
    {
        $redeemed_points =  Loyalty::find()->where(['action' => '-'])
            ->andWhere(['account_id' => $account_id])->sum('points');
        if($redeemed_points > 0){
            return $redeemed_points;
        }else{
            return 0;
        }
    }


}
