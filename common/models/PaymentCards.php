<?php

namespace common\models;

/**
 * This is the model class for table "payment_cards".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $card_number
 * @property integer $card_exp_month
 * @property integer $card_exp_year
 * @property integer $card_security_token
 * @property string $status
 * @property string $added_date
 */
class PaymentCards extends \yii\db\ActiveRecord
{
    public $cardInfo;
    public $invoice_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_cards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'status'], 'required'],
            [['member_id', 'card_number', 'card_exp_month', 'card_exp_year', 'card_security_token'], 'integer'],
            [['status','card_name'], 'string'],
            [['added_date','cardInfo','invoice_id'], 'safe'],
            /*[['card_name', 'card_number', 'card_exp_month', 'card_exp_year', 'card_security_token'], 'required', 'when' => function ($model) {
                return $model->cardInfo == "new_card";
            }, "enableClientValidation" => true]*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'card_number' => 'Card Number',
            'card_exp_month' => 'Card Exp Month',
            'card_exp_year' => 'Card Exp Year',
            'card_security_token' => 'Card Security Token',
            'status' => 'Status',
            'added_date' => 'Added Date',
        ];
    }
}
