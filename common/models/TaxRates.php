<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tax_rates".
 *
 * @property integer $id
 * @property string $title
 * @property integer $rate
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class TaxRates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_rates';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'rate'], 'required'],
            [['rate', 'created_at', 'updated_at', 'status'], 'integer'],
            [['type'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'rate' => 'Rate',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxGroup()
    {
        return $this->hasOne(Tours::className(), ['id' => 'tax_id']);
    }

}
