<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_event".
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $country_id
 * @property string $city_id
 * @property string $address
 * @property string $title
 * @property string $slug
 * @property string $search_title
 * @property string $provience_state
 * @property string $other_region
 * @property string $postal_code
 * @property string $tba
 * @property string $map_cordinate
 * @property string $registration_start
 * @property string $registration_end
 * @property string $cancellation_tillDate
 * @property string $event_startDate
 * @property string $event_endDate
 * @property string $short_description
 * @property string $group_size
 * @property string $fb_gallery
 * @property string $condition
 * @property string $description
 * @property string $member_fee
 * @property resource $nonmember_fee
 * @property string $committee_fee
 * @property string $honourary_fee
 * @property string $sponsor_fee
 * @property string $focus_chair_fee
 * @property string $currency
 * @property string $display_price 1:all, 2:connections only
 * @property string $sticky
 * @property string $userNote
 * @property string $status
 * @property string $quizNight
 * @property string $active
 * @property string $expiry_day
 * @property string $create_date
 * @property string $modify_date
 * @property string $longitude
 * @property string $latitude
 */
class TblEvent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['country_id', 'city_id', 'tba', 'userNote', 'quizNight', 'longitude', 'latitude'], 'required'],
            [['address', 'short_description', 'description', 'currency', 'display_price', 'sticky', 'userNote', 'status', 'quizNight', 'active'], 'string'],
            [['create_date', 'modify_date'], 'safe'],
            [['type', 'slug', 'registration_start', 'registration_end', 'cancellation_tillDate', 'event_startDate', 'event_endDate', 'condition', 'member_fee', 'nonmember_fee', 'committee_fee', 'honourary_fee', 'sponsor_fee', 'focus_chair_fee', 'expiry_day'], 'string', 'max' => 25],
            [['country_id', 'city_id', 'provience_state', 'postal_code', 'map_cordinate', 'group_size'], 'string', 'max' => 11],
            [['title'], 'string', 'max' => 150],
            [['search_title'], 'string', 'max' => 100],
            [['other_region'], 'string', 'max' => 500],
            [['tba'], 'string', 'max' => 5],
            [['fb_gallery'], 'string', 'max' => 250],
            [['longitude', 'latitude'], 'string', 'max' => 50],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'country_id' => Yii::t('app', 'Country ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'address' => Yii::t('app', 'Address'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'search_title' => Yii::t('app', 'Search Title'),
            'provience_state' => Yii::t('app', 'Provience State'),
            'other_region' => Yii::t('app', 'Other Region'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'tba' => Yii::t('app', 'Tba'),
            'map_cordinate' => Yii::t('app', 'Map Cordinate'),
            'registration_start' => Yii::t('app', 'Registration Start'),
            'registration_end' => Yii::t('app', 'Registration End'),
            'cancellation_tillDate' => Yii::t('app', 'Cancellation Till Date'),
            'event_startDate' => Yii::t('app', 'Event Start Date'),
            'event_endDate' => Yii::t('app', 'Event End Date'),
            'short_description' => Yii::t('app', 'Short Description'),
            'group_size' => Yii::t('app', 'Group Size'),
            'fb_gallery' => Yii::t('app', 'Fb Gallery'),
            'condition' => Yii::t('app', 'Condition'),
            'description' => Yii::t('app', 'Description'),
            'member_fee' => Yii::t('app', 'Member Fee'),
            'nonmember_fee' => Yii::t('app', 'Nonmember Fee'),
            'committee_fee' => Yii::t('app', 'Committee Fee'),
            'honourary_fee' => Yii::t('app', 'Honourary Fee'),
            'sponsor_fee' => Yii::t('app', 'Sponsor Fee'),
            'focus_chair_fee' => Yii::t('app', 'Focus Chair Fee'),
            'currency' => Yii::t('app', 'Currency'),
            'display_price' => Yii::t('app', 'Display Price'),
            'sticky' => Yii::t('app', 'Sticky'),
            'userNote' => Yii::t('app', 'User Note'),
            'status' => Yii::t('app', 'Status'),
            'quizNight' => Yii::t('app', 'Quiz Night'),
            'active' => Yii::t('app', 'Active'),
            'expiry_day' => Yii::t('app', 'Expiry Day'),
            'create_date' => Yii::t('app', 'Create Date'),
            'modify_date' => Yii::t('app', 'Modify Date'),
            'longitude' => Yii::t('app', 'Longitude'),
            'latitude' => Yii::t('app', 'Latitude'),
        ];
    }
}
