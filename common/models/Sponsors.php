<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sponsors".
 *
 * @property int $title
 * @property string $type
 * @property string $image
 * @property string $short_description
 * @property string $description
 * @property string $Status
 */
class Sponsors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sponsors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','type', 'short_description', 'status'], 'required'],
            [['sort_order'], 'integer'],
            [['title','type', 'description', 'status'], 'string'],
            [['title','image'], 'string', 'max' => 255],
            [['short_description'], 'string', 'max' => 400],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Title',
            'type' => 'Type',
            'image' => 'Image',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'Status' => 'status',
        ];
    }
}
