<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $new_type
 * @property string $link
 * @property string $image
 * @property string $isPublished
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'category'], 'required'],
            [['re_post', 'short_description', 'description', 'news_type', 'isPublished','homepage_show'], 'string'],
            [['created_at', 'updated_at', 'sort_order', 'category'], 'integer'],
            [['title'], 'string', 'max' => 512],
            [['related_to','rel_id'],'safe'],
            [['link', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'news_type' => 'News Type',
            'link' => 'Link',
            'image' => 'Image',
            'isPublished' => 'Published Status',
        ];
    }
}
