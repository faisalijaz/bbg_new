<?php

namespace common\models;

/**
 * This is the model class for table "account_company".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category
 * @property string $url
 * @property string $phonenumber
 * @property string $fax
 * @property string $postal_code
 * @property string $emirates_number
 * @property string $address
 * @property string $about_company
 *
 * @property CompanyCategory[] $companyCategory
 */
class AccountCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
           // [['name', 'category', 'phonenumber', 'postal_code', 'emirates_number', 'address', 'about_company'], 'required'],
            [['category'], 'integer'],
            [['about_company'], 'string'],
            [[ 'vat_number', 'show_in_directory', 'is_active','name', 'url', 'phonenumber', 'logo', 'fax', 'postal_code', 'emirates_number', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category' => 'Category',
            'url' => 'Url',
            'phonenumber' => 'Phonenumber',
            'fax' => 'Fax',
            'postal_code' => 'Postal Code',
            'emirates_number' => 'Emirates Number',
            'address' => 'Address',
            'about_company' => 'About Company',
            'logo' => 'Company Logo'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category']);
    }

    /**
     * @param $member
     * @param $contact
     * @return null|static
     */
    public function checkMemberCompany($member, $company)
    {
        return MemberCompanies::findOne(['member_id' => $member, 'member_company' => $company]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMembers()
    {
        return $this->hasMany(Members::className(), ['company' => 'id']);
    }
}
