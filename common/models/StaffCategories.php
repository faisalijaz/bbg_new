<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "staff_categories".
 *
 * @property int $id
 * @property string $category_name
 * @property int $sort_order
 * @property int $date
 */
class StaffCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'sort_order'], 'required'],
            [['sort_order'], 'integer'],
            [['category_name', 'status'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_name' => Yii::t('app', 'Category Name'),
            'sort_order' => Yii::t('app', 'Sort Order'),
        ];
    }


    public function getStaffInfo()
    {
        return $this->hasMany(CommitteMembers::className(), ['type' => 'id']);
    }
}
