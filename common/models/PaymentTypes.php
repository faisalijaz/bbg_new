<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment_types".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $status
 */
class PaymentTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code'], 'required'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'code' => 'Code',
            'status' => 'Status',
        ];
    }
}
