<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "membership_types".
 *
 * @property integer $mt_id
 * @property string $mt_name
 * @property string $mt_fee
 * @property string $mt_joining_fee
 * @property string $mt_description
 */
class MembershipTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membership_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mt_name', 'mt_fee', 'mt_joining_fee', 'mt_description'], 'required'],
            [['mt_fee', 'mt_joining_fee'], 'number'],
            [['mt_description'], 'string'],
            [['mt_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mt_id' => 'ID',
            'mt_name' => 'Name',
            'mt_fee' => 'Fee',
            'mt_joining_fee' => 'Joining Fee',
            'mt_description' => 'Description',
        ];
    }
}
