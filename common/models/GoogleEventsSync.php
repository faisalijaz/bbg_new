<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "google_events_sync".
 *
 * @property int $id
 * @property int $user_id
 * @property int $event_id
 * @property int $google_event_id
 * @property string $date
 */
class GoogleEventsSync extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'google_events_sync';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id', 'google_event_id'], 'required'],
            [['user_id', 'event_id'], 'integer'],
            [['google_event_id'], 'string'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'google_event_id' => Yii::t('app', 'Google Event ID'),
            'date' => Yii::t('app', 'Date'),
        ];
    }
}
