<?php

namespace common\models;

use common\helpers\EmailHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "event_subscriptions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $user_type
 * @property integer $registered_by
 * @property integer $event_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $userNote
 * @property string $mobile
 * @property string $company
 * @property string $walkin
 * @property string $attended
 * @property string $fee_paid
 * @property string $payment_status
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $gen_invoice
 * @property string $focGuest
 * @property string $paymentMethod
 *
 * @property MemberData[] = $memberData
 * @property InvoiceData[] = $InvoiceData
 * @property EventData[] = $eventData
 */
class EventSubscriptions extends \yii\db\ActiveRecord
{
    public $invoice;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_subscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id', 'created_at', 'updated_at'], 'integer'],
            [['designation','user_type', 'user_type_relation', 'userNote', 'walkin', 'attended', 'payment_status', 'status', 'focGuest', 'paymentMethod'], 'string'],
            [['registered_by', 'gen_invoice', 'fee_paid', 'tax_group_id', 'tax', 'subtotal', 'cancel_request'], 'number'],
            [['firstname', 'lastname', 'mobile'], 'string', 'max' => 25],
            [['email'], 'email'],
            [['company'], 'string', 'max' => 150],
            [['invoice'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Member',
            'user_type' => 'User Type',
            'registered_by' => 'Registered By',
            'event_id' => 'Event ID',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Email',
            'userNote' => 'User Note',
            'mobile' => 'Mobile',
            'company' => 'Company',
            'walkin' => 'Walkin',
            'attended' => 'Attended',
            'fee_paid' => 'Fee Paid',
            'payment_status' => 'Payment Status',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'gen_invoice' => 'Gen Invoice',
            'focGuest' => 'Foc Guest',
            'paymentMethod' => 'Payment Method',
            'user_type_relation' => 'User Type'
        ];
    }

    /**
     * @param string $q
     * @return mixed
     */
    public function getMembersList($q = '')
    {

        $query = new Query;

        $query->select('first_name')
            ->from('accounts')
            ->where('name LIKE "%' . $q . '%"')
            ->orderBy('first_name');
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['first_name']];
        }
        return Json::encode($out);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {

            $event = Events::findOne($this->event_id);
            $subscriptions = EventSubscriptions::find()->where(['event_id' => $this->event_id])->count();

            if ($event <> null && $subscriptions < $event->group_size) {

                $this->payment_status = 'pending';

                // if (!$this->fee_paid) {

                    if ($this->user_type == 'guest') {
                        $this->fee_paid = ($event <> null) ? $event->nonmember_fee : 0;
                    }

                    if ($this->user_type_relation) {


                        if ($this->user_type_relation == "guest") {
                            $this->fee_paid = ($event <> null) ? $event->nonmember_fee : 0;
                        }

                        if ($this->user_type_relation == "committee") {
                            $this->fee_paid = ($event <> null) ? $event->committee_fee : 0;
                        }

                        if ($this->user_type_relation == "focus-chair") {
                            $this->fee_paid = ($event <> null) ? $event->focus_chair_fee : 0;
                        }

                        if ($this->user_type_relation == "platinum-sponsor") {
                            $this->fee_paid = ($event <> null) ? $event->sponsor_fee : 0;
                        }

                        if ($this->user_type_relation == "honourary") {
                            $this->fee_paid = ($event <> null) ? $event->honourary_fee : 0;
                        }

                        if ($this->user_type_relation == "business-associate") {
                            $this->fee_paid = ($event <> null) ? $event->member_fee : 0;
                        }
                    }
                    
                // }

                if ($this->focGuest) {
                    $this->fee_paid = 0;
                }

                if ($this->fee_paid == 0) {
                    $this->payment_status = 'paid';
                    $this->status = 'approved';
                }

                if ($this->walkin) {
                    $this->attended = 1;
                    $this->status = 'approved';
                }

                $this->subtotal = round($this->fee_paid, 2);
                $this->tax = round($this->itemTaxInformation($this->tax_group_id, $this->fee_paid), 2);
                $this->fee_paid = round($this->subtotal + $this->tax, 2);

            } else {
                $this->addError('error', 'Your registration cannot proceed all seats are reserved!');
                return false;
            }

        }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        if ($insert) {

            if ($this->gen_invoice) {

                $invoiceItems = new InvoiceItems();

                $invoiceItems->invoice_id = $this->gen_invoice;
                $invoiceItems->user_id = $this->user_id;
                $invoiceItems->invoice_related_to = 'event';
                $invoiceItems->invoice_rel_id = $this->id;
                $invoiceItems->invoice_category = $this->eventData->title;
                $invoiceItems->payment_status = 'unpaid';

                if ($this->payment_status == 'paid') {
                    $invoiceItems->payment_status = 'paid';
                }

                $invoiceItems->amount = round($this->fee_paid, 2);
                $invoiceItems->subtotal = round($this->subtotal, 2);
                $invoiceItems->tax = round($this->tax, 2);
                $invoiceItems->invoice_date = date('Y-m-d');

                if (!$invoiceItems->save()) {
                    $this->addError('error', $invoiceItems->getErrors());
                }

                if ($this->invoice) {

                    $updateInv = Invoices::findOne($this->invoice);

                    $updateInv->subtotal = round($this->subtotal, 2);
                    $updateInv->tax = round($this->tax, 2);
                    $updateInv->total = round($this->fee_paid, 2);

                    $payment = Payments::find()->where(['invoice_id' => $this->invoice])->all();

                    if ($payment == null) {
                        $updateInv->payment_status == 'unpaid';
                    }

                    if (!$updateInv->save()) {
                        return ['errors' => \yii\widgets\ActiveForm::validate($updateInv)];
                    }
                }

            }

            if ($this->payment_status == 'paid') {

                $eventInvoice = Invoices::findOne($this->gen_invoice);
                if ($eventInvoice <> null) {
                    $eventInvoice->payment_status = 'paid';
                    $eventInvoice->save();
                }

            }
        }
    }

    /**
     * @param $data
     * @return array|int
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function memberEventRegisteration($data)
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {

            if (Yii::$app->user->isGuest) {
                $user = null;
            } else {
                $user = Yii::$app->user->identity;
            }

            $html = '';
            $count = 1;
            $subtotal = 0;
            $tax_group = 1;

            if (count($data) > 0) {

                $eventData = Events::findOne(\Yii::$app->request->post('event_id'));
                $event_id = ($eventData <> null) ? $eventData->id : "";
                $event_title = ($eventData <> null) ? $eventData->title : "";

                $invoice = new Invoices();
                $invoice->user_id = (!Yii::$app->user->isGuest) ? \Yii::$app->user->identity->id : 0;
                $invoice->invoice_related_to = 'event';
                $invoice->invoice_prefix = 'E';
                $invoice->invoice_rel_id = $event_id;
                $invoice->invoice_category = ($eventData <> null) ? $eventData->title : "";
                $invoice->payment_status = 'unpaid';

                if (!$invoice->save()) {
                    ['errors' => $invoice->getErrors()];
                    return false;
                }

                $registered_ids = [];

                if (isset($data['contact']) && count($data['contact'])) {

                    $contacts = $data['contact'];
                    $member_fee = $data['contactfee'];

                    if (count($contacts) > 0) {

                        foreach ($contacts as $id) {

                            $user = Members::findOne($id);

                            $model = new EventSubscriptions();

                            $model->event_id = $event_id;
                            $model->registered_by = (!Yii::$app->user->isGuest) ? \Yii::$app->user->identity->id : 0;
                            $model->user_id = $id;
                            $model->user_type = 'member';
                            $model->firstname = $user->first_name;
                            $model->lastname = $user->last_name;
                            $model->email = $user->email;
                            $model->mobile = $user->phone_number;
                            $model->designation = $user->designation;
                            $model->company = ($user->companyData <> null) ? $user->companyData->name : '';

                            $model->fee_paid = round($data['member_fee'], 2);

                            if ($user <> null) {

                                if ($user->user_type_relation == "guest") {
                                    $model->fee_paid = ($eventData <> null) ? $eventData->nonmember_fee : 0;
                                }

                                if ($user->user_type_relation == "committee") {
                                    $model->fee_paid = ($eventData <> null) ? $eventData->committee_fee : 0;
                                }

                                if ($user->user_type_relation == "focus-chair") {
                                    $model->fee_paid = ($eventData <> null) ? $eventData->focus_chair_fee : 0;
                                }

                                if ($user->user_type_relation == "platinum-sponsor") {
                                    $model->fee_paid = ($eventData <> null) ? $eventData->sponsor_fee : 0;
                                }

                                if ($user->user_type_relation == "honourary") {
                                    $model->fee_paid = ($eventData <> null) ? $eventData->honourary_fee : 0;
                                }

                                if ($user->user_type_relation == "business-associate") {
                                    $model->fee_paid = ($eventData <> null) ? $eventData->member_fee : 0;
                                }
                            }

                            $model->user_type_relation = $user->user_type_relation;
                            $model->payment_status = 'pending';
                            $model->tax_group_id = 1;
                            $model->gen_invoice = $invoice->invoice_id;
                            $model->invoice = $invoice->invoice_id;
                            $model->user_type_relation = "member";

                            if (!$model->save()) {

                                return ['errors' => $model->getErrors()];
                            } else {

                                if ($model->user_id == $model->registered_by) {

                                    $subjectLine = "Event Registration for " . $event_title;

                                } else {
                                    if ($user <> null) {
                                        $subjectLine = $user->first_name . " " . $user->last_name . " registered " . $model->firstname . " " . $model->lastname . " for " . $event_title;
                                    }
                                }

                                (new EmailHelper())->sendEmail(trim($model->email), [], $subjectLine, 'events/foc-email', [
                                    'emaildata' => $model
                                ]);
                            }

                            // $html .= $this->makeHtml($model->firstname . ' ' . $model->lastname, 'Member', $model->subtotal, $model->tax, $model->fee_paid, $model->id, $count);

                            $count++;
                            $subtotal = round($subtotal + $model->fee_paid, 2);
                        }
                    }
                }

                if (isset($data['guestInfo']) && count($data['guestInfo']) > 0) {

                    $guestInfo = $data['guestInfo'];


                    foreach ($guestInfo as $guest) {
                        $name = '';
                        if ($guest['name'] <> "") {

                            if (isset($guest['name'])) {
                                $name = explode(' ', $guest['name'],2);
                            }

                            $model = new EventSubscriptions();

                            $model->event_id = $data['event_id'];
                            $model->registered_by = (!Yii::$app->user->isGuest) ? \Yii::$app->user->identity->id : 0;
                            $model->user_type = 'guest';
                            $model->firstname = (isset($name[0])) ? $name[0] : "";
                            $model->lastname = (isset($name[1])) ? $name[1] : "";
                            $model->email = $guest['email'];
                            $model->mobile = $guest['phone_number'];
                            $model->designation = $guest['designation'];
                            $model->company = $guest['company'];
                            $model->fee_paid = $data['nonmember_fee'];
                            $model->tax_group_id = 1;
                            $model->gen_invoice = $invoice->invoice_id;
                            $model->payment_status = 'pending';
                            $model->user_type_relation = "guest";

                            if (isset($guest['diet_option'])) {

                                $model->diet_option = (isset($guest['diet_option'])) ? $guest['diet_option'] : "";
                                $model->diet_option_description = (isset($guest['diet_option_description'])) ? $guest['diet_option_description'] : "";
                                $model->other_diet_option_description = (isset($guest['other_diet_option_description'])) ? $guest['other_diet_option_description'] : "";

                            } else {
                                $model->diet_option = 0;
                                $model->diet_option_description = "";
                                $model->other_diet_option_description = "";
                            }


                            if (!$model->save()) {
                                return ['errors' => \yii\widgets\ActiveForm::validate($model)];
                            } else {

                                if ($user <> null) {
                                    $subjectLine = $user->first_name . " " . $user->last_name . " registered " . $model->firstname . " " . $model->lastname . " for " . $event_title;
                                } else {
                                    $subjectLine = "Event registration for " . $event_title;
                                }

                                (new EmailHelper())->sendEmail(trim($model->email), [], $subjectLine, 'events/foc-email', [
                                    'emaildata' => $model
                                ]);

                                if ($user == null) {
                                    (new EmailHelper())->sendInvoiceEmail(trim($model->email), "info@bbgdxb.com", "Invoice for " . $event_title, 'invoices/event_registartion', [
                                        'invoice' => Invoices::findOne($invoice->invoice_id),
                                        'subscriber' => $model
                                    ]);
                                }
                            }

                            // $html .= $this->makeHtml($model->firstname . ' ' . $model->lastname, 'Member', round($model->subtotal,2), round($model->tax,2), round($model->fee_paid,2), $model->id, $count);

                            $count++;
                            $subtotal = round($subtotal + $model->fee_paid, 2);
                        }
                    }
                }

                /*
                    $updateInv = Invoices::findOne($invoice->invoice_id);

                    $updateInv->subtotal = $subtotal;
                    $updateInv->tax = $this->itemTaxInformation($model->tax_group_id, $subtotal);
                    $updateInv->total = $updateInv->subtotal + $updateInv->tax;

                    if (!$updateInv->save()) {
                        return ['errors' => $updateInv->getErrors()];
                    }
                */

                $transaction->commit();

                if ($user <> null) {

                    (new EmailHelper())->sendInvoiceEmail(trim($user->email), "info@bbgdxb.com", "Invoice for " . $event_title, 'invoices/event_registartion', [
                        'invoice' => Invoices::findOne($invoice->invoice_id)
                    ]);
                }

                return $invoice->invoice_id;

            }

            return ['msg' => true, 'users' => $html, 'total' => $subtotal];

        } catch (\Exception $exception) {

            $transaction->rollback();
            throw $exception;
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventData()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoices::className(), ['invoice_id' => 'gen_invoice']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Members::className(), ['id' => 'user_id']);
    }

    /**
     * @param $name
     * @param $type
     * @param $subtotal
     * @param $tax
     * @param $amount
     * @param $id
     * @param $serial
     * @return string
     */
    public function makeHtml($name, $type, $subtotal, $tax, $amount, $id, $serial)
    {
        $html = '  <tr>';
        $html .= '     <td class="text-center">' . $serial . '</td>';
        $html .= '     <td class="text-center">' . $name . '</td>';
        $html .= '     <td class="text-center">' . ucwords($type) . '</td>';
        $html .= '     <td class="text-center"><span class="pull-right pricebox">' . $subtotal . '</span></td>';
        $html .= '     <td class="text-center"><span class="pull-right pricebox">' . $tax . '</span></td>';
        $html .= '     <td class="text-center"><span class="pull-right pricebox">' . $amount . '</span></td>';
        $html .= '     <td class="text-center">';
        $html .= '         <span class="Editbtn">';
        $html .= Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', '#', [
            'class' => 'showModalButton',
            'value' => \yii\helpers\Url::to(['/event/event-subscribers-badges', 'event_id' => 1]),
            'title' => Yii::t('yii', 'Edit'),
        ]);
        $html .= '         </span>';
        $html .= '         <span class="delbtn">';
        $html .= Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i>', '#', [
            'class' => 'showModalButton',
            'value' => \yii\helpers\Url::to(['/event/event-subscribers-badges', 'event_id' => 1]),
            'title' => Yii::t('yii', 'Delete'),
        ]);
        $html .= '         </span>';
        $html .= '     </td>';
        $html .= '</tr>';
        return $html;
    }

    /**
     * @param $tax_group
     * @param $price
     * @return float|int
     */
    public function itemTaxInformation($tax_group, $price)
    {
        $taxModel = new TaxModel();
        $itemTax = 0;

        if ($tax_group) {
            $taxes = $taxModel->findItemTax($tax_group);
            foreach ($taxes as $tax) {
                if ($tax['type'] == 'percentage') {
                    $taxAmount = ($tax['rate'] * $price) / 100;
                } else {
                    $taxAmount = $tax['rate'];
                }
                $itemTax = $taxAmount + $itemTax;
            }
            return $itemTax;
        } else {
            return $itemTax;
        }
    }
}
