<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "membership_documents".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $trade_licence
 * @property string $passport_copy
 * @property string $residence_visa
 * @property string $passport_size_pic
 */
class MembershipDocuments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membership_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['account_id', 'trade_licence', 'passport_copy', 'residence_visa', 'passport_size_pic'], 'required'],
            [['account_id'], 'required'],
            [['account_id'], 'integer'],
            [['trade_licence', 'passport_copy', 'residence_visa', 'passport_size_pic','trade_link_proof'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'trade_licence' => 'Copy of UAE Trade Licence',
            'passport_copy' => 'Copy of passport',
            'residence_visa' => 'Copy of residence visa',
            'passport_size_pic' => 'Passport size photograph (upload as JPEG)',
        ];
    }
}
