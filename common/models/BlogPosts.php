<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blog_posts".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $status
 * @property string $date
 * @property int $author
 * @property string $video link
 * @property string $banner_image
 * @property string $show_until_date
 */
class BlogPosts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'short_description', 'description', 'status', 'date', 'show_until_date'], 'required'],
            [['short_description', 'description', 'status'], 'string'],
            [['date', 'show_until_date'], 'safe'],
            [['author'], 'integer'],
            [['title', 'video_link', 'banner_image'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'short_description' => Yii::t('app', 'Short Description'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'date' => Yii::t('app', 'Date'),
            'author' => Yii::t('app', 'Author'),
            'video link' => Yii::t('app', 'Video Link'),
            'banner_image' => Yii::t('app', 'Banner Image'),
            'show_until_date' => Yii::t('app', 'Show Until Date'),
        ];
    }
}
