<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "search_expertise".
 *
 * @property integer $id
 * @property string $search_keyword
 * @property integer $search_by
 * @property integer $user_viewed
 * @property string $date
 */
class SearchExpertise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_expertise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search_keyword', 'search_by', 'user_viewed'], 'required'],
            [['search_by', 'user_viewed'], 'integer'],
            [['date'], 'safe'],
            [['search_keyword'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'search_keyword' => Yii::t('app', 'Search Keyword'),
            'search_by' => Yii::t('app', 'Search By'),
            'user_viewed' => Yii::t('app', 'User Viewed'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMemberViewed()
    {
        return $this->hasOne(Members::className(), ['id' => 'user_viewed']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchBy()
    {
        return $this->hasOne(Members::className(), ['id' => 'search_by']);
    }
}
