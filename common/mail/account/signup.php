<?php

$this->title = 'Application Received';

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */
$url = Yii::$app->urlManager->createAbsoluteUrl(['site/verify', 'token' => $user->verification_code]);
?>
<style>
    body {
        font-family: "Century Gothic", Gadget, sans-serif;
        font-size: 13px;
        background: #ffffff;
        line-height: 1.5
    }

    h2 {
        font-size: 22px;
        font-weight: normal;
        color: #D11349;
        margin: 15px 0px 5px;
    }

    h3 {
        font-size: 16px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 0px;
    }

    h4 {
        font-size: 12px;
        font-weight: bold;
        color: #002D5D;
    }

    p {
        margin: 20px 0px;
        font-size: 13px;
    }

    .eventLabel, .eventValue {
        color: #002D5D;
        margin: 5px 0px;
        line-height: 1.5;
        font-size: 13px;
        font-weight: bold;
    }

    .eventValue {
        color: #D11349;
        font-weight: normal;
    }

</style>

<tr>
        <td valign="top" colspan="3">

            <table width="93%" align="center">
                <tr>
                    <td align="left">
                        <h3>
                            Dear <?= ($user <> null) ? ucfirst($user->first_name . " " . $user->last_name) : ""; ?>
                        </h3>
                        <p>Your application for membership is received with thanks. The request will now be processed and I will contact you shortly with an invoice to complete your application. Should you require any information or have any queries please do not hesitate to contact me.</p>

                        <h4>Lindsay Aitken<br>
                            Operations Manager<br><br>
                            British Business Group<br>
                            P.O. Box 9333 Dubai, UAE<br>
                            T: +971 4 397 0303<br>
                            E: Lindsay.aitken@bbgdxb.com<br>
                        </h4>
                    </td>
                </tr>

            </table>

        </td>
    </tr>

