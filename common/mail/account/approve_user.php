<style>
    body {
        font-family: "Century Gothic", Gadget, sans-serif;
        font-size: 13px;
        background: #ffffff;
        line-height: 1.5
    }

    h2 {
        font-size: 22px;
        font-weight: normal;
        color: #D11349;
        margin: 15px 0px 5px;
    }

    h3 {
        font-size: 16px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 0px;
    }

    h4 {
        font-size: 12px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 15px;
    }

    p {
        margin: 20px 0px;
        font-size: 13px;
    }

    .eventLabel, .eventValue {
        color: #002D5D;
        margin: 5px 0px;
        line-height: 1.5;
        font-size: 13px;
        font-weight: bold;
    }

    .eventValue {
        color: #D11349;
        font-weight: normal;
    }

</style>



    <tr>
        <td valign="top" colspan="3" width="90%" align='center'>
            <table width="600" height="100%">
                <tr>
                    <td>Dear <?= ($member <> null) ? $member->first_name . " " . $member->last_name : ''; ?>,</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Congratulations! Your BBG membership application has been approved.</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>I can now confirm that you are officially signed up as a new member of the British Business Group. We're delighted to have you on board.
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>You may now <a href="http://bbgdubai.org/login">log in</a> to the BBG website with your details
                        and enjoy access to all its features and functionalities.
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <?php if ($member->account_type == 'business') { ?>
                    <tr>
                        <td>Please know that as a Business member or a Not for Profit member, you are entitled to an
                            Alternate member from your company. Once you have decided who your Alternate will be, please
                            <a href="/login">log in</a> and add the member to your profile. Once I
                            receive the application, I will check the details and activate the membership.
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>To find out more about making the most of your membership please contact me and I will be
                            more than happy to discuss.
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                <?php } ?>
                <?php if ($member->account_type == 'alternate' || $member->account_type == 'additional') { ?>
                    <tr>
                        <td>
                            <p>You may use the below credentials for <a href="/login">log in</a>.</p>
                            <p><b>Username:</b> <?php echo 'user'; ?></p>
                            <p><b>Password:</b> <?php echo 'password'; ?></p>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>Welcome to the BBG.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <h4>Lindsay Aitken<br>
                            Membership Relations Officer<br><br>
                            British Business Group<br>
                            P.O. Box 9333 Dubai, UAE<br>
                            T: +971 4 397 0303<br>
                            E: lindsay.aitken@bbgdxb.com<br>
                        </h4></td>

                </tr>
            </table>
        </td>
    </tr>

