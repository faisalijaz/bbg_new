<style>
    #cetificate_header {
        width: 100%;
        text-align: center;
        margin-top: 0px;
    }

    #cetificate_footer {
        border-right: 0.5px solid #ddd;
        border-bottom: 0.5px solid #ddd;
    }

    @media print {
        #cetificate_header {
            margin-top: 0px;
        }
    }
</style>
<div id="certificate">
    <table  height="842" border="0" cellspacing="0"
           id="mainTable" cellpadding="0"
           style="border:1px solid #ddd; background-color: #fff; font-family: 'Times New Roman';">
        <tbody>
        <tr>
            <td height="268" id="header">
                <img id="cetificate_header" height="" alt="" src="<?= Yii::$app->params['certificateHeader']; ?>">
            </td>
        </tr>
        <tr>
            <td valign="top" align="center" id="body">
                <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center"
                       style="text-align: center;">
                    <tr>
                        <td colspan="3" valign="middle" style="text-align: center;" height="150">
                            <h2 style="font-size:22px; font-family: 'Times New Roman';
            text-transform: uppercase; letter-spacing: 8px; font-weight: normal;
            color: #999; margin: 25px 0px;" id="cerTitle"> Membership Certificate</h2>
                        </td>
                    </tr>
                    <tr>
                        <td height="80" colspan="3" valign="bottom" style="border-bottom: solid 1px #ccc;">
                            <h2 id="memberData"
                                style="font-size: 30px; color: #999; font-family: 'Times New Roman';text-transform:none; "><?= ucwords(trim($model->first_name . ' ' . $model->last_name)); ?></h2>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"></td>
                        <td style="border-bottom: solid 1px #ccc;" height="95">
                            <img src="<?= Yii::$app->params['LindasSign']; ?>"/>
                        </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="" valign="middle" height="40">
                            <label>Membership Relations Officer</label>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%"></td>
                        <td style="border-bottom: solid 1px #ccc;" height="95">
                            <img src="<?= Yii::$app->params['JoananeSign']; ?>"/></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="middle" height="40"><label>Office Manager</label></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="69">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="txt">
                            <?php echo "<span id=mid>Member ID Number: " . $model->id . "</span>"; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="txt">Membership Expiry
                            Date: <?= formatDate($model->expiry_date); ?>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td valign="bottom" id="footer" height="105">
                <img id="cetificate_footer" align="right" src="<?= Yii::$app->params['certificateFooter']; ?>">
            </td>
        </tr>
        </tbody>
    </table>
</div>