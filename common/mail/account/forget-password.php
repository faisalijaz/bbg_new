<?php

$url =  Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

    <style>
        body {
            font-family: "Century Gothic", Gadget, sans-serif;
            font-size: 13px;
            background: #ffffff;
            line-height: 1.5
        }

        h2 {
            font-size: 22px;
            font-weight: normal;
            color: #D11349;
            margin: 15px 0px 5px;
        }

        h3 {
            font-size: 16px;
            font-weight: bold;
            color: #002D5D;
            margin: 15px 0px;
        }

        h4 {
            font-size: 12px;
            font-weight: bold;
            color: #002D5D;
        }

        p {
            margin: 20px 0px;
            font-size: 13px;
        }

        .eventLabel, .eventValue {
            color: #002D5D;
            margin: 5px 0px;
            line-height: 1.5;
            font-size: 13px;
            font-weight: bold;
        }

        .eventValue {
            color: #D11349;
            font-weight: normal;
        }

    </style>

        <tr>
            <td valign="top" colspan="3" width="90%" align='center'>
                <table width="600" height="100%">
                    <tr>
                        <td> Hi <?= ($user <> null) ? $user->first_name . " " . $user->last_name : ""; ?>,
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            You have asked to change your password.
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Username: <?= ($user <> null) ? $user->email : ""; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <a href="<?= $url; ?>">Click here</a> to change password
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3"><h4>
                                British Business Group</br>
                                P.O. Box 9333 Dubai, UAE.</br>
                            </h4>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
