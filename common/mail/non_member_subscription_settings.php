<?php

$this->title = 'Contact us';

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>

<!-- Contents -->
<table class="body-wrap"
       style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%;  background-color: #fff; margin: 0;"
       bgcolor="#ffffff">
    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
        <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
            valign="top"></td>

        <td class="container" width="100%"
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 100% !important; clear: both !important; margin: 0 auto;"
            valign="top">

            <div class="content"
                 style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 100%; display: block; margin: 0 auto;">

                <table class="main" width="100%" cellpadding="0" cellspacing="0"
                       style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0;"
                       bgcolor="#fff">

                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap"
                            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;"
                            valign="top">

                            <table width="100%" cellpadding="0" cellspacing="0"
                                   style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">

                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <td class="content-block"
                                        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px; text-align: left"
                                        valign="top">
                                        <strong style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
                                            <br/>
                                            Dear BBG supporter,
                                        </strong>
                                        <br/><br/>
                                        We all knew this day had to come before Friday 25 May.
                                        <br/><br/>

                                        Ever since you joined the family we have been regularly sending you BBG
                                        information. 
                                        <br/><br/>

                                        As you know, because you’ve received more emails than you can count about this,
                                        while we would like to continue sharing this information with you, changes to
                                        the EU’s data protection regulations mean that we can only do so if you allow us
                                        to.

                                        <br/><br/>
                                        So please, click our GDPR compliance button. 

                                        <br/><br/>
                                        <div style="text-align: center;">
                                            <a href="<?= $setting->app_url; ?>newsletter/subscribe?email=<?= ($model <> null) ? $model->email : ''; ?>"
                                               title="Manage my Email Subscriptions" target="_blank"><img width="200"
                                                        src="https://d1l2trc10ppjt4.cloudfront.net/Site/GDPRCOMPLIANCEIcon5b04f3823539a.jpg"
                                                        style="max-width:200px ;border-radius: 5px;"/> </a>
                                        </div>

                                        <br/><br/>
                                        If you don’t click the button by Friday 25 May, we will have to take you off our
                                        mailing list. And we really don’t want to because that means you will no longer
                                        receive information about the BBG.  
                                        <br/><br/>
                                         

                                        A member of the BBG Operations Team will give you a call after the GDPR ‘go
                                        live’ date just to make sure this e-mail was safely delivered.
                                        <br/><br/>
                                         

                                        On a final note, please know that we value your privacy and will never share
                                        your data outside the BBG, other than for the purposes stated in our <a
                                                target="_blank" class="pull-right"
                                                href="<?= $setting->app_url; ?>pages/view-by-seo-url?seo_url=privacy-policy">Privacy
                                            Policy</a>, without your prior
                                        approval. 

                                        <br/><br/>

                                        If you would like to discuss this with a member of our team please contact us at
                                        <a
                                                href="tel:<?= $setting->telephone ?>"> <?= $setting->telephone ?> </a>
                                        and we can help you out immediately.


                                        <br/>
                                        <br/>
                                        Before you go, we should let you know that if you’re using Safari to update your subscription, you will have to try twice (it seems Safari doesn’t like GDPR compliance!)


                                    </td>
                                </tr>


                                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <td class="content-block"
                                        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 15px 5px; text-align: left;"
                                        valign="top">
                                        <h3>Cheerfully yours,</h3>
                                        <?= $setting->app_name ?>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>

                </table>

            </div>
        </td>

        <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
            valign="top"></td>
    </tr>
</table>