BBG Dubai
===================
Brtish business group
  Main Module
  
Backend
---------
  
   All admin side application is developend under backend
Frontend
--------
   All frontend customer facing is developed under frontend
Common
-------
   All common logic for front and backend is be inside the common like all models should be inside common/models directory and helper classes also should inside the common/helpers folder.
   Common folder also contain emails template, models classes, helper classes.
   
   
Project Configuration
---------------------

Step 1: 
-------

Setup composer & Vendor folder 
------------------------------

If you don't have installed composer then follow the link (https://getcomposer.org/download/) to setup it,

Once you installed composer on your system then run below command to install yii2 required composer dependencies 

```
composer global require "fxp/composer-asset-plugin:^1.3.1"
```

IF you already have installed then simply RUN command **composer install** on your root dir.

step 2
-------

Create a new file under common/config/main-local.php with below configuraiton and change db connection as per your's 

```
<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=bbg',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'htmlLayout' => '@common/mail/layouts/html',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'username',
                'password' => 'password',
                'port' => '465', // Port 25 is a very common port too
                'encryption' => 'ssl', // It is often used, check your provider or mail server specs
            ],
        ]
    ],
];

```

Step 3
------
After installing composer and database kindly run *./yii migrate* to make live all database tables.

Step 4: 
Import database into your created database to have seeding contents . 


Step 5.
-------
Create two virtual hosts for admin and for frontend like **http://admin.tour.local** should point on **backend/web** and **http://tour.local** should point on **frontend/web** to run both front and backend applications.


Step 6.
-------
YOu have done! here are the admin credentials 

```
UserName: admin@tour.com
pass: admin123
```

Helping links
-------------

** Active Models Queries**
You can follow model queries formats and pattern from below link
http://www.bsourcecode.com/yiiframework2/select-query-model/

** Model data validate rules**
When you need to validate model attribute please follow the link below or already created model classes 
http://www.bsourcecode.com/yiiframework2/validation-rules-for-model-attributes-in-yiiframework-2-0/

** DB transaction **
If you need to run a transaction query then follow the below link to understand how it's working 
http://www.bsourcecode.com/yiiframework2/transaction/

** Joins in Yii2 with active models**
http://www.bsourcecode.com/yiiframework2/select-query-joins/

** Batch insert**
http://www.bsourcecode.com/yiiframework2/insert-query/

**YII2 document**
http://www.yiiframework.com/doc-2.0/yii-db-activerecord.html

File Storage
------------
We are using AWS s3 services for file storage and assets managing like images, pdf etc 


DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
