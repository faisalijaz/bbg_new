var app = angular.module("tourDubai", ['yaru22.angular-timeago']);
app.constant('APPURL', 'http://' + window.location.host);

app.run(['$http', function ($http) {
    $http.defaults.headers.common['app-tz'] = 'asia/dubai';
}]);