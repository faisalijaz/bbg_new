// productFactory.js
(function() {
    'use strict';

    angular
        .module('tourDubai')
        .factory('CheckoutService', CheckoutService);

    /** @ngInject */
    function CheckoutService($http, APPURL) {

        var apiHost = '/setting';

        var service = {
            apiHost: apiHost,
            getCityAreas: getCityAreas,
            getCities: getCities,
            getHotels: getHotels
        };

        return service;

        function getCityAreas(city_id) {

            return $http.get(APPURL + apiHost + '/areas/?city_id=' + city_id)
                .then(getSuccess)
                .catch(getError);

            function getSuccess(response) {
                return response.data;
            }

            function getError(error) {
                console.log('XHR Failed for items.\n' + angular.toJson(error.data, true));
            }
        }

        function getCities() {

            return $http.get(APPURL + apiHost + '/cities')
                .then(getSuccess)
                .catch(getError);

            function getSuccess(response) {
                return response.data;
            }

            function getError(error) {
                console.log('XHR Failed for items.\n' + angular.toJson(error.data, true));
            }
        }

        function getHotels() {

            return $http.get(APPURL + apiHost + '/hotels')
                .then(getSuccess)
                .catch(getError);

            function getSuccess(response) {
                return response.data;
            }

            function getError(error) {
                console.log('XHR Failed for items.\n' + angular.toJson(error.data, true));
            }
        }
    }
})();
