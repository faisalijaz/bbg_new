<?php include ("header.php");?>
<?php include ("banner.php");?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="membershipcontent">
                    <div class="Heading">
                        <h3>BBG Membership</h3>
                    </div>
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptateusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectet adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                    </div>
                    <div class="Heading">
                        <h3>Business</h3>
                    </div>
                    <div class="content">
                        <p>Voluptateusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectet adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                    </div>
                    <div class="Heading">
                        <h3>Individual</h3>
                    </div>
                    <div class="content">
                        <p>Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectet adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                    </div>
                    <div class="Heading">
                        <h3>Not For Profit</h3>
                    </div>
                    <div class="content">
                        <p>Qut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectet adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                    </div>
                </div>
                <div class="blueAlert">
                    <h3>Membership types</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias, aliquid assumenda culpa dolorem illum itaque laborum maiores molestiae nam obcaecati quae quasi reprehenderit suscipit tenetur vero voluptate? Eos, odit.</p>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 PaddingTopBtm30px">
                <div class="type-bg">
                    <div class="TypeHeading">
                        <h2>Business</h2>
                        <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                    </div>
                    <div class="Type">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Member Type</th>
                                <th>Membership Fee</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nominee</td>
                                <td>AED 2540</td>
                            </tr>
                            <tr>
                                <td>Alternate</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td>Additional Members</td>
                                <td>AED 895</td>
                            </tr>
                            <tr>
                                <td>Named Associates</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td colspan="2">Document Required</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ol class="numbering">
                                        <li>Copy of UAE Trade Licence.</li>
                                        <li>Copy of your Lorem ipsum dolor sit amet,</li>
                                        <li>Passport Size picture (Upload as JPEG).</li>
                                    </ol>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="fullbtn"><a href="#">Apply Now</a></div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 PaddingTopBtm30px">
                <div class="type-bg">
                    <div class="TypeHeading">
                        <h2>individual</h2>
                        <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                    </div>
                    <div class="Type">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Member Type</th>
                                <th>Membership Fee</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nominee</td>
                                <td>AED 2540</td>
                            </tr>
                            <tr>
                                <td>Alternate</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td>Additional Members</td>
                                <td>AED 895</td>
                            </tr>
                            <tr>
                                <td>Named Associates</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td colspan="2">Document Required</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ol class="numbering">
                                        <li>Copy of UAE Trade Licence.</li>
                                        <li>Copy of your Lorem ipsum dolor sit amet,</li>
                                        <li>Passport Size picture (Upload as JPEG).</li>
                                    </ol>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="fullbtn"><a href="#">Apply Now</a></div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 PaddingTopBtm30px">
                <div class="type-bg">
                    <div class="TypeHeading">
                        <h2>not for profit</h2>
                        <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                    </div>
                    <div class="Type">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Member Type</th>
                                <th>Membership Fee</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nominee</td>
                                <td>AED 2540</td>
                            </tr>
                            <tr>
                                <td>Alternate</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td>Additional Members</td>
                                <td>AED 895</td>
                            </tr>
                            <tr>
                                <td>Named Associates</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td colspan="2">Document Required</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ol class="numbering">
                                        <li>Copy of UAE Trade Licence.</li>
                                        <li>Copy of your Lorem ipsum dolor sit amet,</li>
                                        <li>Passport Size picture (Upload as JPEG).</li>
                                    </ol>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="fullbtn"><a href="#">Apply Now</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>



