<section>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="resources/images/home-banner.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block hidden-md-down mainbanner">
                    <h3>UPDATE YOUR COMPANY PROFILE</h3>
                    <p>Give yourself a high visibility by completing your company profile, select <br>
                        company type, add logo's, web address etc. This is helpful for other members <br> wanting to connect and find services you provide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="resources/images/home-banner.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block hidden-md-down mainbanner">
                    <h3>UPDATE YOUR COMPANY PROFILE</h3>
                    <p>Give yourself a high visibility by completing your company profile, select <br>
                        company type, add logo's, web address etc. This is helpful for other members <br> wanting to connect and find services you provide.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="resources/images/home-banner.jpg" alt="First slide">
                <div class="carousel-caption d-none d-md-block hidden-md-down mainbanner">
                    <h3>UPDATE YOUR COMPANY PROFILE</h3>
                    <p>Give yourself a high visibility by completing your company profile, select <br>
                        company type, add logo's, web address etc. This is helpful for other members <br> wanting to connect and find services you provide.</p>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>