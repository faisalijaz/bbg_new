$('body').unbind().on("click", "#submitModelLogin", function (e) {

    $("#submitModelLogin").prop("disabled", true);

    if ($(document).find("#accountloginform_user").val() == "" || $(document).find("#accountloginform_password").val() == "") {

        if ($(document).find("#accountloginform_user").val() == "") {

            swal({
                title: "Error!",
                text: "Email cannot be empty!",
                type: "error",
                timer: 5000,
                confirmButtonText: "OK"
            });
            
            return false;
        }

        if ($(document).find("#accountloginform_password").val() == "") {

            swal({
                title: "Error!",
                text: "Password cannot be empty!",
                type: "error",
                timer: 5000,
                confirmButtonText: "OK"
            });

            return false;
        }

        $("#submitModelLogin").prop("disabled", false);

    }
    else {

        e.preventDefault();

        var form = $("form#model_login_form");
        var formData = form.serialize();

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,

            success: function (data) {
                var data = JSON.parse(data);

                if(data.msg == "success"){
                    $("#myModal").hide();
                    window.location.href = data.url;
                }

                if (data.msg == "error") {

                    $("body").find("#ajax_login_errors").html(data.text);
                    $("body").find("#ajax_login_errors").removeClass("hidden");

                    setTimeout(function(){
                        $("body").find("#ajax_login_errors").addClass("hidden");
                    }, 6000);

                }

                $("#submitModelLogin").attr("disabled", false);
            },
            error: function (e) {

                $("#submitModelLogin").prop("disabled", false);


                swal({
                    title: "Error!",
                    text: e.responseText,
                    type: "error",
                    timer: 5000,
                    confirmButtonText: "OK"
                });

            }
        });

    }

});
