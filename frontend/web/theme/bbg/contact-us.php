<?php include ("header.php");?>
<?php include ("banner.php");?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Contact Us</h3>
                    </div>
                    <div class="contactus PaddingTopBtm">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <h3>British Business Group Dubai & Northern Emirates</h3>
                                <p>
                                    British Embassy Compound <br>
                                    Al Seef Road, Bur Dubai <br>
                                    Tel: +971 4 3970303 <br>
                                    Fax: +971 4 3970939 <br>
                                    Email: info@britbiz-uae.com
                                </p>
                                <p>
                                    <strong>Social Media Search:</strong> <br>
                                    BBGOnline
                                </p>
                                <p>
                                    <strong>Timings:</strong> <br>
                                    Sunday - Thursday 8.30am to 5.30pm
                                </p>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <img src="resources/images/map.jpg" class="img-fluid" alt="">
                            </div>

                        </div>
                        <div class="Heading">
                            <h3>Feedback From</h3>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 feedbackform">
                                <h2>Please fill in the following form specifying your requirement. We will get back to you shortly.</h2>
                                <form action="">
                                    <input type="text" class="form-control" placeholder="Username">
                                    <input type="text" class="form-control" placeholder="Password">
                                    <select class="form-control" id="exampleSelect1">
                                        <option>Contact Category</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                    <input type="text" class="form-control" placeholder="Subject">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="15"></textarea><br>
                                    <button class="Mybtn" type="button" >Send Email</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="SideHeading">
                        <h3>Quick Links</h3>
                    </div>
                    <div class="SideCalender">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">
                            <ul class="quicklinks">
                                <li>About BBG</li>
                                    <li>Objectives</li>
                                    <li>BBG Profile</li>
                                    <li>Chairman's Message</li>
                                    <li>Constitution</li>
                                    <li>Committee</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>
