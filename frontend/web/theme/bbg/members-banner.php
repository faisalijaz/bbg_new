<section class="bannerBG">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 Tcenter">
                <img src="resources/images/profilepic.jpg" class="img-fluid PaddingTopBtm30px" alt="">
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 Tcenter">
                <div style="position: unset !important; padding-top: 25px;" class="BannerText">
                    <h1>Faisal Ijaz</h1>
                    <h2>
                        CEO / British Business Group <br>
                        Villa 24, Al Tawaash Street, Jumeirah 3, Dubai <br>
                        P.O.Box 119526
                    </h2>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 Tcenter">
                <div class="editprofilebtn">
                    <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Edit Profile</a>
                </div>
            </div>
        </div>
        <div class="row whiteline text-right">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="topnav1" id="myTopnav">
                    <a href="#">Upgrade Membership</a>
                    <a href="#">Payment History</a>
                    <a href="#">My Contacts</a>
                    <a href="#" class="active">Member Offers</a>
                </div>
            </div>
        </div>
    </div>
</section>
