<?php include ("header.php");?>
<?php include ("banner.php");?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Committee</h3>
                    </div>
                    <div class="Event1 committee">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <div class="SiteText">
                                    <p>
                                        Having founded Links Group in 2002, John has spearheaded the growth of a Partnership into the corporate structure that now exists. In 2016 Links Group was acquired by the global corporate service provider, Equiom. John remains with the business to lead it through its next growth phase, sitting on the Middle East & Asia board.

                                        Since arrival in Dubai in 1998 with family, John has been an active BBG member - previously serving on the committee in various director roles: focus chair, membership, events, government liaison and Deputy Chair.

                                        For many years John has supported the group through Platinum sponsorship via Links Group, as he was unable to devote sufficient time and attention to the group by way of committee involvement due to his own business growth and development.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Event1 committee">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <div class="SiteText">
                                    <p>
                                        Having founded Links Group in 2002, John has spearheaded the growth of a Partnership into the corporate structure that now exists. In 2016 Links Group was acquired by the global corporate service provider, Equiom. John remains with the business to lead it through its next growth phase, sitting on the Middle East & Asia board.

                                        Since arrival in Dubai in 1998 with family, John has been an active BBG member - previously serving on the committee in various director roles: focus chair, membership, events, government liaison and Deputy Chair.

                                        For many years John has supported the group through Platinum sponsorship via Links Group, as he was unable to devote sufficient time and attention to the group by way of committee involvement due to his own business growth and development.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Event1 committee">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <div class="SiteText">
                                    <p>
                                        Having founded Links Group in 2002, John has spearheaded the growth of a Partnership into the corporate structure that now exists. In 2016 Links Group was acquired by the global corporate service provider, Equiom. John remains with the business to lead it through its next growth phase, sitting on the Middle East & Asia board.

                                        Since arrival in Dubai in 1998 with family, John has been an active BBG member - previously serving on the committee in various director roles: focus chair, membership, events, government liaison and Deputy Chair.

                                        For many years John has supported the group through Platinum sponsorship via Links Group, as he was unable to devote sufficient time and attention to the group by way of committee involvement due to his own business growth and development.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?php include ("RightSide.php");?>
            </div>
         </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>
