<?php include ("header.php");?>
<?php include ("banner.php");?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Committee info</h3>
                    </div>
                    <div class="committee text-center">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <h3><i class="fa fa-envelope" aria-hidden="true"></i> info@domainname.com</h3>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <h3><i class="fa fa-envelope" aria-hidden="true"></i> info@domainname.com</h3>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <h3><i class="fa fa-envelope" aria-hidden="true"></i> info@domainname.com</h3>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <h3><i class="fa fa-envelope" aria-hidden="true"></i> info@domainname.com</h3>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <h3><i class="fa fa-envelope" aria-hidden="true"></i> info@domainname.com</h3>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <h3><i class="fa fa-envelope" aria-hidden="true"></i> info@domainname.com</h3>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/committee-pic1.jpg" class="img-fluid" alt="">
                                <h5>John Martin St.Valery</h5>
                                <h6>Chairman & CEO</h6>
                                <h3><i class="fa fa-envelope" aria-hidden="true"></i> info@domainname.com</h3>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="SideHeading">
                        <h3>Get In Touch</h3>
                    </div>
                    <div class="SideCalender">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 contentcenter">
                                        <i class="fa fa-envelope blueicon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                        <p>
                                            <strong>British Business Group </strong><br>
                                            P.O. Box 9333, Dubai, <br>
                                            United Arab Emirates
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 contentcenter">
                                        <i class="fa fa-map-marker blueicon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                        <p>
                                            <strong>BBG Executive Office, </strong><br>
                                            The David May Building,<br>
                                            British Embassy <br>
                                            Al Seef Road, Dubai, <br>
                                            United Arab Emirates
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 contentcenter">
                                        <i class="fa fa-phone blueicon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                        <p>
                                            Telephone: +971 4 3970303 <br>
                                            Fax: +971 4 3970939
                                        </p>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>
