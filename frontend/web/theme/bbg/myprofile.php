<?php include ("header.php");?>
<?php include ("members-banner.php");?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>My Profile</h3>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Personal Information <span class="float-right"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></span></h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                            <div class="profile">
                                <h2>Faisal Ijaz</h2>
                                <h3>CEO / British Business Group</h3>
                                <p>
                                    <strong>Email</strong> - abhilashnair@domainname.com <br>
                                    <strong>Secondary Email</strong> - abcdefg@domainname.com <br>
                                    <strong>Contact Number</strong> - 00971501234567 <br>
                                    <strong>Address</strong> - Villa 24, Al Tawaash Street, Jumeirah 3, Dubai <br>
                                    <strong>Nationality</strong> - UK <br>
                                    <strong>Linkedin</strong> - http://linkedin.com/profilename <br>
                                    <strong>Twitter URL</strong> - http://twitter.com/profilename <br>
                                    <strong>Company Type</strong> - Construction <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Interests <span class="float-right"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></span></h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="profile text-center">
                                <h2>No Category Added Yet</h2>
                                <h3>Not Found</h3>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>My Contacts <span class="float-right"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></span></h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="profile text-center">
                                <h2>You Have No Contacts</h2>
                                <h3>Please Add contacts From members Directory</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>



