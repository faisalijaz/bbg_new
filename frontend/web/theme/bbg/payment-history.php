<?php include ("header.php");?>
<?php include ("members-banner.php");?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>Payment History</h3>
                <br>
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Invoice ID</th>
                        <th>Invoice Category</th>
                        <th>Pyment Status</th>
                        <th>Invoice Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">000001245</th>
                        <td>Member Registration</td>
                        <td>Pending</td>
                        <td>Tue, 04 April, 2017</td>
                        <td class="invoicelink"><a href="#">Invoice</a></td>
                    </tr>
                    <tr>
                        <th scope="row">000001245</th>
                        <td>Member Registration</td>
                        <td>Pending</td>
                        <td>Tue, 04 April, 2017</td>
                        <td class="invoicelink"><a href="#">Invoice</a></td>
                    </tr>
                    <tr>
                        <th scope="row">000001245</th>
                        <td>Member Registration</td>
                        <td>Pending</td>
                        <td>Tue, 04 April, 2017</td>
                        <td class="invoicelink"><a href="#">Invoice</a></td>
                    </tr>
                    </tbody>
                </table>
                <br>
            </div>

        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>



