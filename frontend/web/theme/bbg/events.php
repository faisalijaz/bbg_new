<?php include ("header.php");?>
<?php include ("banner.php");?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Upcoming events</h3>
                    </div>
                    <div class="Event1">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/event-pic.png" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <div class="EventText">
                                    <h3><i class="fa fa-calendar" aria-hidden="true"></i> 2 AUG 2017</h3>
                                    <h4>Summer Networking with...</h4>
                                    <p>Join members of the BBG and the American Business Council for an evening of networking at Vivaldi by Alfredo Russo Restaurant & Lounge. Heralded as one of the best Italian Restaurants in Dubai.</p>
                                    <span class="EventDetail"><a href="event-detail.php">Event Detail</a></span><span class="pull-right SocialLinksEvents"><a
                                                href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a
                                                href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Event1">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/event-pic.png" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <div class="EventText">
                                    <h3><i class="fa fa-calendar" aria-hidden="true"></i> 2 AUG 2017</h3>
                                    <h4>Summer Networking with...</h4>
                                    <p>Join members of the BBG and the American Business Council for an evening of networking at Vivaldi by Alfredo Russo Restaurant & Lounge. Heralded as one of the best Italian Restaurants in Dubai.</p>
                                    <span class="EventDetail"><a href="event-detail.php">Event Detail</a></span><span class="pull-right SocialLinksEvents"><a
                                                href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a
                                                href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Event1">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/event-pic.png" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <div class="EventText">
                                    <h3><i class="fa fa-calendar" aria-hidden="true"></i> 2 AUG 2017</h3>
                                    <h4>Summer Networking with...</h4>
                                    <p>Join members of the BBG and the American Business Council for an evening of networking at Vivaldi by Alfredo Russo Restaurant & Lounge. Heralded as one of the best Italian Restaurants in Dubai.</p>
                                    <span class="EventDetail"><a href="event-detail.php">Event Detail</a></span><span class="pull-right SocialLinksEvents"><a
                                                href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a
                                                href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="Event1">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <img src="resources/images/event-pic.png" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                <div class="EventText">
                                    <h3><i class="fa fa-calendar" aria-hidden="true"></i> 2 AUG 2017</h3>
                                    <h4>Summer Networking with...</h4>
                                    <p>Join members of the BBG and the American Business Council for an evening of networking at Vivaldi by Alfredo Russo Restaurant & Lounge. Heralded as one of the best Italian Restaurants in Dubai.</p>
                                    <span class="EventDetail"><a href="event-detail.php">Event Detail</a></span><span class="pull-right SocialLinksEvents"><a
                                                href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a
                                                href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?php include ("RightSide.php");?>
            </div>
         </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>
