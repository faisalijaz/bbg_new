<?php include ("header.php");?>
<?php include ("banner.php");?>
<section class="MainArea">
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="mybutn btn btn-primary btn-circle">Step 1</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="mybutn btn btn-default btn-circle" disabled="disabled">Step 2</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="mybutn btn btn-default btn-circle" disabled="disabled">Step 3</a>
                    </div>
                </div>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 EventformPadding">
                <form role="form">
                    <div class="row setup-content" id="step-1">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <h3> Select Members:</h3>
                            <div class="form-group">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>John W Carter <span class="pull-right pricebox">125</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td class="text-center">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Peter Parker <span class="pull-right pricebox">125</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td class="text-center">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>John Rambo <span class="pull-right pricebox">125</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <h3>
                                Do you want to Invite Guest ?
                                <span class="pull-right">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span>Yes</span>
                                        </label>
                                    </div>
                                </span>
                            </h3>
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td class="numberofguests">Number of Guests:</td>
                                    <td class="text-right">
                                        <span class="pricebox">125</span>
                                        <span>x 150 = 550</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <h3 class="text-right">Total: 675</h3>
                            <div class="Heading text-center">
                                <h3>Guest Information</h3>
                            </div>
                            <div class="form-group">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="Redbg">1</td>
                                        <td class="paddingtop20px">
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Full Name">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Phone Number">
                                                </div>
                                            </div>
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Email Address">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Company Name">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="Redbg">2</td>
                                        <td class="paddingtop20px">
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Full Name">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Phone Number">
                                                </div>
                                            </div>
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Email Address">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Company Name">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="Redbg">3</td>
                                        <td class="paddingtop20px">
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Full Name">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Phone Number">
                                                </div>
                                            </div>
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Email Address">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Company Name">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <button class="Mybtn nextBtn pull-right" type="button" >Save And Next</button>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-2">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center">Peter Parker</td>
                                        <td class="text-center">Member</td>
                                        <td class="text-center"><span class="pull-right pricebox">125</span></td>
                                        <td class="text-center">
                                            <span class="Editbtn">
                                                <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </span>
                                            <span class="delbtn">
                                                <a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td class="text-center">John Rambo</td>
                                        <td class="text-center">Member</td>
                                        <td class="text-center"><span class="pull-right pricebox">125</span></td>
                                        <td class="text-center">
                                            <span class="Editbtn">
                                                <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </span>
                                            <span class="delbtn">
                                                <a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td class="text-center">Robert Downey JR.</td>
                                        <td class="text-center">Member</td>
                                        <td class="text-center"><span class="pull-right pricebox">125</span></td>
                                        <td class="text-center">
                                            <span class="Editbtn">
                                                <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </span>
                                            <span class="delbtn">
                                                <a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td class="text-center">Christian Bale</td>
                                        <td class="text-center">Member</td>
                                        <td class="text-center"><span class="pull-right pricebox">125</span></td>
                                        <td class="text-center">
                                            <span class="Editbtn">
                                                <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </span>
                                            <span class="delbtn">
                                                <a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td class="numberofguests">Total Amount:</td>
                                    <td class="text-right numberofguests">675.00 AED </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="Heading text-center">
                                <h3>Payment Detail</h3>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    </label>
                                    <span class="numberofguests">Pay by Existing Card - xxxx-xxxx-xxxx-0000</span>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    </label>
                                    <span class="numberofguests">Add New Card</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="paddingtop20px">
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Name on Card">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <input type="text" class="form-control" placeholder="Card Number">
                                                </div>
                                            </div>
                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                    <select class="selectbox custom-select">
                                                        <option selected>Expiry Month</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                    <select class="selectbox custom-select">
                                                        <option selected>Expiry Year</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                    <input type="text" class="form-control" placeholder="CV Code">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>

                                </table>
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td><span class="numberofguests">Amount Payable: 675.00 AED</span></td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                            <button class="Mybtn nextBtn pull-right" type="button" >Pay Now</button>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-3">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="ThankyouMsg Eventform">
                                <h4>Thank you for your Registration</h4>
                                <div class="numberofguests text-center">Do you want to tell others BBG Members that your are Attending this event?</div>
                                <div class="checkbox paddingtop20px text-center">
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    </label>
                                    <span class="numberofguests">Yes</span>
                                    <label>
                                        <input type="checkbox" value="">
                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                    </label>
                                    <span class="numberofguests">No</span>
                                </div>
                                <div class="numberofguests text-left">Select Members</div>
                                <div class="TopMostSearch">
                                    <div class="input-group">
                                    <input type="search" class="form-control" placeholder="Select Members....">
                                    <span class="input-group-btn">
                                    <button class="MySearchBtn btn btn-default" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                    </span>
                                    </div>

                                </div>
                                <div class="FinishBtn text-center">
                                    <button class="Mybtn nextBtn" type="button" >Back</button>
                                    <button class="Mybtn nextBtn" type="button" >Finish</button>
                                </div>
                                <h3>Share</h3>
                                <!-- AddToAny BEGIN -->
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                                    <a class="a2a_button_facebook"></a>
                                    <a class="a2a_button_twitter"></a>
                                    <a class="a2a_button_google_plus"></a>
                                </div>
                                <script async src="https://static.addtoany.com/menu/page.js"></script>
                                <!-- AddToAny END -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>
