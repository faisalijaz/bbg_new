<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="resources/css/mystyles.css">
    <link rel="stylesheet" type="text/css" href="resources/css/reset.css">
    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/font-awesome/css/font-awesome.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="resources/calender/style.css">


    <title>BBG-DUBAI</title>
</head>
<body>
<header>
    <div class="TopMost">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                    <div class="MembershipBtn PaddingTopBtm">
                        <a href="#">apply for membership</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                    <div class="MemberLogin PaddingTopBtm">
                        <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-user usericonsize" aria-hidden="true"></i> Member Login</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                    <div class="MemberLogin PaddingTopBtm">
                        <a id="request_experties" class="request_experties" href="javascript:void(0)">Reqeuset Experties</a>
                    </div>
                </div>
                <div class="hidden-md-down col-lg-2 col-xl-2"></div>
                <div class="hidden-md-down col-lg-3 col-xl-3">
                    <div class="TopMostSearch">
                        <div class="input-group">
                            <input type="search" class="form-control" placeholder="I’m looking for....">
                            <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="SiteHeader">
        <div class="container">
            <div class="row">
                <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                    <a href="index.php"><img src="resources/images/logo.png" alt="BBG-Dubai"></a>
                </div>
                <div class="hidden-md-down col-lg-8 col-xl-8">
                    <div class="topnav" id="myTopnav">
                        <a href="#contact">Contact</a>
                        <a href="#news">News</a>
                        <a href="#directory">Directory</a>
                        <a href="events.php">Events</a>
                        <a href="about.php" class="active">About BBG</a>
                    </div>
                </div>
                <div class="col-8 col-sm-8 col-md-8 hidden-lg-up">
                    <div id="mySidenav" class="sidenav">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <a href="about.php" class="active">About BBG</a>
                        <a href="events.php">Events</a>
                        <a href="#directory">Directory</a>
                        <a href="#news">News</a>
                        <a href="#contact">Contact</a>
                    </div>
                    <div class="text-right">
                        <span style="font-size:30px;cursor:pointer;" onclick="openNav()">&#9776;</span>
                    </div>

                </div>


            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Members Login</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <input type="text" class="form-control" placeholder="Username">
                            <input type="text" class="form-control" placeholder="Password">
                            <button class="Mybtn" type="button" >Sign In</button>
                            <button class="Mybtn " type="button" >Forget Password</button>
                        </div>

                    </form>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
</header>

