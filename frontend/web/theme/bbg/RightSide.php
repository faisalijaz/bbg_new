<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
    <div class="SideHeading">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Events Calendar</h3>
    </div>
    <div class="SideCalender">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="EventCalender">
                    <div id="tempust"></div>
                    <!--<div id="output"><div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
    <div class="SideHeading">
        <h3><i class="fa fa-bullhorn" aria-hidden="true"></i> Upcoming events</h3>
    </div>
    <div class="SideCalender">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="UpcomingEvent1">
                    <h2>Networking with the German Busin...</h2>
                    <h3>Wed, 26 July , 2017</h3>
                    <p>Nestled in the heart of Souk Madinat Jumeirah, McGet is a popular, lively Irish...</p>
                </div>
                <div class="UpcomingEvent1">
                    <h2>Networking with the German Busin...</h2>
                    <h3>Wed, 26 July , 2017</h3>
                    <p>Nestled in the heart of Souk Madinat Jumeirah, McGet is a popular, lively Irish...</p>
                </div>
                <div class="UpcomingEvent1">
                    <h2>Networking with the German Busin...</h2>
                    <h3>Wed, 26 July , 2017</h3>
                    <p>Nestled in the heart of Souk Madinat Jumeirah, McGet is a popular, lively Irish...</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
    <div class="SideHeading">
        <h3><i class="fa fa-history" aria-hidden="true"></i> Past events</h3>
    </div>
    <div class="SideCalender">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="UpcomingEvent1">
                    <h2>Networking with the German Busin...</h2>
                    <h3>Wed, 26 July , 2017</h3>
                    <p>Nestled in the heart of Souk Madinat Jumeirah, McGet is a popular, lively Irish...</p>
                </div>
                <div class="UpcomingEvent1">
                    <h2>Networking with the German Busin...</h2>
                    <h3>Wed, 26 July , 2017</h3>
                    <p>Nestled in the heart of Souk Madinat Jumeirah, McGet is a popular, lively Irish...</p>
                </div>
                <div class="UpcomingEvent1">
                    <h2>Networking with the German Busin...</h2>
                    <h3>Wed, 26 July , 2017</h3>
                    <p>Nestled in the heart of Souk Madinat Jumeirah, McGet is a popular, lively Irish...</p>
                </div>
            </div>
        </div>
    </div>
</div>