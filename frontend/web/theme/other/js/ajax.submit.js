$('form#ajaxSubmit').on('beforeSubmit',function () {

    var form = $(this);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
        return false;
    }

    // dataGridId is the datagrid id that need to reload
    // refId is a custom form attribute that will allow to access datagrid
    var dataGridId = form.attr('refId');


    // submit form
    $.ajax({
        url: form.attr('action'),
        type: 'post',
        data: form.serialize(),
        success: function (response) {
            if(response.status == 'OK') {
                var html = "<div class='alert alert-success' role='alert'>"+response.message+"</div>";
                html += "<div class='modal-footer'>";
                html += "<a href='#' class='btn btn-success close_link' data-dismiss='modal'>Close</a>";
                html += "</div>";
                form.html(html);

                $.pjax.reload({container:'#'+dataGridId});
            }

            if(response.status == 'ERROR') {
                alert(response.message);
            }
        }
    });

    return false;
});