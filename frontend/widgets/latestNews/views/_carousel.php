<?php
$first_html = '';
$second_html = '';
if ($news <> null &&  count($news) > 0) {

    if (isset($news) && $news <> null) {
        $i = 0;


        $news_size = count($news);

        foreach ($news as $data) {

            $image = Yii::$app->params["no_image"];
            $title = "";
            $description = "";

            if ($data <> null) {
                $image = (!empty($data->image)) ? $data->image : Yii::$app->params["no_image"];

                $title = $data->title;

                if (strlen($data->title) > 60) {
                    $title = substr($title, 0, 60) . '...';
                }


                $description = $data->short_description;

                if (strlen($data->short_description) > 115) {
                    $description = substr($description, 0, 115) . '...';
                }
            }


            if ($i <= 3) {

                $first_html .= '    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">';
                $first_html .= '        <div class="Slides text-center">';
                $first_html .= '            <img class="img-fluid imgNewsSlider" src="'.  $image .'" alt="">';
                $first_html .= '            <div class="Slides-body">';
                $first_html .= '                <h4>' . $title . '</h4><p>' . $description . '</p>';
                $first_html .= '                    <a href="/news/news-details?id='.$data->id.'"><button class="Mybtn" type="button">Read More</button></a>';
                $first_html .= '            </div >
                                        </div>
                                    </div>';

            } else {

                $second_html .= '    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">';
                $second_html .= '        <div class="Slides text-center">';
                $second_html .= '            <img class="img-fluid imgNewsSlider" src="'.$image.'" alt="">';
                $second_html .= '            <div class="Slides-body">';
                $second_html .= '            <h4>' . $title . '</h4><p>' . $description . '</p>';
                $second_html .= '                <a href="/news/news-details?id='.$data->id.'"><button class="Mybtn" type="button">Read More</button></a>';
                $second_html .= '            </div >
                                        </div>
                                    </div>';

            }
            $i++;
        }
    }
?>
<section class="News clientsbg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center">
                    <h3>Latest News</h3>
                </div>
                <!--Carousel Wrapper-->
                <div id="multi-item-example1" class="carousel slide carousel-multi-item" data-ride="carousel">

                    <!--Controls-->
                    <div class="controls-top">
                        <a class="btn-floating" href="#multi-item-example1" data-slide="prev"><i
                                    class="fa fa-chevron-left"></i></a>
                        <a class="btn-floating" href="#multi-item-example1" data-slide="next"><i
                                    class="fa fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->

                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">

                        <!--First slide-->
                        <div class="carousel-item active">
                            <?= $first_html;?>
                        </div>
                        <!--/.First slide-->

                        <!--Second slide-->
                        <div class="carousel-item">
                            <?= $second_html;?>
                            <!--/.Second slide-->
                        </div>
                        <!--/.Slides-->
                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
            </div>
        </div>
</section>
<?php
}
?>


<?= $this->registerJS('
    
    $("#mixedSlider").multislider({
        duration: 750,
        interval: 3000
    }); 

'); ?>
