<?php
$first_html = '';
$second_html = '';
if ($news <> null &&  count($news) > 0) {
?>
<section class="News clientsbg">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center" style="margin-bottom: 25px;">
                    <h3>Latest News</h3>
                </div>
                <!--Carousel Wrapper-->
                <div id="mixedSlider1">
                    <div class="MS-content">
                        <?php
                        if (isset($news) && $news <> null) {
                            foreach ($news as $data) {
                                $image = Yii::$app->params["no_image"];
                                $title = "";
                                $description = "";

                                if ($data <> null) {


                                    $image = (!empty($data->image)) ? $data->image : Yii::$app->params["no_image"];

                                    $title = $data->title;
                                    if (strlen($data->title) > 60) {
                                        $title = substr($title, 0, 60) . '...';
                                    }

                                    $description = $data->short_description;
                                    if (strlen($data->short_description) > 115) {
                                        $description = substr($description, 0, 115) . '...';
                                    }
                                }
                                ?>
                                <div class="Slides text-center item">
                                    <div class="imgTitle">
                                        <img class="img-fluid imgNewsSlider" src="<?= $image; ?>" alt="">
                                    </div>
                                    <div class="Slides-body">
                                        <h4><?= $title; ?></h4>
                                        <p><?= $description; ?></p>
                                        <?= \yii\helpers\Html::a('<button class="Mybtn" type="button">Read More</button>', ['/news/news-details', 'id' => $data->id], []); ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="MS-controls">
                        <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                        <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
</section>
<?php
}
?>

<?= $this->registerJS('
    
    $("#mixedSlider1").multislider({
        duration: 750,
        interval: 3000
    }); 

'); ?>

<style>
    #mixedSlider1 .imgTitle{
        height: 200px;
    }
    #mixedSlider1  .Slides .Slides-body h4 {
        height: 80px;
    }
    #mixedSlider1  .Slides .Slides-body p {
        height: 80px;
    }
</style>