<?php

namespace frontend\widgets\latestNews;

use common\models\News;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class LatestNewsWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'carousel';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Latest News';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        LatestNewsWidgetAsset::register($this->getView());

        $lastmonth = date("Y-m-01", strtotime(date('Y-m-d')));

        $news = News::find()->where(['isPublished' => '1', 'homepage_show' => '1'])
            ->andWhere(['>=', 'FROM_UNIXTIME(created_at,"%Y-%m-%d")', $lastmonth])->orderBy([
                'news.sort_order' => SORT_ASC
            ])->all();

        return $this->render($this->view, [
            'news' => $news
        ]);
    }
}
