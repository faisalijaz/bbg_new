<?php

namespace frontend\widgets\cart;

use common\models\CartModel;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class CartWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'cart';

    /**
     * Report View title
     * @var string
     */
    public $title = 'My Booking';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        CartWidgetAsset::register($this->getView());

        $totals = [
            'total' => 0,
            'sub_total' => 0,
            'discount' => 0,
            'tax' => 0,
            'addons' => 0
        ];

        $cart = new CartModel();

        $priceSummary = $cart->totalCartAmount();
        $cartItems = $cart->getItems();

        $totals['sub_total'] = $priceSummary['price'];
        $totals['tax'] = $priceSummary['totalTax'];
        $totals['discount'] = $priceSummary['discount'];
        $totals['addonsPrice'] = $priceSummary['addonsPrice'];
        $totals['total'] = $priceSummary['totalTax'] + ($priceSummary['price'] - $priceSummary['discount']) + $priceSummary['pickup_fee'] + $priceSummary['addonsPrice'];
        $totals['pickup_fee'] = $priceSummary['pickup_fee'];
        return $this->render($this->view, [
            'items' => $cartItems,
            'total' => $totals
        ]);
    }
}
