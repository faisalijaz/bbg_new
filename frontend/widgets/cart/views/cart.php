<?php
use yii\widgets\Pjax;

?>

<?php Pjax::begin(['id' => 'cart-widget']) ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="height-300-overflow">
                <table class="table table-hover">
                    <tbody>
                    <?php foreach ($items as $key => $item) {
                        ?>
                        <tr id="cart-item-<?= $key ?>">
                            <td colspan="2" class="col-sm-12">
                                <div class="media">
                                    <a class="thumbnail pull-left cart-image" href="#">
                                        <img class="media-object" src="<?= $item['image'] ?>"
                                             style="width: 72px; height: 72px;">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="#"><?= $item['title'] ?></a></h4>

                                        <div class="row">
                                            <div class="col-lg-8 cart-short-summry">

                                                <span>Adults: <?= $item['adults'] ?>x<?= $item['price'] ?> </span>
                                                <br/>
                                                <?php if ($item['children'] > 0) { ?>
                                                    <span>Child: <?= $item['children'] ?>
                                                        x<?= $item['child_price'] ?> </span>
                                                    <br/>
                                                <?php } ?>
                                                <span>Addons AED: <?= $item['addonsTotal'] ?> </span>
                                                <br/>
                                                <span>Qty: <?= $item['addonsQty'] ?> </span>
                                            </div>
                                            <div class="col-lg-4">
                                            <span class="text-info text-right">
                                                <strong>AED <?= $item['total_price'] ?></strong>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <?php echo \yii\helpers\Html::a('x', 'javascript:void(0)', [
                                    'class' => 'close pull-right delete-item',
                                    'id' => $key,
                                    'style' => 'padding: 0px 10px'
                                ]) ?>

                                <?php echo \yii\helpers\Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/tour/view-slug/', 'slug' => $item['seo_url'], 'sessionId' => $key], [
                                    'class' => 'close pull-right edit-item',
                                    'id' => $key,
                                    'style' => 'padding: 0px 10px'
                                ]) ?>

                                <?php

                                foreach ($item['addons'] as $addon) { ?>
                                    <a href="javascript:void(0)" title="<?= $addon['title']; ?>">
                                        <img style="width: 32px; height: 32px;" class="img-thumbnail img-check"
                                             src="<?= $addon['image'] ?>" alt="<?= $addon['title']; ?>">
                                    </a>
                                <?php } ?>

                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>

            <table class="table table-hover">
                <tbody>
                <tr>
                    <td><h5>Subtotal</h5></td>
                    <td class="text-right"><h5><strong>AED <?= $total['sub_total'] ?></strong></h5></td>
                </tr>
                <!-- <tr>

                    <td><h5>Addons</h5></td>
                    <td class="text-right"><h5><strong>AED <?php /*// echo $total['addonsPrice']; */ ?></strong></h5></td>
                </tr>-->
                <tr>
                    <td><h5>Pickup Fee</h5></td>
                    <td class="text-right"><h5><strong>AED <?= $total['pickup_fee'] ?></strong></h5></td>
                </tr>
                <tr>

                    <td><h5>Discount</h5></td>
                    <td class="text-right"><h5><strong>AED <?= $total['discount'] ?></strong></h5></td>
                </tr>

                <tr>
                    <td><h3>Total</h3></td>
                    <td class="text-right"><h3><strong>AED <?= $total['total']; ?></strong></h3></td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>

<?php Pjax::end(); ?>


    <style type="text/css">
        #right-cart {
            background: #f8f8f8;
            -webkit-transition: 1s;
            -moz-transition: 1s;
            transition: 1s;
            padding: 10px;
        }

        .height-300-overflow {
            height: 300px;
            max-height: 250px;
            overflow-y: scroll;
        }

        .affix#right-cart {
            position: fixed;
            top: 80px;
            z-index: 999;
            background: #F8F8F8;
            padding: 10px;
            width: 25%;
        }
    </style>

<?php
/*
 $this->registerJs('
    $( document ).ready(function() {
$("#right-cart").affix({
    offset: {
    top: 200
    , bottom: function () {
        return (this.bottom = $("#footer").outerHeight(true))
      }
    }
  })
});
    ');
*/
?>