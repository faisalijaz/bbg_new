<?php

namespace frontend\widgets\homeReviews;

use common\models\ClientsReviews;
use common\models\Widgets;
use common\models\Reviews;
use Yii;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class HomeReviewsWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'home-reviews';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Customer Reviews';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        HomeReviewsWidgetAsset::register($this->getView());
        $reviews = ClientsReviews::find()->where(['rating' => 5, 'status' => 'active'])->limit(5)->all();
        $sliderWidget = Widgets::find()->with('widgetItems', 'widgetItems.tour')->where(['code' => 'HR'])->one();

        return $this->render('home-reviews', [
            'sliderWidget' => $sliderWidget,
            'reviews' => $reviews
        ]);
    }
}
