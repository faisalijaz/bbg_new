<?php

namespace frontend\widgets\homeReviews;

use yii\web\AssetBundle;

class HomeReviewsWidgetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [

    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

    /**
     * init function
     * @return void
     */
    public function init()
    {
        // Tell AssetBundle where the assets files are
        $this->sourcePath = __DIR__ . '/assets';

        parent::init();
    }
}
