<?php

namespace frontend\widgets\rightSidebar;

use common\models\Events;
use common\models\EventSubscriptions;
use frontend\models\EventCalendar;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class RightSidebarWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'sidebar';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Right Sidebar';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        RightSidebarWidgetAsset::register($this->getView());

        $upcoming_events = Events::find()->where(['>=', 'event_startDate', date('Y-m-d')])->andWhere(['active' => '1'])
            ->orderBy(['event_startDate' => SORT_ASC])->limit(5)->all();

        $past_events = Events::find()->where(['<', 'event_startDate', date('Y-m-d')])
            ->orderBy(['event_startDate' => SORT_DESC])->limit(5)->all();

        $data = [];


         if ($past_events <> null) {

            foreach ($past_events as $event) {

                $color = "#1f3760";
                $getSubscription = null;
                if (!\Yii::$app->user->isGuest) {
                    $getSubscription = EventSubscriptions::find()->where(['user_id' => \Yii::$app->user->id, 'event_id' => $event->id])->one();
                    if($getSubscription <> null){
                        $color = "#cd2c47";
                    }
                }


                $data[] = [
                    'id' => $event->id,
                    'name' => $event->title,
                    'startdate' => \Yii::$app->formatter->asDate($event->event_startDate, 'php:Y-m-d'),
                    'enddate' => "",
                    'starttime' => "",
                    'endtime' => "",
                    'color' => $color,
                    'url' => "/events/event-details?id=" . $event->id,
                ];
            }
        }

        if ($upcoming_events <> null) {

            foreach ($upcoming_events as $event) {
                $color = "#1f3760";
                $getSubscription = null;
                if (!\Yii::$app->user->isGuest) {
                    $getSubscription = EventSubscriptions::find()->where(['user_id' => \Yii::$app->user->id, 'event_id' => $event->id])->one();
                    if($getSubscription <> null){
                        $color = "#cd2c47";
                    }
                }

                $data[] = [

                    'id' => $event->id,
                    'name' => $event->title,
                    'startdate' => \Yii::$app->formatter->asDate($event->event_startDate, 'php:Y-m-d'),
                    'enddate' => "",
                    'starttime' => "",
                    'endtime' => "",
                    'color' => $color,
                    'url' => "/events/event-details?id=" . $event->id,
                ];
            }
        }

        /**/



        /*if(file_exists('../widgets/rightSidebar/views/events.json')){
            unlink('../widgets/rightSidebar/views/events.json');
        }
        if (file_put_contents('../widgets/rightSidebar/views/events.json', \GuzzleHttp\json_encode(['monthly' => $data]))) {
             // echo "Site"; die;
        }*/


        return $this->render($this->view, [
            'upcomingEvents' => $upcoming_events,
            'pastEvents' => $past_events,
            'sampleEvents' =>\GuzzleHttp\json_encode(['monthly' => $data])
        ]);
    }
}
