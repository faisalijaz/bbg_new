<?php

namespace frontend\widgets\rightSidebar;

use yii\web\AssetBundle;

class RightSidebarWidgetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'theme/bbg/resources/js/right_side_bar.js'
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

    /**
     * init function
     * @return void
     */
    public function init()
    {
        // Tell AssetBundle where the assets files are
        $this->sourcePath = __DIR__ . '/assets';

        parent::init();
    }
}
