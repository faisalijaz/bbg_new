<?php

namespace frontend\widgets\homeTop;

use common\models\Widgets;
use Yii;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class HomeTopWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'top-items';

    /**
     * Report View title
     * @var string
     */
    public $title = 'My Slider';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        HomeTopWidgetAsset::register($this->getView());

        $sliderWidget = Widgets::find()->with('widgetItems', 'widgetItems.tour')->where(['code' => 'dHome-page-top-listing-items'])->one();

        return $this->render($this->view, [
            'sliderWidget' => $sliderWidget
        ]);
    }
}