<?php
use yii\helpers\Html;

?>

<!--<div class="row no-padd">
    <?php /*if( count($sliderWidget) > 0 ) { */ ?>

        <?php /*foreach( $sliderWidget->widgetItems as $key => $item ) { */ ?>

            <div class="<? /*= $item->display_type */ ?>">
                <div class="item-block">
                    <div class="tour-layer delay-1 index_overlay"></div>
                    <div class="vertical-align">
                        <h3 class="cus_index_heading"><? /*= $item->tour->title */ ?></h3>
                        <p><? /*= $item->tour->short_description */ ?></p>
                        <? /*= Html::a('view more', ['/tour/view-slug', 'slug' => $item->tour->seo_url], ['class' => 'c-button small border-white']) */ ?>

                    </div>
                    <img src="<? /*= $item->tour->banner */ ?>" alt="<? /*= $item->tour->title */ ?>">
                </div>
            </div>

        <?php /*} */ ?>

    <?php /*} */ ?>

</div>-->

<div class="row no-padd">
    <?php
    $categoris = \common\models\Categories::find()->all();
    if (count($categoris) > 0) {
        foreach ($categoris as $category) {
            ?>
            <div class="col-lg-6 col-sm-12">
                <div class="item-block">
                    <div class="tour-layer delay-1 index_overlay"></div>
                    <div class="vertical-align" style="z-index: 999999999;">
                        <h3 class="cus_index_heading">
                            <?= Html::a($category->title, ['/tour/destinations', 'category' => $category->id], []); ?>
                        </h3>
                        <p><?= $category->short_description; ?></p>
                        <?= Html::a('Book more', ['/tour/destinations', 'category' => $category->id], ['class' => 'c-button small border-white']); ?>
                    </div>
                    <?php
                    if ($category->id == 1) {
                        $video = '/theme/video/sea.mov';
                    }
                    if ($category->id == 2) {
                        $video = '/theme/video/desert.mov';
                    }
                    ?>
                    <video width="100%" autoplay loop fullscreen class="fullscreen-video">
                        <source src="<?= $video; ?>" type="video/mp4">
                    </video>
                  <!--  <img src="">-->
                </div>
            </div>
            <?php
        }
    }
    ?>

</div>