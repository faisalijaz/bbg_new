<?php

use yii\helpers\Html;

if ($upcomingEvents <> null) {
    ?>
    <section class="EventSwiper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="Heading text-center" style="margin-bottom: 25px;">
                        <h3>Upcoming Events</h3>
                    </div>
                    <div id="mixedSlider">
                        <div class="MS-content">
                            <?php
                            foreach ($upcomingEvents as $event) {
                                ?>
                                <div class="Slides text-center item">
                                    <div class="imgTitle">
                                        <img class="img-fluid imgEventSlider"
                                             src="<?= ($event->image) ? $event->image : Yii::$app->params['no_image']; ?>"
                                             alt="">
                                    </div>
                                    <div class="Slides-body">
                                    <h4><?= (strlen($event->title) > 60) ? substr($event->title, 0, 60) . '...' : $event->title; ?></h4>
                                    <p><?= substr($event->short_description, 0, 90) . '...'; ?></p>
                                    <?= Html::a('<button class="Mybtn" type="button">Read More</button>', ['/events/event-details', 'id' => $event->id], []); ?>
                                </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="MS-controls">
                            <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                            <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <?php
}
?>

<?= $this->registerJS('
    
    $("#mixedSlider").multislider({
        duration: 750,
        interval: 3000
    }); 

'); ?>

<style>
    #mixedSlider .imgTitle{
        height: 250px;
    }
    #mixedSlider  .Slides .Slides-body h4 {
        height: 80px;
    }
    #mixedSlider  .Slides .Slides-body p {
        height: 75px;
    }
</style>