<?php

namespace frontend\widgets\upcomingEvents;

use yii\web\AssetBundle;

class UpcomingEventsWidgetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [

    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

    /**
     * init function
     * @return void
     */
    public function init()
    {
        // Tell AssetBundle where the assets files are
        $this->sourcePath = __DIR__ . '/assets';

        parent::init();
    }
}
