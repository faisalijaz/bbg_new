<?php

namespace frontend\widgets\upcomingEvents;

use common\models\Events;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class UpcomingEventsWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'carousel';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Upcoming Events';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        UpcomingEventsWidgetAsset::register($this->getView());

        $events = Events::find()->where(['>=', 'event_startDate', date('Y-m-d')])->andWhere(['active' => '1'])
                    ->orderBy(['event_startDate' => 'ASC'])->limit(12)->all();

        return $this->render($this->view, [
            'upcomingEvents' => $events
        ]);
    }
}
