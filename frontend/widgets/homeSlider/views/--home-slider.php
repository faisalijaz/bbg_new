<?php

use yii\helpers\Html;

?>
<section>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php
            $i = 0;
            if ($banner <> null && $banner->bannerImages <> null) {
                foreach ($banner->bannerImages as $image) { ?>

                    <div class="carousel-item <?= ($i == 0) ? 'active' :'';?>">
                        <img class="d-block img-fluid" src="<?= $image->image;?>"
                             alt="First slide">
                        <?php
                        if (!empty($image->title)) {
                            ?>
                            <div style="width: 50%"
                                 class="carousel-caption d-none d-md-block hidden-md-down landing_page_slider_alligned mainbanner">
                                <h3><?= Html::a($image->title, $image->link, ['style' => 'color:#ffffff', 'target' => '_blank']); ?></h3>
                                <p><?= $image->description; ?></p>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

            <?php
                    $i++;
                }
            }
            ?>

            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
</section>

