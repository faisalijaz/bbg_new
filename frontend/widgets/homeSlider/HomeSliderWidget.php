<?php

namespace frontend\widgets\homeSlider;

use common\models\Banners;
use common\models\Widgets;
use Yii;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class HomeSliderWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'home-slider';

    /**
     * Report View title
     * @var string
     */
    public $title = 'My Slider';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        HomeSliderWidgetAsset::register($this->getView());

        $banner = Banners::find()->where(['slug' => 'home-page-banner'])->one();

        return $this->render('home-slider', [
            'banner' => $banner
        ]);
    }
}
