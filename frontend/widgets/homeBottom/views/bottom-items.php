<script src="/theme/bbg/resources/js/jquery-1.12.4.js" ></script>
<section class="clientsbg PaddingTopBtm">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="ClientsLogos">
                    <div class="nbs-flexisel-inner">
                        <ul id="flexiselDemo3" class="nbs-flexisel-ul" style="left: -2665.67px; display: block;">
                            <?php
                            $i = 0;
                            if ($clients <> null && $clients->bannerImages <> null) {
                                foreach ($clients->bannerImages as $image) {
                                    ?>
                                    <li class="nbs-flexisel-item <?= ($i == 0) ? 'index' : ''; ?>"
                                        style="width: 444.333px;">
                                        <a href="<?= $image->link; ?>" title="<?= $image->title; ?>"> <img
                                                    src="<?= $image->image; ?>" height="125" />
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</section>
