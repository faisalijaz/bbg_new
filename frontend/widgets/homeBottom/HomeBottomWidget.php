<?php

namespace frontend\widgets\homeBottom;

use common\models\Banners;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class HomeBottomWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'bottom-items';

    /**
     * Report View title
     * @var string
     */
    public $title = 'My Slider';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        HomeBottomWidgetAsset::register($this->getView());

        $clients = Banners::find()->where(['slug' => 'clinets'])->one();
        return $this->render($this->view, [
            'clients' => $clients
        ]);
    }
}
