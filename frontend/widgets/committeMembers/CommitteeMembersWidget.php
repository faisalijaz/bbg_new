<?php

namespace frontend\widgets\committeMembers;

use yii\base\Widget;
use common\models\CommitteMembers;
/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class CommitteeMembersWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'carousel';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Committee Members';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        CommitteeMembersWidgetAsset::register($this->getView());

        $committee = CommitteMembers::find()->where(['type' => 'committe_member', 'status' => '1'])
            ->limit(6)->orderBy('sort_order')->all();

        return $this->render($this->view, [
            'committee' => $committee
        ]);
    }
}
