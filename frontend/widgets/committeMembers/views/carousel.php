<?php

if ($committee <> null) {


    if (isset($committee) && $committee <> null) {
        $i = 0;
        $first_html = '';
        $second_html = '';
        foreach ($committee as $member) {

            $image = ($member <> null) ? $member->image : "";
            $full_name = ($member <> null) ? $member->full_name : '';
            $designation = ($member <> null) ? $member->designation : '';

            if ($i <= 2) {

                $first_html .= '<div class="col-md-4">';
                $first_html .= '    <div class="Slidescmty text-center">';
                $first_html .= '        <img class="3-fluid" src="' . $image . '"  alt="">';
                $first_html .= '        <h3>' . $full_name . '</h3>';
                $first_html .= '        <h4>' . $designation . '</h4>';
                $first_html .= '    </div>';
                $first_html .= '</div >';

            } else {

                $second_html .= '<div class="col-md-4">';
                $second_html .= '    <div class="Slidescmty text-center">';
                $second_html .= '        <img class="img-fluid" src="' . $image . '"  alt="">';
                $second_html .= '        <h3>' . $full_name . '</h3>';
                $second_html .= '        <h4>' . $designation . '</h4>';
                $second_html .= '    </div>';
                $second_html .= '</div >';

            }
            $i++;
        }
    }
    ?>
    <section class="cmty">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="Heading text-center">
                        <h3>Committee members</h3>
                    </div>
                    <!--Carousel Wrapper-->
                    <div id="multi-item-example0" class="carousel slide carousel-multi-item" data-ride="carousel">

                        <!--Controls-->
                        <div class="controls-top">
                            <a class="btn-floating" href="#multi-item-example0" data-slide="prev"><i
                                        class="fa fa-chevron-left"></i></a>
                            <a class="btn-floating" href="#multi-item-example0" data-slide="next"><i
                                        class="fa fa-chevron-right"></i></a>
                        </div>
                        <!--/.Controls-->


                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">

                            <!--First slide-->
                            <div class="carousel-item active">
                                <?= $first_html;?>
                            </div>
                            <!--/.First slide-->

                            <!--Second slide-->
                            <div class="carousel-item">
                                <?= $second_html;?>
                            </div>
                            <!--/.Slides-->

                        </div>
                        <!--/.Carousel Wrapper-->
                    </div>
                </div>
            </div>
    </section>

    <?php
}
?>