<section class="bannerBG">
    <div class="container">
        <div class="row">
            <div class="col-7 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                <div class="BannerText">
                    <h1>The British Business Group</h1>
                    <h2>To be Recognised and respected as one of the premier business group in the world.</h2>
                </div>
            </div>
            <div class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                <img src="/theme/bbg/resources/images/picture.png" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>