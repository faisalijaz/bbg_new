<?php

namespace frontend\widgets\topBanner;

use common\models\Widgets;
use Yii;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class TopBannerWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'banner';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Top Banner';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        TopBannerWidgetAsset::register($this->getView());

        return $this->render($this->view, [

        ]);
    }
}
