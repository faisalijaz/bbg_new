<?php

namespace frontend\controllers;

use common\models\BlogPosts;
use yii\filters\VerbFilter;

class BlogPostsController extends \yii\web\Controller
{
    /**
     * @inheritgitdoc
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'view-invoice' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {

        $blogPosts = BlogPosts::find()->where(['status' => '1'])->andWhere(['>=', 'show_until_date', date('Y-m-d')])->all();

        return $this->render('index', [
            'blogPosts' => $blogPosts
        ]);
    }

    /**
     * @return string
     */
    public function actionBlogDetails($id)
    {

        $blogPost = BlogPosts::find()->where(['id' => $id, 'status' => '1'])->andWhere(['>=', 'show_until_date', date('Y-m-d')])->one();

        if($blogPost <> null){
            Yii::$app->session->set('opengraph_title', $blogPost->title);
            Yii::$app->session->set('opengraph_url', Yii::$app->params['appUrl'] . "/blog-posts/blog-details?id=" . $blogPost->id);
            Yii::$app->session->set('opengraph_image', (empty($blogPost->image)) ? Yii::$app->params['no_image'] : $blogPost->image);
            Yii::$app->session->set('opengraph_description', $blogPost->short_description);
        }


        return $this->render('blog_detail', [
            'blogPost' => $blogPost
        ]);
    }

}
