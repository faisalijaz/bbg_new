<?php

namespace frontend\controllers;

use common\models\CmsPages;
use common\models\Gallery;
use common\models\MenuWidget;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class PagesController
 * @package frontend\controllers
 */
class PagesController extends controller
{
    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionFaq()
    {
        $model = $this->findModel('seo_url', 'faq');
        return $this->render('faq', [
            'model' => $model
        ]);
    }

    public function actionAboutBbg(){
        $model = $this->findModel('seo_url', 'about-bbg');
        return $this->render('about', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     */
    public function actionBlog()
    {
        return $this->render('blog');
    }

    /**
     * Displays a single CmsPages model.
     * @param integer $id of page
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel('id', $id)
        ]);
    }

    /**
     * @param string $seo_url url to be view
     * @return string
     */
    public function actionViewBySeoUrl($seo_url)
    {
        $view = 'view';
        $gallery = '';

        $model = $this->findModel('seo_url', $seo_url);

        if ($seo_url == 'gallery') {
            $gallery = Gallery::find()->where([])->all();
            $view = 'gallery';
        }

        if($model <> null) {

            $url = "/pages/view-by-seo-url?seo_url=" . $seo_url;
            $menu = MenuWidget::findOne(['url' => $url]);

            if ($menu <> null) {

                if ($menu->members_only && \Yii::$app->user->isGuest) {
                    return $this->goHome();
                }
            }
        }


        return $this->render($view, [
            'model' => $model,
            'gallery' => $gallery
        ]);
    }

    /**
     * @return string
     */
    public function actionGallery()
    {
        return $this->render('gallery');
    }

    /**
     * Finds the CmsPages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $attribute to be viewed
     * @param string $value value for the attribute used
     * @return CmsPages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($attribute, $value)
    {
        $model = CmsPages::find()->where([$attribute => $value])->one();
        if ($model !== null) {

            return $model;

        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
