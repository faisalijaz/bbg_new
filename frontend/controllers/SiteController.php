<?php

namespace frontend\controllers;

use common\helpers\EmailHelper;
use common\models\AccountLoginForm;
use common\models\CmsPages;
use common\models\CommitteMembers;
use common\models\Events;
use common\models\Gallery;
use common\models\Groups;
use common\models\Invoices;
use common\models\JobPosts;
use common\models\Members;
use common\models\News;
use common\models\NewsletterSubscriptions;
use common\models\PaymentResponses;
use common\models\Payments;
use common\models\StaffCategories;
use common\models\VerifyAccount;
use common\models\ViewFromChair;
use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $search = Yii::$app->request->get('search');

        if(isset($search)){

            $search = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $search));

            $query_events = "SELECT id,title,short_description FROM `events` where `title` LIKE '%$search%' ";
            $search_events = Events::findBySql($query_events)->all();

            $query_news = "SELECT id,title,short_description FROM `news` where `title` LIKE '%$search%' ";
            $search_news = News::findBySql($query_news)->all();

            return $this->render('search', [
                'search_events' => $search_events,
                'search_news' => $search_news,
            ]);

        } else {

            // $chairmna_meassage = CmsPages::findOne(['seo_url' => 'chairman-message']);
            $chairmna_meassage = ViewFromChair::find()->Where(['display_on_home' => '1'])->orderBy([
                'date' => SORT_DESC
            ])->limit(1)->one();

            return $this->render('index', [
                'events',
                'chairmna_meassage' => $chairmna_meassage,
            ]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new AccountLoginForm();
        $guest = false;

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if (\Yii::$app->session->get('backToURL') <> null) {

                $url = \Yii::$app->session->get('backToURL');
                \Yii::$app->session->set("backToURL", null);

                return $this->goBack($url);
            }

            if (\Yii::$app->session->get('backToEventRegisterURL') <> null) {

                $url = \Yii::$app->session->get('backToEventRegisterURL');
                \Yii::$app->session->set("backToEventRegisterURL", null);

                return $this->goBack($url);
            }
            echo 1;
            return $this->redirect(['/account/profile']);

        } else {

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionAjaxLogin()
    {
        $model = new AccountLoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $url = Url::to('/account/profile', true);
            // return $this->redirect($url);

            echo Json::encode((['msg' => "success", 'url' => $url]));

        } else {

            $login_error = "";
            $errors = $model->getErrors();

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    if (is_array($error) && count($error) > 0) {
                        foreach ($error as $err) {
                            $login_error .= "<span class='alert alert-danger'>" . $err . "</span>";
                        }
                    } else {
                        $login_error .= "<span class='alert alert-danger'>" . $error . "</span>";
                    }
                }
            }

            echo Json::encode(['msg' => "error", 'text' => $login_error]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        $banner = CmsPages::find()->where(['seo_url'=>'contact-us'])->one();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
                'banner' => $banner,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionCommitteeInfo()
    {
        $staffCategories = StaffCategories::find()->where(['status' => '1'])->andWhere(['!=','id' , '3'])->orderBy([
            'sort_order' => SORT_ASC
        ])->all();



        return $this->render('committee-info', [
            'staffCategories' => $staffCategories,
            // 'staff' => $staff
        ]);
    }

    /**
     * @return string
     */
    public function actionCommitteeDetails($id)
    {
        $committee = CommitteMembers::find()->where(['id' => $id])->andWhere(['status' => '1'])->orderBy([
            'sort_order' => SORT_ASC
        ])->one();

        return $this->render('committee_staff_detail', [
            'data' => $committee
        ]);
    }

    /**
     * @return string
     */
    public function actionOperationsInfo()
    {
        $staff = CommitteMembers::find()->where(['type' => '3', 'status' => '1'])->orderBy([
            'sort_order' => SORT_ASC
        ])->all();

        return $this->render('operations-team-info', [
            'staff' => $staff
        ]);

    }


    /**
     * @return string
     */
    public function actionOperationsTeamDetails($id)
    {
        $staff = CommitteMembers::find()->where(['id' => $id])->andWhere(['type' => 'office_staff', 'status' => '1'])->orderBy([
            'sort_order' => SORT_ASC
        ])->one();

        return $this->render('committee_staff_detail', [
            'data' => $staff
        ]);

    }



    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $model = $this->findModel('seo_url', 'faq');
        return $this->render('about', [
            'model' => $model
        ]);
    }


    public function actionApplyMembership()
    {

        $membership_types = Groups::find()->orderBy(['sort_order' => SORT_ASC])->all();
        $model = CmsPages::findOne(['seo_url' => 'bbg-membership']);
        return $this->render('membership', [
            'model' => $model,
            'membership_types' => $membership_types
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup($membership = 0)
    {
        $model = new SignupForm();
        $errors = [];

        if (Yii::$app->request->get()) {
            $model->group_id = Yii::$app->request->get('membership');
            $model->account_type = 'member';
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($user = $model->signup()) {
                \Yii::$app->session->setFlash('success', 'Your application for membership to the British Business Group is received with thanks. Check your email for further instructions.');
                return $this->redirect(['/login']);
            }

        } else{
            $errors[] = $model->getErrors();
        }

        return $this->render('signup', [
            'model' => $model,
            'errors' => $errors
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token user Auth Token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {

            $model = new ResetPasswordForm($token);

        } catch (InvalidParamException $e) {

            Yii::$app->session->setFlash('error', 'Link is expired, please create a new one by entering your email');
            return $this->redirect('/site/request-password-reset');

            // throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify your account.
     *
     * @param string $token user Auth Token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionVerify($token) {

        try {
            $model = new VerifyAccount($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->verify()) {
            Yii::$app->session->setFlash('success', 'Your Account is verified now.');
            $this->redirect('/login');
        } else {
            throw new UnauthorizedHttpException('Verification code has been expired or invalid.');
        }
    }


    /**
     * @return string
     */
    public function actionInvoice(){
        return $this->render('invoice');
    }

    /**
     * @return string
     */
    public function actionSubscribeNewsletter()
    {
        if (Yii::$app->request->post()) {

            $email = Yii::$app->request->post("email");

            if ($email) {
                $model = NewsletterSubscriptions::find()->where(['email' => $email])->one();
                if ($model == null) {
                    $model = new NewsletterSubscriptions();
                }
            } else {
                $model = new NewsletterSubscriptions();
            }

            $model->email = Yii::$app->request->post("email");
            $model->first_name = Yii::$app->request->post("first_name");
            $model->last_name = Yii::$app->request->post("last_name");

            $model->active = "1";
            $model->events_news = "1";
            $model->weekly_newsletter = "1";
            $model->special_offers = "1";
            $model->is_member = 0;

            $text = "";

            if (!$model->save()) {

                foreach ($model->getErrors() as $error) {

                    if (is_array($error)) {
                        foreach ($error as $err) {
                            $text .= $err . "<br/>";
                        }
                    } else {
                        $text .= $error . "<br/>";
                    }
                }

                return \GuzzleHttp\json_encode(['type' => 'error', 'msg' => $error]);
            }

            return \GuzzleHttp\json_encode(['type' => 'success', 'msg' => "You are now subscribed to receive email updates, special offers and announcements from the BBG!"]);
        }
    }

    /**
     * @param $invoice_id
     */
    public function actionInvoicePayment($invoice_id = "")
    {
        if ($invoice_id) {
            \Yii::$app->session->set('payInvoiceId', $invoice_id);
        } else {

            if (Yii::$app->session->get('payInvoiceId')) {
                $invoice_id = \Yii::$app->session->get('payInvoiceId');
            } else {
                Yii::$app->session->setFlash('error', "Invoice number does not exists");
                return $this->goHome();
            }
        }

        if (isset($invoice_id) && !empty($invoice_id)) {

            $errors = [];
            $invoice = Invoices::findOne($invoice_id);
            $result = "";

            $title = "Invoice Payment";
            $prod_name = "Invoice Payment";

            if ($invoice <> null) {

                // If invoice is already paid
                if ($invoice->payment_status == 'paid') {

                    \Yii::$app->session->setFlash('error', "This invoice is already paid");
                    return $this->redirect('/');

                }

                if ($invoice->invoice_related_to == "event") {

                    $title = "Event Registration";
                    $prod_name = Events::findOne($invoice->invoice_rel_id)->title;

                } else {

                    $title = "Membership";
                    if ($invoice->invoice_category != "") {
                        $prod_name = $invoice->invoice_category;
                    }
                }
            } else{
                Yii::$app->session->setFlash('error',"Invoice is invalid!");
                return $this->goHome();
            }

            return $this->render('payment', [
                'invoice' => $invoice,
                'title' => $title,
                'prod_name' => $prod_name
            ]);
        }
    }

    /**
     *
     */
    public function actionVerifyPayment()
    {
        if (\Yii::$app->request->post()) {

            $response = \Yii::$app->request->post();
            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;

            if ($response['response_code'] == "100") {
                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);
            }else{

            }
            $model_response = new PaymentResponses();
            $model_response->invoice_id = $invoice_id;
            if(isset($response['response_code'])) {
                $response_text = $this->responseCodes($response['response_code']);
                $model_response->response_code = $response['response_code'];
                $model_response->response_text = $response_text;

                if (isset($response['transaction_id'])) {
                    $model_response->payment_reference = $response['transaction_id'];
                }
                $model_response->save();
            }

            \Yii::$app->session->set('payInvoiceId', 0);

            return $this->render('payment_result', [
                'response' => $response,
                'invoice' => $invoice
            ]);
        }

        $this->goHome();
    }

    /**
     * @param $invoice
     */
    public function actionPaymentNotifications()
    {

        /*(new EmailHelper())->sendEmail(
            'se.faisalijaz@gmail.com',[],
            'Testing IPN', 'testing_ipn',
            ['ipn' => Yii::$app->request->post()]
        );*/

        if (Yii::$app->request->post()) {

            $response = \Yii::$app->request->post();

            /*(new EmailHelper())->sendEmail(
                'se.faisalijaz@gmail.com',[],
                'Testing IPN Inside Condition', 'testing_ipn',
                ['ipn' => $response]
            );*/


            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;
            $invoice_update = false;

            if ($response['response_code'] == "100") {

                $model = new Payments();
                $payment = $model->paytabs_make_payment_ipn($response);
                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);

                if ($payment <> null && count($payment) > 0) {
                    if (isset($payment['status']) && $payment['status'] == 'succes') {

                        $payment_id = $payment['id'];
                        $invoices = \Yii::$app->request->post('order_id');

                        if (strpos($invoices, '|') !== false) {

                            $invoices = array_filter(explode('|', $invoices));

                            if (is_array($invoices) && count($invoices) > 0) {
                                foreach ($invoices as $inv_id) {

                                    $invoice = Invoices::findOne(['invoice_id' => $inv_id]);
                                    if ($invoice <> null) {
                                        if ($invoice->invoice_related_to == 'event') {
                                            $event = Events::findOne($invoice->invoice_rel_id);
                                        }
                                    }

                                    $invoice_update = $model->updateInvoicePaidIpn($response, $invoice, $payment_id);
                                }
                            }

                        } else {

                            $invoice = Invoices::findOne(['invoice_id' => $invoices]);

                            if ($invoice <> null) {
                                if ($invoice->invoice_related_to == 'event') {
                                    $event = Events::findOne($invoice->invoice_rel_id);
                                }
                            }

                            $invoice_update = $model->updateInvoicePaidIpn($response, $invoice, $payment_id);
                        }

                    } else {
                        return false;
                    }
                }

                if (isset($payment['status']) && $payment['status'] == 'succes') {

                    $payment_id = $payment['id'];

                    if ($invoice <> null) {
                        if ($invoice->invoice_related_to == 'event') {
                            $event = Events::findOne($invoice->invoice_rel_id);
                        }
                    }

                    $invoice_update = $model->updateInvoicePaidIpn($response, $invoice, $payment_id);
                }

                if($invoice_update){
                    return true;
                }

                return false;
            }
        }
    }

    /**
     * @param $invoice
     * @return mixed
     */
    public function actionViewInvoice($invoice){

        /*if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }*/

        $member = Members::findOne(Yii::$app->user->id);
        $invoices = Invoices::findOne($invoice);

        $pdf = \Yii::$app->pdf;
        $mpdf = $pdf->api;
        $mpdf->format = Pdf::FORMAT_A4;
        $mpdf->orientation = Pdf::ORIENT_PORTRAIT;

        if ($invoices->invoice_related_to == "event") {

            $content = $this->renderPartial('/account/send_event_invoice', [
                'member' => ($member <> null) ? $member : null,
                'invoice' => $invoices
            ]);

        } else {

            $content = $this->renderPartial('/account/send_invoice', [
                'member' => $member,
                'invoice' => $invoices
            ]);
        }

        $mpdf->WriteHtml($content);
        return $mpdf->Output('Invoice', 'I');
    }

    /**
     * @param string $id
     * @return string
     */
    public function actionViewFromChair($id = "")
    {

        $single_message = null;

        if ($id) {

            $single_message = ViewFromChair::findOne($id);

            if($single_message <> null){
                Yii::$app->session->set('opengraph_title', $single_message->title);
                Yii::$app->session->set('opengraph_url', Yii::$app->params['appUrl'] . "/site/view-from-chair?id=" . $single_message->id);
                Yii::$app->session->set('opengraph_image', (empty($single_message->image)) ? Yii::$app->params['no_image'] : $single_message->image);
                Yii::$app->session->set('opengraph_description', $single_message->short_description);
            }

        }

        $chariman_messages = ViewFromChair::find()->Where(['status' => '1'])->orderBy([
            'date' => SORT_DESC
        ])->limit(5)->all();

        return $this->render('chairman_message_detail', [
            'single_message' => $single_message,
            'chariman_messages' => $chariman_messages,
        ]);

    }

    /**
     * @param string $id
     * @return string
     */
    public function actionViewFromChairList()
    {
        $query = ViewFromChair::find()->Where(['status' => '1']);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 5
        ]);

        $chariman_messages = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy([
                'date' => SORT_DESC
            ])->all();

        return $this->render('chairman_message_list', [
            'chariman_messages' => $chariman_messages,
            'pages' => $pages
        ]);

    }


    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionJobList()
    {
        $query = JobPosts::find()->Where(['published' => '1'])->andWhere(['>=', 'auto_remove_after', date('Y-m-d')]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $job_details = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['posted_on' => SORT_DESC])
            ->all();

        return $this->render('jobs_list', [
            'jobs_list' => $job_details,
            'pages' => $pages
        ]);

    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionJobPostDetail($id)
    {
        if (!$id) {
            return $this->goHome();
        }

        $job_details = JobPosts::find()->Where(['id' => $id])->orderBy([
            'posted_on' => SORT_DESC
        ])->one();

        if($job_details <> null){
            Yii::$app->session->set('opengraph_title', $job_details->job_title);
            Yii::$app->session->set('opengraph_url', Yii::$app->params['appUrl'] . "/site/job-post-detail?id=" . $job_details->id);
            Yii::$app->session->set('opengraph_image',  Yii::$app->params['no_image']);
            Yii::$app->session->set('opengraph_description', $job_details->short_description);
        }

        return $this->render('job_detail', [
            'job_details' => $job_details
        ]);

    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionGallery()
    {
        $query = Gallery::find()->where(['status' => 'active']);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $gallery = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['id' => SORT_DESC])
            ->all();


        return $this->render('gallery', [
            'gallery' => $gallery,
            'pages' => $pages
        ]);

    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionGalleryItem($id)
    {
        if (!$id) {
            return $this->goHome();
        }

        $item = Gallery::findOne($id);


        return $this->render('gallery_item_detail', [
            'item' => $item
        ]);

    }


    /*public function actionVerifyPaymentOld()
    {
        if (\Yii::$app->request->post()) {

            $response = \Yii::$app->request->post();

            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;

            if ($response['response_code'] == "100") {

                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);
                /**
                $model = new Payments();
                $payment = $model->paytabs_make_payment($response);

                if (isset($payment['status']) && $payment['status'] == 'succes') {

                $payment_id = $payment['id'];

                if ($invoice <> null) {
                if ($invoice->invoice_related_to == 'event') {
                $event = Events::findOne($invoice->invoice_rel_id);
                }
                }

                $model->updateInvoicePaid($response, $invoice, $payment_id);

                }
            }

            \Yii::$app->session->set('payInvoiceId', 0);

            return $this->render('payment_result', [
                'response' => $response,
                'invoice' => $invoice
            ]);
        }

        $this->goHome();
    }*/

    Public function responseCodes($code)
    {
        $response_array = array(
            "0" => "The transaction is unsuccessful",
            "100" => "Payment is completed.",
            "101" => "The transaction is missing one or more required fields.",
            "102" => "The transaction contains one or more invalid data fields",
            "104" => "The reference code sent with this authorization request matches the reference code of another authorization request that you sent in the last 15 minutes",
            "110" => "The transaction was only approved for a partial amount",
            "111" => "The transaction is authorized successfully",
            "112" => "The authorization is partially captured",
            "113" => "The authorization is fully captured",
            "114" => "The authorization is expired",
            "115" => "The authorization is partially captured and the remaining expired",
            "116" => "The authorization is fully reversed",
            "150" => "A technical error has occurred during the transaction. Please try again.",
            "151" => "The server has timed out. Please try again.",
            "152" => "The request has not been completed. Please try again.",
            "200" => "The authorization request was approved by the card issuing bank but declined by PayTabs because it did not pass the Address Verification Service (AVS) check.",
            "201" => "An error was encountered while processing your transaction. Please contact your bank for further clarification.",
            "202" => "An expired card has been used in this transaction",
            "203" => "The card used in this transaction has been declined. Please contact your bank for further clarification",
            "204" => "The funds are insufficient to cover this transaction",
            "205" => "The card used in this transaction is potentially stolen or lost.",
            "207" => "The system was unable to establish a connection with the card issuing bank. Please contact your bank for further clarification.",
            "208" => "The card used in this transaction is inactive or not authorized for card-not-present transactions. Please contact your bank for further clarification.",
            "209" => "American Express Card Identification Digits (CID) did not match",
            "210" => "The transaction amount exceeds the maximum withdrawal amount limit.",
            "211" => "The cardholder has entered an invalid card verification number.",
            "220" => "There may be a problem with your bank account. Please contact your bank for further clarification",
            "221" => "An error was encountered during this transaction. Please contact your bank for further clarification.",
            "222" => "Your bank account may be frozen. Please contact your bank for further clarification.",
            "230" => "The authorization request was approved by the card issuing bank but declined by PayTabs because it did not pass the card verification (CV) check.",
            "231" => "The card number is invalid.",
            "232" => "The card type is not accepted by the payment processor.",
            "233" => "This transaction is declined by the processor.",
            "234" => "An error was encountered while processing the transaction. Kindly contact PayTabs customer service for further clarification.",
            "235" => "An error was encountered while processing the transaction. Kindly contact PayTabs customer service for further clarification.",
            "236" => "An error was encountered while processing the transaction. Kindly contact PayTabs customer service for further clarification.",
            "237" => "The transaction has been cancelled and the funds reversed.",
            "238" => "The transaction is successful.",
            "240" => "The card type is invalid or doesn't correlate with the credit card number",
            "242" => "The transaction is unsuccessful",
            "244" => "Merchant not active",
            "250" => "An error was encountered while processing your transaction. Please try again.",
            "301" => "An error was encountered during this transaction. Please contact your bank for further clarification.",
            "305" => "The card holder has entered an incorrect PIN.",
            "306" => "The transaction amount exceeds the maximum withdrawal amount limit.",
            "307" => "The system was unable to establish a connection with the card issuing bank. Please contact your bank for further clarification.",
            "308" => "The card number has not yet been mapped to the related bank.",
            "328" => "Sadad account is not valid. Please contact your bank.",
            "330" => "The transaction amount exceeds the maximum per transaction limit",
            "331" => "The transaction amount exceeds the maximum per day limit",
            "333" => "Sadad account does not exist. Please enter valid Sadad account",
            "334" => "The transaction is rejected",
            "336" => "Transaction has been cancelled by the Consumer",
            "337" => "Invalid input data. Please verify the input details provided.",
            "338" => "MerchantRefNum is already Used",
            "339" => "Sorry, Unexpected System Error, Please try again",
            "340" => "Transaction cannot be processed",
            "341" => "Transaction cannot be processed at this moment",
            "342" => "Transaction failed",
            "343" => "Sadad account password entered is incorrect. Please provide the valid password",
            "344" => "Second level authentication credentials incorrect. Please provide valid credentials.",
            "345" => "This transaction cannot be processed due to insufficient funds in your account",
            "346" => "Transaction timed out",
            "347" => "Cannot proceed with checkout process.",
            "348" => "SADAD Account status is not active/inactive",
            "349" => "Merchant Name and Merchant Id does not match",
            "350" => "Duplicate Transaction Found",
            "360" => "Payment is pending for the transaction.",
            "361" => "Payment reference number is expired for this transaction.",
            "401" => "The payment processing provider, or an intermediary, refused to authorize the payment despite the details being syntactically correct.",
            "402" => "The consumer is on the GoInterpay blacklist and GoInterpay has refused to process the order.",
            "403" => "Based on the parameters of the order, GoInterpay suspects that the order is fraudulent and/or represents too high a risk to proceed with processing.",
            "475" => "Authentication for this transaction is incomplete. Please enter your 3D Secure PIN in the pop-up authentication window.",
            "476" => "The cardholder has entered an invalid 3DSecure authentication PIN.",
            "483" => "This transaction is under review and will be reversed based on your card issuing bank's policy, if its not approved within 24 hours.",
            "484" => "Transaction is declined from UnderReview state.",
            "485" => "Device FingerPrint id was missing",
            "486" => "Transaction restricted due to risk limitations or parameters outside merchant risk setting",
            "700" => "There is a PayTabs System request error. Please try again",
            "800" => "There is a PayTabs System request error. Please try again.",
            "801" => "American Express cards are not accepted by the Payment Processor.",
            "802" => "The total transaction amount has exceeded the maximum transaction limit set by PayTabs. Please contact your PayTabs account manager for further clarification",
            "803" => "The total transaction amount does not meet the minimum allowable transaction limit set by PayTabs. Please contact your PayTabs account manager for further clarification.",
            "804" => "Transaction currency is not supported, please contact your merchant",
            "805" => "Payment is not completed. Sadad Account password is not entered/invalid.",
            "806" => "This transaction is not Verified by Visa or MasterCard Secure. Please try other card.",
            "807" => "This token has been canceled by PayTabs Operations",
            "808" => "There seems to be a problem connecting to the bank, Kindly try again in a few moments",
            "481" => "This transaction may be suspicious. If this transaction is genuine, please contact PayTabs customer service to enquire about the feasibility of processing this transaction",
            "482" => "This transaction may be suspicious. If this transaction is genuine, please contact PayTabs customer service to enquire about the feasibility of processing this transaction",
            "810" => "You already requested Refund for this Transaction ID",
            "811" => "Amount is above or below the invoice and also the minimum balance",
            "812" => "Refund request is sent to Operation for Approval. You can track the Status",
            "813" => "You are not authorized to view this transaction",
            "0404" => "You don’t have permissions",
            "4001" => "Variable not found",
            "4002" => "Invalid Credentials.",
            "4006" => "Your time interval should be less than 60 days",
            "4007" => "'currency' code used is invalid. Only 3 character ISO currency codes are valid.",
            "4008" => "Your SITE URL is not matching with your profile URL",
            "4012" => "PayPage created successfully",
            "4013" => "Your 'amount' post variable should be between 0.27 and 5000.00 USD",
            "4014" => "Products titles, Prices, quantity are not matching",
            "4090" => "Data Found",
            "4091" => "There are no transactions available",
            "4094" => "Your total amount is not matching with the sum of unit price amounts per quantity ",
            "4404" => "You don't have permissions to create an Invoice",
            "5000" => "Payment has been rejected",
            "5001" => "Payment has been accepted successfully",
            "5002" => "Payment has been forcefully accepted",
            "5003" => "Payment has been refunded",
        );

        if (array_key_exists($code, $response_array)) {
            return $response_array[$code];
        } else {
            echo " the transaction has been rejected!";
        }

    }


}
