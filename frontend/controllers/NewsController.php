<?php

namespace frontend\controllers;

use common\helpers\EmailHelper;
use common\models\Accounts;
use common\models\EventSubscriptions;
use common\models\News;
use common\models\NewsletterSubscriptions;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * Class EventsController
 * @package frontend\controllers
 */
class NewsController extends \yii\web\Controller
{
    /**
     * @return string
     */
    public function actionIndex($type = "")
    {

        $lastmonth = date("Y-m-01", strtotime(date('Y-m-d')));
        $title = "News";

        if(Yii::$app->request->get('month') || Yii::$app->request->get('year') || Yii::$app->request->get('category')){

            $query = News::find()->where([
                'isPublished' => '1',
            ])->orWhere(['re_post' => '1']);

            if(Yii::$app->request->get('category')){
                $query->andWhere((['category' => Yii::$app->request->get('category')]));
            }

            if(Yii::$app->request->get('month')){
                $query->andWhere(['=', 'FROM_UNIXTIME(created_at,"%m")', Yii::$app->request->get('month')]);
            }

            if(Yii::$app->request->get('year')){
                $query->andWhere(['=', 'FROM_UNIXTIME(created_at,"%Y")', Yii::$app->request->get('year')]);
            }

        } else{

            if ($type == 'past') {
                $title = "Past News";
                $query = News::find()->where([
                    'isPublished' => '1'
                ])->andWhere(['<=', 'FROM_UNIXTIME(created_at,"%Y-%m-%d")', $lastmonth]);

            } else {
                $query = News::find()->where([
                    'isPublished' => '1',
                ])->andWhere(['>=', 'FROM_UNIXTIME(created_at,"%Y-%m-%d")', $lastmonth])->orWhere(['re_post' => '1']);
            }


        }


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $news = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy([
                'FROM_UNIXTIME(created_at,"%Y-%m-%d")' => SORT_DESC
            ])->all();

        return $this->render('index', [
            'news' => $news,
            'pages' => $pages,
            'title' => $title
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionNewsDetails($id)
    {
        $news = News::findOne($id);

        if($news <> null){
            Yii::$app->session->set('opengraph_title', $news->title);
            Yii::$app->session->set('opengraph_url', Yii::$app->params['appUrl'] . "/news/news-details?id=" . $news->id);
            Yii::$app->session->set('opengraph_image', (empty($news->image)) ? Yii::$app->params['no_image'] : $news->image);
            Yii::$app->session->set('opengraph_description', $news->short_description);
        }
        return $this->render('news_detail', [
            'news' => $news
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEventRegister($id)
    {
        $member_fee = 0;
        $userData = [];

        \Yii::$app->session->set("backToEventRegisterURL", null);

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id]));
            return $this->redirect(['/login']);
        }

        $event = Events::findOne($id);

        if ($event->registration_start >= date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is not opend yet. Please wait for Registeration to open!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        if ($event->registration_end <= date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is Closed!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        $member_contacts = Accounts::findAll(['parent_id' => Yii::$app->user->identity->id]);

        if (\Yii::$app->request->post()) {
            $userData = $this->eventRegisterForm(Yii::$app->request->post());
        }

        return $this->render('register_event', [
            'event' => $event,
            'contacts' => $member_contacts
        ]);
    }

    /**
     *
     */
    public function actionInformAboutEvent()
    {
        if (Yii::$app->request->post()) {
            $members = Yii::$app->request->post('members');
            $event = Events::findOne(Yii::$app->request->post('event'));

            if (count($members) > 0) {
                foreach ($members as $member) {

                    $user_email = Accounts::findOne($member)->email;
                    try {
                        echo $user_email;
                        (new EmailHelper())->sendEventEmail($user_email, 'faisal@800wisdom.ae', 'Event Information', 'events/event-registration.php', [
                            'event' => $event,
                            'user' => $user_email
                        ]);

                    } catch (Exception $e) {
                        /* echo '<pre>';
                         print_r($e->getMessage());
                         die();*/
                    }
                }
            }
        }
    }

    /**
     * @param $data
     * @return array|\yii\web\Response
     */
    public function eventRegisterForm($data)
    {

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (count($data) > 0) {
            $subscribe = new EventSubscriptions();
            $resp = $subscribe->memberEventRegisteration($data);

            if ($resp['msg']) {
                print_r(\GuzzleHttp\json_encode($resp));
                echo " | ";
            } else {
                return $subscribe->getErrors();
            }
        }
    }

    /**
     * @return string
     */
    public function actionSubscribeNewsletter($email = "")
    {

        if ($email) {
            $model = NewsletterSubscriptions::find()->where(['email' => $email])->one();
            if($model == null){
                $model = new NewsletterSubscriptions();
            }
        } else {
            $model = new NewsletterSubscriptions();
        }

        $model->active = "1";
        $model->events_news = "1";
        $model->weekly_newsletter = "1";
        $model->special_offers = "1";
        $model->is_member = 0;

        if ($model->load(Yii::$app->request->post())) {

            $record = NewsletterSubscriptions::find()->where(['email' => $model->email])->one();

            if ($record <> null) {

                $record->active = "1";
                $record->events_news = "1";
                $record->weekly_newsletter = "1";
                $record->special_offers = "1";
                $record->is_member = 0;


                if ($record->load(Yii::$app->request->post()) && $record->save()) {
                    \Yii::$app->session->setFlash('success', 'You are now subscribed to receive email updates, special offers and announcements from the BBG!');
                    $this->redirect(['/']);
                } else {
                    \Yii::$app->session->setFlash('error', $record->getErrors());
                    $this->redirect(['/']);
                }

            } else {

                if (!$model->save()) {
                    \Yii::$app->session->setFlash('error', $model->getErrors());
                    $this->redirect(['/']);
                }

                \Yii::$app->session->setFlash('success', 'You are now subscribed to receive email updates, special offers and announcements from the BBG!');
                $this->redirect(['/']);
            }


        } else {
            return $this->render('subscribe', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionUnSubscribe($newsletter = "")
    {
        echo $newsletter;

        if ($newsletter) {

            $model = NewsletterSubscriptions::findOne($newsletter);
            $model->active = "0";

            if ($model->save()) {
                \Yii::$app->session->setFlash('error', 'We’re sorry to see you go. You are now unsubscribed from our mailing list.');
                $this->redirect(['/']);
            }else{
                echo '<pre>';
                print_r($model->getErrors());
                die;
            }

        } else {

            $model = new NewsletterSubscriptions();

            return $this->render('subscribe', [
                'model' => $model,
            ]);
        }
    }
}
