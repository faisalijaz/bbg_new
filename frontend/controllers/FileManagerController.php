<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\helpers\Utf8Helper;
use common\models\MediaManager;
use yii\web\UploadedFile;

/**
 * BannerController implements the CRUD actions for Banners model.
 * @author Nadeem Akhtar <nadeem@myswich.com>
 */
class FileManagerController extends Controller
{
    /**
     * Init function that will init the parent
     * @return null
     * */
    public function init()
    {
        $this->enableCsrfValidation = false;
        parent::init();
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */

    public function actionUpload()
    {
        $json = [];

        // Make sure we have the correct directory

        $currentDir = MediaManager::findOne(193);
        $parent_id = $currentDir->id;
        $path = $currentDir->path . '/';

        if (!$json) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');

            if (!empty($file->name)) {
                // Sanitize the filename
                $filename = basename(html_entity_decode($file->name, ENT_QUOTES, 'UTF-8'));
                $extension = $file->extension;
                $name = $file->baseName;

                // Validate the filename length
                if ((Utf8Helper::utf8_strlen($filename) < 3) || (Utf8Helper::utf8_strlen($filename) > 255)) {
                    $json['error'] = Yii::$app->params['error_filename'];
                }

                // Allowed file extension types
                $allowed = [
                    'jpg',
                    'jpeg',
                    'gif',
                    'png',
                    'pdf',
                    'doc',
                    'docx',
                    'xlsx',
                    'xls'
                ];

                if (!in_array(Utf8Helper::utf8_strtolower(Utf8Helper::utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = Yii::$app->params['error_filetype'];
                }

                // Allowed file mime types
                $allowed = [
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    'image/gif',
                    'application/pdf',
                    'application/msword',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.ms-excel'
                ];

                if (!in_array($file->type, $allowed)) {
                    $json['error'] = Yii::$app->params['error_filetype'];
                }

                // Return any upload error
                if ($file->error != UPLOAD_ERR_OK) {
                    $json['error'] = Yii::$app->params['error_upload' . $file->error];
                }
            } else {
                $json['error'] = Yii::$app->params['error_upload'];
            }
        }

        if (!$json) {

            $uniqueName = trim((str_replace(' ', '', $name) . uniqid()));

            $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' . $extension;

            $uploadObject = Yii::$app->get('s3bucket')->upload($path . $randomName, $file->tempName);

            if ($uploadObject) {
                // check if CDN host is available then upload and get cdn URL.
                if (Yii::$app->get('s3bucket')->cdnHostname) {
                    $url = Yii::$app->get('s3bucket')->getCdnUrl($path . $randomName);
                } else {
                    $url = Yii::$app->get('s3bucket')->getUrl($path . $randomName);
                }

                // Save Data into Database
                $data = ['name' => $file->name, 'parent_id' => $parent_id, 'type' => 'file', 'href' => $url, 'path' => $path . $randomName ];
                $this->saveObject($data);
                $json = $data;
                $json['success'] = Yii::$app->params['text_uploaded'];
            } else {
                $json['error'] = Yii::$app->params['error_upload'];
            }
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $json;

    }

    /**
     * Save Object Onformaiton
     * @param array $data of object
     * @return boolean tru or false
     * */
    public function saveObject($data)
    {
        $folderObj = new MediaManager();
        $folderObj->attributes = $data;
        $folderObj->created_at = Yii::$app->dateTime->getTime();

        return $folderObj->save();
    }
}
