<?php

namespace frontend\controllers;

use app\models\MemberSearch;
use common\models\AccountCompany;
use common\models\Accounts;
use common\models\Categories;
use common\models\Country;
use common\models\EventSubscriptions;
use common\models\Groups;
use common\models\InterestedCategories;
use common\models\Invoices;
use common\models\JobPosts;
use common\models\MemberCompanies;
use common\models\MemberContacts;
use common\models\MemberOffers;
use common\models\Members;
use common\models\NamedAssosiate;
use common\models\NewsletterSubscriptions;
use common\models\Payments;
use common\models\SearchExpertise;
use frontend\models\Additonal;
use frontend\models\ChangePassword;
use frontend\models\UpgradeForm;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AccountController implements the CRUD actions for Accounts model.
 */
class AccountController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @inheritgitdoc
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'view-invoice' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Accounts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MemberSearch();

        if (Yii::$app->request->get('account_type')) {
            $searchModel->account_type = Yii::$app->request->get('account_type');
            $searchModel->status = 1;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *  Search the Experties
     * Search from the members / Members Company
     *
     */
    public function actionSearchExperties()
    {

        $search = Yii::$app->request->get('search_request_experties');

        if (isset($search)) {

            $query = Members::find();

            $query->orWhere(['like', 'first_name', $search])
                ->orWhere(['like', 'last_name', $search])
                ->orWhere(['like', 'designation', $search])
                ->orWhere(['like', 'email', $search])
                ->orWhere(['like', 'first_name', strtolower($search)])
                ->orWhere(['like', 'last_name', strtolower($search)])
                ->orWhere(['like', 'designation', strtolower($search)])
                ->orWhere(['like', 'email', strtolower($search)])
                ->orWhere(['like', 'first_name', ucwords($search)])
                ->orWhere(['like', 'last_name', ucwords($search)])
                ->orWhere(['like', 'designation', ucwords($search)])
                ->orWhere(['like', 'email', ucwords($search)]);

            $query->join('LEFT JOIN', 'account_company ac', '	accounts.company = ac.id');
            $query->orWhere(['like', 'ac.name', strtolower($search)]);
            $query->orWhere(['like', 'ac.emirates_number', strtolower($search)]);
            $query->orWhere(['like', 'ac.address', strtolower($search)]);
            $query->orWhere(['like', 'ac.about_company', strtolower($search)]);
            $query->orWhere(['like', 'ac.name', ucwords($search)]);
            $query->orWhere(['like', 'ac.emirates_number', ucwords($search)]);
            $query->orWhere(['like', 'ac.address', ucwords($search)]);
            $query->orWhere(['like', 'ac.about_company', ucwords($search)]);
            $query->orWhere(['like', 'ac.name', $search]);
            $query->orWhere(['like', 'ac.emirates_number', $search]);
            $query->orWhere(['like', 'ac.address', $search]);
            $query->orWhere(['like', 'ac.about_company', $search]);


            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => 30
            ]);

            $dataProvider = $query->andWhere(['approved' => '1'])->andWhere(['!=', 'status', '0'])
                ->andWhere(['status' => '1'])->offset($pages->offset)
                ->limit($pages->limit)->orderBy(['ac.name' => SORT_ASC])
                ->all();

            return $this->render('search_experties', [
                'dataProvider' => $dataProvider,
                'pages' => $pages,
            ]);

        } else {
            return $this->redirect(['/']);
        }
    }

    /**
     * Displays a single Accounts model.
     * @param Integer $id id of user
     * @return mixed
     */
    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member = $this->findModel(Yii::$app->user->id);

        if ($member->login_first) {
            return $this->redirect(['/account/change-password']);
        }

        $categories = Categories::find()->where(['type' => 'general'])->all();
        $categories_selected = ArrayHelper::map(
            InterestedCategories::find()
                ->where(['member_id' => Yii::$app->user->id])->all(), 'category_id', 'id');

        return $this->render('myprofile', [
            'model' => $member,
            'interested_categories' => $categories,
            'categories_selected' => $categories_selected
        ]);
    }


    /**
     * Creates a new Accounts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Members();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['profile', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Accounts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($user = "")
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }
        $flag = false;
        $id = Yii::$app->user->id;

        if ($user) {

            $model = $this->findModel($user);

            if ($model == null) {
                if ($model->parent_id == Yii::$app->user->id || $model->company == Yii::$app->user->identity->company) {
                } else {
                    \Yii::$app->session->set("backToURL", Url::to(['/company/members']));
                    return $this->redirect(['/login']);
                }
            }

            $flag = true;

        } else {
            $model = $this->findModel($id);
        }

        $countries = ArrayHelper::map(Country::find()->all(), 'id', 'country_name');

        if (Yii::$app->request->post()) {

            $post = Yii::$app->request->post();
            $member_data = $post['Members'];
            $company_data = $post['Members']['companyData'];


            /*// $picture = UploadedFile::getInstance($model, 'picture');
            if ($picture <> null) {
                $picture_img = $model->actionUpload($picture);
                if(isset($picture_img['error'])){
                    \Yii::$app->session->setFlash('error', $picture_img['error']);
                    return $this->redirect('/account/update');
                }
                $model->picture = (isset($picture_img['url'])) ? $picture_img['url'] : "";
            }*/

            $model->title = $member_data['title'];
            $model->email = trim($member_data['email']);
            $model->secondry_email = $member_data['secondry_email'];
            $model->gender = $member_data['gender'];
            $model->phone_number = $member_data['phone_number'];
            $model->designation = $member_data['designation'];
            $model->nationality = $member_data['nationality'];
            $model->country_code = $member_data['country_code'];
            $model->city = $member_data['city'];
            $model->vat_number = $member_data['vat_number'];
            $model->address = $member_data['address'];
            $model->linkedin = $member_data['linkedin'];
            $model->twitter = $member_data['twitter'];
            $model->vat_number = $member_data['vat_number'];
            $model->picture = $member_data['picture'];

            if ($model->save()) {

                $company = AccountCompany::findOne($model->company);
                if ($company == null) {
                    $company = new AccountCompany();
                }

                $company->name = $company_data['name'];
                $company->emirates_number = $company_data['emirates_number'];
                $company->category = (isset($company_data['category'])) ? $company_data['category'] : "";
                $company->url = $company_data['url'];
                $company->phonenumber = $company_data['phonenumber'];
                $company->fax = $company_data['fax'];
                $company->postal_code = $company_data['postal_code'];
                $company->address = $company_data['address'];
                $company->vat_number = $company_data['vat_number'];
                $company->about_company = $company_data['about_company'];

                if(isset($company_data['logo']) && $company_data['logo'] <> null){
                    $company->logo = $company_data['logo'];
                }

                if (!$company->save()) {
                    \Yii::$app->session->setFlash('error', $company->getErrors());
                    return $this->redirect('/account/update');
                }

                /* echo '<pre>';
                 print_r($company->getErrors());
                 die;*/

            } else {
                \Yii::$app->session->setFlash('error', $model->getErrors());
                return $this->redirect('/account/update');
            }

            \Yii::$app->session->setFlash('success', "Great! Profile is updated");

            if ($flag) {
                return $this->redirect(['/account/update', 'user' => $model->id]);
            } else {
                return $this->redirect('/account/update');
            }

        } else {

            return $this->render('update', [
                'model' => $model,
                'country' => $countries
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionChangePassword()
    {
        $password = new ChangePassword();


        if ($password->load(Yii::$app->request->post()) && $password->validate()) {
            if (!$password->changeUserPassword()) {
                Yii::$app->getSession()->setFlash(
                    'error',
                    'Password not changed'
                );
            } else {
                Yii::$app->getSession()->setFlash(
                    'success',
                    'Password changed'
                );
            }
            $this->redirect(['/account/change-password/', 'id' => Yii::$app->user->id]);
        } else {
            return $this->render('change_password', [
                'model' => $this->findModel(Yii::$app->user->id),
                'password' => $password
            ]);
        }
    }

    /**
     * Deletes an existing Accounts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id , to delete the data against specific ID
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Accounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id to find something against the primary key
     * @return Accounts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Members::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id id
     * @return static
     * @throws NotFoundHttpException
     */
    public function actionPaymentHistory()
    {
        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);

        return $this->render('_payment_history', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),

        ]);

    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMemberContacts()
    {
        return $this->render('member-contact', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * @param string $search
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */

    public function actionMemberDirectory($search = "")
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member_directory_search = Yii::$app->request->get('md');
        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);

        if ($search) {

            if (strlen($search) == 1) {

                $query = Members::find()->where(['status' => '1'])
                    ->andWhere(['approved' => '1']);

                $query->andWhere(['!=', 'id', Yii::$app->user->id])
                    ->andWhere(['like', 'accounts.last_name', $search . '%', false])->orderBy([
                        'accounts.last_name' => SORT_ASC
                    ]);;
            } else {

                $searchR = array_filter(explode(' ', $search));

                // $query = Members::find()->where(['approved' => '1'])->andWhere(['!=', 'status', '0'])->andWhere(['!=', 'status', '4']);
                $query = Members::find()->where(['status' => '1'])->andWhere(['approved' => '1']);

                if (count($searchR) > 0) {
                    foreach ($searchR as $search) {

                        $query->andWhere(['!=', 'id', Yii::$app->user->id])
                            ->andWhere(['like', 'accounts.first_name', $search . '%', false])
                            ->orWhere(['like', 'accounts.last_name', strtolower($search . '%'), false])->orderBy([
                                'accounts.first_name' => SORT_ASC, 'accounts.last_name' => SORT_ASC
                            ]);
                    }
                }
            }

        } else {
            
            $query = Members::find()->where(['status' => 1])->andWhere(['approved' => '1'])
                ->andWhere(['!=', 'id', Yii::$app->user->id])->orderBy([
                    'accounts.first_name' => SORT_ASC,
                    'accounts.last_name' => SORT_ASC
                ]);
        }

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $members = $query->offset($pages->offset)
            ->limit($pages->limit)->all();

        return $this->render('members-directory', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),
            'members' => $members,
            'pages' => $pages,
        ]);

    }

    /**
     * @param string $company
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCompanyMembers($company = "")
    {
        $payments = [];

        if (!Yii::$app->user->isGuest) {
            $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);
        }

        $members = null;
        $pages = null;
        $companyInfo = AccountCompany::findOne($company);
        $showComapnyInfo = true;

        if ($company) {

            $query = Members::find()->where(['company' => $company])->andWhere(['!=', 'status', '0'])->andWhere(['status' => '1'])->orderBy([
                'first_name' => SORT_ASC,
                'last_name' => SORT_ASC
            ]);

            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => 20
            ]);

            $members = $query->offset($pages->offset)
                ->limit($pages->limit)->orderBy([
                    'first_name' => SORT_ASC,
                    'last_name' => SORT_ASC
                ])
                ->all();

        }

        return $this->render('company-members', [
            'payments' => $payments,
            'model' => (!Yii::$app->user->isGuest) ? $this->findModel(Yii::$app->user->id) : null,
            'members' => $members,
            'pages' => $pages,
            'companyInfo' => $companyInfo,
            'showComapnyInfo' => $showComapnyInfo
        ]);
    }

    /**
     * @param string $search
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCompanyDirectory($category = "", $search = "")
    {
        if ($search || $category) {

            $query = AccountCompany::find();


            $query->where(['like', 'account_company.name', $search . '%', false]);
                $query->orWhere(['like', 'account_company.name', $search . '%', false])->orderBy([
                    'account_company.name' => SORT_ASC
                ]);
            $query->join('LEFT JOIN', 'accounts ac', '	account_company.id = ac.company');
            $query->andWhere(['!=', 'ac.status', '0']);
            $query->andWhere(['!=', 'ac.status', '4']);

            if ($category) {
                $query->andWhere(['account_company.category' => $category]);
            }

        } else {

            $query = AccountCompany::find();

            $query->join('LEFT JOIN', 'accounts ac', '	account_company.id = ac.company');
            $query->andWhere(['!=', 'ac.status', '0']);
            $query->andWhere(['!=', 'ac.status', '4']);
            $query->orderBy([
                'account_company.name' => SORT_ASC
            ]);
        }

        $query->andWhere(['account_company.show_in_directory' => '1']);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 30
        ]);

        $companies = $query->offset($pages->offset)
            ->limit($pages->limit)->andWhere(['is_active' => '1'])->orderBy(['name' => SORT_ASC])
            ->all();

        return $this->render('company-directory', [
            'payments' => (!Yii::$app->user->isGuest) ? Payments::find()->where(['user_id' => Yii::$app->user->id]) : null,
            'model' => (!Yii::$app->user->isGuest) ? $this->findModel(Yii::$app->user->id) : null,
            'companies' => $companies,
            'pages' => $pages,
        ]);

    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMemberOffers()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member_offers_data = MemberOffers::find()->where(['is_active' => '1'])->orderBy(['sort_order' => SORT_ASC] )->all();
        return $this->render('members-offer', [
            'member_offers_data' => $member_offers_data,
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionMemberOffersDetail($id)
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member_offer_detail = MemberOffers::findOne($id);
        return $this->render('member_offer_detail', [
            'member_offer_detail' => $member_offer_detail
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPayments()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = Payments::find()->where([
            'user_id' => Yii::$app->user->id,
        ]);


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $payments = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('payment-history', [
            'model' => $this->findModel(Yii::$app->user->id),
            'payments' => $payments,
            'pages' => $pages
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMembershipUpgrade()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->user->identity->group_id <> 2) {
            $this->redirect('/account/profile');
        }

        $membership_types = Groups::findOne(1);

        return $this->render('upgrade', [
            'model' => $this->findModel(Yii::$app->user->id),
            'type' => $membership_types
        ]);

    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionUpgrade()
    {

        $model = new UpgradeForm();
        $member = Members::findOne(Yii::$app->user->id);

        $model->first_name = $member->first_name;
        $model->last_name = $member->last_name;
        $model->email = $member->email;
        $model->secondry_email = $member->secondry_email;
        $model->country_code = $member->country;
        $model->city = $member->city;
        $model->address = $member->address;
        $model->phone_number = $member->phone_number;
        $model->designation = $member->designation;
        $model->nationality = $member->nationality;
        $model->title = $member->title;
        $model->upgrade = true;

        $errors = [];

        if (Yii::$app->request->get()) {
            $model->group_id = Yii::$app->request->get('membership');
            $model->account_type = 'member';
        }

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->user->identity->group_id <> 2) {
            // $this->redirect('/account/profile');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $licence = UploadedFile::getInstance($model, 'trade_licence');
            $visa = UploadedFile::getInstance($model, 'residence_visa');
            $passport = UploadedFile::getInstance($model, 'passport_copy');
            $pic = UploadedFile::getInstance($model, 'passport_size_pic');

            if ($licence <> null) {
                $trade_licence = $model->actionUpload($licence);
                $model->documents['trade_licence'] = $trade_licence['url'];
            }

            if ($visa <> null) {
                $residence_visa = $model->actionUpload($visa);
                $model->documents['residence_visa'] = $residence_visa['url'];
            }

            if ($passport <> null) {
                $passport_copy = $model->actionUpload($passport);
                $model->documents['passport_copy'] = $passport_copy['url'];
            }

            if ($pic <> null) {
                $passport_size_pic = $model->actionUpload($pic);
                $model->documents['passport_size_pic'] = $passport_size_pic['url'];
            }


            if ($user = $model->upgrade()) {
                \Yii::$app->session->setFlash('success', 'Your application for Business membership upgrade to the British Business Group is received with thanks. Check your email for further instructions.');
                return $this->redirect(['/login']);
            }

        } else {
            $errors[] = $model->getErrors();
        }

        return $this->render('/account/upgrade-form', [
            'model' => $model,
            'errors' => $errors
        ]);

    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddMemberContact()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->request->post()) {

            if (Yii::$app->request->post('type') == "add") {

                $model = new MemberContacts();
                $model->member_id = Yii::$app->user->id;
                $model->contact_id = Yii::$app->request->post('id');
                $model->date_added = date("Y-m-d");

                if ($model->save()) {
                    echo 1;
                } else {
                    print_r($model->getErrors());
                }
            } else {
                if (MemberContacts::deleteAll([
                    'contact_id' => Yii::$app->request->post('id'),
                    'member_id' => Yii::$app->user->id,
                ])) {
                    echo 1;
                }
            }
        }
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddMemberCompany()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->request->post()) {

            if (Yii::$app->request->post('type') == "add") {

                $model = new MemberCompanies();
                $model->member_id = Yii::$app->user->id;
                $model->member_company = Yii::$app->request->post('id');
                $model->date_added = date("Y-m-d");

                if ($model->save()) {
                    echo 1;
                } else {
                    print_r($model->getErrors());
                }
            } else {
                if (MemberCompanies::deleteAll([
                    'member_company' => Yii::$app->request->post('id'),
                    'member_id' => Yii::$app->user->id,
                ])) {
                    echo 1;
                }
            }
        }
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMemberDetails($id, $type = "")
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if ($type) {

            $search = SearchExpertise::findOne([
                'search_keyword' => $type,
                'user_viewed' => $id,
                'search_by' => Yii::$app->user->id
            ]);


            if ($search == null) {
                $search = new SearchExpertise();
            }

            $search->search_keyword = $type;
            $search->user_viewed = $id;
            $search->search_by = Yii::$app->user->id;
            $search->save();
        }

        $user = Members::find()->where(['id' => $id])
            ->andWhere(['status' => '1'])->one();

        return $this->render('member-details', [
            'model' => $this->findModel(Yii::$app->user->id),
            'member' => $user
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEventsRegistered()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = EventSubscriptions::find()->where([
            'user_id' => Yii::$app->user->id,
        ])->orWhere(['registered_by' => Yii::$app->user->id]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $events = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('events_registered', [
            'model' => $this->findModel(Yii::$app->user->id),
            'events' => $events,
            'pages' => $pages
        ]);

    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionInvoices()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = Invoices::find()->where([
            'user_id' => Yii::$app->user->id,
            /*'payment_status' => 'unpaid'*/
        ]);


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $invoices = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['invoice_id' => SORT_DESC])
            ->all();

        return $this->render('invoices', [
            'model' => $this->findModel(Yii::$app->user->id),
            'invoices' => $invoices,
            'pages' => $pages
        ]);
    }

    /**
     * @param $invoice
     * @return \yii\web\Response
     */
    public function actionViewInvoice($invoice)
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member = Members::findOne(Yii::$app->user->id);
        $invoices = Invoices::findOne($invoice);

        if ($invoices <> null) {

            $pdf = \Yii::$app->pdf;
            $mpdf = $pdf->api;
            $mpdf->format = Pdf::FORMAT_A4;
            $mpdf->orientation = Pdf::ORIENT_PORTRAIT;

            if ($invoices->invoice_related_to == "event") {

                $content = $this->renderPartial('/account/send_event_invoice', [
                    'member' => $member,
                    'invoice' => $invoices
                ]);

            } else {

                $content = $this->renderPartial('/account/send_invoice', [
                    'member' => $member,
                    'invoice' => $invoices
                ]);
            }

            $mpdf->WriteHtml($content);
            return $mpdf->Output('Invoice', 'I');
        }

        \Yii::$app->session->setFlash('error', 'Invoice not available!');
        return $this->redirect('/account/profile');
    }

    /**
     * @return \yii\web\Response
     */
    public function actionRenewMembership()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member = Members::findOne(Yii::$app->user->id);

        if ($member->invoice_renewal > 0) {
            return $this->redirect('/site/invoice-payment?invoice_id=' . $member->invoice_renewal);
        }

        $invoice_data = new Members();
        $data = $invoice_data->createRenewalInvoice($member);

        if ($data <> null && count($data) > 0) {
            return $this->redirect('/site/invoice-payment?invoice_id=' . $data['invoice_num']);
        } else {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/profile']);
        }

    }

    /**
     * @return bool|\yii\web\Response
     */
    public function actionSaveIntrestedCategories()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->request->post()) {

            $categories = Yii::$app->request->post('interested_category');

            if (count($categories) > 0) {

                InterestedCategories::deleteAll(['member_id' => Yii::$app->user->id]);

                foreach ($categories as $value) {

                    $cat = new InterestedCategories();
                    $cat->member_id = Yii::$app->user->id;
                    $cat->category_id = $value;
                    $cat->save();

                }
            }

            return false;
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionExpertRequests()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = SearchExpertise::find()->where([
            'user_viewed' => Yii::$app->user->id
        ]);


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $requests = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['date' => SORT_DESC])
            ->all();

        return $this->render('expert_requesst', [
            'model' => $this->findModel(Yii::$app->user->id),
            'requests' => $requests,
            'pages' => $pages
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMyCompanyMembers()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $company = Yii::$app->user->identity->company;
        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);
        $companyInfo = AccountCompany::findOne($company);
        $showComapnyInfo = false;
        $members = null;
        $pages = null;

        if ($company) {


            $query = Members::find()->where(['approved' => '1', 'company' => $company])->andWhere(['!=', 'status', '0'])
                ->andWhere(['status' => '1'])->andWhere(['!=', 'id', Yii::$app->user->id]);

            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => 20
            ]);

            $members = $query->offset($pages->offset)
                ->limit($pages->limit)->orderBy(['first_name' => SORT_ASC])
                ->all();
        }

        return $this->render('company-members', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),
            'members' => $members,
            'pages' => $pages,
            'companyInfo' => $companyInfo,
            'showComapnyInfo' => $showComapnyInfo
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionAddAdditional()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }
        $members = new Members();

        $errors = [];

        $additional = new Additonal();

        $additional->group_id = Yii::$app->user->identity->group_id;

        $additional->companyData = null;
        $additional->companyInfo = null;

        if ($additional->load(Yii::$app->request->post()) && $additional->validate()) {


            if ($user = $additional->createAdditional()) {
                \Yii::$app->session->setFlash('success', 'Additional Member is added..!');
                return $this->redirect(['/company/members']);
            }

        } else {
            $errors[] = $additional->getErrors();
        }

        return $this->render('additional_form', [
            'model' => $this->findModel(Yii::$app->user->id),
            'additional' => $additional,
            'errors' => $errors
        ]);

    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionAssociates()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $model = $this->findModel(Yii::$app->user->id);

        if ($model->group <> null && !$model->group->add_associate) {

            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = NamedAssosiate::find()->where(['parent_user_id' => Yii::$app->user->id]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $members = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['assosiate_id' => SORT_DESC])
            ->all();


        return $this->render('associates_list', [
            'model' => $model,
            'associates' => $members,
            'pages' => $pages
        ]);

    }

    /**
     * @param string $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionAssociateMember($id = "")
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $model = $this->findModel(Yii::$app->user->id);

        if ($model->group <> null && !$model->group->add_associate) {

            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if ($id) {
            $member = NamedAssosiate::findOne($id);
        } else {
            $member = new NamedAssosiate();
            $member->parent_user_id = Yii::$app->user->id;
        }


        if ($member->load(Yii::$app->request->post()) && $member->save()) {
            $this->redirect('/account/associates');
        }

        return $this->render('associate_form', [
            'model' => $model,
            'associate' => $member,
        ]);

    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEditEmailSubscriptions()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->setFlash('login', 'Please log in to update email subscriptions page');
            \Yii::$app->session->set("backToURL", Url::to(['/account/edit-email-subscriptions']));
            return $this->redirect(['/login']);
        }

        $model = $this->findModel(Yii::$app->user->id);
        $subscriptions = NewsletterSubscriptions::find()->where(['email' => $model->email])->one();

        if ($subscriptions == null) {
            $subscriptions = new NewsletterSubscriptions();
            $subscriptions->email = $model->email;
        }

        if ($subscriptions->load(Yii::$app->request->post()) && $subscriptions->save()) {

            \Yii::$app->session->setFlash('success', 'Great! Subscription updated.');
            $this->redirect(['/account/edit-email-subscriptions?msg=success']);
            //$this->redirect('/account/profile');
        }

        return $this->render('_edit_email_subscriptions', [
            'model' => $model,
            'subscriptions' => $subscriptions,
        ]);
    }


    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionJobsPortal()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->setFlash('login', 'Please log in to view jobs portal page');
            \Yii::$app->session->set("backToURL", Url::to(['/account/jobs-portal']));
            return $this->redirect(['/login']);
        }

        $query = JobPosts::find()->where(['user_id' => Yii::$app->user->id]);


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $jobs = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['posted_on' => SORT_DESC])
            ->all();

        return $this->render('jobs-posted', [
            'model' => $this->findModel(Yii::$app->user->id),
            'jobs' => $jobs,
            'pages' => $pages
        ]);
    }


    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPostJobs($id = "")
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->setFlash('login', 'Please log in to view jobs portal page');
            \Yii::$app->session->set("backToURL", Url::to(['/account/post-jobs']));
            return $this->redirect(['/login']);
        }

        if ($id) {
            $job = JobPosts::find()->where(['user_id' => Yii::$app->user->id, 'id' => $id])->one();
        } else {
            $job = new JobPosts();
        }

        $job->user_id = Yii::$app->user->id;
        $job->user_type = "member";

        if ($job->load(Yii::$app->request->post()) && $job->validate()) {

            if($job->save()){
                return $this->redirect(['/account/jobs-portal']);
            }

        }

        return $this->render('add_edit_job', [
            'model' => $this->findModel(Yii::$app->user->id),
            'jobModel' => $job
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteJob($id = "")
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->setFlash('login', 'Please log in to view jobs portal page');
            \Yii::$app->session->set("backToURL", Url::to(['/account/post-jobs']));
            return $this->redirect(['/login']);
        }

        if ($id) {
            JobPosts::deleteAll(['id' => $id]);
            return $this->redirect(['/account/jobs-portal']);
        }

    }
}
