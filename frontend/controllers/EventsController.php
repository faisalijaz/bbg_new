<?php

namespace frontend\controllers;

use common\helpers\EmailHelper;
use common\helpers\GoogleCalendarApi;
use common\helpers\ICS;
use common\models\Events;
use common\models\EventSubscriptions;
use common\models\GoogleEventsSync;
use common\models\Invoices;
use common\models\Members;
use common\models\Payments;
use google\appengine\api;
use Yii;
use yii\base\Exception;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * Class EventsController
 * @package frontend\controllers
 */
class EventsController extends \yii\web\Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex($type = "")
    {
        $isPast = false;
        $months = [];
        $selectedMonth = "";


        if ($type == 'past') {

            $title = 'Past Events';
            $year_start = 2015;
            $months = [];

            $year = date('Y');
            $current_month = strtotime(date('F Y'));

            $months_array = [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ];

            for($i = $year_start; $i <= $year; $i++){
                foreach ($months_array as $month){
                    $search_year = $month ." ". $i;
                    if(strtotime($search_year) <= $current_month) {
                        $months[$search_year] = $search_year;
                    }
                }
            }

            /*for ($i = 0; $i <= $m; $i++) {
                $date = date('F Y', strtotime("+". $i ." month", strtotime($date)));
                $months[$date] = $date;
            }*/
            $isPast = true;

            if(Yii::$app->request->post()){

                $selectedMonth = Yii::$app->request->post('past_event_month');

                $monthStart = date("Y-m-01", strtotime($selectedMonth));
                $monthEnd = date("Y-m-t", strtotime($selectedMonth));

                $query = Events::find()->where([
                    '>=', 'event_startDate', $monthStart
                ])->andWhere([
                    '<=', 'event_endDate', $monthEnd
                ])->andWhere(['active' => '1']);


            }else{
                $query = Events::find()->where(['<=', 'event_startDate', date('Y-m-d')])->andWhere(['active' => '1']);

            }


        } else {
            $title = 'Upcoming Events';
            $query = Events::find()->where(['>=', 'event_startDate', date('Y-m-d')])->andWhere(['active' => '1']);
        }

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        if ($isPast) {

            $events = $query->offset($pages->offset)
                ->limit($pages->limit)->orderBy([
                    'event_startDate' => SORT_DESC
                ])->all();
        } else {

            $events = $query->offset($pages->offset)
                ->limit($pages->limit)->orderBy([
                    'event_startDate' => SORT_ASC
                ])->all();
        }

        return $this->render('index', [
            'events' => $events,
            'pages' => $pages,
            'title' => $title,
            'isPast' => $isPast,
            'months' => array_reverse($months),
            'selectedMonth' => $selectedMonth
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEventDetails($id)
    {
        $event = Events::findOne($id);
        $reg_num = EventSubscriptions::find()->where(['event_id' => $id])->count();
        $reglist = null;
        Yii::$app->session->set('google_calender_event_sync', $id);

        if($event <> null){
            Yii::$app->session->set('opengraph_title', $event->title);
            Yii::$app->session->set('opengraph_url', Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event->id);
            Yii::$app->session->set('opengraph_image', (empty($event->image)) ? Yii::$app->params['no_image'] : $event->image);
            Yii::$app->session->set('opengraph_description', $event->short_description);
        }


        if (!Yii::$app->user->isGuest) {
            $reglist = EventSubscriptions::find()->where(['event_id' => $id])->all();
        }

        return $this->render('event_detail', [
            'event' => $event,
            'reg_num' => $reg_num,
            'registered_list' => $reglist
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEventRegister($id, $s = "")
    {
        \Yii::$app->session->set("backToEventRegisterURL", null);

        $event = Events::findOne($id);

        if (Yii::$app->user->isGuest) {

            if (!$event->members_only) {
                return $this->redirect(['/events/event-guest-register', 'id' => $id]);
            }

            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id, 'members_only' => $event->members_only]));
            return $this->redirect(['/login']);
        }

        if ($event->registration_start > date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is not opend yet. Please wait for Registeration to open!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        if ($event->registration_end < date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is Closed!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        $event_subscriptions = EventSubscriptions::findAll([
            'registered_by' => Yii::$app->user->identity->id,
            'event_id' => $id
        ]);


        $member_contacts = Members::find()
            ->where(['parent_id' => Yii::$app->user->identity->id])
            ->orWhere(['company' => Yii::$app->user->identity->company])
            ->andWhere(['status' => '1'])->andWhere(['!=', 'id', Yii::$app->user->identity->id])->all();


        if (\Yii::$app->request->post()) {
            $this->eventRegisterForm(Yii::$app->request->post());
        }

        return $this->render('register_event', [
            'event' => $event,
            'contacts' => $member_contacts,
            'event_subscriptions' => $event_subscriptions,
            'step' => $s,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionEventGuestRegister($id)
    {
        $event = Events::findOne($id);

        if($event <> null) {
            if (Yii::$app->user->isGuest && $event->members_only) {
                \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id, 'members_only' => $event->members_only]));
                return $this->redirect(['/login']);
            }
        }

        if (Yii::$app->request->post()) {

            $model = new EventSubscriptions();
            $invoice = $model->memberEventRegisteration(Yii::$app->request->post());

            if ($invoice) {
                $this->redirect('/site/invoice-payment?invoice_id=' . $invoice);
            }
        }

        return $this->render('register_guest', [
            'event' => $event,
        ]);

    }

    /**
     *
     */
    public function actionInformAboutEvent()
    {
        if (Yii::$app->request->post()) {

            $user = Yii::$app->user->identity;
            $members = Yii::$app->request->post('members');
            $event = Events::findOne(Yii::$app->request->post('event'));

            if (count($members) > 0) {

                foreach ($members as $member) {

                    $member = Members::findOne($member);
                    $toName = $member->first_name . " " .$member->last_name;
                    $fromName = $user->first_name . " " .$user->last_name;

                    $subject = "BBG event invitation for " . $event->title;

                    try {
                        // echo $user_email;
                        if ((new EmailHelper())->sendEmail(trim($member->email), [], $subject, 'events/event-registration', [
                            'event' => $event,
                            'user' => $member->email,
                            'toName' => $toName,
                            'fromName' => $fromName
                        ])) {
                            echo 1;
                        } else{
                            echo 0;
                        }

                    } catch (Exception $e) {
                        echo false;
                    }
                }
            }
        }
    }

    /**
     * @param $data
     * @return array|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function eventRegisterForm($data)
    {

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (count($data) > 0) {

            $subscribe = new EventSubscriptions();
            $resp = $subscribe->memberEventRegisteration($data);

            if (isset($resp['msg'])) {
                print_r(\GuzzleHttp\json_encode($resp));
                echo " | ";
                // $this->redirect('/events/event-register?id=' . $event . '&s=2');
            } else {
                return $subscribe->getErrors();
            }
        }
    }

    /**
     * @return bool
     */
    public function actionSubscriptionCancelRequest()
    {

        if (Yii::$app->request->post()) {
            $subscription = EventSubscriptions::findOne(Yii::$app->request->post('subscription'));
            $subscription->cancel_request = Yii::$app->request->post('cancel');
            if (!$subscription->save()) {
                print_r($subscription->getErrors());
                return false;
            }
            echo 1;
        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionPayment($id = "")
    {
        if ($id) {
            \Yii::$app->session->set('payInvoiceId', $id);
        } else {
            if (Yii::$app->session->get('payInvoiceId')) {
                $id = \Yii::$app->session->get('payInvoiceId');
            } else {
                Yii::$app->session->setFlash('error', "Invoice number does not exists");
                return $this->goHome();
            }
        }

        $year = [];

        $y = date('y');
        $Y = date('Y');

        for ($i = 0; $i < 30; $i++) {

            if ($i > 0) {
                $y = $y + 1;
                $Y = $Y + 1;
            }

            $year[$y] = $Y;
        }

        $event = Events::findOne($id);

        \Yii::$app->session->set("backToEventRegisterURL", null);

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/payment', 'id' => $id]));
            return $this->redirect(['/login']);
        }

        $event_subscriptions = EventSubscriptions::findAll([
            'registered_by' => Yii::$app->user->identity->id,
            'event_id' => $id,
            'payment_status' => 'pending'
        ]);


        // In case someone else registered the member then find that registration
        if ($event_subscriptions == null) {

            $event_subscriptions = EventSubscriptions::findAll([
                'user_id' => Yii::$app->user->identity->id,
                'event_id' => $id,
                'payment_status' => 'pending'
            ]);
        }


        return $this->render('event-payment', [
            'event' => $event,
            'event_subscriptions' => $event_subscriptions,
            'years' => $year
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionThankYou($id = "")
    {
        \Yii::$app->session->set("backToEventRegisterURL", null);

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id]));
            return $this->redirect(['/login']);
        }

        $response = [];
        $event = Events::findOne($id);

        if(Yii::$app->request->post()){

            $response = \Yii::$app->request->post();

            if ($response['response_code'] != "100") {
                Yii::$app->session->setFlash('error', "Payment processing error");
            }
        }

        $member_contacts = Members::findAll(['parent_id' => Yii::$app->user->identity->id]);

        return $this->render('thankyou', [
            'event' => $event,
            'contacts' => $member_contacts,
            'response' => $response
        ]);
    }

    /**
     *
     */
    public function actionVerifyUser()
    {

        if (Yii::$app->request->get('code')) {

            $event_id = Yii::$app->session->get('google_calender_event_sync');

            try {

                $client_id = Yii::$app->params['gcal_client_id'];
                $app_secret = Yii::$app->params['gcal_client_secret'];
                $redirect_url = Yii::$app->params['appUrl'] . Yii::$app->params['gcal_redirect_url'];
                $calendar_id = "primary";

                $capi = new GoogleCalendarApi();

                $auth_data = $capi->GetAccessToken($client_id, $redirect_url, $app_secret, Yii::$app->request->get('code'));

                echo '<pre>';
                print_r($auth_data);
                echo '</pre>';

                if (isset($auth_data['error'])) {
                    Yii::$app->session->setFlash('error', $auth_data['error_description']);
                    $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                }

                if (isset($auth_data['access_token'])) {

                    $access_token = $auth_data['access_token'];
                    $user_timezone = $capi->GetUserCalendarTimezone($access_token);

                    if (isset($user_timezone['error'])) {
                        Yii::$app->session->setFlash('error', $user_timezone['error_description']);
                        $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                    }
                    echo '<pre>';
                    print_r($user_timezone);
                    echo '</pre>';

                    $calender_list = $capi->GetCalendarsList($access_token);
                    if (isset($calender_list['error'])) {
                        Yii::$app->session->setFlash('error', $calender_list['error_description']);
                        $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                    }
                    echo '<pre>';
                    print_r($calender_list);
                    echo '</pre>';

                    if ($calender_list <> null) {
                        $calendar_id = (isset($calender_list[0])) ? $calender_list[0]['id'] : "primary";
                    }

                    if (Yii::$app->session->get('google_calender_event_sync')) {

                        $event = Events::findOne($event_id);

                        if ($event <> null) {

                            $event_title = $event->title;
                            $start_time = date('Y-m-d', strtotime($event->event_startDate)) . "T" . date('H:i:s', strtotime($event->event_startTime));
                            $end_time = date('Y-m-d', strtotime($event->event_endDate)) . "T" . date('H:i:s', strtotime($event->event_endTime));
                            $full_day_event = 0;
                            $event_time = ['start_time' => $start_time, 'end_time' => $end_time];

                            // Create event on primary calendar
                            $EventSync = $capi->CreateCalendarEvent($calendar_id, $event_title, $full_day_event, $event_time, $user_timezone, $access_token);
                            if (isset($EventSync['error'])) {
                                Yii::$app->session->setFlash('error', $EventSync['error_description']);
                                $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                            }

                            if ($EventSync <> null) {

                                if (!Yii::$app->user->isGuest) {

                                    $google_id = 0;

                                    if (isset($EventSync['id'])) {
                                        $google_id = $EventSync['id'];
                                    }

                                    $googleSync = new GoogleEventsSync();
                                    $googleSync->event_id = $event_id;
                                    $googleSync->user_id = Yii::$app->user->id;
                                    $googleSync->google_event_id = (string) $google_id;

                                    if (!$googleSync->save()) {

                                        Yii::$app->session->setFlash('error', $googleSync->getErrors());
                                        $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                                    }

                                    Yii::$app->session->setFlash('success', "Event has been created in google calender!");
                                    $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);

                                } else {

                                    Yii::$app->session->setFlash('success', "Event has been created in google calender!");
                                    $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                                }
                            }

                        } else {

                            if (isset($calender_list['error'])) {
                                Yii::$app->session->setFlash('error', "Event not found!");
                                $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                            }

                        }
                    }
                }

            } catch (Exception $e) {

                if (isset($calender_list['error'])) {
                    Yii::$app->session->setFlash('error', $e->getMessage());
                    $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                }
            }
        }
    }

    /**
     *
     */
    public function actionSyncToCalendar()
    {
        $event_id = Yii::$app->session->get('google_calender_event_sync');

        if (Yii::$app->request->post()) {

            $siteSetting = null;

            if (count((array)\Yii::$app->appSettings->getSettings()) > 0) {
                $siteSetting = \Yii::$app->appSettings->getSettings();
            }

            $id = Yii::$app->request->post('event_id');

            $event = Events::findOne($id);
            $to = Yii::$app->request->post('email');

            $organizer = "mailto:" . (isset($siteSetting['admin_email'])) ? $siteSetting['admin_email'] : "";
            $location = ($event <> null) ? $event->venue : '';

            $date = gmdate('Ymd') . 'T' . gmdate('His') . "Z";

            if (strpos($to, 'gmail') != false || strpos($to, 'outlook') != false) {
                $startTime = date('Y-m-d', strtotime($event->event_startDate)) . "T" . date('H:i:s', strtotime($event->event_startTime));
                $endTime = date('Y-m-d', strtotime($event->event_endDate)) . "T" . date('H:i:s', strtotime($event->event_endTime));

            } else {
                $startTime = $event->event_startDate . " " . $event->event_startTime;
                $endTime = $event->event_endDate . " " . $event->event_endTime;
            }

            $subject = ($event <> null) ? $event->title : '';
            $desc = ($event <> null) ? $event->short_description : '';
            $url = Yii::$app->params['appUrl'] . '/events/event-details?id=' . $id;

            $properties = array(
                'description' => $desc,
                'location' => $location,
                'summary' => $subject,
                'dtstart' => $startTime,
                'dtend' => $endTime,
                'url' => $url,
                'organizer' => $organizer,
                'dtstamp' => $date
            );


            $message = "Please add " . $subject . " to your calendar";


            $ics = new ICS($properties);
            $ics_file_contents = $ics->to_string();

            if((new EmailHelper())->sendIcalInvote($to, [], $subject, $message, $ics_file_contents)){

                Yii::$app->session->setFlash('success','Invite Sent Successfully');
                $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
                
            } else {

                Yii::$app->session->setFlash('error', 'Invite Sent error');
                $this->redirect(Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event_id);
            }
        } else {

            return $this->render('add_to_calendar', [
                'event' => $event_id,
            ]);
        }



    }


   /* public function actionThankYouOld($id = "")
    {
        $member_fee = 0;
        $userData = [];

        \Yii::$app->session->set("backToEventRegisterURL", null);

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id]));
            return $this->redirect(['/login']);
        }

        $response = [];
        $event = Events::findOne($id);

        $isArray = false;

        if(Yii::$app->request->post()){

            $response = \Yii::$app->request->post();
            $model = new Payments();
            $payment = $model->paytabs_make_payment($response);

            if ($payment <> null && count($payment) > 0) {

                if (isset($payment['status']) && $payment['status'] == 'succes') {

                    $payment_id = $payment['id'];

                    $invoices = \Yii::$app->request->post('order_id');

                    if (strpos($invoices, '|') !== false) {

                        $invoices = array_filter(explode('|', $invoices));

                        if (is_array($invoices) && count($invoices) > 0) {

                            foreach ($invoices as $inv_id) {

                                $invoice = Invoices::findOne(['invoice_id' => $inv_id]);

                                if ($invoice <> null) {
                                    if ($invoice->invoice_related_to == 'event') {
                                        $event = Events::findOne($invoice->invoice_rel_id);
                                    }
                                }

                                $model->updateInvoicePaid($response, $invoice, $payment_id);
                            }
                        }

                    } else {

                        $invoice = Invoices::findOne(['invoice_id' => $invoices]);

                        if ($invoice <> null) {
                            if ($invoice->invoice_related_to == 'event') {
                                $event = Events::findOne($invoice->invoice_rel_id);
                            }
                        }

                        $invoice_update = $model->updateInvoicePaid($response, $invoice, $payment_id);
                    }

                } else {
                    Yii::$app->session->setFlash('error', "Payment processing error");
                }
            } else {
                Yii::$app->session->setFlash('error', "Payment processing error");
            }
        }

        $member_contacts = Members::findAll(['parent_id' => Yii::$app->user->identity->id]);

        return $this->render('thankyou', [
            'event' => $event,
            'contacts' => $member_contacts,
            'response' => $response
        ]);
    }*/

}