<?php

namespace frontend\controllers;

use common\helpers\EmailHelper;
use common\models\AccountLoginForm;
use common\models\CmsPages;
use common\models\CommitteMembers;
use common\models\Events;
use common\models\Gallery;
use common\models\Groups;
use common\models\Invoices;
use common\models\JobPosts;
use common\models\Members;
use common\models\News;
use common\models\NewsletterSubscriptions;
use common\models\Payments;
use common\models\StaffCategories;
use common\models\VerifyAccount;
use common\models\ViewFromChair;
use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\InvalidParamException;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $search = Yii::$app->request->get('search');

        if(isset($search)){

            $search = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $search));

            $query_events = "SELECT id,title,short_description FROM `events` where `title` LIKE '%$search%' ";
            $search_events = Events::findBySql($query_events)->all();

            $query_news = "SELECT id,title,short_description FROM `news` where `title` LIKE '%$search%' ";
            $search_news = News::findBySql($query_news)->all();

            return $this->render('search', [
                'search_events' => $search_events,
                'search_news' => $search_news,
            ]);

        } else {

            // $chairmna_meassage = CmsPages::findOne(['seo_url' => 'chairman-message']);
            $chairmna_meassage = ViewFromChair::find()->Where(['display_on_home' => '1'])->orderBy([
                'date' => SORT_DESC
            ])->limit(1)->one();

            return $this->render('index', [
                'events',
                'chairmna_meassage' => $chairmna_meassage,
            ]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new AccountLoginForm();
        $guest = false;

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if (\Yii::$app->session->get('backToURL') <> null) {

                $url = \Yii::$app->session->get('backToURL');
                \Yii::$app->session->set("backToURL", null);

                return $this->goBack($url);
            }

            if (\Yii::$app->session->get('backToEventRegisterURL') <> null) {

                $url = \Yii::$app->session->get('backToEventRegisterURL');
                \Yii::$app->session->set("backToEventRegisterURL", null);

                return $this->goBack($url);
            }
            echo 1;
            return $this->redirect(['/account/profile']);

        } else {

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionAjaxLogin()
    {
        $model = new AccountLoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $url = Url::to('/account/profile', true);
            // return $this->redirect($url);

            echo Json::encode((['msg' => "success", 'url' => $url]));

        } else {

            $login_error = "";
            $errors = $model->getErrors();

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    if (is_array($error) && count($error) > 0) {
                        foreach ($error as $err) {
                            $login_error .= "<span class='alert alert-danger'>" . $err . "</span>";
                        }
                    } else {
                        $login_error .= "<span class='alert alert-danger'>" . $error . "</span>";
                    }
                }
            }

            echo Json::encode(['msg' => "error", 'text' => $login_error]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        $banner = CmsPages::find()->where(['seo_url'=>'contact-us'])->one();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
                'banner' => $banner,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionCommitteeInfo()
    {
        $staffCategories = StaffCategories::find()->where(['status' => '1'])->andWhere(['!=','id' , '3'])->orderBy([
            'sort_order' => SORT_ASC
        ])->all();



        return $this->render('committee-info', [
            'staffCategories' => $staffCategories,
            // 'staff' => $staff
        ]);
    }

    /**
     * @return string
     */
    public function actionCommitteeDetails($id)
    {
        $committee = CommitteMembers::find()->where(['id' => $id])->andWhere(['status' => '1'])->orderBy([
            'sort_order' => SORT_ASC
        ])->one();

        return $this->render('committee_staff_detail', [
            'data' => $committee
        ]);
    }

    /**
     * @return string
     */
    public function actionOperationsInfo()
    {
        $staff = CommitteMembers::find()->where(['type' => '3', 'status' => '1'])->orderBy([
            'sort_order' => SORT_ASC
        ])->all();

        return $this->render('operations-team-info', [
            'staff' => $staff
        ]);

    }


    /**
     * @return string
     */
    public function actionOperationsTeamDetails($id)
    {
        $staff = CommitteMembers::find()->where(['id' => $id])->andWhere(['type' => 'office_staff', 'status' => '1'])->orderBy([
            'sort_order' => SORT_ASC
        ])->one();

        return $this->render('committee_staff_detail', [
            'data' => $staff
        ]);

    }



    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $model = $this->findModel('seo_url', 'faq');
        return $this->render('about', [
            'model' => $model
        ]);
    }


    public function actionApplyMembership()
    {

        $membership_types = Groups::find()->orderBy(['sort_order' => SORT_ASC])->all();
        $model = CmsPages::findOne(['seo_url' => 'bbg-membership']);
        return $this->render('membership', [
            'model' => $model,
            'membership_types' => $membership_types
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup($membership = 0)
    {
        $model = new SignupForm();
        $errors = [];

        if (Yii::$app->request->get()) {
            $model->group_id = Yii::$app->request->get('membership');
            $model->account_type = 'member';
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($user = $model->signup()) {
                \Yii::$app->session->setFlash('success', 'Your application for membership to the British Business Group is received with thanks. Check your email for further instructions.');
                return $this->redirect(['/login']);
            }

        } else{
            $errors[] = $model->getErrors();
        }

        return $this->render('signup', [
            'model' => $model,
            'errors' => $errors
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token user Auth Token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {

            $model = new ResetPasswordForm($token);

        } catch (InvalidParamException $e) {

            Yii::$app->session->setFlash('error', 'Link is expired, please create a new one by entering your email');
            return $this->redirect('/site/request-password-reset');

            // throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify your account.
     *
     * @param string $token user Auth Token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionVerify($token) {

        try {
            $model = new VerifyAccount($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->verify()) {
            Yii::$app->session->setFlash('success', 'Your Account is verified now.');
            $this->redirect('/login');
        } else {
            throw new UnauthorizedHttpException('Verification code has been expired or invalid.');
        }
    }


    /**
     * @return string
     */
    public function actionInvoice(){
        return $this->render('invoice');
    }

    /**
     * @return string
     */
    public function actionSubscribeNewsletter()
    {
        if (Yii::$app->request->post()) {

            $email = Yii::$app->request->post("email");

            if ($email) {
                $model = NewsletterSubscriptions::find()->where(['email' => $email])->one();
                if ($model == null) {
                    $model = new NewsletterSubscriptions();
                }
            } else {
                $model = new NewsletterSubscriptions();
            }

            $model->email = Yii::$app->request->post("email");
            $model->first_name = Yii::$app->request->post("first_name");
            $model->last_name = Yii::$app->request->post("last_name");

            $model->active = "1";
            $model->events_news = "1";
            $model->weekly_newsletter = "1";
            $model->special_offers = "1";
            $model->is_member = 0;

            $text = "";

            if (!$model->save()) {

                foreach ($model->getErrors() as $error) {

                    if (is_array($error)) {
                        foreach ($error as $err) {
                            $text .= $err . "<br/>";
                        }
                    } else {
                        $text .= $error . "<br/>";
                    }
                }

                return \GuzzleHttp\json_encode(['type' => 'error', 'msg' => $error]);
            }

            return \GuzzleHttp\json_encode(['type' => 'success', 'msg' => "You are now subscribed to receive email updates, special offers and announcements from the BBG!"]);
        }
    }

    /**
     * @param $invoice_id
     */
    public function actionInvoicePayment($invoice_id = "")
    {
        if ($invoice_id) {
            \Yii::$app->session->set('payInvoiceId', $invoice_id);
        } else {

            if (Yii::$app->session->get('payInvoiceId')) {
                $invoice_id = \Yii::$app->session->get('payInvoiceId');
            } else {
                Yii::$app->session->setFlash('error', "Invoice number does not exists");
                return $this->goHome();
            }
        }

        if (isset($invoice_id) && !empty($invoice_id)) {

            $errors = [];
            $invoice = Invoices::findOne($invoice_id);
            $result = "";

            $title = "Invoice Payment";
            $prod_name = "Invoice Payment";

            if ($invoice <> null) {

                // If invoice is already paid
                if ($invoice->payment_status == 'paid') {

                    \Yii::$app->session->setFlash('error', "This invoice is already paid");
                    return $this->redirect('/');

                }

                if ($invoice->invoice_related_to == "event") {

                    $title = "Event Registration";
                    $prod_name = Events::findOne($invoice->invoice_rel_id)->title;

                } else {

                    $title = "Membership";
                    if ($invoice->invoice_category != "") {
                        $prod_name = $invoice->invoice_category;
                    }
                }
            } else{
                Yii::$app->session->setFlash('error',"Invoice is invalid!");
                return $this->goHome();
            }

            return $this->render('payment', [
                'invoice' => $invoice,
                'title' => $title,
                'prod_name' => $prod_name
            ]);
        }
    }

    /**
     *
     */
    public function actionVerifyPayment()
    {
        if (\Yii::$app->request->post()) {

            $response = \Yii::$app->request->post();

            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;

            if ($response['response_code'] == "100") {
                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);
            }

            \Yii::$app->session->set('payInvoiceId', 0);

            return $this->render('payment_result', [
                'response' => $response,
                'invoice' => $invoice
            ]);
        }

        $this->goHome();
    }

    /**
     * @param $invoice
     */
    public function actionPaymentNotifications()
    {

        /*(new EmailHelper())->sendEmail(
            'se.faisalijaz@gmail.com',[],
            'Testing IPN', 'testing_ipn',
            ['ipn' => Yii::$app->request->post()]
        );*/

        if (Yii::$app->request->post()) {

            $response = \Yii::$app->request->post();

            /*(new EmailHelper())->sendEmail(
                'se.faisalijaz@gmail.com',[],
                'Testing IPN Inside Condition', 'testing_ipn',
                ['ipn' => $response]
            );*/


            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;
            $invoice_update = false;

            if ($response['response_code'] == "100") {

                $model = new Payments();
                $payment = $model->paytabs_make_payment_ipn($response);
                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);

                if ($payment <> null && count($payment) > 0) {
                    if (isset($payment['status']) && $payment['status'] == 'succes') {

                        $payment_id = $payment['id'];
                        $invoices = \Yii::$app->request->post('order_id');

                        if (strpos($invoices, '|') !== false) {

                            $invoices = array_filter(explode('|', $invoices));

                            if (is_array($invoices) && count($invoices) > 0) {
                                foreach ($invoices as $inv_id) {

                                    $invoice = Invoices::findOne(['invoice_id' => $inv_id]);
                                    if ($invoice <> null) {
                                        if ($invoice->invoice_related_to == 'event') {
                                            $event = Events::findOne($invoice->invoice_rel_id);
                                        }
                                    }

                                    $invoice_update = $model->updateInvoicePaidIpn($response, $invoice, $payment_id);
                                }
                            }

                        } else {

                            $invoice = Invoices::findOne(['invoice_id' => $invoices]);

                            if ($invoice <> null) {
                                if ($invoice->invoice_related_to == 'event') {
                                    $event = Events::findOne($invoice->invoice_rel_id);
                                }
                            }

                            $invoice_update = $model->updateInvoicePaidIpn($response, $invoice, $payment_id);
                        }

                    } else {
                        return false;
                    }
                }

                if (isset($payment['status']) && $payment['status'] == 'succes') {

                    $payment_id = $payment['id'];

                    if ($invoice <> null) {
                        if ($invoice->invoice_related_to == 'event') {
                            $event = Events::findOne($invoice->invoice_rel_id);
                        }
                    }

                    $invoice_update = $model->updateInvoicePaidIpn($response, $invoice, $payment_id);
                }

                if($invoice_update){
                    return true;
                }

                return false;
            }
        }
    }

    /**
     * @param $invoice
     * @return mixed
     */
    public function actionViewInvoice($invoice){

        /*if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }*/

        $member = Members::findOne(Yii::$app->user->id);
        $invoices = Invoices::findOne($invoice);

        $pdf = \Yii::$app->pdf;
        $mpdf = $pdf->api;
        $mpdf->format = Pdf::FORMAT_A4;
        $mpdf->orientation = Pdf::ORIENT_PORTRAIT;

        if ($invoices->invoice_related_to == "event") {

            $content = $this->renderPartial('/account/send_event_invoice', [
                'member' => ($member <> null) ? $member : null,
                'invoice' => $invoices
            ]);

        } else {

            $content = $this->renderPartial('/account/send_invoice', [
                'member' => $member,
                'invoice' => $invoices
            ]);
        }

        $mpdf->WriteHtml($content);
        return $mpdf->Output('Invoice', 'I');
    }

    /**
     * @param string $id
     * @return string
     */
    public function actionViewFromChair($id = "")
    {

        $single_message = null;

        if ($id) {

            $single_message = ViewFromChair::findOne($id);

            if($single_message <> null){
                Yii::$app->session->set('opengraph_title', $single_message->title);
                Yii::$app->session->set('opengraph_url', Yii::$app->params['appUrl'] . "/site/view-from-chair?id=" . $single_message->id);
                Yii::$app->session->set('opengraph_image', (empty($single_message->image)) ? Yii::$app->params['no_image'] : $single_message->image);
                Yii::$app->session->set('opengraph_description', $single_message->short_description);
            }

        }

        $chariman_messages = ViewFromChair::find()->Where(['status' => '1'])->orderBy([
            'date' => SORT_DESC
        ])->limit(5)->all();

        return $this->render('chairman_message_detail', [
            'single_message' => $single_message,
            'chariman_messages' => $chariman_messages,
        ]);

    }

    /**
     * @param string $id
     * @return string
     */
    public function actionViewFromChairList()
    {
        $query = ViewFromChair::find()->Where(['status' => '1']);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 5
        ]);

        $chariman_messages = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy([
                'date' => SORT_DESC
            ])->all();

        return $this->render('chairman_message_list', [
            'chariman_messages' => $chariman_messages,
            'pages' => $pages
        ]);

    }


    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionJobList()
    {
        $query = JobPosts::find()->Where(['published' => '1'])->andWhere(['>=', 'auto_remove_after', date('Y-m-d')]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $job_details = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['posted_on' => SORT_DESC])
            ->all();

        return $this->render('jobs_list', [
            'jobs_list' => $job_details,
            'pages' => $pages
        ]);

    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionJobPostDetail($id)
    {
        if (!$id) {
            return $this->goHome();
        }

        $job_details = JobPosts::find()->Where(['id' => $id])->orderBy([
            'posted_on' => SORT_DESC
        ])->one();

        if($job_details <> null){
            Yii::$app->session->set('opengraph_title', $job_details->job_title);
            Yii::$app->session->set('opengraph_url', Yii::$app->params['appUrl'] . "/site/job-post-detail?id=" . $job_details->id);
            Yii::$app->session->set('opengraph_image',  Yii::$app->params['no_image']);
            Yii::$app->session->set('opengraph_description', $job_details->short_description);
        }

        return $this->render('job_detail', [
            'job_details' => $job_details
        ]);

    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionGallery()
    {
        $query = Gallery::find()->where(['status' => 'active']);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $gallery = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy(['id' => SORT_DESC])
            ->all();


        return $this->render('gallery', [
            'gallery' => $gallery,
            'pages' => $pages
        ]);

    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionGalleryItem($id)
    {
        if (!$id) {
            return $this->goHome();
        }

        $item = Gallery::findOne($id);


        return $this->render('gallery_item_detail', [
            'item' => $item
        ]);

    }


    /*public function actionVerifyPaymentOld()
    {
        if (\Yii::$app->request->post()) {

            $response = \Yii::$app->request->post();

            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;

            if ($response['response_code'] == "100") {

                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);
                /**
                $model = new Payments();
                $payment = $model->paytabs_make_payment($response);

                if (isset($payment['status']) && $payment['status'] == 'succes') {

                $payment_id = $payment['id'];

                if ($invoice <> null) {
                if ($invoice->invoice_related_to == 'event') {
                $event = Events::findOne($invoice->invoice_rel_id);
                }
                }

                $model->updateInvoicePaid($response, $invoice, $payment_id);

                }
            }

            \Yii::$app->session->set('payInvoiceId', 0);

            return $this->render('payment_result', [
                'response' => $response,
                'invoice' => $invoice
            ]);
        }

        $this->goHome();
    }*/

}
