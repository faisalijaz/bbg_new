<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\Accounts',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'yii\web\UrlManager',
            // 'baseUrl' => '/',
            'enableStrictParsing' => false,
            'rules' => [
                '/' => 'site/index',
                //'pages/{seo_url}' => 'pages/view-by-seo-url',
                'contact-us' => 'site/contact',
                'signup' => 'site/signup',
                'login' => 'site/login',
                'logout' => 'site/logout',
                'apply/membership' => '/site/apply-membership',
                'membership/profile' => '/account/profile',
                'membership/directory' => 'account/member-directory',
                'company/directory' => 'account/company-directory',
                'events/upcoming' => 'events/index',
                'events/registrations' => 'account/events-registered',
                'membership/offers' => 'account/member-offers',
                'membership/contacts' => 'account/member-contacts',
                'membership/invoices' => 'account/invoices',
                'membership/payments' => 'account/payments',
                'membership/renew' => 'account/renew-membership',
                'membership/expertise-requests' => 'account/expert-requests',
                'membership/update' => 'account/update',
                'newsletter/subscribe' => 'news/subscribe-newsletter/',
                'company/members' => '/account/my-company-members',
                'membership/update-password' => 'account/change-password',



            ],
        ],
    ],
    'params' => $params,
];
