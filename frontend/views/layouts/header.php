<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$url = Url::current();

$userData = \Yii::$app->user;
$logo = Yii::$app->appSettings->getByAttribute("app_logo");
$siteSetting =  Yii::$app->appSettings->settings;

$menu_list = Yii::$app->appSettings->getMenuItems();

?>

<header id="topHeaderSection">
    <div id="fixedTopNav">
        <div class="TopMost">
            <div class="container">
                <div class="row hidden-lg-up">
                    <?php

                    $padding_zero = "";

                    if (!$userData->isGuest) {
                        $padding_zero = "padding_zero";
                        ?>
                        <div class="col-8 col-sm-8 col-md-8 col-lg-2 col-xl-2 display-block">
                            <div class="MemberLogin PaddingTopBtm">
                                <a href="/account/profile">
                                    <i class="fa fa-sign-o usericonsize"
                                       aria-hidden="true"></i> Welcome, <?= $userData->identity->first_name; ?>
                                </a>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 display-block">
                            <div class="MemberLogin PaddingTopBtm">
                                <a href="#" title="Member Login" data-original-title="Member Login" data-toggle="modal"
                                   data-target="#myModal">
                                    <i class="fa fa-user usericonsize" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if (!$userData->isGuest) {
                        ?>
                        <div class="col-1 col-sm-1 col-md-1 <?= $padding_zero;?> display-block">
                            <div class="MemberLogin PaddingTopBtm">
                                <?= Html::a('<i class="fa  fa-sign-out usericonsize" aria-hidden="true"></i> ',
                                    ['/site/logout'], ['class' => 'usericonsize', 'data' => ['method' => 'post']]); ?>
                            </div>
                        </div>

                        <div class="col-1 col-sm-1 col-md-1 <?= $padding_zero;?>">
                            <div class="MemberLogin PaddingTopBtm">
                                <a id="request_experties" class="request_experties" href="#" data-toggle="modal"
                                   data-target="#myModalRequestExpertise">
                                    <i class="fa fa-search usericonsize" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="col-1 col-sm-1 col-md-1 <?= $padding_zero;?> display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <a target="_blank"
                               href="/news/subscribe-newsletter"><i
                                        class="fa fa-envelope usericonsize" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-1 col-sm-1 col-md-1 <?= $padding_zero;?> display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <a id="search_box_on_mobile" class="request_experties" href="#" data-toggle="modal"
                               data-target="#myModalSearchOnMobile"><i
                                        class="fa fa-search-plus usericonsize" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

                    <?php
                    if ($userData->isGuest) {
                        ?>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-3 col-xl-3 display-block">

                        </div>
                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <div class="MembershipBtn PaddingTopBtm">
                                <a href="/site/apply-membership">apply</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
                <!--- ########################## -->
                <div class="row hidden-md-down">
                    <?php
                    if ($userData->isGuest) {
                        ?>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                            <div class="MembershipBtn PaddingTopBtm">
                                <a href="/site/apply-membership">apply for membership</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="col-8 col-sm-8 col-md-4 col-lg-2 col-xl-2 display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <?php
                            if (!$userData->isGuest) {
                                ?>
                                <a href="/account/profile">
                                    <i class="fa fa-user-o usericonsize"
                                       aria-hidden="true"></i> Welcome, <?= $userData->identity->first_name; ?>
                                </a>
                                <?php
                            } else {
                                ?>
                                <a href="#" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-user usericonsize" aria-hidden="true"></i> Member Login
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <?php
                    if (!$userData->isGuest) {
                        ?>
                        <div class="col-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 display-block">
                            <div class="MemberLogin PaddingTopBtm">
                                <?= Html::a('<i class="fa  fa-sign-out usericonsize" aria-hidden="true"></i> Logout',
                                    ['/site/logout'], ['class' => '', 'data' => ['method' => 'post']]); ?>
                            </div>
                        </div>

                        <div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2 display-block">
                            <div class="MemberLogin PaddingTopBtm">
                                <a id="request_experties" class="request_experties" href="#" data-toggle="modal"
                                   data-target="#myModalRequestExpertise">
                                    <i class="fa fa-search usericonsize" aria-hidden="true"></i> Request Expertise
                                </a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 display-block">
                        <div class="MemberLogin PaddingTopBtm">

                            <a target="_blank"
                               href="/news/subscribe-newsletter"><i
                                        class="fa fa-envelope" aria-hidden="true"></i> Subscribe to updates</a>
                        </div>
                    </div>

                    <div class="hidden-md-down col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 display-block">
                        <?php $form = ActiveForm::begin(['id' => 'searcher', 'method' => 'get', 'action' => '/', 'options' => ['class' => '']]); ?>
                        <div class="TopMostSearch">
                            <div class="input-group">
                                <input type="search" name="search" class="form-control"
                                       placeholder="Find News and Events ...">
                                <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="SiteHeader">
            <div class="container">
                <div class="row">
                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                        <?= Html::a('<img class="MyAppLogo" src="' . $logo . '" alt="BBG-Dubai">', ['/site/index'], []) ?>
                    </div>
                    <div class="hidden-md-down col-lg-9 col-xl-9">
                        <nav class="float-left">
                            <ul id='menu'>

                                <?php
                                foreach ($menu_list as $key => $value){ ?>
                                <li>
                                    <?= Html::a($key, [$value], ['class' => Yii::$app->appSettings->getSubMenuItems($key) ? 'prett' : '']) ?>
                                    <?php
                                        $submenu = Yii::$app->appSettings->getSubMenuItems($key);
                                        echo '<ul class="menus">';
                                                    foreach ($submenu as $key1 => $value1){
                                                        if ($value1 == "/apply/membership" && !Yii::$app->user->isGuest){}
                                                        else if($value1 == "{news_categories}"){

                                                            $categories = \common\models\NewsCategories::find()->where(['status' => '1'])->orderBy(['sort_order' => SORT_ASC])->all();
                                                            foreach($categories as $category){

                                                                $url = '/news?category=' . $category->id;

                                                                echo '<li>';
                                                                echo Html::a(strtoupper(str_replace('-', ' ', $category->title)), [$url], ['class' => 'home']);
                                                                echo  '</li>';
                                                            }
                                                        }
                                                        else{

                                                            ?>
                            <li class="<?= Yii::$app->appSettings->getSubMenuItems($key1) ? 'has-submenu' : '' ?>">
                            <?= Html::a(strtoupper(str_replace('-', ' ', $key1)), [$value1], ['class' => Yii::$app->appSettings->getSubMenuItems($key1) ? 'prett' : 'home']) ?>
                            <?= Yii::$app->appSettings->getSubMenuItems($key1) ? '' : '</li>' ?>

                            <?php
                            $submenu_child = Yii::$app->appSettings->getSubMenuItems($key1);
                            if (isset($submenu_child)){
                            if ($submenu_child <> null && !empty($submenu_child)) {
                            echo '<ul class="submenu">';
                            foreach ($submenu_child

                                     as $key2 => $value2) {
                            if ($value2 == "/apply/membership" && !Yii::$app->user->isGuest){

                            }

                            else{

                            ?>
                                <li class=""><?= Html::a(strtoupper(str_replace('-', ' ', $key2)), [$value2], ['class' => 'home']) ?></li>
                            <?php
                            }
                            }
                            echo '</ul>';
                            Yii::$app->appSettings->getSubMenuItems($key1) ? '</li>' : '';
                            /**/
                            }
                            }

                            }
                            }
                            ?>
                                </li>

                            </ul>

                            <?php } ?>
                        </nav>
                    </div>


                    <div class="col-8 col-sm-8 col-md-8 hidden-lg-up">
                        <div id="mySidenav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

                            <?php
                            $i = 1;
                            foreach ($menu_list as $key => $value) {

                                $submenu = Yii::$app->appSettings->getSubMenuItems($key);
                                $hasSubMenu = (count($submenu) > 0) ? 1 : 0;

                                if ($hasSubMenu) {

                                    echo Html::a($key . " +", "#", ['id' => "coll" . $i]);
                                    echo '  <div class="submenu-1" style="display: none" id="coll' . $i . '-a">';

                                                foreach ($submenu as $key1 => $value1) {

                                                    if ($value1 == "{news_categories}") {

                                                        $categories = \common\models\NewsCategories::find()->where(['status' => '1'])->orderBy(['sort_order' => SORT_ASC])->all();
                                                        foreach ($categories as $category) {

                                                            $url = '/news?category=' . $category->id;

                                                            echo Html::a(strtoupper(str_replace('-', ' ', $category->title)), [$url], ['class' => 'home']);
                                                        }
                                                    } else {

                                                        echo Html::a(strtoupper(str_replace('-', ' ', $key1)), [$value1], []);
                                                    }
                                                }

                                    echo '  </div>';
                                } else {
                                    echo Html::a($key, [$value], []);
                                }

                                $i++;
                            } ?>
                        </div>
                        <div class="text-right">
                            <span style="font-size:30px;cursor:pointer;" onclick="openNav()">&#9776;</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Members Login</h4>
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin(['id' => 'model_login_form', 'action' => '/site/ajax-login', 'options' => ['class' => 'f-login-form']]); ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <input type="email" required id="accountloginform_user" class="form-control"
                                           name="AccountLoginForm[username]" autofocus=""
                                           placeholder="Please Enter Username"
                                           aria-required="true">
                                    <p class="help-block help-block-error"></p>
                                </div>
                                <div class="form-group">
                                    <input type="password" required id="accountloginform_password" class="form-control"
                                           name="AccountLoginForm[password]" placeholder="Enter your password"
                                           aria-required="true">
                                </div>
                                <div id="ajax_login_errors" class="alert hidden row"></div>
                                <button type="submit" id="submitModelLogin"
                                        class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o"
                                        name="LOGIN TO YOUR ACCOUNT">Sign In
                                </button>
                                <a href="/site/request-password-reset">
                                    <button class="Mybtn " type="button">Forgot Password</button>
                                </a>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary secondary_button" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- The Modal for request expertise-->
        <div class="modal fade" id="myModalRequestExpertise" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Request Expertise</h4>
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin(['id' => 'model_request_experties', 'method' => 'get', 'action' => '/account/search-experties', 'options' => ['class' => 'c_model_request_experties']]); ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <input type="text" required id="search_request_experties" class="form-control"
                                           name="search_request_experties" autofocus="" placeholder="Keyword Search e.g Consultancy, IT, Director Communications etc.."
                                           aria-required="true">
                                    <p class="help-block help-block-error"></p>
                                </div>
                                <button type="submit" id="submitModelSearchExperties"
                                        class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Search Expertise
                                </button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary secondary_button" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
            <div class="modal fade" id="myModalSearchOnMobile" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Search News & Events</h4>
                            <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 " id="searchMobile">
                                <?php $form = ActiveForm::begin(['id' => 'searcher', 'method' => 'get', 'action' => '/', 'options' => ['class' => '']]); ?>
                                <div class="TopMostSearch" style="padding-top: 4px;">
                                    <div class="input-group" >
                                        <input type="search" name="search" class="form-control"
                                               value="<?= Yii::$app->request->get('search');?>"
                                               placeholder="Find News and Events ...">

                                    </div>
                                    <div id="ajax_login_errors" class="alert hidden row"></div>
                                    <br/>
                                        <button class="Mybtn  btn btn-default" type="submit">
                                        Search <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>

                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary secondary_button" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
    </div>
</header>
<div style="clear: both"></div>
<style>
    .secondary_button {
        background: #bd1f2f !important;
        color: #ffffff !important;
        background-color: #bd1f2f !important;
        border-color: #bd1f2f !important;
    }
</style>