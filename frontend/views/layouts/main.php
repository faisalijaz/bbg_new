<?php
ini_set('memory_limit', '2048M');

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5ad5d1e2d0b9d300137e39e2&product=custom-share-buttons' async='async'></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122992335-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-122992335-1');
    </script>

    <meta charset="<?= Yii::$app->charset ?>">

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta property="og:site_name" content="<?= Yii::$app->appSettings->getByAttribute("app_name"); ?>" />
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?= (\Yii::$app->session->get('opengraph_url')) ? \Yii::$app->session->get('opengraph_url' ) : ''; ?>"/>

    <!-- Open Graph data -->
    <meta property="og:title" content="<?= (\Yii::$app->session->get('opengraph_title')) ? \Yii::$app->session->get('opengraph_title' ) : ''; ?>"/>
    <meta property="og:image:url" content="<?= (\Yii::$app->session->get('opengraph_image')) ?  (\Yii::$app->session->get('opengraph_image' )) : ''; ?>" />
    <meta property="og:image" content="<?= (\Yii::$app->session->get('opengraph_image')) ?  (\Yii::$app->session->get('opengraph_image' )) : ''; ?>" />

    <meta property="og:image" content="<?= (\Yii::$app->session->get('opengraph_image')) ?  (\Yii::$app->session->get('opengraph_image' )) : ''; ?>" />

    <meta property="og:description" content="<?= (\Yii::$app->session->get('opengraph_description')) ? \Yii::$app->session->get('opengraph_description' ) : '';  ?>"/>
    <!-- Open Graph data End -->

    <!-- Open Graph data -->
    <meta itemprop="headline" content="<?= (\Yii::$app->session->get('opengraph_title')) ? \Yii::$app->session->get('opengraph_title' ) : ''; ?>"/>
    <meta property="og:image:url" content="<?= (\Yii::$app->session->get('opengraph_image')) ? (\Yii::$app->session->get('opengraph_image' )) : ''; ?>" />
    <meta property="og:image" content="<?= (\Yii::$app->session->get('opengraph_image')) ? (\Yii::$app->session->get('opengraph_image' )) : ''; ?>" />

    <meta itemprop="image" content="<?= (\Yii::$app->session->get('opengraph_image')) ?  (\Yii::$app->session->get('opengraph_image' )) : ''; ?>" />

    <meta itemprop="description"  content="<?= (\Yii::$app->session->get('opengraph_description')) ? \Yii::$app->session->get('opengraph_description' ) : '';  ?>"/>
    <!-- Open Graph data End -->

    <meta property="twitter:site" content="@BBGOnline"/>
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:title" content="<?= (\Yii::$app->session->get('opengraph_title')) ? \Yii::$app->session->get('opengraph_title' ) : ''; ?>"/>

    <meta property="twitter:image" content="<?= (\Yii::$app->session->get('opengraph_image')) ?  (\Yii::$app->session->get('opengraph_image' )) : ''; ?>"/>
    <meta property="twitter:image:url" content="<?= (\Yii::$app->session->get('opengraph_image')) ?  (\Yii::$app->session->get('opengraph_image' )) : ''; ?>"/>
    <meta property="twitter:description" content="<?= (\Yii::$app->session->get('opengraph_description')) ? \Yii::$app->session->get('opengraph_description' ) : '';  ?>"/>
    <meta name="twitter:creator" content="@BBGOnline">

    <meta property="fb:app_id" content="208152629851485"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
    <meta name="format-detection" content="telephone=no"/>
    <meta name="google-site-verification" content="MyNyD6fWXzV91fKnUDHgfxlyiA8xODLzexTUNo5DQ7o" />

    <link rel="shortcut icon" type="image/ico" href="theme/bbg/resources/icons/favicon.ico"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="style-2" data-color="theme-1">

<?php $this->beginBody() ?>

<!-- Theme loading -->
<?= $this->render('theme_loading', []); ?>

<!--Applicaiton Header-->
<?= $this->render('header', []); ?>

<?= \yii2mod\alert\Alert::widget() ?>

<?= $content ?>

<!--Applicaiton Footer-->
<?= $this->render('footer', []); ?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true, 'tabindex' => false],
    'options' => ['tabindex' => false]
]);
echo "<div id='modalContent'><div style='text-align:center'></div>";
yii\bootstrap\Modal::end();
?>

<?php $this->endBody() ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


