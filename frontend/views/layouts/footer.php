<?php
if (count((array)\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}
$year = date('Y');
?>
<footer>
    <section class="footerarea">
        <div class="container">
            <div class="row footerwidgets">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Contact</h2>
                        <p>Telephone: <?= (isset($siteSetting['telephone'])) ? $siteSetting['telephone'] : ''; ?>
                            <br>
                            Email: <?= (isset($siteSetting['admin_email'])) ? \yii\helpers\Html::a($siteSetting['admin_email'], null, ['href' => 'mailto:' . $siteSetting['admin_email']]) : ''; ?>
                            <br>
                        </p>
                        <br/>
                        <p>
                            <a style="background: #cd2c47" target="_blank" class="Mybtn" href="/blog-posts">
                                Help <i class="fa fa-question-circle" aria-hidden="true"></i>
                            </a>
                        </p>
                        <br/>
                        <p>
                            <a target="_blank" class="Mybtn" href="/pages/view-by-seo-url?seo_url=privacy-policy"
                               style="background: #cd2c47; margin-top: 10px;" >Privacy Policy</a>
                        </p>
                        <br/>
                        <p>
                            
                            <script src="https://platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
                            <script type="IN/FollowCompany" data-id="1043402" data-counter="right"></script>
                        </p>
                        <br/>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Address</h2>
                        <p><?= (isset($siteSetting['address'])) ? $siteSetting['address'] : ''; ?></p>
                    </div>
                    <span class="socialmedia">
                         <?php
                         if (isset($siteSetting['facebook']) && !empty($siteSetting['facebook'])) {
                             ?>
                             <a href="<?= $siteSetting['facebook']; ?> "><i class="fa fa-facebook-square"
                                                                            aria-hidden="true"></i></a>

                             <?PHP
                         }
                         ?>
                        <?php
                        if (isset($siteSetting['linkedin']) && !empty($siteSetting['linkedin'])) {
                            ?>
                            <a href="<?= $siteSetting['linkedin']; ?> "><i class="fa fa-linkedin-square"
                                                                           aria-hidden="true"></i></a>

                            <?PHP
                        }
                        ?>
                        <?php
                        if (isset($siteSetting['twitter']) && !empty($siteSetting['twitter'])) {
                            ?>
                            <a href="<?= $siteSetting['twitter']; ?> "><i class="fa fa-twitter-square"
                                                                          aria-hidden="true"></i></a>

                            <?PHP
                        }
                        ?>

                        <?php
                        if (isset($siteSetting['instagram']) && !empty($siteSetting['instagram'])) {
                            ?>
                            <a href="<?= $siteSetting['instagram']; ?> "><i class="fa fa-instagram"
                                                                          aria-hidden="true"></i></a>

                            <?PHP
                        }
                        ?>

                        <?php
                        if (isset($siteSetting['youtube']) && !empty($siteSetting['youtube'])) {
                            ?>
                            <a href="<?= $siteSetting['youtube']; ?> "><i class="fa fa-youtube-square"
                                                                          aria-hidden="true"></i></a>

                            <?PHP
                        }
                        ?>

                        <?php
                        if (isset($siteSetting['pinterest']) && !empty($siteSetting['pinterest'])) {
                            ?>
                            <a href="<?= $siteSetting['pinterest']; ?> "><i class="fa fa-pinterest-square"
                                                                          aria-hidden="true"></i></a>

                            <?PHP
                        }
                        ?>

                        </span>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Join our mailing list</h2>
                        <p>Get the latest updates</p>
                        <div class="input-group width90percent">
                            <input type="text" id="email_subscribe_f_name" name="f_name"
                                   class="form-control subscribeEmail"
                                   placeholder="Enter Your First Name..."><br/>
                            </button>
                            </span>
                        </div>
                        <div class="input-group width90percent">
                            <input type="text" id="email_subscribe_l_name" name="l_name"
                                   class="form-control subscribeEmail"
                                   placeholder="Enter Your Last Name..."><br/>
                            </button>
                            </span>
                        </div>
                        <div class="input-group width90percent Sendwidth90percent">
                            <input type="email" id="email_subscribe_footer" name="email"
                                   class="form-control subscribeEmail"
                                   placeholder="Enter Your Email Address..">
                            <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" id="subscribeNewsletter" type="button">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="copyrights">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 CopyR">
                    
                    <p>
                        CopyRights © <?= $year; ?>
                        - <?= $year + 1; ?>  <?= (isset($siteSetting['app_name'])) ? $siteSetting['app_name'] : ''; ?>
                        . All Right Reserved. <br>
                        <?= \yii\helpers\Html::a("Designed & Developed By", 'https://wistech.biz/', [
                            'target' => '_blank'
                        ]); ?> : <?= \yii\helpers\Html::a("800Wisdom.ae", 'http://800Wisdom.ae/', [
                            'target' => '_blank'
                        ]); ?>
                    </p>
                </div>
            </div>
        </div>

    </section>

</footer>
<div id="fb-root"></div>
<style>
    .Sendwidth90percent{
        width: 90%!important;
    }
    .MySearchBtn{
        margin-left: 0px !important;
    }
    @media screen and (max-width: 767px) {
        .Sendwidth90percent{
            width: 93%!important;
        }
    }
    @media screen and (max-width: 420px) {
        .Sendwidth90percent{
            width: 90%!important;
        }
    }
    @media screen and (max-width: 360px) {
        .Sendwidth90percent{
            width: 89%!important;
        }
    }
    @media screen and (max-width: 320px) {
        .Sendwidth90percent{
            width: 87%!important;
        }
    }
</style>
</body>

<?= $this->registerJs("

    $('body').find('#subscribeNewsletter').click(function (e) { 
     
        $(this).prop('disabled', true);
        var email_data = $(document).find('#email_subscribe_footer').val();
        var Fname = $(document).find('#email_subscribe_f_name').val()
        var Lname = $(document).find('#email_subscribe_l_name').val()
        
        if ( email_data == '') {
            
            swal({
                title: 'Error!',
                text: 'Please enter email address!',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            $('#subscribeNewsletter').prop('disabled', false);
            return false; 
        }
        
        if(!isEmail(email_data)){
            swal({
                title: 'Error!',
                text: 'Please enter correct email address!',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            $('#subscribeNewsletter').prop('disabled', false);
            return false; 
        }
        
        $.ajax({
        
            url: '/site/subscribe-newsletter',
            type: 'POST',
            data : {email : email_data, first_name : Fname, last_name : Lname},
            success: function (data) {
                
                var data = JSON.parse(data);
                console.log(data);
                
                if(data.type == 'error'){
                    swal({
                        title: 'Error!', 
                         text: data.msg, 
                        type: 'error',
                        timer: 5000,
                        confirmButtonText: 'OK'
                    });
                } else{
                
                    swal({
                        title: 'Success!',
                        text: data.msg,
                        type: 'success', 
                        timer: 5000,
                        confirmButtonText: 'OK'
                    });
                
                }
                $('#subscribeNewsletter').prop('disabled', false)
            },
            error: function (e) {

                swal({
                    title: 'Error!',
                    text: 'Registration could not be completed!',
                    type: 'error',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });

                $('#subscribeNewsletter').prop('disabled', false);
            }
        });

        $('#subscribeNewsletter').prop('disabled', false);
        e.stopImmediatePropagation();
        return false;
    });
    
    
    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
     
");

if(Yii::$app->session->getFlash('login')){
    echo $this->registerJS("
       
        swal({
            title: 'Oooopss!',
            text: 'Please log in to update this page',
            type: 'info',
            timer: 5000,
            confirmButtonText: 'OK'
        });
        
    ");
}

echo $this->registerJS("
        $('.MainArea').find('img').each(function () {
            if(!$(this).hasClass('img-fluid')){
                $(this).addClass('img-fluid');
            }
        });
    ");
?>

