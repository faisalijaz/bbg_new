<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;

?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>Payment</h3>
                <br>
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Invoice ID</th>
                        <th>User</th>
                        <th>Company</th>
                        <th>Invoice Category</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Payment Status</th>
                        <th>Invoice Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $amount_paid = 0;
                    $subscriber = null;
                    $sub_name = "";
                    $sub_company = "";
                    $event_title = "";

                    if ($invoice <> null) {

                        if($invoice->invoice_related_to == "event") {
                            if ($invoice->invoiceItems <> null) {
                                $subscriber = null;
                                if (isset($invoice->invoiceItems[0])) {
                                    if ($invoice->invoiceItems[0]->eventSubscription <> null) {
                                        $subscriber = $invoice->invoiceItems[0]->eventSubscription;
                                    }

                                    if ($subscriber <> null) {
                                        $sub_name = $subscriber->firstname . " " . $subscriber->lastname;
                                        $sub_company = $subscriber->company;
                                        $event_title = ($subscriber->eventData <> null) ? $subscriber->eventData->title :"";
                                    }
                                }
                            }
                        }

                    $amount_paid = \common\models\Payments::find()->where(['invoice_id' => $invoice->invoice_id])->sum('amount');
                    ?>
                    <tr>
                        <th scope="row">
                            <?= Html::a(
                                str_pad($invoice->invoice_id, 7, "0", STR_PAD_LEFT),
                                ['/account/view-invoice', 'invoice' => $invoice->invoice_id],
                                [
                                    'target' => '_blank',
                                    'Title' => 'View Invoice',
                                    'data' => [
                                        'method' => 'post'
                                    ]
                                ]);
                            ?>
                        </th>
                        <td><?= ($invoice->member <> null) ? $invoice->member->first_name . " " . $invoice->member->last_name : $sub_name; ?></td>
                        <td><?= ($invoice->member <> null) ? ($invoice->member->accountCompany <> null)? $invoice->member->accountCompany->name : $sub_company : $sub_company; ?></td>
                        <td><?= ($subscriber <> null) ? $event_title : ""; ?></td>
                        <td><?= $invoice->total; ?></td>
                        <td><?= $amount_paid; ?></td>
                        <td>
                            <?php
                            if ($invoice->payment_status == 'unpaid') {
                                ?>
                                <span class="label label-danger">
                                            Pending
                                        </span>
                                <?php
                            } else if ($invoice->payment_status == 'partialy_paid') {
                            ?>
                                <span class="label label-warning">
                                            Partially Paid
                                        </span>
                                <?php
                            }
                            else {
                                ?>
                                <span class="label label-success">Paid
                                        </span>
                                <?php
                            }
                            ?>
                        </td>
                        <td><?= formatDate($invoice->invoice_date); ?></td>
                        <td class="invoicelink">
                            <?= Html::a(
                                'Invoice',
                                ['/site/view-invoice', 'invoice' => $invoice->invoice_id],
                                [
                                    'target' => '_blank',
                                    'Title' => 'View Invoice',
                                    'data' => [
                                        'method' => 'post'
                                    ],
                                ]
                            ); ?>
                        </td>
                    </tr>

                    </tbody>
                </table>
                <div class="pull-right">
                    <h3>Pay Now: AED <?= ($invoice <> null) ? $invoice->total - $amount_paid : 0;?> </h3>
                    <div class="PT_express_checkout"></div>
                </div>
                <link rel="stylesheet" href="https://www.paytabs.com/express/express.css">
                <script src="https://www.paytabs.com/theme/express_checkout/js/jquery-1.11.1.min.js"></script>
                <script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>
                <!-- Button Code for PayTabs Express Checkout -->
                <script type="text/javascript">
                    Paytabs("#express_checkout").expresscheckout({
                        settings: {
                            secret_key: "<?= \Yii::$app->params['testPaytabsSecureHash'];?>",
                            merchant_id: "<?= \Yii::$app->params['testPaytabsMerchant'];?>",
                            amount: "<?= ($invoice <> null) ? $invoice->total - $amount_paid : 0;?>",
                            currency: "AED",
                            title: '<?= (isset($title) && !empty($title)) ? $title : "Invoice Payment";?>',
                            product_names: '<?= (isset($title) && !empty($title)) ? $title : "Invoice Payment";?>',
                            order_id: <?= ($invoice <> null) ? $invoice->invoice_id : '';?>,
                            url_redirect: "<?= \Yii::$app->params['payment_verify_url'];?>",
                            redirect_on_reject: 1,
                        }
                    });
                </script>
                <?php
                } else{
                }
                ?>
                <br>
            </div>

        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>



