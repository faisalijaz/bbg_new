<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = (isset($title)) ? $title : "Jobs";
$this->params['breadcrumbs'][] = $this->title;

?>
<? /*= TopBannerWidget::widget(); */ ?>
    <section class="MainArea">
        <div class="container">
            <div class="row">

                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <div class="Heading">
                            <h3><?= $this->title; ?></h3>
                        </div>
                        <?php
                        if (isset($jobs_list) && $jobs_list <> null && count($jobs_list) > 0) {
                            foreach ($jobs_list as $list) {
                                ?>
                                <div class="Event1">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <?php
                                            $url = Yii::$app->params['no_image'];
                                            if ($list->member <> null) {
                                                if($list->member->accountCompany <> null){
                                                    $url = $list->member->accountCompany->logo;
                                                }
                                            }
                                            ?>
                                            <img src="<?= $url; ?>" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                            <div class="EventText">
                                                <h3><i class="fa fa-calendar"
                                                       aria-hidden="true"></i> <?= formatDate($list->posted_on); ?>
                                                </h3>
                                                <h4><?= $list->job_title; ?></h4>
                                                <p><?= $list->short_description; ?></p>
                                                <span class="EventDetail">
                                                <?= Html::a('Job Detail', ['/site/job-post-detail', 'id' => $list->id], []); ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="alert alert-warning">Job vancancies coming soon</div>
                            <?php
                        }
                        ?>
                        <div class="col-md-12 text-center">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>