<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'News Details';
$this->params['breadcrumbs'][] = $this->title;

$share_url = "";
$title = "";
?>

<? /*= TopBannerWidget::widget(); */ ?>

<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <?php
                    if (isset($chariman_messages) && $chariman_messages <> null && count($chariman_messages) > 0) {
                        foreach ($chariman_messages as $chariman_message) {
                            ?>
                            <div class="Event1">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <img src="<?= $chariman_message->image; ?>" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                        <div class="EventText">
                                            <h3><i class="fa fa-calendar"
                                                   aria-hidden="true"></i> <?= formatDate($chariman_message->date); ?>
                                            </h3>
                                            <h4 style="text-transform: initial !important;"><?= $chariman_message->title; ?></h4>
                                            <p><?= $chariman_message->short_description; ?></p>
                                            <span class="EventDetail">
                                                <?= Html::a('Read Detail', ['/site/view-from-chair', 'id' => $chariman_message->id], []); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="alert alert-warning">There isn't any news yet,you hear soon</div>
                        <?php
                    }
                    ?>
                    <div class="col-md-12 text-center">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                         ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>

<?= $this->registerJS('

    $("#share").jsSocials({
        url : "' . $share_url . '",
        text: "' . $title . '",
        shareIn: "popup",
        showLabel: false,
          showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "whatsapp"]
    });

'); ?>
