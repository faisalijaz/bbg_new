<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use kartik\checkbox\CheckboxX;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Membership Application';
$this->params['breadcrumbs'][] = $this->title;

$placeHolderTellUsAbout = "E.g. educated in the UAE and studied in the Netherlands I have lived, worked and studied in a cosmopolitan environment. Through this experience I was able to achieve a wide-ranging background. Through my travels I was able to gain knowledge in the following areas public speaking, event management, creative writing, marketing and production.";

?>
<style>
    form div.required label.control-label:after {
        content: " * ";
        color: #a94442;
    }

    textarea#signupform-tellusaboutyourself {
        min-height: 100px;
    }

    textarea#signupform-purposeofjoining {
        min-height: 100px;
    }

    textarea#signupform-purposeofjoining::-webkit-input-placeholder,
    textarea#signupform-tellusaboutyourself::-webkit-input-placeholder {
        font-size: 14px;
    }
</style>
<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'f-login-form']]); ?>
<? /*= TopBannerWidget::widget(); */ ?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                    <div class="col-md-12">
                        <?php
                        if (isset($errors) && count($errors) > 0) {
                            foreach ($errors as $error) {
                                if (is_array($error) && count($error) > 0) {
                                    ?>
                                    <div class="alert alert-danger">
                                        <?php
                                        foreach ($error as $k => $v) {
                                            if (isset($v[0])) {
                                                echo '<p>' . $v[0] . '</p>';
                                            }
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                    <div class="col-md-12 ">
                        <div class="Heading text-left">
                            <h3>Personal Information</h3><br/>
                        </div>
                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'title')->dropDownList(Yii::$app->params['userTitle'], ['prompt' => 'Select ...'])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'first_name')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'last_name')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'phone_number')->textInput([])->label('Mobile') ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'email')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'country_code')->dropDownList(['228' => 'United Arab Emirates', '229' => 'United Kingdom', '247' => 'Other'],
                                    ['prompt' => 'Select ...'])->label('Your Office Based Country') ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'city')->dropDownList(Yii::$app->params['emiratesList'],
                                    ['prompt' => 'Select ...'])->label('Your Office Based City') ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="row">
                                    <div id="other_country" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 hidden">
                                        <?= $form->field($model, 'other_country')->textInput([])->label('Please specify Country Name') ?>
                                    </div>
                                    <div id="other_city" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 hidden">
                                        <?= $form->field($model, 'other_city')->textInput([])->label('Please specify City Name') ?>
                                    </div>
                                </div>

                                <?= $this->registerJS('
    
                                     $(document).find("#signupform-country_code").change(function(e){
                                        
                                        if($(this).val() == "247"){
                                            $(document).find("#other_country").removeClass("hidden");
                                        }else{
                                            $(document).find("#other_country").addClass("hidden");
                                        }
                                        
                                    });  
                                    
                                     $(document).find("#signupform-city").change(function(e){
                                        
                                        if($(this).val() == "Other"){
                                            $(document).find("#other_city").removeClass("hidden");
                                        }else{
                                            $(document).find("#other_city").addClass("hidden");
                                        }
                                        
                                    });  
                                    
                                    
                                    if($(document).find("#signupform-country_code option:selected").val() == "247"){
                                        $(document).find("#other_country").removeClass("hidden");
                                    }
                                    
                                    if($(document).find("#signupform-city option:selected").val() == "Other"){
                                        $(document).find("#other_city").removeClass("hidden");
                                    }
                                    
                                     
                                '); ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($model, 'address')->textInput([])->label('Physical Mailing Address') ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'secondry_email')->textInput([])->label('Secondary Email') ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'designation')->textInput([])->label('Corporate Title') ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                <?= $form->field($model, 'nationality')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'country_name', 'country_name'),
                                    ['prompt' => 'Select ...'])->label() ?>
                            </div>
                            <?php
                            if ($model->group_id == 2) {
                                ?>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'company_type')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title'),
                                        ["class" => "form-control"])->label('Category') ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'howDidYouHear')->dropDownList(
                                    \yii\helpers\ArrayHelper::map(\common\models\ReferralSources::find()->where(['status' => '1'])->all(), 'id', 'title'),
                                    ['prompt' => 'Select ...', 'maxlength' => true]
                                ); ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'referred_by')->textInput([])->label('Referred By') ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($model, 'tellUsAboutYourself')->textarea([
                                    "row" => 5,
                                    "class" => "form-control",
                                    'maxlength' => true,
                                    'placeholder' => $placeHolderTellUsAbout
                                ]); ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($model, 'purposeOfJoining')->textarea(["row" => 5,
                                    "class" => "form-control", 'maxlength' => true,
                                    'placeholder' => 'E.g. having the ability to request for personal introductions to other BBG Members, without the usual awkwardness when approaching new contacts'])->label('Reasons for joining the BBG'); ?>
                            </div>

                        </div>
                        <?php
                        if ($model->group_id <> 2) {
                            ?>
                            <div class="Heading text-left">
                                <h3>Company Info</h3><br/>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'compnay_name')->textInput([])->label('Company Name') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'company_type')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Categories::find()->where(['type' => 'general'])->all(), 'id', 'title'), ['prompt' => 'Select your industry'],["class" => "form-control"])->label('Sector') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <?= $form->field($model, 'compnay_url')->textInput([])->label('Company Url') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'vat_number')->textInput([])->label("VAT TRN") ?>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'company_phone')->textInput([])->label("Telephone") ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'postal_zip')->textInput([])->label("P.O. Box/Zip") ?>
                                    <?= $form->field($model, 'fax')->hiddenInput([])->label(false) ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'emirates')->dropDownList(Yii::$app->params['emiratesList'],
                                        ['prompt' => 'Select ...'])->label('UAE/UK Head Office') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="row">
                                        <div id="other_office_emirates"
                                             class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 hidden">
                                            <?= $form->field($model, 'other_emirates')->textInput([])->label('Please Specify') ?>
                                        </div>
                                    </div>
                                </div>

                                <?= $this->registerJS(' 
                                    
                                     $(document).find("#signupform-emirates").change(function(e){
                                        
                                        if($(this).val() == "Other"){
                                            $(document).find("#other_office_emirates").removeClass("hidden");
                                        }else{
                                            $(document).find("#other_office_emirates").addClass("hidden");
                                        }
                                        
                                    });   
                                    if($(document).find("#signupform-emirates option:selected").val() == "Other"){
                                        $(document).find("#other_office_emirates").removeClass("hidden");
                                    }
                                    
                                     
                                '); ?>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <?= $form->field($model, 'comapny_address')->textInput([])->label("Address") ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <?= $form->field($model, 'about_company')->textarea([
                                        "row" => 5,
                                        "class" => "form-control",
                                        'style' => 'height:100px'
                                    ])->label("About Company (Use Keywords)") ?>

                                </div>
                            </div>
                            <?php
                        } ?>
                        <div class="Heading text-left">
                            <h3>Upload Required Documents</h3><br/>
                        </div>
                        <div class="row">

                            <?php
                            if ($model->group_id <> 2) {
                                ?>
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <label class="docs_upload_label control-label" for="signupform-trade_licence">Copy
                                        of
                                        UAE Trade Licence.</label>
                                    <div class="form-group">
                                        <a href="javascript:;"
                                           id="upload-document1"
                                           onclick="uploadAttachment(1)"
                                           data-toggle="tooltip"
                                           class="img-thumbnail"
                                           title="Upload Document">
                                            <i></i>
                                            <img src="<?= ($model->trade_licence) ? $model->trade_licence : Yii::$app->params['no_image']; ?>"
                                                 width="100" alt="" title="" data-placeholder="no_image.png"/>
                                            <br/>
                                        </a>
                                        <p class="image_upload_label">click Image to upload</p>
                                        <?= $form->field($model, 'trade_licence')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment1'])->label(false) ?>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of
                                    passport</label>
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document2"
                                       onclick="uploadAttachment(2)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <i></i>
                                        <img src="<?= ($model->passport_copy) ? $model->passport_copy : Yii::$app->params['no_image']; ?>"
                                             width="100" alt="" title="" data-placeholder="no_image.png"/>
                                        <br/>
                                    </a>
                                    <p class="image_upload_label">click Image to upload</p>
                                    <?= $form->field($model, 'passport_copy')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment2'])->label(false) ?>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of
                                    residence visa</label>
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document3"
                                       onclick="uploadAttachment(3)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <i></i>
                                        <img src="<?= ($model->residence_visa) ? $model->residence_visa : Yii::$app->params['no_image']; ?>"
                                             width="100" alt="" title="" data-placeholder="no_image.png"/>
                                        <br/>
                                    </a>
                                    <p class="image_upload_label">click Image to upload</p>
                                    <?= $form->field($model, 'residence_visa')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment3'])->label(false) ?>
                                </div>
                            </div>
                            <?php
                            if ($model->group_id <> 2) {
                                ?>
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <label class="docs_upload_label control-label" for="signupform-trade_licence">Company
                                        Logo.</label>
                                    <div class="form-group">
                                        <a href="javascript:;"
                                           id="upload-document5"
                                           onclick="uploadAttachment(5)"
                                           data-toggle="tooltip"
                                           class="img-thumbnail"
                                           title="Upload Document">
                                            <i></i>
                                            <img src="<?= ($model->trade_licence) ? $model->trade_licence : Yii::$app->params['no_image']; ?>"
                                                 width="100" alt="" title="" data-placeholder="no_image.png"/>
                                            <br/>
                                        </a>
                                        <p class="image_upload_label">click Image to upload</p>
                                        <?= $form->field($model, 'trade_licence')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment5'])->label(false) ?>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <label class="docs_upload_label control-label" for="signupform-trade_licence">Profile
                                    Picture (upload as JPEG)</label>
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document4"
                                       onclick="uploadAttachment(4)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <i></i>
                                        <img src="<?= ($model->passport_size_pic) ? $model->passport_size_pic : Yii::$app->params['no_image']; ?>"
                                             width="100" alt="" title="" data-placeholder="no_image.png"/>
                                        <br/>
                                    </a>
                                    <p class="image_upload_label">click Image to upload</p>
                                    <?= $form->field($model, 'passport_size_pic')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment4'])->label(false) ?>
                                </div>
                            </div>

                        </div>

                        <div class="Heading text-left">
                            <h3>BBG Profile Info</h3><br/>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'user_name')->textInput([])->label() ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'password')->passwordInput([])->label("Password") ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($model, 'repassword')->passwordInput([])->label("Retype password") ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                    'template' => '<div class="row"><div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">{input}</div><div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">{image}</div></div>',
                                ]) ?>
                            </div>


                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($model, 'newsletter')->widget(CheckboxX::classname(), [
                                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                    'pluginOptions' => [
                                        'theme' => 'krajee-flatblue',
                                        'enclosedLabel' => true,
                                        'threeState' => false,
                                    ]
                                ])->label(false); ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($model, 'terms')->widget(CheckboxX::classname(), [
                                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                    'pluginOptions' => [
                                        'theme' => 'krajee-flatblue',
                                        'enclosedLabel' => true,
                                        'threeState' => false,
                                    ]
                                ])->label(false); ?>
                                <span>
                                        <a target="_blank" href="/pages/view-by-seo-url?seo_url=terms-and-conditions"
                                           style="font-size: 10px;font-weight: 800;font-style: italic;">Read Terms & Condition here</a>
                                    </span>
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
                        <?= Html::submitButton('Sign Up', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>
                    </div>
                </div>
            </div>
        </div>

</section>
<?php ActiveForm::end(); ?>


<?php
if (count($model->getErrors()) > 0) {

    foreach ($model->getErrors() as $error) {
        if (is_array($error)) {
            foreach ($error as $err) {
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        } else {
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}
?>
<script>
    var uploadAttachment = function (attachmentId) {

        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);
                            $('#upload-document' + attachmentId + ' img').prop('src', json.href);
                            $('#input-attachment' + attachmentId).val(json.href);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }
</script>
