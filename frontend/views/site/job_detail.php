<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\helpers\Html;


$this->title = 'Job Details';
$this->params['breadcrumbs'][] = $this->title;

$share_url = "";
$title = "";
?>

<? /*= TopBannerWidget::widget(); */ ?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <?php
                    if (isset($job_details) && $job_details <> null) {
                        $share_url = Yii::$app->params['appUrl'] . "/news/news-details?id=" . $job_details->id;
                        $title = $job_details->job_title;
                        ?>
                        <div class="Heading">
                            <h3><?= $this->title; ?></h3>
                            <hr/>
                            <h4><?= $title; ?></h4>
                            <div class="row">
                                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <h3>Details</h3>
                                    <hr/>
                                    <p>
                                        <span class="BoldText">Position:</span> <?= ($job_details->position); ?>
                                    </p>
                                    <p>
                                        <span class="BoldText">Industry:</span> <?= ($job_details->industry); ?>
                                    </p>
                                    <p>
                                        <span class="BoldText">Skills:</span> <?= ($job_details->skills); ?>
                                    </p>
                                    <p>
                                        <span class="BoldText">Experience:</span> <?= ($job_details->experience); ?>
                                    </p>
                                    <p>
                                        <span class="BoldText">Location:</span> <?= ($job_details->location); ?>
                                    </p>
                                    <p>
                                        <span class="BoldText">Date Posted:</span> <?= ($job_details->posted_on); ?>
                                    </p>
                                    <p>
                                        <span class="BoldText">Apply Last Date:</span> <?= ($job_details->apply_by); ?>
                                    </p>
                                </div>
                                <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <h3>Contact Info</h3>
                                    <hr/>

                                    <?php
                                    if($job_details->contact_name) {
                                        ?>
                                        <p>
                                            <span class="BoldText">Name:</span> <?= ($job_details->contact_name); ?>
                                        </p>
                                        <?php
                                    }
                                    if($job_details->contact_email) {
                                        ?>
                                        <p>
                                            <span class="BoldText">Email:</span>
                                            <?= ($job_details <> null) ? Html::a($job_details->contact_email, null, ['href' => 'mailto:' . $job_details->contact_email]) : ""; ?>
                                        </p>
                                        <?php
                                    }
                                    if($job_details->contact_phone) {
                                        ?>
                                        <p>
                                            <span class="BoldText">Phone:</span>
                                            <?= ($job_details <> null) ? Html::a($job_details->contact_phone, null, ['href' => 'mailto:' . $job_details->contact_phone]) : ""; ?>

                                        </p>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="EventDetail">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="DescriptionHeading">
                                        <h4><?= $job_details->short_description; ?></h4>
                                        <?= $job_details->description; ?>
                                    </div>
                                    <div id="share"></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>

<?= $this->registerJS('

    $("#share").jsSocials({
        url : "' . $share_url . '",
        text: "' . $title . '",
        shareIn: "popup",
        showLabel: false,
         showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "whatsapp"]
    });

'); ?>
