<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;

$this->title = 'Committee Info';
$this->params['breadcrumbs'][] = $this->title;

$siteSetting = null;

if (count(\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}

?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <?php
                if ($staffCategories <> null) {

                    foreach ($staffCategories as $category) {

                        $committee = \common\models\CommitteMembers::find()->where(['type' => $category->id, 'status' => '1'])->orderBy([
                            'sort_order' => SORT_ASC
                        ])->all();

                        if ($committee <> null) {
                            ?>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                                <div class="Heading">
                                    <h3><?= $category->category_name; ?></h3>
                                </div>
                                <hr/>
                                <div class="committee text-center">
                                    <div class="row">
                                        <?php

                                        foreach ($committee  as $member) {
                                            $no_image = Yii::$app->params["no_image"];
                                            ?>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
                                                <!--<img src="<?/*= $member->image; */ ?>" class="committee_staff_image img-fluid"
                                             alt="<?/*= $member->full_name; */ ?>">-->



                                                <a class="committee_info" href="/site/committee-details?id=<?= $member->id;?>" style="color: #666666 !important;" target="_blank">
                                                    <img src="<?= ($member->image) ? $member->image : $no_image; ?>" class="committee_staff_image img-fluid"
                                                         alt="<?= $member->full_name;?>">
                                                </a>

                                                <div class="name_and_postion">
                                                    <h5><?= Html::a($member->full_name, ["/site/committee-details", 'id' => $member->id], ['target' => '_blank', 'style' => 'color: #666666 !important;']); ?></h5>
                                                    <h6><?= $member->designation; ?></h6>
                                                </div>
                                                <h3 class="commitee_staff_email"><?= Html::a($member->email, null, ['href' => 'mailto:' . $member->email]); ?>
                                                </h3>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="SideHeading">
                        <h3>Get In Touch</h3>
                    </div>
                    <div class="SideCalender">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
                                    <i class="fa fa-map-marker blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p> 
                                        <?= (isset($siteSetting['address'])) ? $siteSetting['address'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
                                    <i class="fa fa-envelope blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                        <?php
                                        $email = isset($siteSetting['admin_email']) ? $siteSetting['admin_email'] : '';
                                        ?>

                                        <strong>Email:</strong> <?= Html::a($email, null, ['href' => $email]); ?>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
                                    <i class="fa fa-phone blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                        <strong>Telephone:</strong> <?= (isset($siteSetting['telephone'])) ? $siteSetting['telephone'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<style>
    .blueicon {
        color: #1f3760 !important;
        font-size: 25px !important;
    }
</style>
