<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Forgot your password?';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="f-login-content loginFormstyle paddingRightLeft">
                        <div class="Heading text-center">
                            <h3>Forget Your Password?</h3>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="f-login-header">
                                    <div class="f-login-desc color-grey">Enter your email address to reset your password.</div>
                                </div>
                                <div class="SiteText">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'Enter your email address'])->label(false) ?>
                                        <?= Html::submitButton('Send Recovery Email', ['class' => 'login-btn Mybtn c-button full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'Send Recovery Email']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?php ActiveForm::end(); ?>