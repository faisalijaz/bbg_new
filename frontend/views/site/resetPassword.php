<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="f-login-content loginFormstyle paddingRightLeft">
                        <div class="f-login-header">
                            <div class="Heading text-center">
                                <h3><?= Html::encode($this->title) ?></h3>
                            </div>
                        </div>
                        <div class="SiteText">
                            <div class="loginFormstyle col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => 'Enter your Password'])->label(false) ?>
                                <?= $form->field($model, 'confirm_password')->passwordInput(['autofocus' => true, 'placeholder' => 'Confirm your Password'])->label(false) ?>
                                <?= Html::submitButton('Reset password', ['class' => 'login-btn Mybtn c-button full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'Reset password']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
    </div>
</section>
<?php ActiveForm::end(); ?>