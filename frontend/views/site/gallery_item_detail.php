<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget; 

$this->title = (isset($item->name)) ? $item->name : "";
$this->params['breadcrumbs'][] = $this->title;

?>

<? /*= TopBannerWidget::widget(); */ ?>

    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <?php
                        if (isset($item) && $item <> null) {
                            ?>

                            <div class="Heading">
                                <h3><?= $this->title; ?></h3>
                                <hr/>
                                <h4><?= $item->name; ?></h4>

                            </div>
                            <div class="EventDetail">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <img src="<?= (empty($item->image)) ? Yii::$app->params['no_image'] : $item->image; ?>"
                                             class="img-fluid" alt="<?= $item->name; ?>">
                                    </div>
                                    <?php
                                    if (!empty($item->video_link)) {
                                        ?>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <br/><br/>
                                            <div class="myvideolink">
                                                <?= $item->video_link; ?>
                                            </div>
                                            
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="DescriptionHeading">
                                            <br/> <br/> <br/>
                                            <?= $item->description; ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>