<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\bootstrap\ActiveForm;

$this->title = 'Payment';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'f-login-form']]); ?>
<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 loginFormstyle paddingRightLeft">
                        <div class="col-md-12  loginFormstyle">
                            <div class="Heading text-center">
                                <?php
                                if ($response <> null && $response['response_code'] == "100") {
                                    ?>
                                    <div class="alert alert-success">
                                        <h3>Payment Successful</h3>
                                        <p>Thank you; we look forward to seeing you there.</p>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="alert alert-danger">
                                        <h3>Oooopss!! Payment Error</h3>
                                        <p><?= $response['response_message']; ?></p>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?php ActiveForm::end(); ?>