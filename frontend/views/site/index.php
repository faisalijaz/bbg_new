<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\homeSlider\HomeSliderWidget;
use frontend\widgets\latestNews\LatestNewsWidget;
use frontend\widgets\upcomingEvents\UpcomingEventsWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Welcome to BBG';
if (count((array)\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}
?>

<!-- TOP BANNER -->
<?= HomeSliderWidget::widget(); ?>
<!--<a href="https://britishbusiness.org/covid-19" target="_blank">-->
    <img style="width: 100%" src="https://d1l2trc10ppjt4.cloudfront.net/Covid-19_Bannerforbothhomepages5e9ea01c31d4a.jpg">
<!--</a>-->
<?php
if(isset($chairmna_meassage) && $chairmna_meassage <> null) {
    ?>
    <section class="MainArea chairmanmessage">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                    <div class="CMessage">
                        <h2><?= ($chairmna_meassage <> null) ? $chairmna_meassage->title : ''; ?></h2>
                        <p><?= ($chairmna_meassage <> null) ? $chairmna_meassage->short_description : ''; ?></p>
                        <?= Html::a('<button class="Mybtn pull-left" type="button" > Read More...</button>', ['/site/view-from-chair', 'id' => $chairmna_meassage->id], []); ?>
                        <!--Add buttons to initiate auth sequence and sign out-->
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2" style="padding-top: 5px;">
                    <?php
                    if ($chairmna_meassage <> null) {
                        if ($chairmna_meassage->image != "") {
                            ?>
                            <img src="<?= $chairmna_meassage->image; ?>" class="img-fluid img100 johnsImg" alt="">
                            <?php
                        } else {
                            ?>
                            <img src="<?= Yii::$app->params['ChairmanImage']; ?>" class="img-fluid img100 johnsImg" alt="">
                            <?php
                        }
                    } else {
                        ?>
                        <img src="<?= Yii::$app->params['ChairmanImage']; ?>" class="img-fluid img100 johnsImg" alt="">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>
    <!-- ############# Calling Events Carousel ####### -->
<?= UpcomingEventsWidget::widget(); ?>

    <!-- ############# Calling Events Carousel ####### -->
<?= LatestNewsWidget::widget(); ?>

    <!-- ############# Calling Members Carousel ####### -->
<?/*= CommitteeMembersWidget::widget(); */?>



<section class="socialfeeds">
    <div class="container">

        <div class="row">
            <!--<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="padding-top: 25px">
                <div class="fbplugin">
                    <h2>Facebook</h2>
                </div>

                <div class="fb-page" height=475 width=500 data-href="<? /*= (isset($siteSetting['facebook'])) ? $siteSetting['facebook'] : ''; */ ?>" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/BBGOnline/" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/BBGOnline/">British Business Group, Dubai &amp; Northern Emirates</a>
                    </blockquote>
                </div>
            </div>-->
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="fbplugin">
                    <h2>Tweets by BBG Dubai</h2>
                </div>
                </div>

            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 social_feeds force-overflow" id="style-3">
                <div class="force-overflow" id="style-3">
                    <a class="twitter-timeline" href="https://twitter.com/BBGOnline">Tweets by BBG Dubai</a>
                </div>
            </div>

        </div>
        <br/><br/><br/>
        <!--<div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 social_feeds">
                <div class="fbplugin">
                    <h2>Instagram</h2>
                </div>
                <div class="scrollable">
                    <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfsV8KXhPk0/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BfsV8KXhPk0/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">It&#39;s that time of the year again. Join our Annual Golf Day &amp; Dinner on Thursday 8 March at Trump International Golf Club. Sign up now for this or any of our other events at bbgdubai.org</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/bbgonline/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> British Business Group</a> (@bbgonline) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-02-27T08:07:46+00:00">Feb 27, 2018 at 12:07am PST</time></p></div></blockquote> <script async defer src="//www.instagram.com/embed.js"></script>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 social_feeds">
                <div class="fbplugin">
                    <h2>LinkedIn</h2>
                </div>
                <div class="scrollable">
                    <script type="IN/CompanyProfile" data-id="1043402" data-format="inline" data-related="false"></script>
                    <script type="IN/CompanyInsider" data-id="1043402" data-format="inline" data-width="400"></script>
                </div>
            </div>
        </div>-->
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<style>
    .EventSwiper {
        padding-bottom: 35px;
    }

    .socialfeeds {
        background: #ffffff;
    }

    #style-3::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar {
        width: 6px;
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar-thumb {
        background-color: #000000;
    }

    .force-overflow {
        min-height: 450px;
    }

    @media screen and (max-width: 575px) {
        .johnsImg{
            width: 50% !important;
        }
    }

    @media screen and (max-width: 767px) {
        .johnsImg{
            width: 50% !important;
        }
    }

</style>