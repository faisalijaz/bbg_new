<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;

$this->title = 'Operations Team';
$this->params['breadcrumbs'][] = $this->title;

$siteSetting = null;

if (count(\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}
?>
<? /*= TopBannerWidget::widget(); */ ?>

<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Business Team</h3>
                    </div>
                    <hr/>
                    <div class="committee text-center">
                        <div class="row">
                            <?php
                            if (isset($staff) && $staff <> null) {
                                foreach ($staff as $data) {
                                    if($data <> null) {
                                        // ["/site/operations-team-details", 'id' => $data->id]
                                        ?>
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <?= Html::a('<img src="' . $data->image . '" class="committee_staff_image img-fluid"
                                             alt="' . $data->full_name . '">',
                                                "#", ['style' => 'color: #666666 !important;', 'class' => 'committee_info', 'id' => '']); ?>
                                            <div class="name_and_postion">
                                                <h5><?= Html::a($data->full_name, '#', ['style' => 'color: #666666 !important;']); ?></h5>
                                                <h6><?= $data->designation; ?></h6>
                                            </div>
                                            <h3 class="commitee_staff_email"><?= Html::a($data->email, null, ['href' => 'mailto:' . $data->email]); ?>
                                            </h3>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="SideHeading">
                        <h3>Get In Touch</h3>
                    </div>
                    <div class="SideCalender">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
                                    <i class="fa fa-map-marker blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                         
                                        <?= (isset($siteSetting['address'])) ? $siteSetting['address'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
                                    <i class="fa fa-envelope blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                        <?php
                                        $email = isset($siteSetting['admin_email']) ? $siteSetting['admin_email'] : '';
                                        ?>

                                        <strong>Email:</strong> <?= Html::a($email, null, ['href' => $email]); ?>
                                    </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
                                    <i class="fa fa-phone blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                        <strong>Telephone:</strong> <?= (isset($siteSetting['telephone'])) ? $siteSetting['telephone'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<style>
    .blueicon {
        color: #1f3760 !important;
        font-size: 25px !important;
    }
</style>
