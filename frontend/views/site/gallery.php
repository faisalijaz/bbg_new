<?php
use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\widgets\LinkPager;
?>

<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="membershipcontent">
                    <div class="Heading">
                        <h3>Gallery Items</h3>
                    </div>
                </div>
            </div>
            <?php
            if ($gallery <> null) {
            foreach ($gallery as $data){

                ?>
                <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 PaddingTopBtm30px">
                <div class="offer-bg1 offer text-center">
                    <img src="<?= $data->image;?>" class="img-fluid" alt="">
                    <h2 style="margin-bottom: 10%;"><?= $data->name; ?></h2>
                    <span class="EventDetail">
                        <a href="/site/gallery-item?id=<?= $data->id?>">View Detail</a>
                    </span>
                </div>
            </div>
            <?php } }else{ ?>
                <div class="col-md-12">
                    <div class="col-md-12 alert alert-danger">Sorry! We're updating this section at the moment.
                        It'll be up soon!</div>
                </div>
            <?php }?>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>



