<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    a.member_link:hover{
        color: #1F3760 !important;
        font-weight: bold;
    }
</style>

<?php $form = ActiveForm::begin(['enableClientScript' => false,'id' => 'login-form', 'options' => ['class' => 'f-login-form']]); ?>
<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 loginFormstyle paddingRightLeft">
                        <?php
                        if (Yii::$app->session->getFlash('login')) {
                            ?>
                            <div class="col-md-12 alert alert-danger">
                                <?= Yii::$app->session->getFlash('login'); ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <div class="EventText alert alert-info" style="padding-top: 20px">
                                    <p>This area of our web site is for individual or company members of the BBG. If you would like to know more about becoming a member, please email us at <a class="member_link" href="mailto:info@bbgdxb.com" target="_top">info@bbgdxb.com</a> or go to <a class="member_link" href="<?= Yii::$app->params['appUrl'] . "/apply/membership"; ?>">Membership</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 loginFormstyle">

                            <div class="Heading text-center">
                                <h3>Member Login</h3>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="SiteText">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Please Enter Email'])->label(false) ?>
                                            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Enter your password'])->label(false) ?>
                                            <?= Html::submitButton('Sign In', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>
                                            <?= Html::a('Forget Password', '/site/request-password-reset', [
                                                'class' => 'Mybtn',
                                                'style' => 'padding:15px;'
                                            ]); ?>
                                        </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?php ActiveForm::end(); ?>

<?php
if (count($model->getErrors()) > 0) {

    foreach ($model->getErrors() as $error) {
        if(is_array($error)){
            foreach ($error as $err){
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        }
        else{
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}
?>