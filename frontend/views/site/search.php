<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;
?>

<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="f-login-content loginFormstyle paddingRightLeft">
                        <div class="Heading text-center">
                            <h3><?= Html::encode($this->title) ?></h3>
                        </div>
                        <div class="alert alert-info">
                            Search Results for <strong>"<?= Yii::$app->request->get('search'); ?>"</strong>
                            <!--<strong>( Results)</strong>-->
                        </div>
                    </div>
                    <h2>Events
                        <small><i class="fa fa-angle-double-right"></i> (<?= count($search_events);?>)</small>
                    </h2>
                    <div id="events" class="list-view searchResult">
                        <?php foreach ($search_events as $search_event) { ?>
                            <div class="view">
                                <a href="/events/event-details?id=<?= $search_event->id; ?>"><?= $search_event->title; ?></a>
                                <p><?= strlen($search_event->short_description) > 250 ? substr($search_event->short_description,0,250)."..." : $search_event->short_description;  ?></p>

                            </div>
                        <?php } if(empty($search_events)){ ?>
                            <div class="items">
                                <div class="alert alert-danger">No results found.</div>
                            </div>
                        <?php } ?>

                    </div>
                    <div style="clear: both;"></div>
                    <h2>News
                        <small><i class="fa fa-angle-double-right"></i> (<?= count($search_news);?>)</small>
                    </h2>
                    <div id="news" class="list-view searchResult">
                        <?php foreach ($search_news as $search_news) { ?>
                            <div class="view">
                                <a href="/news/news-details?id=<?= $search_news->id; ?>"><?= $search_news->title; ?></a>
                                <p><?= strlen($search_news->short_description) > 250 ? substr($search_news->short_description,0,250)."..." : $search_news->short_description;  ?></p>

                            </div>
                        <?php } if(empty($search_news)){ ?>
                            <div class="items">
                                <div class="alert alert-danger">No results found.</div>
                            </div>
                        <?php } ?>
                    </div>

                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>


    <style>
        .searchResult .view a {
            font-weight: bold;
        }
        #events,#news{
            background: #ffffff;
        }

        .searchResult .view {
            padding: 5px 5px 10px;
            margin-bottom: 10px;
            border-bottom: dashed 1px #ccc;
            -webkit-transition: background 0.2s;
            transition: background 0.2s;
        }
    </style>