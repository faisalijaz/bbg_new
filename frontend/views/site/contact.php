<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;

/*print_r(\Yii::$app->appSettings->getSettings());
die;*/
if (count((array)\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}
?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Contact Us</h3>
                    </div>
                    <div class="contactus PaddingTopBtm">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <h3>British Business Group Dubai & Northern Emirates</h3>
                                <p>
                                    <?= (isset($siteSetting['address'])) ? $siteSetting['address'] : ''; ?><br>
                                    <b>Tel:</b> <a
                                            href="tel:<?= (isset($siteSetting['telephone'])) ? $siteSetting['telephone'] : ''; ?>">
                                        <?= (isset($siteSetting['telephone'])) ? $siteSetting['telephone'] : ''; ?> <br>
                                    </a>

                                    <b>Email:</b> <a
                                            href="tel:<?= (isset($siteSetting['admin_email'])) ? $siteSetting['admin_email'] : ''; ?>">
                                        <?= (isset($siteSetting['admin_email'])) ? $siteSetting['admin_email'] : ''; ?>
                                    </a>
                                </p>
                                <p>
                                    <strong>Social Media:</strong> <br>
                                    <?= (isset($siteSetting['facebook']) && !empty($siteSetting['facebook'])) ? '<a class="fontsize24" target="_blank" href="' . $siteSetting['facebook'] . '">' . $siteSetting['facebook_icon'] . '</a>' : ''; ?>
                                    <?= (isset($siteSetting['twitter']) && !empty($siteSetting['twitter'])) ? '<a  class="fontsize24" target="_blank" href="' . $siteSetting['twitter'] . '">' . $siteSetting['twitter_icon'] . '</a>' : ''; ?>
                                    <?= (isset($siteSetting['linkedin']) && !empty($siteSetting['linkedin'])) ? '<a  class="fontsize24" target="_blank" href="' . $siteSetting['linkedin'] . '">' . $siteSetting['linkedin_icon'] . '</a>' : ''; ?>
                                    <?= (isset($siteSetting['instagram']) && !empty($siteSetting['instagram'])) ? '<a  class="fontsize24" target="_blank" href="' . $siteSetting['instagram'] . '">' . $siteSetting['instagram_icon'] . '</a>' : ''; ?>
                                    <?= (isset($siteSetting['youtube']) && !empty($siteSetting['youtube'])) ? '<a  class="fontsize24" target="_blank" href="' . $siteSetting['youtube'] . '">' . $siteSetting['youtube_icon'] . '</a>' : ''; ?>
                                    <?= (isset($siteSetting['pinterest']) && !empty($siteSetting['pinterest'])) ? '<a  class="fontsize24" target="_blank" href="' . $siteSetting['pinterest'] . '">' . $siteSetting['pinterest_icon'] . '</a>' : ''; ?>
                                </p>
                                <!--<p>
                                    <strong>Timings:</strong> <br>
                                    <?/*= (isset($siteSetting['about'])) ? $siteSetting['about'] : ''; */?>
                                </p>-->
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <iframe width="100%" height="300" frameborder="1" scrolling="no" marginheight="20" marginwidth="20"
                                        src="https://maps.google.com/maps?q=<?= urlencode((isset($siteSetting['location'])) ? $siteSetting['location'] : '') ?>&sensor=false&hl=en;z=14&amp;output=embed"></iframe>
                            </div>
                        </div>
                        <div class="Heading">
                            <h3>Feedback Form</h3>
                        </div>
                        <div class="row">
                            <?php $form = ActiveForm::begin(['enableClientScript' => false,'id' => 'contact-form']); ?>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 feedbackform">
                                <h2>Please fill in the following form specifying your requirement. We will get back to
                                    you shortly.</h2>

                                    <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Please enter name'])->label(false) ?>

                                    <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'Please enter email'])->label(false) ?>

                                    <?= $form->field($model, 'subject')->textInput(['autofocus' => true, 'placeholder' => 'Please enter subject'])->label(false) ?>

                                    <?= $form->field($model, 'body')->textarea(['rows' => 6, 'autofocus' => true, 'placeholder' => 'Please enter message'])->label(false) ?>

                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6 ">{input}</div></div>',
                                    ]) ?>

                                    <?= Html::submitButton('Send Email', ['class' => 'Mybtn pull-right', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>

                                    <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<style>
    .fontsize24{
        font-size: 24px !important;
    }
</style>
