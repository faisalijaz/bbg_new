<?php

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use frontend\widgets\homeBottom\HomeBottomWidget;

$this->title = 'PARTNERS AND SPONSORS';
$this->params['breadcrumbs'][] = $this->title;

?>
<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <div class="Heading">
                            <h3><?= $this->title; ?></h3>
                        </div>
                        <?php
                        if (isset($data) && $data <> null && count($data) > 0) {
                            foreach ($data as $data) {
                                ?>
                                <div class="Event1">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <img src="<?= $data->image; ?>" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                            <div class="EventText">
                                                <h4><?= $data->title; ?></h4>
                                                <p><?= strlen($data->short_description) > 240 ? substr($data->short_description , 0, 240).'...' : $data->short_description; ?></p>
                                                <span class="EventDetail">
                                                <?= Html::a('Read More...', ['/sponsors/sponsor-details', 'id' => $data->id], []); ?>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <div class="col-md-12 text-center">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                            ]);
                            ?>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>