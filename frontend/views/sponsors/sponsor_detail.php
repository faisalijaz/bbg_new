<?php

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;


$this->title = 'Sponsor Details';
$this->params['breadcrumbs'][] = $this->title;

?>

<?/*= TopBannerWidget::widget(); */?>

    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <?php
                        if (isset($data) && $data <> null) {
                            ?>
                            <div class="Heading">
                                <h3><?= $data->title; ?></h3>
                                <hr/>
                            </div>
                            <div class="EventDetail">
                                <div class="row">
                                    <?php
                                    if(!empty($data->image)) {
                                        ?>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <img src="<?= $data->image; ?>" class="img-fluid"
                                                 alt="<?= $data->title; ?>">
                                        </div>
                                        <?php
                                    }
                                        ?>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="DescriptionHeading">
                                            <h3>&nbsp;</h3>
                                            <h4><?= $data->short_description; ?></h4>
                                        </div>
                                        <div class="DescriptionHeading">
                                            <?= $data->description; ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>