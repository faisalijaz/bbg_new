<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<style>
    .TopMostSearch1 {
        margin-top: 20px;
    }

</style>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <h3>members directory</h3>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-md-offset-1 col-lg-4 col-xl-4">
                        <?php $form = ActiveForm::begin(['id' => 'member-searcher', 'enableClientScript' => false, 'method' => 'get', 'action' => '/account/member-directory', 'options' => ['class' => '']]); ?>
                        <div class="TopMostSearch1">
                            <div class="input-group">
                                <input type="search" value="<?= (Yii::$app->request->get('search')) ? Yii::$app->request->get('search') : "";?>"  name="search" class="form-control" placeholder="I’m looking for....">
                                <span class="input-group-btn">
                                    <button class="MySearchBtn btn btn-default" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" class="text-center">
                        <ul class="pagination flex-wrap">
                            <li class="page-item">
                                 <a class="page-link" href="/account/member-directory">
                                    All
                                </a>
                            </li>
                            <?php
                            $letters = range('A', 'Z');
                            foreach ($letters as $letter) {
                                ?>
                                <li class="page-item">
                                    <a class="page-link" href="/account/member-directory?search=<?= $letter; ?>">
                                        <?= $letter; ?>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="row PaddingTopBtm30px">
                    <?php
                    if ($members <> null) {
                        foreach ($members as $member) {
                            $isContact = $member->checkMemberContact(Yii::$app->user->id, $member->id);

                            $img = $member->picture;

                            if(strpos($img,'cloudfront') == false){

                                if(strpos($img,'.') == false){
                                    $img = Yii::$app->params['no_image'];
                                }else{
                                    $img = Yii::$app->params['appUrl'] .    $img;
                                }

                            }

                            ?>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <div class="directorymember">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <img src="<?= ($img <> "") ? $img : Yii::$app->params['no_image']; ?>"
                                                  class="img-fluid image_150" alt="">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                            <div class="directory" style="min-height: 120px;">
                                                <span class="pull-right memberContactIcon"
                                                      id="memberContact<?= $member->id; ?>">
                                                    <?php
                                                    if($isContact <> null){
                                                       echo Html::a('<i class="fa fa-user-times" aria-hidden="true"></i>',
                                                            '#', [
                                                                'class' => 'add_member_contact add-false',
                                                                'id' => $member->id,
                                                                'title' => "Remove",
                                                                'style' => 'color: #bd1f2f !important;'
                                                            ]);
                                                    } else{
                                                        echo Html::a('<i class="fa fa-user-plus" aria-hidden="true"></i>',
                                                            '#', [
                                                                'class' => 'add_member_contact add-true',
                                                                'id' => $member->id,
                                                                'title' => "Add to contact",
                                                                'style' => 'color: #228B22 !important;'
                                                            ]);
                                                    }
                                                    ?>

                                                </span>
                                                <h2><?= ($member <> null) ? $member->first_name . " " . $member->last_name : ""; ?></h2>
                                                <h3>
                                                    <?= ($member <> null) ? ($member->accountCompany <> null) ? $member->accountCompany->name : ""   : ""; ?><br>
                                                    <?= ($member <> null) ? $member->designation : ""; ?><br>
                                                </h3>
                                            </div>
                                            <span class="EventDetail">
                                                <a href="/account/member-details/?id=<?= ($member <> null) ? $member->id : '';?>">View Detail</a>
                                            </span>
                                            <?php
                                            if ($member <> null && $member->group_id <> 2) { ?>
                                                <span class="EventDetail">
                                                <a href="<?= ($member <> null && $member->company > 0) ? '/account/company-members/?company=' . $member->company : ''; ?>">Company Detail</a>
                                            </span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="col-md-12">
                            <div class="col-md-12 alert alert-danger">No Record Found</div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <br>
            </div>
            <div class="col-md-12 text-center">
                <?= LinkPager::widget([
                    'pagination' => $pages,
                    'options' => ['class' => 'pagination flex-wrap'],
                ]);
                ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<?= $this->registerJs('
    
    $("body").find("a.add_member_contact").click(function(e) {
     
        e.preventDefault();  
        
        $(this).prop("disabled", true);
        
        var memberId = $(this).attr("id");
        var type = "add";  
        
        if( $("#"+memberId+".add_member_contact").hasClass("add-false")){
            type = "remove"; 
        }
        
        $.ajax({
        
            url: "/account/add-member-contact",
            type: "POST",
            data: {id : memberId, type : type}, 
            success : function(res){ 
                 
                if(res == "1"){ 
                    
                    if(type == "add"){ 
                        
                        $("#"+memberId+".add_member_contact").removeClass("add-true");
                        $("#"+memberId+".add_member_contact").addClass("add-false");  
                        $("#"+memberId+".add_member_contact").css("color","#228B22","!important"); 

                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-times");
                        
                        $(this).attr("title", "Add to contact");
                        
                    } else{ 
                    
                       $("#"+memberId+".add_member_contact").removeClass("add-false");
                       $("#"+memberId+".add_member_contact").addClass("add-true"); 
                       $("#"+memberId+".add_member_contact").css("color","","!important"); 
                        
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-times"); 
                            
                        $(this).attr("title", "Remove from contact");
                    }              
                
                 } 
            },
            error : function(data){
                console.log(data);
            }
        }); 
        
        e.stopImmediatePropagation(); 
        return false;
    });
    
'); ?>



