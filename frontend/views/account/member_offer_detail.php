<?php

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;


$this->title = 'Member Offer';
$this->params['breadcrumbs'][] = $this->title;

?>

<?/*= TopBannerWidget::widget(); */?>

    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <?php
                        if (isset($member_offer_detail) && $member_offer_detail <> null) {
                            ?>

                            <div class="Heading">
                                <h3><?= $this->title; ?></h3>
                                <hr/>
                                <h4><?= $member_offer_detail->title; ?></h4>

                            </div>
                            <div class="EventDetail">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <img src="<?= (empty($member_offer_detail->image)) ? Yii::$app->params['no_image'] : $member_offer_detail->image; ?>" class="img-fluid" alt="<?= $member_offer_detail->title; ?>">
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="DescriptionHeading">
                                            <br/> <br/> <br/>
                                            <h4><?= $member_offer_detail->short_description; ?></h4>
                                            <?= $member_offer_detail->description; ?>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>