<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $associate \common\models\LoginForm */

use kartik\checkbox\CheckboxX;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Membership Application';
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        form div.required label.control-label:after {
            content: " * ";
            color: #a94442;
        }
    </style>
<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['encrypt' => 'multipart/form-data', 'class' => 'f-login-form']]); ?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                        <div class="col-md-12 ">
                            <div class="Heading text-left">
                                <h3>Edit my email subscriptions</h3><br/>
                            </div>
                        </div>
                        <?php
                        if (Yii::$app->request->get('msg') == "success") {
                            ?>
                            <div class="col-md-12 alert alert-success">
                                Great! Subscription updated.
                            </div>
                            <?php
                        }
                        ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <?= $form->field($subscriptions, 'events_news')->widget(CheckboxX::classname(), [
                                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                'pluginOptions' => [
                                    'theme' => 'krajee-flatblue',
                                    'enclosedLabel' => true,
                                    'threeState' => false,
                                ]
                            ])->label(false); ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <?= $form->field($subscriptions, 'weekly_newsletter')->widget(CheckboxX::classname(), [
                                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                'pluginOptions' => [
                                    'theme' => 'krajee-flatblue',
                                    'enclosedLabel' => true,
                                    'threeState' => false,
                                ]
                            ])->label(false); ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <?= $form->field($subscriptions, 'special_offers')->widget(CheckboxX::classname(), [
                                'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                'pluginOptions' => [
                                    'theme' => 'krajee-flatblue',
                                    'enclosedLabel' => true,
                                    'threeState' => false,
                                ]
                            ])->label(false); ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
                            <?= Html::submitButton('Update Subscriptions', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>
                        </div>
                    </div>
                </div>
            </div>
    </section>
<?php ActiveForm::end(); ?>

<?php
if (count($subscriptions->getErrors()) > 0) {

    foreach ($subscriptions->getErrors() as $error) {
        if (is_array($error)) {
            foreach ($error as $err) {
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        } else {
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}

if (Yii::$app->session->getFlash('success')) {

    echo $this->registerJs('
        swal({
            title: "Great!",
            text: "' . Yii::$app->session->getFlash('success') . '",
            timer: 5000,
            type: "success",
            html: true,
            showConfirmButton: true
        });
    ');
}

?>