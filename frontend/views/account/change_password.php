<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading" style="min-height: 500px">

                <?php $form = ActiveForm::begin(); ?>
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h3>Update Password</h3>
                    <br>
                    <div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5">
                        <?= $form->field($password, 'currentPassword')->passwordInput(['autofocus' => true, 'placeholder' => 'Enter your Current Password'])->label(false) ?>
                    </div>

                    <div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5">
                        <?= $form->field($password, 'password')->passwordInput(['autofocus' => true, 'placeholder' => 'Enter new password'])->label(false) ?>
                    </div>

                    <div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5">
                        <?= $form->field($password, 'repassword')->passwordInput(['autofocus' => true, 'placeholder' => 'Re-enter new passowrd'])->label(false) ?>
                    </div>

                    <div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5">
                        <?= Html::submitButton('Update Password', ['class' => 'mybutn btn btn-default btn-circle', 'name' => 'Password Update']) ?>
                    </div>
                    <br> <br> <br>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
