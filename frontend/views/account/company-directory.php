<?php

use common\models\Categories;
use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

?>
<?php
if (!Yii::$app->user->isGuest) {
    echo $this->render('_banner', ['model' => $model]);
} ?>
<style>
    .TopMostSearch1{
        margin-top: 20px;
    }
    .directorymember{
        background: #e2e2e2 !important;
        padding: 10px;
        margin-bottom: 25px;
        height: 230px;
    }
</style>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <h3>Company directory</h3>
                    </div>
                    <div class="col-12 col-sm-12 col-md-7 col-md-offset-1 col-lg-7 col-xl-7">
                        <?php $form = ActiveForm::begin(['id' => 'member-searcher', 'enableClientScript' => false,'method' => 'get', 'action' => '/account/company-directory', 'options' => ['class' => '']]); ?>
                        <div class="TopMostSearch1">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <?= Html::dropDownList('category', Yii::$app->request->get('category'), ArrayHelper::map(Categories::find()->where(['type' => 'general', 'status' => 1])->all(), 'id', 'title'), ['class' => 'form-control', 'prompt' => 'Select category ...']); ?>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input type="search" name="search"
                                           value="<?= (Yii::$app->request->get('search')) ? Yii::$app->request->get('search') : ""; ?>"
                                           class="form-control" placeholder="I’m looking for...." style="height: 29px;">
                                    <span class="input-group-btn">
                                    <button class="MySearchBtn btn btn-default" type="submit" style="height: 29px;">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <ul class="pagination flex-wrap">
                            <li class="page-item">
                                <a class="page-link" href="/account/company-directory">
                                    All
                                </a>
                            </li> 
                            <?php
                            $letters = range('A', 'Z');
                            foreach ($letters as $letter) {
                                ?>
                                <li class="page-item">
                                    <a class="page-link" href="/account/company-directory?search=<?= $letter; ?>">
                                        <?= $letter; ?>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                </div>
                <div class="row PaddingTopBtm30px">
                    <?php
                    if ($companies <> null) {
                        foreach ($companies as $company) {
                            $isContact = $company->checkMemberCompany(Yii::$app->user->id, $company->id);
                            ?>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <div class="directorymember">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <img  src="<?= ($company <> null && !empty($company->logo)) ? $company->logo : Yii::$app->params['no_image']; ?> " class="img-fluid image_150" alt="">
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                            <div class="directory">
                                                <span class="pull-right memberContactIcon"
                                                      id="memberContact<?= $company->id; ?>">
                                                    <?php
                                                    if ($isContact <> null) {
                                                        echo Html::a('<i class="fa fa-user-times" aria-hidden="true"></i>',
                                                            '#', [
                                                                'class' => 'add_member_contact add-false',
                                                                'id' => $company->id,
                                                                'title' => "Remove",
                                                                'style' => 'color: #bd1f2f !important;'
                                                            ]);
                                                    } else {
                                                        echo Html::a('<i class="fa fa-user-plus" aria-hidden="true"></i>',
                                                            '#', [
                                                                'class' => 'add_member_contact add-true',
                                                                'id' => $company->id,
                                                                'title' => "Add to contact",
                                                                'style' => 'color: #228B22 !important;'
                                                            ]);
                                                    }
                                                    ?>

                                                </span>
                                                <h2><?= trim($company->name); ?></h2>
                                                <h3>
                                                    <i class="fa fa-map-marker"></i> <?= trim($company->address); ?>
                                                    <br/>
                                                    <i class="fa fa-tags"></i> <?= ($company->companyCategory <> null) ? "<span style='color: #bd1f2f !important'>" . $company->companyCategory->title . "</span>" : ""; ?>

                                                </h3>

                                                <p><?= substr(trim($company->about_company), 0, 80) . " ..."; ?></p>
                                                <span class="EventDetail">
                                                    <a href="/account/company-members/?company=<?= $company->id;?>">Read More...</a></span>
                                                <?php
                                                if (!Yii::$app->user->isGuest) {
                                                    ?>
                                                    <span class="EventDetail"><a
                                                                href="/account/company-members/?company=<?= $company->id; ?>#CompanyMembers">View Members</a></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>
                        <div class="col-md-12 text-center">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                                'options' => ['class' => 'pagination flex-wrap'],
                            ]);
                            ?>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-md-12">
                            <div class="col-md-12 alert alert-danger">No Record Found</div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <br>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<?= $this->registerJs('
    
    $("body").find("a.add_member_contact").click(function(e) {
     
        e.preventDefault();  
        
        $(this).prop("disabled", true);
        
        var memberId = $(this).attr("id");
        var type = "add";  
        
        if( $("#"+memberId+".add_member_contact").hasClass("add-false")){
            type = "remove"; 
        }
        
        $.ajax({
        
            url: "/account/add-member-company",
            type: "POST",
            data: { id : memberId, type : type }, 
            success : function(res){ 
                 
                if(res == "1"){ 
                    
                    if(type == "add"){ 
                        
                        $("#"+memberId+".add_member_contact").removeClass("add-true");
                        $("#"+memberId+".add_member_contact").addClass("add-false");  
                        
                        $("#"+memberId+".add_member_contact").removeClass("colorAdd");
                        $("#"+memberId+".add_member_contact").addClass("colorRem");  
                         

                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-times");
                        
                        
                        
                        $(this).attr("title", "Add to contact");
                        
                    } else{ 
                    
                       $("#"+memberId+".add_member_contact").removeClass("add-false");
                       $("#"+memberId+".add_member_contact").addClass("add-true"); 
                        
                        $("#"+memberId+".add_member_contact").removeClass("colorRem");
                       $("#"+memberId+".add_member_contact").addClass("colorAdd"); 
                        
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-times"); 
                            
                        $(this).attr("title", "Remove from contact");
                    }              
                } 
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
'); ?>




