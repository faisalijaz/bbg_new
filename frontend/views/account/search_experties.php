<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Search Results</h3>
                    </div>
                    <hr/>
                    <div class="committee text-center">
                        <div class="row">
                            <?php
                            if ($dataProvider <> null) {
                                foreach ($dataProvider as $member) {
                                        $img = $member->picture;
                                        if($img && strpos($member->picture,'.' == false)){
                                            $img = Yii::$app->params['no_image'];
                                        }else{
                                            $img = Yii::$app->params['no_image'];
                                        }
                                        ?>
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
                                            <img src="<?= $img; ?>"
                                                 class="committee_staff_image img-fluid"
                                                 alt="<?= $member->first_name . " " . $member->last_name; ?>">
                                            <div class="name_and_postion">
                                                <h5><?= Html::a($member->first_name . " " . $member->last_name,
                                                        [
                                                            '/account/member-details/', 'id' => $member->id, 'type' => Yii::$app->request->get('search_request_experties')
                                                        ],
                                                        [

                                                        ]); ?></h5>
                                                <p><strong><?= substr($member->designation, 0, 40); ?></strong></p>
                                                <h6 ><?= ($member->accountCompany <> null) ? "<a style='color:#cd2c47;' href='/account/company-members/?company=".$member->accountCompany->id."'>" .substr($member->accountCompany->name ."</a>", 0, 40) : ""; ?></h6>
                                            </div>
                                            <h3 class="commitee_staff_email">
                                                <i class="fa fa-envelope"
                                                   aria-hidden="true"></i> <?= Html::a($member->email, null, ['href' => 'mailto:' . $member->email]); ?>
                                            </h3>
                                        </div>
                                    <?php
                                }
                            } ?>
                        </div>
                    </div>
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>

<style>
    .searchResult .view a {
        font-weight: bold;
    }
    #events,#news{
        background: #ffffff;
    }
    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
</style>