<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $additional \common\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Membership Application';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
    <style>
        form div.required label.control-label:after {
            content: " * ";
            color: #a94442;
        }
    </style>
<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'f-login-form']]); ?>
<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                        <div class="col-md-12 ">
                            <div class="Heading text-left">
                                <h3>Additional Member</h3><br/>
                            </div>
                            <div class="row">

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'account_type')->dropDownList([
                                        'additional' => 'Additional Member',
                                        'alternate' => 'Alternate Member'
                                    ])->label("Member Type") ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'title')->dropDownList(Yii::$app->params['userTitle'], ['prompt' => 'Select ...'])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'user_name')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'first_name')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'last_name')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'email')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'phone_number')->textInput([])->label('Mobile') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'designation')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'country_code')->dropDownList(
                                        \yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'country_name'),
                                        ['prompt' => 'Select ...'])->label('Country') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'city')->textInput([])->label() ?>
                                </div>

                                <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'address')->textInput([])->label() ?>
                                </div>


                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($additional, 'nationality')->textInput([])->label() ?>
                                </div>

                            </div>



                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                        <label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of UAE Trade Licence.</label>
                                        <div class="form-group">
                                            <a href="javascript:;"
                                               id="upload-document1"
                                               onclick="uploadAttachment(1)"
                                               data-toggle="tooltip"
                                               class="img-thumbnail"
                                               title="Upload Document">
                                                <i></i>
                                                <img src="<?= ($additional->trade_licence) ? $additional->trade_licence : Yii::$app->params['no_image']; ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
                                                <br/>
                                            </a>
                                            <p class="image_upload_label">click Image to upload</p>
                                            <?= $form->field($additional, 'trade_licence')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment1'])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                        <label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of passport</label>
                                        <div class="form-group">
                                            <a href="javascript:;"
                                               id="upload-document2"
                                               onclick="uploadAttachment(2)"
                                               data-toggle="tooltip"
                                               class="img-thumbnail"
                                               title="Upload Document">
                                                <i></i>
                                                <img src="<?= ($additional->passport_copy) ? $additional->passport_copy : Yii::$app->params['no_image']; ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
                                                <br/>
                                            </a>
                                            <p class="image_upload_label">click Image to upload</p>
                                            <?= $form->field($additional, 'passport_copy')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment2'])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                        <label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of residence visa</label>
                                        <div class="form-group">
                                            <a href="javascript:;"
                                               id="upload-document3"
                                               onclick="uploadAttachment(3)"
                                               data-toggle="tooltip"
                                               class="img-thumbnail"
                                               title="Upload Document">
                                                <i></i>
                                                <img src="<?= ($additional->residence_visa) ? $additional->residence_visa : Yii::$app->params['no_image']; ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
                                                <br/>
                                            </a>
                                            <p class="image_upload_label">click Image to upload</p>
                                            <?= $form->field($additional, 'residence_visa')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment3'])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                        <label class="docs_upload_label control-label" for="signupform-trade_licence">Passport size photograph (upload as JPEG)</label>
                                        <div class="form-group">
                                            <a href="javascript:;"
                                               id="upload-document4"
                                               onclick="uploadAttachment(4)"
                                               data-toggle="tooltip"
                                               class="img-thumbnail"
                                               title="Upload Document">
                                                <i></i>
                                                <img src="<?= ($additional->passport_size_pic) ? $additional->passport_size_pic : Yii::$app->params['no_image']; ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
                                                <br/>
                                            </a>
                                            <p class="image_upload_label">click Image to upload</p>
                                            <?= $form->field($additional, 'passport_size_pic')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment4'])->label(false) ?>
                                        </div>
                                    </div>
                                </div>



                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
                            <?= Html::submitButton('Add Member', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>
                        </div>
                    </div>
                </div>
            </div>

    </section>
<?php ActiveForm::end(); ?>

<?php
if (count($additional->getErrors()) > 0) {

    foreach ($additional->getErrors() as $error) {
        if (is_array($error)) {
            foreach ($error as $err) {
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        } else {
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}

echo $this->registerJs('
    
    $("#additonal-account_type").change(function(){
       
        console.log(1);
       
        if($(this).val() == "alternate"){
            $("#div-documents").addClass("hidden");
        }else{
            $("#div-documents").removeClass("hidden");
        }
    });
    
');
?>
<script>
    var uploadAttachment = function (attachmentId) {

        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function() {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function(json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);
                            $('#upload-document' + attachmentId + ' img').prop('src', json.href);
                            $('#input-attachment' + attachmentId).val(json.href);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }
</script>
