<?php

/* @var $this yii\web\View */

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;

$this->title = 'My Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<link rel="stylesheet" type="text/css" href="/theme/bbg/resources/css/checkbox-style.css" />
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>My Profile</h3>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Personal Information
                        <span class="float-right">
                             <?= Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit ', ['/account/update'], []) ?>

                        </span>
                    </h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <img src="<?= ($model->picture) ? $model->picture : Yii::$app->params['no_image']; ?>"
                                 class="img-fluid" alt="">
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="profile">
                                <h2><?= ($model <> null) ? $model->first_name . " " . $model->last_name : ""; ?></h2>
                                <h3><?= ($model <> null) ? $model->designation : ""; ?></h3>
                                <p>
                                    <strong>Email</strong>
                                    - <?= ($model <> null) ? Html::a($model->email, null, ['href' => 'mailto:' . $model->email]) : ""; ?>
                                    <br>
                                  <!--  <strong>Secondary Email</strong>
                                    - <?/*= ($model <> null) ? Html::a($model->secondry_email, null, ['href' => 'mailto:' . $model->secondry_email]) : ""; */?>
                                    <br>-->
                                    <strong>Contact Number</strong>
                                    - <?= ($model <> null) ? Html::a($model->phone_number, null, ['href' => 'tel:' . $model->phone_number]) : ""; ?>
                                    <br>
                                    <strong>Address</strong> - <?= ($model <> null) ? $model->address : ""; ?> <br>
                                    <strong>Linkedin</strong>
                                    - <?= ($model <> null) ? "<a href='" . $model->linkedin . "' >" . $model->linkedin . "</a>" : ""; ?>
                                    <br>
                                    <strong>Twitter URL</strong>
                                    - <?= ($model <> null) ? "<a href='" . $model->twitter . "' >" . $model->twitter . "</a>" : ""; ?>
                                    <br>
                                    <strong>Company Type</strong> - <?php
                                    if ($model->accountCompany <> null && $model->accountCompany->companyCategory <> null) {
                                        echo $model->accountCompany->companyCategory->title;
                                    } else {
                                        echo "";
                                    }; ?> <br/>
                                </p>
                            </div>
                        </div>
                        <div class="profile col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <p>
                            <strong>Status</strong>
                            - <?=  ($model->status <> null && $model->status <> "") ? \Yii::$app->params['statusTitle'][$model->status] : "";
                            ?>                             <br>

                            <strong>Group</strong>
                            - <?=  ($model->group <> null ) ? $model->group->title : "";
                            ?>                             <br>
                            <strong>Account Type</strong>
                            - <?=  ($model->account_type <> null) ? \Yii::$app->params['accountTypes'][$model->account_type] : "";
                            ?>                             <br>
                            <strong>Registration Date</strong>
                            - <?=  ($model->registeration_date <> null ) ? formatDate($model->registeration_date) : "";
                            ?>                             <br>

                            <strong>Expiry Date</strong>
                            - <?=  ($model->expiry_date <> null ) ? formatDate($model->expiry_date) : "";
                            ?>                             <br>

                            <strong>Last Renewal Date</strong>
                            - <?=  ($model->last_renewal <> null ) ? formatDate($model->last_renewal) : "";
                            ?>                             <br>

                        </p>
                        </div>
                    </div>
                </div>
                <?php
                if ($model->group <> null) {
                    if ($model->group->show_company && $model->accountCompany <> null) {

                        $company = $model->accountCompany;

                        ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                            <h2>
                                Company Information
                                <span class="float-right">
                                    <a href="/membership/update"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a>
                                </span>
                            </h2>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                            <div class="row PaddingTopBtm30px">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <img src="<?= ($company <> null && !empty($company->logo)) ? $company->logo : Yii::$app->params['no_image']; ?> " class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                                    <div class="profile">
                                        <h2><?= ($company <> null) ? $company->name : ""; ?></h2>
                                        <h3><?= ($company <> null) ? $company->address : ""; ?></h3>
                                        <p>
                                            <strong>Company Type</strong> -
                                            <?= ($company->companyCategory <> null) ? $company->companyCategory->title : ""; ?>
                                            <br/>
                                           <!-- <?php
/*                                            if (!Yii::$app->user->isGuest) {
                                                */?>
                                                <strong>VAT Number</strong> -
                                                <?/*= ($company <> null) ? $company->vat_number : ""; */?><br/>
                                                --><?php
/*                                            }
                                            */?>
                                            <strong>Url</strong>
                                            - <?= ($company <> null) ? Html::a($company->url, $company->url, []) : ""; ?>
                                            <br>
                                            <strong>Contact Number</strong>
                                            - <?= ($company <> null) ? $company->phonenumber : ""; ?><br>
                                            <strong>Fax</strong> - <?= ($company <> null) ? $company->fax : ""; ?><br>
                                            <strong>Emirates</strong>
                                            - <?= ($company <> null) ? $company->emirates_number : ""; ?><br>
                                            <strong>Postal Code</strong>
                                            - <?= ($company <> null) ? $company->postal_code : ""; ?><br>
                                            <strong>Address</strong>
                                            - <?= ($company <> null) ? $company->address : ""; ?>
                                        </p>
                                        <p>
                                        <h2>About Company</h2>
                                        <?= ($company <> null) ? $company->about_company : ""; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Interests
                        <!--<span class="float-right">
                            <a href="#">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                            </a>
                        </span>-->
                    </h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="profile text-left">
                                <div class="col-md-12">
                                    <form action="/account/save-intrested-categories" method="post"
                                          id="interested_category_form">
                                        <?php

                                        if ($interested_categories <> null && count($interested_categories) > 0) {
                                            foreach ($interested_categories as $cat) {
                                                $selected = "";

                                                if (array_key_exists($cat->id, $categories_selected)) {
                                                    $selected = "checked";
                                                }
                                                ?>
                                                <div class="col-md-4 form-group">
                                                    <label class="label_checkbox_class"
                                                           for="interested_category_<?= $cat->id ?>">
                                                        <?= $cat->title; ?>
                                                        <input name="interested_category[]" <?= $selected; ?>
                                                               id="interested_category_<?= $cat->id ?>" class="styled"
                                                               value="<?= $cat->id ?>" class="category_checkbox"
                                                               type="checkbox"/>
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <?php
                                            }
                                        } ?>
                                </div>
                                <div class="col-md-12" style="margin-top: 50px;float: right;">
                                    <?= Html::button('Update', [
                                        'class' => 'Mybtn pull-right',
                                        'id' => 'updateIntCat'
                                    ]); ?>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
            </div>

        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<?= $this->registerJs('
    
    $("#updateIntCat").unbind().click(function(e){
        
        e.preventDefault(); 
        $("#updateIntCat").attr("disabled", true);
        var form = $("#interested_category_form");
         
         $.ajax({ 
              url: form.attr("action"),
              type: "post",
              data: form.serialize(),
              success: function (response) { 
                    swal({
                        title: "Success",
                        text: "Data Saved",
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    },
                    function(){
                        window.location.reload(true);
                    }); 
                   $("#updateIntCat").attr("disabled", false); 
              },
              error : function (response, error) { 
                  swal({
                        title: "Error",
                        text: response.responseText,
                        timer: 10000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                  }); 
                  $("#updateIntCat").attr("disabled", false); 
              } 
         });
         
         return false;
        
    });
    
'); ?>



