<?php
    use frontend\widgets\homeBottom\HomeBottomWidget;
?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<style>
    .darkbg {
        background: #e2e2e2;
        min-height: 270px;
        margin: 10px 0px;
    }
</style>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>member’s contact</h3>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Personal Contact Information</h2>
                    </div>
                </div>

                <?php
                if ($model->memberContacts <> null) {
                    foreach ($model->memberContacts as $contact) {
                        if ($contact->memberInfo <> null) {
                            $info = $contact->memberInfo;

                            $img = $info->picture;

                            if(strpos($img,'cloudfront') == false){

                                if(strpos($img,'.') == false){
                                    $img = Yii::$app->params['no_image'];
                                }else{
                                    $img = Yii::$app->params['appUrl'] .    $img;
                                }

                            }

                            ?>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
                                <div class="PaddingTopBtm30px darkbg ">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <img src="<?= ($img <> "" ) ? $img : Yii::$app->params['no_image']; ?>"
                                             class="img-fluid" alt="Image">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                        <div class="profile">
                                            <h2><?= ($info <> null) ? \yii\helpers\Html::a($info->first_name . " " . $info->last_name,'/account/member-details/?id=' . $info->id,[]) : ""; ?></h2>
                                            <h3><?= ($info <> null) ? $info->designation : ""; ?>  <?= ($info->accountCompany <> null) ? $info->accountCompany->name : ""; ?></h3>
                                            <p>
                                                <strong>Designation</strong> - <?= ($info <> null) ? $info->designation : ""; ?> <br>
                                                <strong>Company Name</strong> - <?= ($info->accountCompany <> null) ? " - at " . $info->accountCompany->name : ""; ?> <br>
                                                <strong>Mobile Number</strong> - <?= ($info <> null) ? $info->phone_number : ""; ?> <br>
                                                <strong>Email Address</strong> - <?= ($info <> null) ? $info->email : ""; ?> <br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
                <br>
            </div>

        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>