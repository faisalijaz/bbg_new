<?php

use yii\helpers\Html;

$this->title = ($model <> null) ? $model->first_name . " " . $model->last_name . " - Profile": "My Profile";
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .topnav1 a {
        padding: 14px 10px;
    }
</style>
<section class="bannerBG usertopdetail">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 Tcenter">
                <img src="<?= (!empty($model->picture)) ? $model->picture : Yii::$app->params['no_image']; ?>"
                     class="hidden-md-down img-fluid PaddingTopBtm30px" alt="">
            </div>
            <div class="hidden-md-down col-6 col-sm-6 col-md-8 col-lg-8 col-xl-8 Tcenter">
                <div style="position: unset !important; padding-top: 25px;" class="BannerText">
                    <h1><?= ($model <> null) ? $model->first_name . " " . $model->last_name : ""; ?></h1>
                    <h2>
                        <?= ($model <> null) ? $model->designation: ""; ?> <br>
                        <?= ($model <> null) ? $model->address: ""; ?> <br>
                    </h2>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 Tcenter">
                <div class="editprofilebtn">
                    <?= Html::a('<i class="fa fa-lock" aria-hidden="true"></i> Change Password',['/account/change-password'],[]) ?>
                    <br/><?= Html::a('<i class="fa fa-user" aria-hidden="true"></i> Edit Profile',['/account/update'],[]) ?>
                </div>
            </div>
            <div class="col-6  hidden-lg-up">
                <div id="mySidenav1" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav1()">&times;</a>

                    <?php
                    if ($model->renw_invoice_gen) {
                        echo Html::a('Renew Membership', ['/account/renew-membership'], []);
                    }
                    ?>
                    <?= Html::a('My Email Subscriptions', ['/account/edit-email-subscriptions'], []); ?>
                    <?= Html::a('Company Members', ['/account/my-company-members'], []) ?>
                    <? /*= Html::a('Member Searches',['/account/expert-requests'],[]) */ ?>
                    <?php
                    if ($model->group_id == 2) {
                        ?>
                        <?= Html::a('Upgrade Membership', ['/account/membership-upgrade'], []) ?>
                        <?php
                    }
                    ?>
                    <? /*= Html::a('Payment History',['/account/payments'],[]) */ ?>
                    <?= Html::a('Payment History', ['/account/invoices'], []) ?>
                    <?= Html::a('My Contacts', ['/account/member-contacts'], []) ?><!--
                    --><?/*= Html::a('Member Offers',['/account/member-offers'],[]) */?>
                    <?= Html::a('Events Registered', ['/account/events-registered'], []) ?>
                    <?= Html::a('Jobs Portal', ['/account/jobs-portal'], ['class' => 'jobsPortalBtn pull-left',]) ?>
                </div>
                <div class="text-right">
                    <span style="color:#fff;font-size:30px;cursor:pointer;" onclick="openNav1()">&#9776;</span>
                </div>

            </div>
        </div>
        <div class="row whiteline text-right">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="hidden-md-down topnav1" id="myTopnav">
                    <?php
                    if ($model->renw_invoice_gen) {
                        echo Html::a('Renew Membership', ['/account/renew-membership'], []);
                    }
                    ?>
                    <?= Html::a('My Email Subscriptions', ['/account/edit-email-subscriptions'], []); ?>
                    <?= Html::a('Company Members', ['/account/my-company-members'], []) ?>
                    <? /*= Html::a('Member Searches',['/account/expert-requests'],[]) */ ?>
                    <?php
                    if ($model->group_id == 2) {
                        ?>
                        <?= Html::a('Upgrade Membership', ['/account/membership-upgrade'], []) ?>
                        <?php
                    }
                    ?>
                    <? /*= Html::a('Payment History',['/account/payments'],[]) */ ?>
                    <?= Html::a('Payment History', ['/account/invoices'], []) ?>
                    <?= Html::a('My Contacts', ['/account/member-contacts'], []) ?><!--
                    --><? /*= Html::a('Member Offers',['/account/member-offers'],[]) */ ?>
                    <?= Html::a('Events Registered', ['/account/events-registered'], []) ?>
                    <?= Html::a('Jobs Portal', ['/account/jobs-portal'], ['class' => 'jobsPortalBtn pull-left',]) ?>
                </div>

        </div>
    </div>
</section>
<script>

    function openNav1() {
        document.getElementById("mySidenav1").style.width = "100%";
        document.getElementById("main").style.marginLeft = "100%";
    }

    function closeNav1() {
        document.getElementById("mySidenav1").style.width = "0";
        document.getElementById("main").style.marginLeft = "0";
    }
</script>