<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center">
                    <h3>Job Posts</h3>
                </div>
                <div class="form-group no-more-tables" style=" overflow-x:auto;">
                    <?= Html::a('<i class="fa fa-plus"></i> Post New Job', '/account/post-jobs', ['class' => 'Mybtn pull-right', 'style' => "margin:10px"]); ?>

                    <table id="memberEventsRegTable"
                           class="table display responsive no-wrap dt-responsive nowrap d-lg-table d-md-table d-sm-table d-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Job Title</th>
                            <th>Position</th>
                            <th>Experience</th>
                            <th>Posted On</th>
                            <th>Apply till</th>
                            <th>Contact Name</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($jobs <> null) {
                            $count = 1;
                            foreach ($jobs as $job) {

                                ?>

                                <tr>
                                    <td class="text-center"><?= $count++; ?></td>
                                    <td class="text-center">
                                        <?= $job->job_title; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $job->position; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $job->experience; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $job->posted_on; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $job->apply_by; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $job->contact_name; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= ($job->published) ? '<span class="label label-success">Published</span>' : '<span class="label label-danger">Un-Published</span>'; ?>
                                    </td>
                                    <td class="invoicelink text-center">
                                        <?=  Html::a(
                                                '<i class="fa fa-eye"></i>',
                                                ['/site/job-post-detail', 'id' => $job->id],
                                                [
                                                    'target' => '_blank',
                                                    'Title' => 'View Details',
                                                    'class' => 'btn-sm Mybtn',
                                                    'style' => 'color:#fff !important',
                                                    'data' => [
                                                        'method' => 'post'
                                                    ],
                                                ]
                                            );

                                        ?>
                                        <?=  Html::a(
                                            '<i class="fa fa-pencil"></i>',
                                            ['/account/post-jobs', 'id' => $job->id],
                                            [
                                                'target' => '_blank',
                                                'Title' => 'Edit',
                                                'class' => 'btn-sm Mybtn',
                                                'style' => 'color:#fff !important',
                                                'data' => [
                                                    'method' => 'post'
                                                ],
                                            ]
                                        );

                                        ?>
                                        <?=  Html::a(
                                                '<i class="fa fa-trash"></i>',
                                                ['/account/delete-job', 'id' => $job->id],
                                                [
                                                    'target' => '_blank',
                                                    'Title' => 'Delete',
                                                    'class' => 'btn-sm Mybtn',
                                                    'style' => 'color:#fff !important',
                                                    'data' => [
                                                        'method' => 'post'
                                                    ],
                                                ]
                                            );

                                        ?>

                                    </td>
                                </tr>

                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <br>
                    <div class="col-md-12 text-center pull-right">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

