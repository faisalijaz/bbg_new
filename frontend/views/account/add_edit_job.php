<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $additional \common\models\LoginForm */

use kartik\widgets\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Post Job';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<style>
    form div.required label.control-label:after {
        content: " * ";
        color: #a94442;
    }
</style>
<?php $form = ActiveForm::begin(['id' => 'jobModel', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'f-login-form']]); ?>
<? /*= TopBannerWidget::widget(); */ ?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                    <div class="col-md-12 ">

                        <div class="Heading text-left">
                            <h3>Post Job</h3><br/>
                        </div>
                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'job_title')->textInput([])->label() ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'position')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'industry')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'skills')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'experience')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'location')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'posted_on')->widget(DatePicker::classname(), [
                                    'options' => ['placeholder' => 'Registration end date'],
                                    'type' => DatePicker::TYPE_INPUT,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-MM-dd'
                                    ]
                                ]);
                                ?>
                                <?= $form->field($jobModel, 'posted_on')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'apply_by')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'auto_remove_after')->textInput([])->label() ?>
                            </div>

                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'contact_name')->textInput([])->label() ?>
                            </div>


                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'contact_email')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'contact_phone')->textInput([])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <?= $form->field($jobModel, 'published')->dropDownList(['1' => 'Published', '0' => 'Un-Published', 'placeholder' => 'Select ..'])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($jobModel, 'short_description')->textarea(['rows' => 6])->label() ?>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?= $form->field($jobModel, 'description')->textarea(['rows' => 6, 'style' => 'height:150px !important;'])->label() ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
                        <?= Html::submitButton('Post Job', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>
                    </div>
                </div>
            </div>
        </div>

</section>
<?php ActiveForm::end(); ?>

<?php
if (count($jobModel->getErrors()) > 0) {

    foreach ($jobModel->getErrors() as $error) {
        if (is_array($error)) {
            foreach ($error as $err) {
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        } else {
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}


