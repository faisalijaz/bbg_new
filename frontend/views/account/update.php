<?php

use frontend\widgets\rightSidebar\RightSidebarWidget;

$this->title = 'Edit Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>

<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <div class="detail-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea">
                                <div class="simple-group">
                                    <div class="row">
                                        <?= $this->render('_form', [
                                            'model' => $model,
                                            'countries' => $country
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>