<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>Member Searches</h3>
                <p>This is a summary of the searches you or your company have appeared in.</p>
                <br>
                <table class="table table-striped table-responsive">
                    <thead>

                    <tr>
                        <th>Date</th>
                        <th>Search Keyword</th>
                        <th>User Name</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th>Phone</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if ($requests <> null) {
                        foreach ($requests as $request) {

                            $user = null;

                            if($request->searchBy <> null){
                                $user = $request->searchBy;
                            }
                            ?>
                            <tr>
                                <th scope="row"> <?= formatDate($request->date); ?> </th>
                                <td class="invoicelink"><?= ($request <> null) ? $request->search_keyword : ""; ?></td>
                                <td><?= ($user <> null) ? $user->first_name . " " .$user->last_name : ""; ?></td>
                                <td><?= ($user <> null && $user->accountCompany <> null) ? $user->accountCompany->name : ""; ?></td>
                                <td><?= ($user <> null) ? $user->email : ""; ?></td>
                                <td><?= ($user <> null) ? $user->phone_number : ""; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <br>
                <div class="col-md-12 text-center pull-right">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>

        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

