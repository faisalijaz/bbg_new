<?php

use yii\helpers\Html;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<style>
    .TopMostSearch1 {
        margin-top: 20px;
    }
</style>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <h3>Member Details</h3>
                    </div>

                </div>
                <div class="row PaddingTopBtm30px">
                    <?php
                    if ($member <> null) {
                            ?>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                                <h2>Personal Information</h2>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                                <div class="row PaddingTopBtm30px">
                                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">

                                        <?php

                                        $img = $member->picture;

                                        if(strpos($img,'cloudfront') == false){

                                            if(strpos($img,'.') == false){
                                                $img = Yii::$app->params['no_image'];
                                            }else{
                                                $img = Yii::$app->params['appUrl'] .    $img;
                                            }

                                        }

                                        ?>

                                        <img src="<?= ($img <> null) ? $img : Yii::$app->params['no_image']; ?>"
                                             class="img-fluid" alt="">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                                        <div class="profile">
                                            <h2><?= ($member <> null) ? $member->first_name . " " . $member->last_name : ""; ?></h2>

                                            <?php
                                            if ($member <> null && $member->group_id <> 2) { ?>

                                                <h3><?= ($member <> null) ? $member->designation : ""; ?></h3>

                                                <?php
                                            }
                                            ?>

                                            <p>
                                                <strong>Email</strong>
                                                - <?= ($member <> null) ? Html::a($member->email, null, ['href' => 'mailto:' . $member->email]) : ""; ?>

                                               <!-- <?php
/*                                                if ($member <> null && $member->group_id <> 2) { */?>
                                                    <br>
                                                    <strong>Secondary Email</strong>
                                                    - <?/*= ($member <> null) ? Html::a($member->secondry_email, null, ['href' => 'mailto:' . $member->secondry_email]) : ""; */?>
                                                    --><?php
/*                                                }
                                                */?>

                                                <br>
                                                <strong>Contact Number</strong>
                                                - <?= ($member <> null) ? Html::a($member->phone_number, null, ['href' => 'tel:' . $member->phone_number]) : ""; ?>

                                                <?php
                                                if ($member <> null && $member->group_id <> 2) { ?>

                                                    <br>
                                                    <strong>Address</strong> - <?= ($member <> null) ? $member->address : ""; ?>
                                                    <?php
                                                }
                                                ?>

                                                <br>
                                                <strong>Linkedin</strong>
                                                - <?= ($member <> null) ? "<a href='" . $member->linkedin . "' >" . $member->linkedin . "</a>" : ""; ?>
                                                <br>
                                                <strong>Twitter URL</strong>
                                                - <?= ($member <> null) ? "<a href='" . $member->twitter . "' >" . $member->twitter . "</a>" : ""; ?>

                                                <?php
                                                if ($member <> null && $member->group_id <> 2) { ?>

                                                    <br>
                                                    <strong>Company Type</strong> - <?php
                                                    if ($member->accountCompany <> null && $member->accountCompany->companyCategory <> null) {
                                                        echo $member->accountCompany->companyCategory->title;
                                                    } else {
                                                        echo "";
                                                    }
                                                  ?> <br/><!---->
                                                    <?php
                                                }
                                                ?>

                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <?php
                        if ($member->group <> null) {
                            if ($member->group->show_company && $member->accountCompany <> null) {
                                $company = $member->accountCompany;

                                if ($member <> null && $member->group_id <> 2) {
                                    ?>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                                        <h2>
                                            Company Information
                                            <span class="float-right">
                                    <a href="#"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a>
                                </span>
                                        </h2>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                                        <div class="row PaddingTopBtm30px">
                                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                                <img src="<?= ($company <> null && !empty($company->logo)) ? $company->logo : Yii::$app->params['no_image']; ?>"
                                                     class="img-fluid" alt="">
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                                                <div class="profile">
                                                    <h2>
                                                        <a href="/account/company-members/?company=<?= ($company <> null) ? $company->id : ""; ?>"> <?= ($company <> null) ? $company->name : ""; ?></a>
                                                    </h2>
                                                    <h3><?= ($company <> null) ? $company->address : ""; ?></h3>
                                                    <p>
                                                        <strong>Company Type</strong> -
                                                        <?= ($company->companyCategory <> null) ? $company->companyCategory->title : ""; ?>
                                                        <br/>
                                                     <!--   <?php
/*                                                        if (!Yii::$app->user->isGuest) {
                                                            */?>
                                                            <strong>VAT Number</strong> -
                                                            <?/*= ($company <> null) ? $company->vat_number : ""; */?>
                                                            <br/>
                                                            --><?php
/*                                                        }
                                                        */?>
                                                        <strong>Url</strong>
                                                        - <?= ($company <> null) ? Html::a($company->url, $company->url, []) : ""; ?>
                                                        <br>
                                                        <strong>Contact Number</strong>
                                                        - <?= ($company <> null) ? $company->phonenumber : ""; ?>
                                                        <br>

                                                        <strong>Emirates</strong>
                                                        - <?= ($company <> null) ? $company->emirates_number : ""; ?>
                                                        <br>
                                                        <strong>Postal Code</strong>
                                                        - <?= ($company <> null) ? $company->postal_code : ""; ?>
                                                        <br>
                                                        <strong>Address</strong>
                                                        - <?= ($company <> null) ? $company->address : ""; ?>
                                                    </p>
                                                    <p>
                                                    <h2>About Company</h2>
                                                    <?= ($company <> null) ? $company->about_company : ""; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                            <?php

                    } else {
                        ?>
                        <div class="col-md-12">
                            <div class="col-md-12 alert alert-danger">No Record Found</div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <br>
            </div>
        </div>
    </div>
</section>
<?= $this->registerJs('
    
    $("body").find("a.add_member_contact").click(function(e) {
     
        e.preventDefault();  
        
        $(this).prop("disabled", true);
        
        var memberId = $(this).attr("id");
        var type = "add";  
        
        if( $("#"+memberId+".add_member_contact").hasClass("add-false")){
            type = "remove"; 
        }
        
        $.ajax({
        
            url: "/account/add-member-contact",
            type: "POST",
            data: {id : memberId, type : type}, 
            success : function(res){ 
                 
                if(res == "1"){ 
                    
                    if(type == "add"){ 
                        
                        $("#"+memberId+".add_member_contact").removeClass("add-true");
                        $("#"+memberId+".add_member_contact").addClass("add-false");  
                        $("#"+memberId+".add_member_contact").css("color","#228B22","!important"); 

                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-times");
                        
                        $(this).attr("title", "Add to contact");
                        
                    } else{ 
                    
                       $("#"+memberId+".add_member_contact").removeClass("add-false");
                       $("#"+memberId+".add_member_contact").addClass("add-true"); 
                       $("#"+memberId+".add_member_contact").css("color","","!important"); 
                        
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-times"); 
                            
                        $(this).attr("title", "Remove from contact");
                    }              
                
                 } 
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
'); ?>



