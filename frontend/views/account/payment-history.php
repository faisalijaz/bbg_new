<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>Payment History</h3>
                <br>
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Payment ID</th>
                        <th>Invoice ID</th>
                        <th>User</th>
                        <th>Company</th>
                        <th>Transaction No</th>
                        <th>Amount (AED)</th>
                        <th>Payment Method</th>
                        <th>Payment Status</th>
                        <th>Payment Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($payments <> null) {
                        foreach ($payments as $payment) {
                            ?>
                            <tr>
                                <th scope="row">
                                    <?= Html::a(
                                        str_pad($payment->id, 7, "0", STR_PAD_LEFT),
                                        ['#'],
                                        [
                                            'target' => '_blank',
                                            'Title' => 'View Receipt',
                                        ]
                                    ); ?>
                                </th>
                                <th scope="row">
                                    <?= Html::a(
                                        str_pad($payment->invoice_id, 7, "0", STR_PAD_LEFT),
                                        ['/account/view-invoice', 'invoice' => $payment->invoice_id],
                                        [
                                            'target' => '_blank',
                                            'Title' => 'View Invoice',
                                            'data' => [
                                                'method' => 'post'
                                            ],
                                        ]
                                    ); ?>
                                </th>
                                <td><?= ($invoice->paymentUser <> null) ? $invoice->paymentUser->first_name . " " . $invoice->paymentUser->last_name : ""; ?></td>
                                <td><?= ($invoice->paymentUser->accountCompany <> null) ?$invoice->paymentUser->accountCompany->name : ""; ?></td>
                                <td><?= ($payment->vpc_TransactionNo) ? $payment->vpc_TransactionNo : "-"; ?></td>
                                <td><?= $payment->total; ?></td>
                                <td><?= $payment->payment_method; ?></td>
                                <td><?php
                                    if($payment->status == 'pending'){
                                        ?>
                                        <span class="label label-warning">
                                            Pending
                                        </span>
                                        <?php
                                    } else if($payment->status == 'done'){
                                        ?> <span class="label label-success"> Paid </span>
                                        <?php
                                    } else if($payment->status == 'cancelled'){
                                        ?>  <span class="label label-danger"> Cancelled </span>
                                        <?php
                                    }
                                    ?></td>
                                <td><?= formatDate($payment->payment_date); ?></td>
                                <td class="invoicelink"><a href="#">Invoice</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <br>
                <div class="col-md-12 text-center pull-right">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

