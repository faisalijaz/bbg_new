<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?php
if (!Yii::$app->user->isGuest) {
    echo $this->render('_banner', [
        'model' => $model
    ]);
} ?>
<style>
    .TopMostSearch1 {
        margin-top: 20px;
    }
</style>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                        <h3><?= ($companyInfo <> null) ? $companyInfo->name . " Members" : "Company Members" ;?></h3>
                    </div>
                    <?php
                    if (!Yii::$app->user->isGuest) {
                        ?>
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2" style="padding: 20px;">
                            <?= Html::a('<i class="fa fa-plus"></i> Add Member', '/account/add-additional', ['class' => 'Mybtn']); ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="row PaddingTopBtm30px">
                    <?php
                    if ($showComapnyInfo) {
                        ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                            <h2>
                                Company Information
                                <span class="float-right">
                                    <a href="#"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a>
                                </span>
                            </h2>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                            <div class="row PaddingTopBtm30px">
                                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                    <img height="200" src="<?= ($companyInfo <> null && $companyInfo->logo <> "") ? $companyInfo->logo :Yii::$app->params['no_image']; ?>" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                                    <div class="profile">
                                        <h2><?= ($companyInfo <> null) ? $companyInfo->name : ""; ?></h2>
                                        <h3><?= ($companyInfo <> null) ? $companyInfo->address : ""; ?></h3>
                                        <p>
                                            <strong>Company Type</strong> -
                                            <?= ($companyInfo->companyCategory <> null) ? $companyInfo->companyCategory->title : ""; ?>
                                            <br/>
                                          <!--  <?php
/*                                            if (!Yii::$app->user->isGuest) {
                                                */?>
                                                <strong>VAT Number</strong> -
                                                <?/*= ($companyInfo <> null) ? $companyInfo->vat_number : ""; */?><br/>
                                                --><?php
/*                                            }
                                            */?>
                                            <strong>Url</strong>
                                            - <?= ($companyInfo <> null) ? Html::a($companyInfo->url, $companyInfo->url, []) : ""; ?>
                                            <br>
                                            <strong>Contact Number</strong>
                                            - <?= ($companyInfo <> null) ? $companyInfo->phonenumber : ""; ?><br>

                                            <strong>Emirates</strong>
                                            - <?= ($companyInfo <> null) ? $companyInfo->emirates_number : ""; ?><br>
                                            <strong>Postal Code</strong>
                                            - <?= ($companyInfo <> null) ? $companyInfo->postal_code : ""; ?><br>
                                            <strong>Address</strong>
                                            - <?= ($companyInfo <> null) ? $companyInfo->address : ""; ?>
                                        </p>
                                        <p>
                                        <h2>About Company</h2>
                                        <?= ($companyInfo <> null) ? $companyInfo->about_company : ""; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if (!Yii::$app->user->isGuest) {
                        ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading" id="CompanyMembers">
                            <h2>
                                Members Information
                            </h2>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <br/>
                            <?php
                            if ($members <> null) {
                                foreach ($members as $member) {
                                    $isContact = $member->checkMemberContact(Yii::$app->user->id, $member->id);
                                    ?>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div class="directorymember">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                                    <?php
                                                    $img = $member->picture;

                                                    if (strpos($img, 'cloudfront') == false) {
                                                        $img = Yii::$app->params['appUrl'] . $img;
                                                    }

                                                    if (strpos($member->picture, '.') == false) {
                                                        $img = Yii::$app->params['no_image'];
                                                    }
                                                    ?>
                                                    <img style="height: 145px"
                                                         src="<?= ($img <> "") ? $img : Yii::$app->params['no_image']; ?>"
                                                         class="img-fluid" alt="">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                                    <div class="directory" style="min-height: 120px;">

                                                <span class="pull-right memberContactIcon"
                                                      id="memberContact<?= $member->id; ?>">
                                                    <?php

                                                    if ($isContact <> null) {
                                                        echo Html::a('<i class="fa fa-user-times" aria-hidden="true"></i>',
                                                            '#', [
                                                                'class' => 'add_member_contact add-false',
                                                                'id' => $member->id,
                                                                'title' => "Remove",
                                                                'style' => 'color: #bd1f2f !important;'
                                                            ]);
                                                    } else {
                                                        echo Html::a('<i class="fa fa-user-plus" aria-hidden="true"></i>',
                                                            '#', [
                                                                'class' => 'add_member_contact add-true',
                                                                'id' => $member->id,
                                                                'title' => "Add to contact",
                                                                'style' => 'color: #228B22 !important;'
                                                            ]);
                                                    }

                                                    if ($member->parent_id == Yii::$app->user->id || $member->company == Yii::$app->user->identity->company) {
                                                        echo Html::a('<i class="fa fa-pencil" aria-hidden="true"></i>',
                                                            ['/membership/update', 'user' => $member->id], [
                                                                'class' => 'update_contact_profile',
                                                                'id' => $member->id,
                                                                'title' => "Update Profile",
                                                                'style' => 'color: #428bca !important;padding-left: 10px;'
                                                            ]);
                                                    }
                                                    ?>
                                                </span>
                                                        <h2><?= ($member <> null) ? $member->first_name . " " . $member->last_name : ""; ?></h2>
                                                        <h3><?= ($member <> null) ? $member->designation : ""; ?><br>
                                                        </h3>
                                                    </div>
                                                    <span class="EventDetail">
                                                <a href="/account/member-details/?id=<?= $member->id; ?>">Read More...</a>
                                            </span>
                                                    <?php
                                                    if ($member <> null && $member->group_id <> 2) { ?>
                                                        <span class="EventDetail">
                                                            <a href="/account/company-members/?company=<?= $member->company; ?>">Company Detail</a>
                                                        </span>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="col-md-12">
                                    <div class="col-md-12 alert alert-danger">No Record Found</div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <?php
                    }
                    ?>
                <br>
            </div>
            <div class="col-md-12 text-center">

                <?php
                if($pages <> null) {
                    LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                }
                ?>
            </div>
        </div>
    </div>
</section>
<?= $this->registerJs('
    
    $("body").find("a.add_member_contact").click(function(e) {
     
        e.preventDefault();  
        
        $(this).prop("disabled", true);
        
        var memberId = $(this).attr("id");
        var type = "add";  
        
        if( $("#"+memberId+".add_member_contact").hasClass("add-false")){
            type = "remove"; 
        }
        
        $.ajax({
        
            url: "/account/add-member-contact",
            type: "POST",
            data: {id : memberId, type : type}, 
            success : function(res){ 
                 
                if(res == "1"){ 
                    
                    if(type == "add"){ 
                        
                        $("#"+memberId+".add_member_contact").removeClass("add-true");
                        $("#"+memberId+".add_member_contact").addClass("add-false");  
                        $("#"+memberId+".add_member_contact").css("color","#228B22","!important"); 

                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-times");
                        
                        $(this).attr("title", "Add to contact");
                        
                    } else{ 
                    
                       $("#"+memberId+".add_member_contact").removeClass("add-false");
                       $("#"+memberId+".add_member_contact").addClass("add-true"); 
                       $("#"+memberId+".add_member_contact").css("color","","!important"); 
                        
                        $("#memberContact"+memberId+ " .fa").addClass("fa-user-plus");    
                        $("#memberContact"+memberId+ " .fa").removeClass("fa-user-times"); 
                            
                        $(this).attr("title", "Remove from contact");
                    }              
                
                 } 
            },
            error : function(data){
                console.log(data);
            }
        }); 
    });
    
'); ?>



