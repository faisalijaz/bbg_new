<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="mybutn btn btn-primary btn-circle">Profile
                        </a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="mybutn btn btn-default btn-circle" disabled="disabled">Events
                        </a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="mybutn btn btn-default btn-circle" disabled="disabled">Contacts</a>
                    </div>
                </div>
            </div>
            <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
            <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 EventformPadding">
                <form role="form">
                    <div class="row setup-content" id="step-1">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="Redbg" style="width: 25%;">
                                            Personal <br/> Information
                                        </td>
                                        <td class="paddingtop20px">

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>First Name:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= $model->first_name; ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Last Name:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= $model->last_name; ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Email:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= Html::a($model->email, null, ['href' => 'mailto:' . $model->email]); ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Phone Number:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= $model->phone_number; ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Gender:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= ($model->gender) ? 'Male' : 'Female'; ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Date of Birth:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= $model->dob; ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Membership Type:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <b><?= ($model->group <> null) ? $model->group->title : ''; ?></b>
                                                    (<?= ($model->account_type == "named_associate") ? 'Named Associate' : ucwords($model->account_type); ?>
                                                    )
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Country:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= ($model->accountCountry <> null) ? $model->accountCountry->country_name : ''; ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Registration Date:</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= formatDate($model->created_at); ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Last Renewal</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <?= formatDate($model->last_renewal); ?>
                                                </div>
                                            </div>

                                            <div class="row PaddingRL">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <label><b>Expiry</b></label>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                    <span class="label label-danger">
                                                        <?= formatDate($model->expiry_date); ?>
                                                        </span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <?php
                            if ($model->accountCompany <> null) {
                                $company = $model->accountCompany;
                                ?>
                                <div class="form-group">
                                    <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                        <tbody>
                                        <tr>
                                            <td class="Redbg" class="Redbg" style="width: 25%;">
                                                Company
                                            </td>
                                            <td class="paddingtop20px">
                                                <div class="row PaddingRL">
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <label><b>Name:</b></label>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <?= $company->name; ?>
                                                    </div>
                                                </div>
                                                <div class="row PaddingRL">
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <label><b>Industry:</b></label>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <?= ($company->companyCategory <> null) ? $company->companyCategory->title : '-'; ?>
                                                    </div>
                                                </div>
                                                <div class="row PaddingRL">
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <label><b>Phone Number:</b></label>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <?= $company->phonenumber; ?>                                                    </div>
                                                </div>



                                                <div class="row PaddingRL">
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <label><b>Url:</b></label>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <?= Html::a($company->url, $company->url); ?>                                                    </div>
                                                </div>

                                                <div class="row PaddingRL">
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <label><b>Postal Code:</b></label>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <?= $company->postal_code; ?>
                                                    </div>
                                                </div>

                                                <div class="row PaddingRL">
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <label><b>Emirates:</b></label>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <?= $company->emirates_number; ?>
                                                    </div>
                                                </div>


                                                <div class="row PaddingRL">
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <label><b>Address:</b></label>
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                        <?= $company->address; ?>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-2" style="background-color: #fff">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="Heading text-center">
                                <h3>Events Registered</h3>
                            </div>
                            <div class="form-group no-more-tables"  style=" overflow-x:auto;">
                                <table id="memberEventsRegTable"
                                       class="table display responsive no-wrap dt-responsive nowrap d-lg-table d-md-table d-sm-table d-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Event</th>
                                        <th>Event Date</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if ($model->eventsRegistered <> null) {
                                        $count = 1;
                                        foreach ($model->eventsRegistered as $event) {
                                            if ($event->eventData <> null) {
                                                $event_detials = $event->eventData;
                                            }
                                            ?>

                                            <tr>
                                                <td class="text-center"><?= $count++; ?></td>
                                                <td class="text-center">
                                                    <?= $event->firstname . ' ' . $event->lastname; ?>
                                                    <br/><?= Html::a($event->email, null, ['href' => 'mailto:' . $event->email]); ?>
                                                </td>
                                                <td class="text-center"><?= (count($event_detials) > 0) ? $event_detials->title : ''; ?></td>
                                                <td class="text-center"><?= (count($event_detials) > 0) ? $event_detials->event_startDate : ''; ?></td>
                                                <td class="text-center">
                                                    <span class="pull-right pricebox"><?= $event->fee_paid; ?></span>
                                                </td>
                                                <td class="text-center">
                                            <span class="Editbtn">
                                                <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </span>
                                                    <span class="delbtn">
                                                <a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </span>
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <!--<table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td class="numberofguests">Total Amount:</td>
                                    <td class="text-right numberofguests">675.00 AED</td>
                                </tr>
                                </tbody>
                            </table>-->
                        </div>
                    </div>

                    <div class="row setup-content" id="step-3">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="ThankyouMsg Eventform">
                                <h4>Contacts List</h4>
                                <div class="form-group no-more-tables" style=" overflow-x:auto;">
                                    <table id="memberContactsTable"
                                           class="table display responsive no-wrap dt-responsive nowrap d-lg-table d-md-table d-sm-table d-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Company</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if ($model->accountContacts <> null) {
                                            $count = 1;
                                            foreach ($model->accountContacts as $contact) {
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?= $count++; ?></td>
                                                    <td class="text-center">
                                                        <?= $contact->first_name . ' ' . $contact->last_name; ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <b><?= ($contact->group <> null) ? $contact->group->title : ''; ?></b>
                                                        <br/>
                                                        (<?= ($contact->account_type == "named_associate") ? 'Named Associate' : ucwords($contact->account_type); ?>
                                                        )
                                                    </td>
                                                    <td class="text-center"><?= Html::a($contact->email, null, ['href' => 'mailto:' . $contact->email]); ?></td>
                                                    <td class="text-center">
                                                        <span class="pull-right"><?= $contact->phone_number; ?></span>
                                                    </td>
                                                    <td class="text-center"><?= ($contact->accountCompany <> null) ? $contact->accountCompany->name : '-'; ?></td>
                                                    <td class="text-center">
                                            <span class="Editbtn">
                                                <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            </span>
                                                        <span class="delbtn">
                                                <a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            </span>
                                                    </td>
                                                </tr>

                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- AddToAny END -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1"></div>
        </div>
    </div>
</section>

<?php $this->registerJs('
    
    $(document).ready(function() {
        
        $("#memberEventsRegTable").DataTable();
        
        $("#memberContactsTable").DataTable();
    });
    
');
?>


