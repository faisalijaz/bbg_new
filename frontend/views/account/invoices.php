<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>Payment History</h3>
                <br>
                <table class="table table-striped table-responsive">
                    <thead>

                    <tr>
                        <th>Invoice ID</th>
                        <th>User</th>
                        <th>Company</th>
                        <th>Invoice Category</th>
                        <th>Total</th>
                        <th>Paid</th>
                        <th>Balance</th>
                        <th>Pyment Status</th>
                        <th>Invoice Date</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($invoices <> null) {
                        foreach ($invoices as $invoice) {
                            $amount_paid = 0;
                            $amount_paid = \common\models\Payments::find()->where(['invoice_id' => $invoice->invoice_id])->sum('amount');
                            ?>
                            <tr>
                                <th scope="row">
                                    <?= Html::a(
                                        str_pad($invoice->invoice_id, 7, "0", STR_PAD_LEFT),
                                        ['/account/view-invoice', 'invoice' => $invoice->invoice_id],
                                        [
                                            'target' => '_blank',
                                            'Title' => 'View Invoice',
                                            'data' => [
                                                'method' => 'post'
                                            ],
                                        ]
                                    ); ?>
                                </th>
                                <td><?= ($invoice->member <> null) ? $invoice->member->first_name . " " . $invoice->member->last_name : ""; ?></td>
                                <td><?= ($invoice->member->accountCompany <> null) ?$invoice->member->accountCompany->name : ""; ?></td>
                                <td><?= $invoice->invoice_category; ?></td>
                                <td><?= $invoice->total; ?></td>
                                <td><?= $amount_paid; ?></td>
                                <td><?= $invoice->total - $amount_paid; ?></td>
                                <td><?php
                                    if ($invoice->payment_status == 'unpaid') {
                                        ?>
                                        <span class="label label-danger">
                                            Pending
                                        </span>
                                        <?php
                                    } else if ($invoice->payment_status == 'partialy_paid') {
                                        ?>
                                        <span class="label label-warning">
                                            Partially Paid
                                        </span>
                                        <?php
                                    }
                                    else {
                                        ?>
                                        <span class="label label-success">
                                            Paid
                                        </span>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td><?= formatDate($invoice->invoice_date); ?></td>
                                <td class="invoicelink">
                                    <?= Html::a(
                                        'Invoice',
                                        ['/account/view-invoice', 'invoice' => $invoice->invoice_id],
                                        [
                                            'target' => '_blank',
                                            'Title' => 'View Invoice',
                                            'data' => [
                                                'method' => 'post'
                                            ],
                                        ]
                                    ); ?>
                                </td>
                                <td class="invoicelink">
                                    <?php
                                    if ($invoice->payment_status <> 'paid') {
                                        echo Html::a(
                                            'Pay Online',
                                            ['/site/invoice-payment', 'invoice_id' => $invoice->invoice_id],
                                            [
                                                'target' => '_blank',
                                                'Title' => 'Pay Online',
                                                'data' => [
                                                    'method' => 'post'
                                                ],
                                            ]
                                        );
                                    }?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <br>
                <div class="col-md-12 text-center pull-right">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>

        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

