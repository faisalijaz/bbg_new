<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center">
                    <h3>Events Registered</h3>
                </div>
                <div class="form-group no-more-tables" style=" overflow-x:auto;">
                    <table id="memberEventsRegTable"
                           class="table display responsive no-wrap dt-responsive nowrap d-lg-table d-md-table d-sm-table d-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Event Title</th>
                            <th>Event Date</th>
                            <th>Registration Status</th>
                            <th>Amount</th>
                            <th>Invoice</th>
                            <th>Payment Status</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($events <> null) {
                            $count = 1;
                            foreach ($events as $event) {
                                $event_detials = null;
                                if ($event->eventData <> null) {
                                    $event_detials = $event->eventData;
                                }
                                $invoices = null;
                                if ($event->fee_paid > 0) {
                                    $invoices = \common\models\Invoices::findOne($event->gen_invoice);
                                }
                                ?>

                                <tr>
                                    <td class="text-center"><?= $count++; ?></td>
                                    <td class="text-center">
                                        <?= $event->firstname . ' ' . $event->lastname; ?>
                                    </td>
                                    <td class="text-center"><?= ($event_detials <> null && count($event_detials) > 0) ? $event_detials->title : ''; ?></td>
                                    <td class="text-center"><?= ($event_detials <> null && count($event_detials) > 0) ? formatDate($event_detials->event_startDate) : ''; ?></td>
                                    <td class="text-center">
                                        <span class="text-center"><?= ucwords($event->status); ?></span>
                                    </td>
                                    <td class="text-center">
                                        <span class="pull-right text-center pricebox"><?= ($invoices <> null) ? $invoices->total : $event->fee_paid; ?></span>
                                    </td>
                                    <td class="invoicelink text-center">
                                        <?php
                                        if ($event->fee_paid > 0) {
                                            if($invoices <> null) {
                                                echo Html::a(
                                                    'view',
                                                    ['/account/view-invoice', 'invoice' => $event->gen_invoice],
                                                    [
                                                        'target' => '_blank',
                                                        'Title' => 'View Invoice',
                                                        'data' => [
                                                            'method' => 'post'
                                                        ],
                                                    ]
                                                );
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td class="invoicelink text-center">
                                        <?php
                                        echo ucwords($event->payment_status);
                                        if ($event->fee_paid > 0 && $event->payment_status == "pending") {

                                            echo '<br/>' . Html::a(
                                                'Pay Online',
                                                ['/site/invoice-payment', 'invoice_id' => $event->gen_invoice],
                                                [
                                                    'target' => '_blank',
                                                    'Title' => 'Pay Online',
                                                    'class' => 'btn-sm Mybtn',
                                                    'style' => 'color:#fff !important',
                                                    'data' => [
                                                        'method' => 'post'
                                                    ],
                                                ]
                                            );
                                        }
                                        ?>
                                    </td>
                                </tr>

                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <br>
                    <div class="col-md-12 text-center pull-right">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

