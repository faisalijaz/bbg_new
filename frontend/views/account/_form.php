<?php

use common\models\Categories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Accounts */
/* @var $form yii\widgets\ActiveForm */
$paymentCycle = [];
for ($i = 0; $i <= 30; $i++) {
    $paymentCycle[$i] = $i;
    if ($i === 0) {
        $paymentCycle['cash'] = 'Cash';
    }
}
$companyInfo = "";
if ($model->accountCompany <> null) {
    $companyInfo = $model->accountCompany;
}

?>
<div class="row">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card-box col-md-12">
        <br/>
           <h3 class="text-center">Personal Information</h3>
        <br/>
        <div class="accounts-form col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <a href="javascript:;"
                           id="upload-document1"
                           onclick="uploadAttachment(1)"
                           data-toggle="tooltip"
                           class="img-thumbnail"
                           title="Upload Document">
                            <i></i>
                            <img src="<?= ($model->picture) ? $model->picture : Yii::$app->params['no_image']; ?>" width="200" alt="" title="" data-placeholder="no_image.png" />
                            <br/>
                        </a>
                        <p class="image_upload_label">click Image to upload</p>
                        <?= $form->field($model, 'picture')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment1'])->label(false) ?>
                    </div>
                </div>
                <div class="accounts-form col-md-6">
                    <?= $form->field($model, 'title')->dropDownList(Yii::$app->params['userTitle'], ['prompt' => 'Select ...'])->label() ?>
                </div>
            </div>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'readonly' => 'readonly']); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'readonly' => 'readonly']); ?>
        </div>
        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'secondry_email')->textInput(['maxlength' => true])->label('Secondary Email') ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'gender')->dropDownList(['Female', 'Male'], ['prompt' => 'Select Gender']) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'nationality')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'country_code')->dropDownList(ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'country_name'), ['prompt' => 'Select Status']); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'vat_number')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-6">
            <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]); ?>
        </div>

        <div class="accounts-form col-md-12">
            <div class="row">
                <hr/>
                <h3>Company Information</h3>
                <hr/>
                <div class="accounts-form col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            if ($model->account_type <> "alternate") {
                                ?>
                                <div class="form-group">
                                    <a href="javascript:;"
                                       id="upload-document2"
                                       onclick="uploadAttachment(2)"
                                       data-toggle="tooltip"
                                       class="img-thumbnail"
                                       title="Upload Document">
                                        <i></i>
                                        <img src="<?= ($companyInfo <> null && !empty($companyInfo->logo)) ? $companyInfo->logo : Yii::$app->params['no_image']; ?>" width="200" alt="" title="" data-placeholder="no_image.png" />
                                        <br/>
                                    </a>
                                    <p class="image_upload_label">click Image to upload</p>
                                    <?= $form->field($model, 'companyData[logo]')->hiddenInput(['maxlength' => true, 'id' => 'input-attachment2'])->label(false) ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="accounts-form col-md-6">
                            <div class="form-group">
                                <label for="usr">Name</label>
                                <input type="text" class="form-control" name="Members[companyData][name]"
                                       value="<?= ($companyInfo <> null) ? $companyInfo->name : ''; ?>" readonly/>
                            </div>
                        </div>


                    </div>
                </div>

                <?php
                $readonly = ($model->account_type == "alternate") ? "readonly" : "";
                ?>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">VAT TRN</label>
                        <input type="text" class="form-control" name="Members[companyData][vat_number]"
                            <?= $readonly; ?>  value="<?= ($companyInfo <> null) ? $companyInfo->vat_number : ''; ?>" />
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Emirates</label>
                        <input type="text" class="form-control" name="Members[companyData][emirates_number]"
                            <?= $readonly; ?>
                               value="<?= ($companyInfo <> null) ? $companyInfo->emirates_number : ''; ?>">
                    </div>
                </div>
                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Category</label>
                        <?= Html::dropDownList('Members[companyData][category]', ($companyInfo <> null) ? $companyInfo->category : '',
                            ArrayHelper::map(Categories::find()->all(), 'id', 'title'), [
                                ($model->account_type == "alternate") ? "disabled" : false,
                                'class' => 'form-control', 'prompt' => 'Select Category'
                            ]) ?>
                    </div>
                </div>
                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Url</label>
                        <input <?= $readonly; ?> type="text" class="form-control" name="Members[companyData][url]"
                                                 value="<?= ($companyInfo <> null) ? $companyInfo->url : ''; ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Phone Number</label>
                        <input <?= $readonly; ?> type="text" class="form-control"
                                                 name="Members[companyData][phonenumber]"
                                                 value="<?= ($companyInfo <> null) ? $companyInfo->phonenumber : ''; ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Fax</label>
                        <input <?= $readonly; ?> type="text" class="form-control" name="Members[companyData][fax]"
                                                 value="<?= ($companyInfo <> null) ? $companyInfo->fax : ''; ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Postal Code</label>
                        <input <?= $readonly; ?> type="text" class="form-control"
                                                 name="Members[companyData][postal_code]"
                                                 value="<?= ($companyInfo <> null) ? $companyInfo->postal_code : ''; ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-6">
                    <div class="form-group">
                        <label for="usr">Address</label>
                        <input <?= $readonly; ?> type="text" class="form-control" name="Members[companyData][address]"
                                                 value="<?= ($companyInfo <> null) ? $companyInfo->address : ''; ?>">
                    </div>
                </div>

                <div class="accounts-form col-md-12">
                    <div class="form-group">
                        <label for="usr">About</label>
                        <textarea <?= $readonly; ?> name="Members[companyData][about_company]"
                                                    class="textarea form-control"><?= ($companyInfo <> null) ? $companyInfo->about_company : ''; ?></textarea>
                    </div>
                </div>
            </div>
    </div>
        <div class="accounts-form col-md-2 mysubmitbtn_parent_btn text-right">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary ' : 'btn btn-primary mysubmitbtn']) ?>
        </div>
    </div>

</div>

<?php ActiveForm::end(); ?>

<style>
    .mysubmitbtn_parent_btn{
        margin-bottom: 3%;
    }
    .mysubmitbtn{
        font-size: x-large;
    }
    .textarea.form-control{
        font-size: 14px;
        height: 112px;
    }

</style>

<script>
    var uploadAttachment = function (attachmentId) {

        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '<?= Url::to(['/file-manager/upload', 'parent_id' => 1]) ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function() {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function(json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);
                            $('#upload-document' + attachmentId + ' img').prop('src', json.href);
                            $('#input-attachment' + attachmentId).val(json.href);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }
</script>