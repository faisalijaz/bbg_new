<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Add to Calendar';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['enableClientScript' => false,'id' => 'login-form', 'options' => ['class' => 'f-login-form']]); ?>
<? /*= TopBannerWidget::widget(); */ ?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 loginFormstyle paddingRightLeft">
                    <div class="col-md-9 col-md-offset-2 loginFormstyle">
                        <div class="Heading text-center">
                            <h3>Add Event to Calendar</h3>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <!--<p>
                                    I would like to receive emails from the British Business Group Dubai & Northern
                                    Emirates
                                </p>-->
                                <div class="SiteText">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <?= Html::input('email', 'email', '', ['class' => 'form-control', 'placeholder' => 'Enter Email', 'required' => 'required']); ?>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <br/><br/>
                                        <?= Html::hiddenInput('event_id', $event, ['placeholder' => 'Enter Email', 'required' => 'required']); ?>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                                    <?= Html::submitButton('Add to Calendar', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'Subscribe Newsletter']) ?>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?php ActiveForm::end(); ?>
