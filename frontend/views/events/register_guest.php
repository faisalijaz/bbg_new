<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Event Registration';
$this->params['breadcrumbs'][] = $this->title;

$nonmember_fee = 0;
$member_fee = 0;
$includeDiet = 0;
if ($event <> null) {
    $nonmember_fee = $event->nonmember_fee;
    $member_fee = $event->member_fee;
    $includeDiet = $event->diet_option;
}

$subTotal = 0;
$vat = 0;
$total = 0;
$invoices = [];
?>
<style>
    .not-active {
        pointer-events: none;
        cursor: default;
    }
</style>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button"
                           class="mybutn btn btn-primary btn-circle <?= (\Yii::$app->request->get('s') == 1) ? "" : " not-active" ?>">Step
                            1</a>
                    </div>
                    <div class="stepwizard-step" id="step-1-btn">
                        <a href="#step-2" type="button"
                           class="mybutn btn btn-default btn-circle <?= (\Yii::$app->request->get('s') == 2) ? "" : " not-active" ?>"
                           disabled="disabled">Step
                            2</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button"
                           class="mybutn btn btn-default btn-circle <?= (\Yii::$app->request->get('s') == 3) ? "" : " not-active" ?>"
                           disabled="disabled">Step
                            3</a>
                    </div>
                </div>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 EventformPadding">
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'id' => 'EventRegistrationForm',
                    'enableClientValidation' => true,
                    'enableClientScript' => false,
                ]); ?>
                <input type="hidden" name="event_id" id="event_id" value="<?= $event->id; ?>">
                <input type="hidden" name="nonmember_fee" value="<?= $nonmember_fee; ?>">
                <input type="hidden" name="member_fee" value="<?= $member_fee; ?>">
                <input type="hidden" value="<?= $includeDiet;?>" id="IncludeDietOption" />

                <div class="row setup-content" id="step-1">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <h3> Guest Information:</h3>

                        <div class="form-group">
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td class="numberofguests">Guest Information:</td>
                                    <td class="text-right"><span id=""> Non-Member Fee: <?= vatInclusive($nonmember_fee); ?></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td class="Redbg">Guest</td>
                                    <td class="paddingtop20px">
                                        <div class="row PaddingRL">
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <input type="text"
                                                       name="guestInfo[90][name]"
                                                       required=""
                                                       class="form-control input_guest_info"
                                                       placeholder="Full Name">
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <input type="text"
                                                       name="guestInfo[90][phone_number]"
                                                       required=""
                                                       class="form-control input_guest_info"
                                                       placeholder="Phone Number">
                                            </div>
                                        </div>
                                        <div class="row PaddingRL">

                                        </div>
                                        <div class="row PaddingRL">
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <input type="email"
                                                       name="guestInfo[90][email]"
                                                       required=""
                                                       class="form-control input_guest_info"
                                                       placeholder="Email Address">
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <input type="text"
                                                       name="guestInfo[90][company]"
                                                       required=""
                                                       class="form-control input_guest_info"
                                                       placeholder="Company Name">
                                            </div>
                                        </div>
                                        <div class="row PaddingRL">

                                        </div>
                                        <div class="row PaddingRL">
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <input type="text"
                                                       name="guestInfo[90][designation]"
                                                       required=""
                                                       class="form-control input_guest_info"
                                                       placeholder="Designation">
                                            </div>
                                        </div>
                                        <div class="row PaddingRL">
                                            <?php
                                            if($event->diet_option){
                                            ?>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="guestInfo[90][diet_option]" value="1" id="dietryPref">
                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        <span>Dietary Preference</span>
                                                    </label>
                                                </div>

                                            </div>
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                                    <?= \yii\helpers\Html::dropDownList('guestInfo[90][diet_option_description]','',
                                                        ['Standard' => 'Standard', 'Vegetarian' => 'Vegetarian', 'other' => 'Other Dietary'],
                                                        ['class' => 'hidden form-control', 'prompt' => 'Select Dietry Option...', 'id' => 'select_diet_pref']); ?>

                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                                                    <?= \yii\helpers\Html::textarea('guestInfo[90][other_diet_option_description]','',
                                                        [
                                                            'style' => 'width: 100%; height: 150px;;',
                                                            'class' => 'hidden',
                                                            'id' => 'extra_diet_instruction'
                                                        ]); ?>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <h3>
                                Do you want to invite a guest ?
                                <span class="pull-right">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="" id="inviteGuest">
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span>Yes</span>
                                        </label>
                                    </div>
                                </span>
                                <span class="pull-right">
                                <input type="number" min="0" name="" value="0" class="pricebox"
                                       id="input_guest_number"/>
                            </span>
                            </h3>
                            <div id="section_invite_guest">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="numberofguests">Number of Guests:</td>
                                        <td class="text-right">
                                            <span id="guestsNumbers">1</span>
                                            <span> x <span id="nonmember_fee"> <?= vatInclusive($nonmember_fee); ?></span> = <span
                                                        class="nonmember_fee_total"> <?= vatInclusive($nonmember_fee); ?></span>
                                        </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <h3 class="text-right">Total: <span class="nonmember_fee_total">0</span></h3>
                                <div class="Heading text-center">
                                    <h3>Guest Information</h3>
                                </div>
                                <div id="guestInformation"></div>
                            </div>
                            <?php

                            ?>
                            <button class="Mybtn pull-left" id="EventRegistersBtn" type="button">
                                Next
                            </button>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<?= $this->registerJs("
    
    /**inviteGuest
     * Submit form and validate fields
     */
     
    $('body').find('#EventRegistersBtn').unbind().on('click', function(e){
            
        e.preventDefault();
        
        $(this).prop('disabled',true);
         
        // Check if guest checkbox is selected and any of the fields is empty
        if (!isValidGuestInput()) {
            swal({
                title: 'Error!',
                text: 'Please Fill out all fields in guest information',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            $('#EventRegistersBtn').prop('disabled',false);
            return false;
            
        } 
 
        $('#EventRegistrationForm').submit();
        
    });
    

    
"); ?>

<?= $this->registerJs('
 $("#select_diet_pref").change(function(){ 
            if($(this).val() == "other"){
                $("#extra_diet_instruction").removeClass("hidden");
            }else{
                $("#extra_diet_instruction").val("");
                $("#extra_diet_instruction").addClass("hidden");
            }
      });
      
      $("#dietryPref").click(function(){ 
            if($(this).is(":checked")){
                $("#select_diet_pref").removeClass("hidden");
            }else{ 
                $("#select_diet_pref").addClass("hidden");
                 $("#extra_diet_instruction").addClass("hidden");
            }
      });
    
    $("#dietryPref").change(function(){
        
        if($("#dietryPref").is(":checked")){
                   
            $("#select_diet_pref").removeClass("hidden");
            
        } else {
            
            $("#extra_diet_instruction").val("");
            $("#select_diet_pref").val("");
            $("#select_diet_pref").addClass("hidden");
            $("#extra_diet_instruction").addClass("hidden");
        }
    
    });
    
     $("body").find("#select_diet_pref").change(function(){
            
        if($("#select_diet_pref option:selected").val() == "other"){
            $("#extra_diet_instruction").removeClass("hidden");
        } else{
            $("#extra_diet_instruction").val("");
            $("#extra_diet_instruction").addClass("hidden");
        }
        
    });
 '); ?>
