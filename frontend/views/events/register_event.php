<?php

use common\models\EventSubscriptions;
use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Event Registration';
$this->params['breadcrumbs'][] = $this->title;

$nonmember_fee = 0;
$member_fee = 0;
$includeDiet = 0;
if ($event <> null) {
    $nonmember_fee = $event->nonmember_fee;
    $member_fee = $event->member_fee;
    $includeDiet = $event->diet_option;
}

$subTotal = 0;
$vat = 0;
$total = 0;
$invoices = [];
?>
<style>
    .not-active {
        pointer-events: none;
        cursor: default;
    }
</style>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button"
                           class="mybutn btn btn-primary btn-circle <?= (\Yii::$app->request->get('s') == 1) ? "" : " not-active" ?>">Step
                            1</a>
                    </div>
                    <div class="stepwizard-step" id="step-1-btn">
                        <a href="#step-2" type="button"
                           class="mybutn btn btn-default btn-circle <?= (\Yii::$app->request->get('s') == 2) ? "" : " not-active" ?>"
                           disabled="disabled">Step
                            2</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button"
                           class="mybutn btn btn-default btn-circle <?= (\Yii::$app->request->get('s') == 3) ? "" : " not-active" ?>"
                           disabled="disabled">Step
                            3</a>
                    </div>
                </div>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 EventformPadding">
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'id' => 'EventRegistrationForm',
                    'enableClientValidation' => true,
                    'enableClientScript' => false,
                ]); ?>
                <input type="hidden" name="event_id" id="event_id" value="<?= $event->id; ?>">
                <input type="hidden" id="register_event_non_mem_fee" name="nonmember_fee" value="<?= $nonmember_fee; ?>">
                <input type="hidden" id="register_event_mem_fee" name="member_fee" value="<?= $member_fee; ?>">
                <input type="hidden" value="<?= $includeDiet;?>" id="IncludeDietOption" />

                <div class="row setup-content" id="step-1">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <h3> Select Members:</h3>

                        <div class="form-group">
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <?php
                                $num = 1;
                                $is_reg = false;

                                if (!Yii::$app->user->isGuest) {

                                    $contact = Yii::$app->user->identity;

                                    $is_reg = false;
                                    $isRegistered = EventSubscriptions::findOne([
                                        'event_id' => $event->id,
                                        'user_id' => $contact->id
                                    ]);

                                    if ($isRegistered == null) {
                                        $is_reg = true;
                                    }

                                    if ($contact <> null) {

                                        if ($contact->user_type_relation == "guest") {
                                            $member_fee = ($event <> null) ? $event->nonmember_fee : 0;
                                        }

                                        if ($contact->user_type_relation == "committee") {
                                            $member_fee = ($event <> null) ? $event->committee_fee : 0;
                                        }

                                        if ($contact->user_type_relation == "focus-chair") {
                                            $member_fee = ($event <> null) ? $event->focus_chair_fee : 0;
                                        }

                                        if ($contact->user_type_relation == "platinum-sponsor") {
                                            $member_fee = ($event <> null) ? $event->sponsor_fee : 0;
                                        }

                                        if ($contact->user_type_relation == "honourary") {
                                            $member_fee = ($event <> null) ? $event->honourary_fee : 0;
                                        }

                                        if ($contact->user_type_relation == "business-associate") {
                                            $member_fee = ($event <> null) ? $event->member_fee : 0;
                                        }
                                    }

                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $num; ?></td>
                                        <td class="text-center">
                                            <div class="checkbox">
                                                <label>
                                                    <input <?= (count($isRegistered) > 0) ? 'disabled checked' : "checked"; ?>
                                                            type="checkbox" class="event_member_contacts"
                                                            name="contact[]"
                                                            value="<?= $contact->id; ?>">
                                                    <span class="cr">
                                                                <i class="cr-icon glyphicon glyphicon-ok"></i>
                                                            </span>
                                                </label>
                                                <input type="hidden" name="contactfee[<?= $contact->id; ?>]"
                                                       value="<?= $member_fee; ?>">
                                            </div>
                                        </td>
                                        <td>

                                            <?= $contact->first_name . " " . $contact->last_name . " (" . ucwords(($contact->group <> null) ? $contact->group->title : '') . ")"; ?>
                                           <!-- <span class="pull-right pricebox">
                                                    <?php /*= vatInclusive($member_fee); */?>
                                                </span>-->
                                        </td>
                                    </tr>
                                    <?php
                                    $num++;
                                }

                                if ($contacts <> null && count($contacts) > 0) {

                                    foreach ($contacts as $contact) {

                                        $isRegistered = EventSubscriptions::findOne([
                                            'registered_by' => Yii::$app->user->identity->id,
                                            'event_id' => $event->id,
                                            'user_id' => $contact->id
                                        ]);

                                        if ($isRegistered <> null) {
                                            $is_reg = true;
                                        }
                                        $member_fee = ($event <> null) ? $event->member_fee : "";

                                        if ($contact <> null) {

                                            if ($contact->user_type_relation == "guest") {
                                                $member_fee = ($event <> null) ? $event->nonmember_fee : 0;
                                            }

                                            if ($contact->user_type_relation == "committee") {
                                                $member_fee = ($event <> null) ? $event->committee_fee : 0;
                                            }

                                            if ($contact->user_type_relation == "focus-chair") {
                                                $member_fee = ($event <> null) ? $event->focus_chair_fee : 0;
                                            }

                                            if ($contact->user_type_relation == "platinum-sponsor") {
                                                $member_fee = ($event <> null) ? $event->sponsor_fee : 0;
                                            }

                                            if ($contact->user_type_relation == "honourary") {
                                                $member_fee = ($event <> null) ? $event->honourary_fee : 0;
                                            }

                                            if ($contact->user_type_relation == "business-associate") {
                                                $member_fee = ($event <> null) ? $event->member_fee : 0;
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $num; ?></td>
                                            <td class="text-center">
                                                <div class="checkbox">
                                                    <label>
                                                        <input <?= (count($isRegistered) > 0) ? 'disabled checked' : ""; ?>
                                                                type="checkbox" class="event_member_contacts"
                                                                name="contact[]"
                                                                value="<?= $contact->id; ?>">
                                                        <span class="cr">
                                                                <i class="cr-icon glyphicon glyphicon-ok"></i>
                                                            </span>
                                                    </label>
                                                    <input type="hidden" name="contactfee[<?= $contact->id; ?>]"
                                                           value="<?= $member_fee; ?>">
                                                </div>
                                            </td>
                                            <td>
                                                <?= $contact->first_name . " " . $contact->last_name; ?>
                                                <!--<span class="pull-right pricebox">
                                                    <?php /*= vatInclusive($member_fee); */?>
                                                </span>--><br/>
                                            </td>
                                        </tr>
                                        <?php
                                        $num++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                        <?php
                        if (!$event->members_only) {
                            ?>
                            <h3>
                                Do you want to invite a guest ?
                                <span class="pull-right">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="" id="inviteGuest">
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span>Yes</span>
                                        </label>
                                    </div>
                                </span>
                                <span class="pull-right">
                                <input type="number" min="0" name="" value="0" class="pricebox"
                                       id="input_guest_number"/>
                            </span>
                            </h3>
                            <div id="section_invite_guest">
                                <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                    <tbody>
                                    <tr>
                                        <td class="numberofguests">Number of Guests:</td>
                                        <td class="text-right">
                                            <span id="guestsNumbers">1</span>
                                            <span> x <span id="nonmember_fee"> <?= vatInclusive($nonmember_fee); ?></span> = <span
                                                        class="nonmember_fee_total"> <?= vatInclusive($nonmember_fee); ?></span>
                                        </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <h3 class="text-right">Total: <span class="nonmember_fee_total">0</span></h3>
                                <div class="Heading text-center">
                                    <h3>Guest Information</h3>
                                </div>
                                <div id="guestInformation"></div>
                            </div>
                            <?php
                        }
                        ?>
                        <button class="Mybtn pull-left" id="EventRegistersBtn" type="button">
                            <i class="fa fa-money"></i> Proceed to Payment
                        </button>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

<?= $this->registerJs("
    
    /**
     * Submit form and validate fields
     */
     
    $('body').find('#EventRegistersBtn').unbind().on('click', function(e){
            
        e.preventDefault();
        // console.log($('body').find('.event_member_contacts:checked:not(:disabled)').length);
        
        $('body').find('#EventRegistersBtn').prop('disabled',true);
        
         
        // Check if guest checkbox is selected and any of the fields is empty
        if (!isValidGuestInput() && $(document).find('#inviteGuest').is(':checked')) {
            
            swal({
                title: 'Error!',
                text: 'Please fill out all fields in guest information',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            $('#EventRegistersBtn').prop('disabled',false);
            return false;
            
        } else {
            
            // Check if guest checkbox is not selected and also not any contact selected
            if (!isValidateFormContacts() && !$(document).find('#inviteGuest').is(':checked')) {
                 
                swal({
                    title: 'Error!',
                    text: 'Please select at-least one contact',
                    type: 'error',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });
                $('#EventRegistersBtn').prop('disabled',false);
                return false;
            }
            
            if(!isValidateFormContacts() && $(document).find('#inviteGuest').is(':checked') && $('body').find('.event_member_contacts:checked:not(:disabled)').length == 0){
                    
                swal({
                    title: 'Error!',
                    text: 'Please select at-least one contact or guest',
                    type: 'error',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });
                $('#EventRegistersBtn').prop('disabled',false);
                return false;
                
            }
        }
        
        if(!$(document).find('#inviteGuest').is(':checked') && $('body').find('.event_member_contacts:checked:not(:disabled)').length == 0){
                    
            swal({
                title: 'Error!',
                text: 'Please select at-least one contact or guest',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            $('#EventRegistersBtn').prop('disabled',false);
            return false;
            
        }
        
 
        /**
         * ############################
         * If all goes well then submit
         * and register user
         * ##############################
         */
 
        var form = $('#EventRegistrationForm');
        var formData = form.serialize();

        $.ajax({
            
            url: form.attr('action'),
            type: form.attr('method'),
            data: formData,
            beforeSend: function() { 
                $('body #custom_bbg_loader').css('display', 'block');
                $('html').css('opacity', '.5');
                $('html').css('background-color', '#000');
            },
            success: function (data) {
            
                console.log(data);
                 
                
                $('body #custom_bbg_loader').css('display', 'none');
                $('html').css('opacity', '1');
                
                swal({
                    title: 'Great!',
                    text: 'You have successfully registered. Please make your payment to guarantee your place!',
                    type: 'success',
                    timer: 5000,
                    confirmButtonText: 'OK'
                }, function(){
                    window.location.href = '/events/payment?id=' + $('#event_id').val(); 
                });
                
                setTimeout(function(){
                    $(this).prop('disabled',false);
                    window.location.href = '/events/payment?id=' + $('#event_id').val(); 
                }, 3000);
                
            },
            error: function (e) {
             
                $('body #custom_bbg_loader').css('display', 'none');
                $('html').css('opacity', '1');
                
                swal({
                    title: 'Error!',
                    text: 'Registration could not be completed!',
                    type: 'error',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });
                
                $(this).prop('disabled',false);
            }
        });
        
        $('#EventRegistersBtn').prop('disabled',false);
        e.stopImmediatePropagation(); 
        return false;
    });
 
"); ?>
