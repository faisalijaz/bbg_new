<?php


use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\helpers\Html;

$this->title = (isset($event) && $event <> null) ? $event->title : "";
$this->params['breadcrumbs'][] = $this->title;

$OAuth_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode(Yii::$app->params["gcal_auth_scope"]) . '&redirect_uri=' . Yii::$app->params['appUrl'] . Yii::$app->params['gcal_redirect_url'] . '&response_type=code&client_id=' . Yii::$app->params['gcal_client_id'] . '&access_type=online';
$add_to_calendar = "/events/sync-to-calendar";
?>
<style>
     a.btn-floating {
         background: none !important;
    }
</style>

<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <?php
                        if (isset($event) && $event <> null) {

                            $title = $event->title;
                            $share_url = Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event->id;

                            ?>
                            <div class="Heading">
                                <?php
                                if (!Yii::$app->user->isGuest) {

                                    $sync_already = \common\models\GoogleEventsSync::find()->where(['user_id' => Yii::$app->user->id, 'event_id' => $event->id])->one();

                                    if ($sync_already <> null) {
                                        echo "<span style='margin-top:10px;' class='pull-right label label-warning'>Event is synced with google calender</span>";
                                    } else {

                                        echo Html::a('<i class="fa fa-calendar" aria-hidden="true"></i> Add to my calendar', $add_to_calendar, [
                                            'style' => 'margin-top:10px;',
                                            'class' => 'pull-right Mybtn',
                                            'target' => '_blank'
                                        ]);

                                        /*echo Html::a('<i class="fa fa-calendar" aria-hidden="true"></i> Add to my calendar', $OAuth_url, [
                                            'style' => 'margin-top:10px;',
                                            'class' => 'pull-right Mybtn',
                                            'target' => '_blank'
                                        ]);*/
                                    }
                                }
                                ?>

                                <h3><?= "Event Details"; ?></h3>
                                <h4><?= $event->title; ?></h4>
                            </div>

                            <div class="EventDetail">
                                <div class="row">

                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                        <div id="multi-item-example1" class="carousel slide carousel-multi-item"
                                             data-ride="carousel">
                                            <div class="controls-top">
                                                <a class="btn-floating" href="#multi-item-example1" data-slide="prev"><i
                                                            class="fa fa-chevron-left"></i></a>
                                                <a class="btn-floating" href="#multi-item-example1" data-slide="next"><i
                                                            class="fa fa-chevron-right"></i></a>
                                            </div>

                                            <div class="carousel-inner" role="listbox">
                                             <!--   <!--First slide-->

                                                <!--/.First slide-->
                                                <!--Second slide-->
                                                <?php
                                                $isFirst = true;
                                                if ($event->image <> null && $event->image <> "") {
                                                    $isFirst = false;
                                                    ?>

                                                    <div class="carousel-item active">
                                                        <div class="col-md-12">
                                                            <div class="Slides text-center">
                                                                <img class="img-fluid"
                                                                     src="<?= ($event->image) ? $event->image : \Yii::$app->params['no_image']; ?>"
                                                                     alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }

                                                $i = 0;
                                                foreach ($event->eventImages as $row) {

                                                    if($i == 0 && $isFirst){
                                                        ?>
                                                        <div class="carousel-item active">
                                                            <div class="col-md-12">
                                                                <div class="Slides text-center">
                                                                    <img class="img-fluid"
                                                                         src="<?= ($row->image) ? $row->image : \Yii::$app->params['no_image']; ?>"
                                                                         alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php

                                                    }    else{
                                                    ?>
                                                    <div class="carousel-item">
                                                        <div class="col-md-12">
                                                            <div class="Slides text-center">
                                                                <img class="img-fluid" src="<?= $row->image ?>" alt="">
                                                            </div>
                                                        </div>
                                                        <!--/.Second slide-->
                                                    </div>
                                                <?php }$i++;
                                                } ?>
                                                <!--/.Slides-->
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div class="EventDetailText">
                                            <p>
                                                <span class="BoldText">Event Date:</span> <?= formatDate($event->event_startDate); ?>
                                                <br>
                                                <span class="BoldText">Event Time:</span>
                                                <?php
                                                $event_start_time = $date = Date('g:i A', strtotime($event->event_startTime));
                                                $event_end_time = $date = Date('g:i A', strtotime($event->event_endTime));
                                                ?>

                                                <?= $event_start_time; ?>
                                                to <?= $event_end_time; ?><br>
                                                <span class="BoldText">Venue:</span> <?= $event->venue; ?> <br>
                                                <span class="BoldText">Location:</span> <?= $event->address; ?><br>
                                            </p>
                                            <span class="ViewMap">
                                            <a href="#event_map" class="smooth_scroll">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> View Map
                                            </a>
                                        </span>
                                            <p>
                                                <br>
                                                <?php

                                                if (!$event->members_only) {
                                                    if ($event->specialEventsFee <> null && $event->special_events == '1') {
                                                      echo '<span class="BoldText">Special Events</span><br/>';
                                                         foreach ($event->specialEventsFee as $spEvent) {
                                                             ?>
                                                            <span class="BoldText"><?= ucwords($spEvent->fee_type); ?>
                                                                :</span> <?= vatInclusive($spEvent->amount); ?>
                                                            AED <br>
                                                            <?php
                                                       }
                                                    } else { ?>
                                                        <span class="BoldText">Member Registrations:</span> <?= vatInclusive($event->member_fee); ?>
                                                        AED <br>
                                                        <span class="BoldText">Pre-members Fee:</span> <?= vatInclusive($event->nonmember_fee); ?>
                                                        AED <br>
                                                        <?php
                                                    }
                                                } else if ($event->members_only && !Yii::$app->user->isGuest) {

                                                    if ($event->specialEventsFee <> null && $event->special_events == '1') {
                                                        echo '<span class="BoldText">Special Events</span><br/>';
                                                        foreach ($event->specialEventsFee as $spEvent) {
                                                            ?>
                                                           <span class="BoldText"><?= ucwords($spEvent->fee_type); ?>
                                                                :</span> <?= vatInclusive($spEvent->amount); ?>
                                                            AED <br>
                                                            <?php
                                                        }
                                                    } else { ?>
                                                        <span class="BoldText">Member Registrations:</span> <?= vatInclusive($event->member_fee); ?>
                                                        AED <br>
                                                        <span class="BoldText">Pre-members Fee:</span> <?= vatInclusive($event->nonmember_fee); ?>
                                                        AED <br>
                                                        <?php
                                                    }
                                                } else if ($event->members_only && Yii::$app->user->isGuest) {

                                                    if ($event->specialEventsFee <> null  && $event->special_events == '1') {
                                                      echo '<span class="BoldText">Special Events</span><br/>';
                                                        foreach ($event->specialEventsFee as $spEvent) {
                                                          ?>
                                                            <span class="BoldText"><?= ucwords($spEvent->fee_type); ?>
                                                                :</span> <?= vatInclusive($spEvent->amount); ?>
                                                            AED <br>
                                                            <?php
                                                      }
                                                    } else { ?>
                                                        <span class="BoldText">Member Registrations:</span>
                                                        <?= vatInclusive($event->member_fee); ?>
                                                        AED <br>
                                                        <span class="BoldText">Pre-members Fee:</span> N/A AED <br>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </p>

                                        </div>
                                        <?php
                                        if ($event->specialEventsFee <> null && $event->special_events == '1') {

                                        } else {

                                            if($event->group_size - $reg_num > 0){

                                                if ($event->registration_start <= date('Y-m-d') && $event->registration_end >= date('Y-m-d')) {

                                                    if (!$event->members_only) {
                                                        ?>
                                                        <span class="RegisterEventbtn">
                                                        <?= Html::a('<i class="fa fa-map-marker" aria-hidden="true"></i> Register Now ', ['/events/event-register', 'id' => $event->id, 's' => 1], []); ?>
                                                    </span>
                                                        <p style="color:#D03C55; text-align: center;">Members must be logged in to receive member rate</p>
                                                        <?php
                                                    } elseif($event->members_only && !Yii::$app->user->isGuest){
                                                        ?>
                                                        <span class="RegisterEventbtn">
                                                        <?= Html::a('<i class="fa fa-map-marker" aria-hidden="true"></i> Register Now ', ['/events/event-register', 'id' => $event->id, 's' => 1], []); ?>
                                                    </span>
                                                        <p style="color:#D03C55; text-align: center;">Members must be logged in to receive member rate</p>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <span class="RegisterEventbtn">
                                                        <?= Html::a('<i class="fa fa-map-marker" aria-hidden="true"></i> Register Now ', '#', ['class' => 'members_only event_reg_btn']); ?>
                                                    </span>
                                                        <p style="color:#D03C55; text-align: center;">Members must be logged in to receive member rate</p>
                                                        <?php
                                                    }
                                                } else if ($event->registration_start > date('Y-m-d')) {
                                                    ?>
                                                    <span class="label label-warning"><i class="fa fa-lock"></i> Not yet open</span>
                                                    <?php
                                                }  else if ($event->registration_end < date('Y-m-d')) {
                                                    ?>
                                                    <span class="label label-warning"><i class="fa fa-lock"></i> Registration Closed</span>
                                                    <?php
                                                }   else if ($event->event_endDate < date('Y-m-d')) {
                                                    ?>
                                                    <span class="label label-warning"><i class="fa fa-lock"></i> Registration Closed</span>
                                                    <?php
                                                }
                                            }else {
                                                ?>
                                                <span class="label label-warning"><i class="fa fa-lock"></i>All seats are reserved.</span>
                                                <?php
                                            }
                                        }

                                        ?>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="DescriptionHeading">
                                            <p><?= $event->description; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>

                            <div id="event_map" class="col-md-8">
                                <div style="width: 100%">
                                    <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="20"
                                            marginwidth="20"
                                            src="https://maps.google.com/maps?q=<?= urlencode($event->map_address); ?>&sensor=false&hl=en;z=14&amp;output=embed"></iframe>
                                </div>
                                <br/>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div id="share-buttons"></div>
                    <div id="share"></div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>

            </div>
        </div>
    </section>
<?php
if (Yii::$app->user->isGuest) {
    echo HomeBottomWidget::widget();
} else {
    ?>
    <section class="clientsbg PaddingTopBtm">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="ClientsLogos">
                    <header>
                        <div class="Heading text-center">
                            <h3> Registered Attendees</h3>
                        </div>
                    </header>
                    <?php
                    if ($registered_list <> null) {
                        ?>

                        <figure>
                            <div class="mis-stage">
                                <!-- The element to select and apply miSlider to - the class is optional -->
                                <ol class="mis-slider">

                                    <?php
                                    $i = 0;
                                    foreach ($registered_list as $list) {

                                        $img = Yii::$app->params['no_image'];
                                        if ($list->member <> null) {

                                            $img = $list->member->picture;

                                            if (strpos($list->member->picture, '.') == false) {
                                                $img = Yii::$app->params['no_image'];
                                            }

                                            if (strpos($list->member->picture, 'uploads/user') == true) {

                                                if(!file_exists($list->member->picture)){
                                                    $img = Yii::$app->params['no_image'];
                                                }
                                            }
                                        }
                                        ?>
                                        <li class="mis-slide">
                                            <!-- A slide element - the class is optional -->
                                            <a href="<?= ($list->member <> null) ? '/account/member-details/?id=' . $list->user_id : '#'; ?>"
                                               class="mis-container">
                                                <!-- A slide container - this element is optional, if absent the plugin adds it automatically -->
                                                <figure>
                                                    <!-- Slide content - whatever you want -->
                                                    <img src="<?= $img; ?>" alt="TExt">
                                                    <figcaption style="color: #cd2c47;">
                                                        <h2><?= $list->firstname . " " . $list->lastname; ?></h2>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ol>
                            </div>
                        </figure>
                        <?php
                    }
                    ?>
                </div>
            </div>
    </section>
    <?php
}
?>

<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');
    if($errors <> null) {
        if(is_array($errors)) {
            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    if (is_array($error)) {
                        foreach ($error as $err) {

                            $this->registerJs('
                                swal({
                                    title: "Opps!",
                                    text: "' . $err . '",
                                    timer: 5000,
                                    type: "error",
                                    html: true,
                                    showConfirmButton: false
                                });
                            ');
                        }
                    } else {
                        $this->registerJs('
                            swal({
                                title: "Opps!",
                                text: "' . $error . '",
                                timer: 5000,
                                type: "error",
                                html: true,
                                showConfirmButton: false
                            });
                        ');
                    }
                }
            }
        }
    }
}
?>
<?= $this->registerJs('
       
     
   
    $(".event_reg_btn").click(function(e){
        
        e.preventDefault();
        
        if($(this).hasClass("members_only")){
            swal({
                    title: "Members Event!",
                    text: "This Event is for members only. You need to be a member to register for this event!",
                    timer: 5000,
                    type: "info",
                    showConfirmButton: false
                });
            
        }
    });
'); ?>
<?php
if (!Yii::$app->user->isGuest) {
?>
    <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="/theme/bbg/resources/miSlider/js/mislider.js"></script>
    <link href="/theme/bbg/resources/miSlider/css/mislider.css" rel="stylesheet" />
    <link href="/theme/bbg/resources/miSlider/css/mislider-cameo.css" rel="stylesheet" />
    <link href="/theme/bbg/resources/miSlider/css/mislider-skin-cameo.css" rel="stylesheet" />
    <link href="/theme/bbg/resources/miSlider/css/styles.css" rel="stylesheet" />


    <style>

        .mis-slider {
            background-color: #cd2c47 !important;
        }

        .clientsbg {
            background: #ffff;
        }

        .mis-slider li img{
            max-height: 270px !important;
        }

    </style>
<?php
    echo $this->registerJs('
    
    $.getScript("//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js");
    $.getScript("/theme/bbg/resources/miSlider/js/mislider.js");
    
    jQuery(function ($) {
        var slider = $(".mis-stage").miSlider({
            //  The height of the stage in px. Options: false or positive integer. false = height is calculated using maximum slide heights. Default: false
            // stageHeight: 380,
            //  Number of slides visible at one time. Options: false or positive integer. false = Fit as many as possible.  Default: 1
            slidesOnStage: 5,
            //  The location of the current slide on the stage. Options: "left", "right", "center". Defualt: "left"
            slidePosition: "center",
            //  The slide to start on. Options: "beg", "mid", "end" or slide number starting at 1 - "1","2","3", etc. Defualt: "beg"
            slideStart: "mid",
            //  The relative percentage scaling factor of the current slide - other slides are scaled down. Options: positive number 100 or higher. 100 = No scaling. Defualt: 100
            slideScaling: 150,
            //  The vertical offset of the slide center as a percentage of slide height. Options:  positive or negative number. Neg value = up. Pos value = down. 0 = No offset. Default: 0
            offsetV: -5,
            //  Center slide contents vertically - Boolean. Default: false
            centerV: true,
            //  Opacity of the prev and next button navigation when not transitioning. Options: Number between 0 and 1. 0 (transparent) - 1 (opaque). Default: .5
            navButtonsOpacity: 0.5,
            navList: false,
             slidesLoaded: false,
                //  The slide transition before 
                //  call back function - called before 
                //  the slide transition.
                beforeTrans: false,
                //  The slide transition complete 
                //  call back function - called at the end 
                //  of a slide transition.
                afterTrans: false
        });
    });
    
');

}?>

<?= $this->registerJS('
    
    $("#share").jsSocials({
        url : $(this).data("url"),
        text: "' . $title . '",
        shareIn: "popup",
        showLabel: false,
        showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin"]
    });  
    
'); ?>

