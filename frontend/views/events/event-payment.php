<?php
Yii::$app->session->set('event_invoices', []);

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Event Registration';
$this->params['breadcrumbs'][] = $this->title;

$subTotal = 0;
$vat = 0;
$total = 0;
$invoice = null;

?>
<style>
    .not-active {
        pointer-events: none;
        cursor: default;
    }
</style>
<link rel="stylesheet" href="https://www.paytabs.com/theme/express_checkout/css/express.css">
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step" id="step-1-btn">
                        <a href="#step-2" type="button"
                           class="mybutn btn btn-default btn-circle not-active"
                           disabled="disabled">Step 1</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button"
                           class="mybutn btn btn-primary btn-circle <?= (\Yii::$app->request->get('s') == 1) ? "" : " not-active" ?>">
                            Step 2</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button"
                           class="mybutn btn btn-default btn-circle not-active"
                           disabled="disabled">Step
                            3</a>
                    </div>
                </div>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 EventformPadding">
                <div class="row setup-content" id="step-1">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group">
                            <?= Html::a('<i class="fa fa-plus"></i> Add More',
                                ['/events/event-register', 'id' => \Yii::$app->request->get('id')], ['class' => 'Mybtn pull-right']); ?>
                            <table id="registeredUsersTable"
                                   class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>VAT (5%)</th>
                                    <th>Total</th>
                                    <th>Cancel Request</th>
                                </tr>
                                </thead>
                                <tbody id="EventUsers">
                                <?php
                                if ($event_subscriptions <> null && count($event_subscriptions) > 0) {

                                    $i = 1;
                                    $isSingle = (count($event_subscriptions) > 1) ? false : true;

                                    foreach ($event_subscriptions as $subscription) {
                                        if ($subscription->fee_paid > 0 && $subscription->payment_status == "pending") {

                                            if ($isSingle) {
                                                $invoice = $subscription->gen_invoice;
                                            } else {
                                                $invoice .= $subscription->gen_invoice . "|";
                                            }

                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <?= $i++; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?= $subscription->firstname . " " . $subscription->lastname; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?= ucwords($subscription->user_type); ?>
                                                </td>
                                                <td class="text-center">
                                                <span class="pricebox">
                                                    <?= $subscription->subtotal; ?>
                                                </span>
                                                </td>
                                                <td class="text-center">
                                                <span class="pricebox">
                                                    <?= $subscription->tax; ?>
                                                </span>
                                                </td>
                                                <td class="text-center">
                                                <span class="pricebox">
                                                    <?= $subscription->fee_paid; ?>
                                                </span>
                                                </td>
                                                <td class="text-center">
                                                    <?php
                                                    if (date('Y-m-d') <= $event->cancellation_tillDate) {
                                                        ?>
                                                        <span class=" cancel-request-<?= $subscription->id; ?>">
                                                            <?php
                                                            if ($subscription->cancel_request <> 1) {
                                                                ?>
                                                                <a style="color: red;" href="#"
                                                                   title="Request for Cancellation"
                                                                   class="cancel-request cancel-true"
                                                                   id="<?= $subscription->id; ?>">
                                                                <i class="fa fa-minus-circle " aria-hidden="true"></i>
                                                            </a>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <a style="color: red;" href="#"
                                                                   title="Remove Cancellation request"
                                                                   class="cancel-request cancel-false"
                                                                   id="<?= $subscription->id; ?>"><i
                                                                            class="fa fa-minus-circle "
                                                                            aria-hidden="true"></i> undo
                                                            </a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </span>
                                                        <?php
                                                    }else{
                                                        echo 'N/A';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $subTotal = round($subTotal + $subscription->subtotal,2);
                                            $vat = round($vat + $subscription->tax,2);
                                            $total = round($total + $subscription->fee_paid,2);
                                        }
                                    }
                                    // echo $invoice;
                                } else {
                                    ?>
                                    <tr class="odd">
                                        <td valign="top" colspan="7" class="text-center">No data available in table</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                            <tbody>
                            <tr>
                                <td class="numberofguests" colspan="3">Total Amount:</td>
                                <td class="text-center"><?= $subTotal; ?> AED</td>
                                <td class="text-center"><?= $vat; ?> AED</td>
                                <td colspan="2" class="text-center numberofguests"
                                    id="totalAmountEventRegistered"><?= $total; ?> AED
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <?php
                        $text = "Pay Later";
                        if($total == 0){
                            $text = "Next";
                        }
                        ?>
                        <?= Html::a($text,['/events/thank-you', 'id' => \Yii::$app->request->get('id')], ['class' => "Mybtn nextBtn"]); ?>

                        <div class="pull-right">
                            <div class="PT_express_checkout"></div>
                        </div>

                    </div>
                </div>

                <div class="row setup-content" id="step-3">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                    </div>
                </div>

            </div>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
    </div>
</section>

<?= HomeBottomWidget::widget(); ?>

<?= $this->registerJs('
    
    $("body").find(".cards_selected").change(function() {
     
        if($(this).val() == "new_card"){
            $("#form_card_details").removeClass("hidden");
        } else{
            $("#form_card_details").addClass("hidden");
        }  
    });
    
    $("body").find(".card_details_btn").click(function() {  
        
        $(this).prop("disabled", true);
            
        if($("body").find(".cards_selected:checked").length == 0){
             
             swal({
                    title: "Error!",
                    text: "Please select Card Details",
                    type: "error",
                    timer: 5000,
                    confirmButtonText: "OK"
             });
                
             $(this).prop("disabled",false);
             
        } else if($("body").find(".cards_selected:checked").val() == "new_card"){
            
            var valid = true;
            var des = "";
            
            if($("body").find("#paymentcards-card_name").val() == ""){
                valid = false;
                des = "Name on Card cannot be empty!   \n";
            }
            
            if($("body").find("#paymentcards-card_number").val() == ""){
                valid = false;
                des += "Card Number cannot be empty!  \n";
            }
            
            if($("body").find("#paymentcards-card_exp_month option:selected").val() == ""){
                valid = false;
                des = "Expiry month on card cannot be empty!  \n";
            }
            
            if($("body").find("#paymentcards-card_exp_year option:selected").val() == ""){
                valid = false;
                des += "Expiry year on card cannot be empty! \n";
            }
            
            if($("body").find("#paymentcards-card_security_token").val() == ""){
                valid = false;
                des += "Security code on card cannot be empty!\n";
            }
            
            if(!valid){
                swal({
                        title: "Error!",  
                        text: des,
                        type: "error",
                        timer: 10000,
                        confirmButtonText: "OK"
                });
            }   
                         
            $(this).prop("disabled",false); 
            $("form#cardForm").submit();
                  
        } else{
            
            $("form#cardForm").submit();
            $(this).prop("disabled",false);
        }       
    });
    
');
?>

<link rel="stylesheet" href="https://www.paytabs.com/express/express.css">
<script src="https://www.paytabs.com/theme/express_checkout/js/jquery-1.11.1.min.js"></script>
<script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>
<!-- Button Code for PayTabs Express Checkout -->

<script type="text/javascript">
    Paytabs("#express_checkout").expresscheckout({
        settings: {
            secret_key: "<?= Yii::$app->params['testPaytabsSecureHash'];?>",
            merchant_id: "<?= Yii::$app->params['testPaytabsMerchant'];?>",
            amount: "<?= $total;?>",
            currency: "AED",
            title: "<?= (isset($event) && $event <> null) ? $event->title : '';?>",
            product_names: "<?= (isset($event) && $event <> null) ? $event->title : '';?>",
            order_id: "<?= $invoice; ?>",
            url_redirect: "<?= Yii::$app->params['payment_verify_url_event'];?>"
        }
    });
</script>
