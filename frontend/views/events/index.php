<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;

?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <?php
            if ($isPast) {
                ?>
                <div class="col-md-8 pull-left"></div>
                <div class="col-md-4 pull-right EventSearch">
                    <?php $form = ActiveForm::begin([
                        'method' => 'post',
                        'action' => '/events?type=past'
                    ]); ?>
                    <div class="col-md-10">
                        <?= Html::dropDownList(
                            'past_event_month', $selectedMonth, $months, [
                                'prompt' => 'Select ...',
                                'class' => 'form-control'
                            ]
                        );
                        ?>
                    </div>
                    <div class="col-md-2">
                        <?= Html::submitButton('Search', ['class' => 'Mybtn Eventbtn']); ?>
                    </div>
                    <?php
                    ActiveForm::end();
                    ?>
                </div>
                <?php
            }
            ?>
            <div class="col-md-12 bbgevents_abudhabi text-right">

            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3><?= $this->title; ?></h3>
                    </div>

                    <?php
                    if (isset($events) && $events <> null && count($events) > 0) {
                        foreach ($events as $event) {
                            $title = $event->title;
                            $share_url = Yii::$app->params['appUrl'] . "/events/event-details?id=" . $event->id;

                            ?>
                            <div class="Event1">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <!--Second slide-->
                                        <?php
                                        $i = 0;
                                        if($event->image <> null && $event->image <> ""){
                                            ?>
                                            <img src="<?= ($event->image) ?  $event->image  : Yii::$app->params['no_image']; ?>"
                                                 class="img-fluid" alt="">
                                            <?php
                                        } else {
                                            foreach ($event->eventImages as $row) {
                                                if ($i == 0) {
                                                    ?>
                                                    <img src="<?= ($row->image) ? $row->image : Yii::$app->params['no_image']; ?>"
                                                         class="img-fluid" alt="">
                                                <?php }
                                                $i++;
                                            }
                                        }?>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                        <div class="EventText">
                                            <h3><i class="fa fa-calendar"
                                                   aria-hidden="true"></i> <?= formatDate($event->event_startDate); ?>
                                                <h5>
                                                    <i class="fa fa-map-marker"
                                                       aria-hidden="true"></i> <?= $event->venue; ?>
                                                </h5>
                                            </h3>

                                            <h4><?= $event->title; ?></h4>
                                            <p><?= $event->short_description; ?></p>
                                            <div class="row">
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div style="padding-bottom:30px; padding-top:0 !important;" class="EventDetail">
                                                <?= Html::a('Event Detail', [
                                                    '/events/event-details',
                                                    'id' => $event->id], [

                                                ]); ?>

                                                    </div>
                                                </div>
                                                <?= $this->registerJS('
                                                $("#share'.$event->id.'" ).jsSocials({
                                                    url : "' . $share_url . '",
                                                    text: "' . $title . '",
                                                    shareIn: "popup",
                                                    showLabel: false,
                                                      showCount: false,
                                                    shares: ["email", "twitter", "facebook", "googleplus", "linkedin"]
                                                });
                                            
                                            '); ?>
                                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                                    <div id="share<?= $event->id;?>"></div>
                                            <!--<span class="pull-right SocialLinksEvents">
                                                <a href="#">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                </a>
                                            </span>-->
                                                </div>
                                            </div>
                                        
                                            
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    } else{
                        ?>
                        <div class="alert alert-danger">No events found!</div>
                        <?php
                    }
                    ?>
                    <div class="col-md-8 text-center">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                            'options' => ['class' => 'pagination flex-wrap']
                        ]);
                        ?>
                    </div>
                    <div class="col-md-12">
                        <span class="bbgevents_abudhabi pull-right text-right" style="margin-top: 38px;">
                            <span class="bbgevents_abudhabi"
                                  style="color:#1f3760;padding: 10px;border: 1px solid #ddd;font-size: 18px;">
                                <?= Html::a('<img width="125" src="https://d1l2trc10ppjt4.cloudfront.net/Site/bbg_abudhabi5ba8dbb3588cf.png" /> Click here for BBG Abu Dhabi Events', 'http://britishbusiness.org/events/', ['target' => '_blank', 'style' => "color:#1f3760;"]); ?>
                            </span>
                        </span>
                    </div>
                </div>
            <br/>
                <br/>
            </div>
            <!--<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <? /*= RightSidebarWidget::widget(); */ ?>
            </div>-->
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<style>
    .bbgevents_abudhabi a:hover {
        color: #428bca !important;
    }
    .paddingRightLeft {
        padding-bottom: 25px;
    }
</style>
