<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget;


$this->title = 'News Details';
$this->params['breadcrumbs'][] = $this->title;

$share_url = "";
$title = "";
?>

<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <?php
                        if (isset($news) && $news <> null) {
                            $share_url = Yii::$app->params['appUrl'] . "/news/news-details?id=" . $news->id;
                            $title = $news->title;
                            ?>
                            <div class="Heading">
                                <h3><?= $this->title; ?></h3>
                                <h4><?= $title; ?></h4>
                                <p>
                                    <span class="BoldText">Date Posted:</span> <?= formatDate($news->created_at); ?>
                                </p>
                            </div>
                            <div class="EventDetail">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <img src="<?= (empty($news->image)) ? Yii::$app->params['no_image'] : $news->image; ?>" class="img-fluid" alt="<?= $news->title; ?>">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="DescriptionHeading"><br/> <br/> <br/>
                                            <h4><?= $news->short_description; ?></h4>
                                            <?= $news->description; ?>
                                        </div>

                                        <div id="share"></div>
                                         
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>

<?= $this->registerJS('

    $("#share").jsSocials({
        url : "' . $share_url . '",
        text: "' . $title . '",
        shareIn: "popup",
        showLabel: false,
         showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin"]
    });

'); ?>


