<?php

use common\models\NewsCategories;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<div class="memberS" style="padding: 15px;border: 1px solid #ddd;margin-bottom: 5px;width: 99% !important;">
         <?php $form = ActiveForm::begin(['id' => 'member-searcher', 'enableClientScript' => false, 'method' => 'get', 'action' => '/news?type=member', 'options' => ['class' => '']]); ?>

        <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
            <?= Html::dropDownList('month', Yii::$app->request->get('month'), Yii::$app->params['months'], ['prompt' => 'Select Month', 'class' => 'form-control']); ?>
        </div>
        <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
            <?php
            $year = date('Y');
            $years[] = $year;
            for ($i = 0; $i < 30; $i++) {
                $year = $year - 1;
                $years[] = $year;
            }
            ?>
            <?= Html::dropDownList('year', Yii::$app->request->get('year'), $years, ['prompt' => 'Select Year', 'class' => 'form-control']); ?>
        </div>
        <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
            <?= Html::dropDownList('category', Yii::$app->request->get('category'), ArrayHelper::map(NewsCategories::find()->all(), 'id', 'title'), ['prompt' => 'Select Category', 'class' => 'form-control']); ?>
        </div>
        <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1">
            <?= Html::submitButton('Search', ['class' => 'Mybtn']); ?>
        </div>
        <?php
        ActiveForm::end();
        ?>
 </div>