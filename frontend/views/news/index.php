<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = (isset($title)) ? $title : "News";
$this->params['breadcrumbs'][] = $this->title;

?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
                <?= $this->render('_search', []); ?>

            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3><?= $this->title; ?></h3>
                    </div>
                    <?php
                    if (isset($news) && $news <> null && count($news) > 0) {
                        foreach ($news as $news) {
                            ?>
                            <div class="Event1">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <img src="<?= $news->image; ?>" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                        <div class="EventText">
                                            <h3><i class="fa fa-calendar"
                                                   aria-hidden="true"></i> <?= formatDate($news->created_at); ?>
                                            </h3>
                                            <h4><?= $news->title; ?></h4>
                                            <p><?= $news->short_description; ?></p>
                                            <span class="EventDetail">
                                                <?= Html::a('News Detail', ['/news/news-details', 'id' => $news->id], []); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }else{
                        ?>
                        <!--<div class="alert alert-warning">There isn't any news yet,you hear soon</div>-->
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <div class="EventText">
                                    <p>As part of the BBG's commitment to provide the latest information on market intelligence and research to its members and wider audience, we are delighted to share information on Expo 2020. We are working closely with the Department for International Trade Expo 2020 team and across industry sectors, to ensure you are up to date on this exciting international event for creativity, innovation and cultural diversity.</p>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="col-md-12 text-center">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>