<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Newsletter Subscription';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['enableClientScript' => false, 'id' => 'login-form', 'options' => ['class' => 'f-login-form']]); ?>
<? /*= TopBannerWidget::widget(); */ ?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 loginFormstyle paddingRightLeft">
                        <div class="col-md-9 col-md-offset-2 loginFormstyle">
                            <div class="Heading text-center">
                                <h3>Newsletter Subscription</h3>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <p>
                                        I would like to receive emails from the British Business Group Dubai & Northern
                                        Emirates
                                    </p>
                                    <div class="SiteText">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <?= $form->field($model, 'first_name')->textInput(['autofocus' => true, 'placeholder' => ' Enter First Name'])->label(false) ?>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <?= $form->field($model, 'last_name')->textInput(['autofocus' => true, 'placeholder' => ' Enter Last Name'])->label(false) ?>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => ' Enter Email'])->label(false) ?>
                                            <?= Html::submitButton('Subscribe', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'Subscribe Newsletter']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?php ActiveForm::end(); ?>

<?php
if (count($model->getErrors()) > 0) {

    foreach ($model->getErrors() as $error) {
        if (is_array($error)) {
            foreach ($error as $err) {
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        } else {
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}
?>