<?php

use common\models\Accounts;
use common\models\MembershipEventFee;
use frontend\widgets\topBanner\TopBannerWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Event Registration';
$this->params['breadcrumbs'][] = $this->title;

$nonmember_fee = 0;
$member_fee = 0;

if ($event <> null) {
    $nonmember_fee = $event->nonmember_fee;
}

?>
<?/*= TopBannerWidget::widget(); */?>

<section class="MainArea">
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="mybutn btn btn-primary btn-circle">Step 1</a>
                    </div>
                    <div class="stepwizard-step" id="step-1-btn">
                        <a href="#step-2" type="button" class="mybutn btn btn-default btn-circle" disabled="disabled">Step
                            2</a>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="mybutn btn btn-default btn-circle" disabled="disabled">Step
                            3</a>
                    </div>
                </div>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 EventformPadding">
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'enableClientValidation' => true,
                ]); ?>
                <input type="hidden" name="event_id" id="event_id" value="<?= $event->id; ?>">
                <input type="hidden" name="nonmember_fee" value="<?= $nonmember_fee; ?>">
                <div class="row setup-content" id="step-1">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <h3> Select Members:</h3>
                        <div class="form-group">
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <?php
                                $num = 1;
                                if ($contacts <> null && count($contacts) > 0) {
                                    foreach ($contacts as $contact) {

                                        $member_fee = MembershipEventFee::findOne([
                                            'member_type' => $contact->account_type,
                                            'member_group' => $contact->group_id,
                                            'event_id' => $event->id
                                        ]);

                                        if ($member_fee <> null) {
                                            $member_fee = $member_fee->fee;
                                        } ?>
                                        <tr>
                                            <td class="text-center"><?= $num; ?></td>
                                            <td class="text-center">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" class="event_member_contacts"
                                                               name="contact[]"
                                                               value="<?= $contact->id; ?>">
                                                        <span class="cr">
                                                                <i class="cr-icon glyphicon glyphicon-ok"></i>
                                                            </span>
                                                    </label>
                                                    <input type="hidden" name="contactfee[<?= $contact->id; ?>]"
                                                           value="<?= $member_fee; ?>">
                                                </div>
                                            </td>
                                            <td>
                                                <?= $contact->first_name . " " . $contact->last_name; ?>
                                                <span class="pull-right pricebox"><?= $member_fee; ?></span>
                                            </td>
                                        </tr>
                                        <?php
                                        $num++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <h3>
                            Do you want to Invite Guest ?
                            <span class="pull-right">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="" id="inviteGuest">
                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                            <span>Yes</span>
                                        </label>
                                    </div>
                                </span>
                            <span class="pull-right">
                                <input type="number" min="0" name="" value="1" class="pricebox"
                                       id="input_guest_number"/>
                            </span>
                        </h3>
                        <div id="section_invite_guest">
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td class="numberofguests">Number of Guests:</td>
                                    <td class="text-right">
                                        <span id="guestsNumbers">1</span>
                                        <span> x <span id="nonmember_fee"> <?= $nonmember_fee; ?></span> = <span
                                                    class="nonmember_fee_total"> <?= $nonmember_fee; ?></span>
                                        </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <h3 class="text-right">Total: <span class="nonmember_fee_total">0</span></h3>
                            <div class="Heading text-center">
                                <h3>Guest Information</h3>
                            </div>
                            <div id="guestInformation"></div>
                        </div>
                        <button class="Mybtn pull-right" id="EventRegisterBtn" type="submit">
                            Save And Next
                        </button>
                        <button class="Mybtn hidden nextBtn pull-right" id="nextInformation1" type="button">
                            Next
                        </button>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

                <div class="row setup-content" id="step-2">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group">
                            <table id="registeredUsersTable"
                                   class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="EventUsers">
                                <tr class="odd">
                                    <td valign="top" colspan="5" class="text-center">No data available in table</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                            <tbody>
                            <tr>
                                <td class="numberofguests">Total Amount:</td>
                                <td class="text-right numberofguests" id="totalAmountEventRegistered">0.00 AED</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="Heading text-center">
                            <h3>Payment Detail</h3>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                </label>
                                <span class="numberofguests">Pay by Existing Card - xxxx-xxxx-xxxx-0000</span>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                </label>
                                <span class="numberofguests">Add New Card</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td class="paddingtop20px">
                                        <div class="row PaddingRL">
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <input type="text" class="form-control" placeholder="Name on Card">
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <input type="text" class="form-control" placeholder="Card Number">
                                            </div>
                                        </div>
                                        <div class="row PaddingRL">
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                <select class="selectbox custom-select">
                                                    <option selected>Expiry Month</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                <select class="selectbox custom-select">
                                                    <option selected>Expiry Year</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                                <input type="text" class="form-control" placeholder="CV Code">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>

                            </table>
                            <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                                <tbody>
                                <tr>
                                    <td><span class="numberofguests">Amount Payable: 675.00 AED</span></td>
                                </tr>
                                </tbody>

                            </table>
                        </div>
                        <button class="Mybtn nextBtn" type="button">Pay Later</button>
                        <button class="Mybtn nextBtn pull-right" type="button">Pay Now</button>
                    </div>
                </div>

                <div class="row setup-content" id="step-3">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="ThankyouMsg Eventform">
                            <h4>Thank you for your Registration</h4>
                            <div class="numberofguests text-center">Do you want to tell others BBG Members that your
                                are Attending this event?
                            </div>

                            <div class="radiobox paddingtop20px text-center">
                                <label>
                                    <input type="radio" class="memberInfoBtn" name="sendToMembers" value="1">
                                    <span class="numberofguests">Yes</span>
                                </label>
                                <label>
                                    <input type="radio" class="memberInfoBtn" name="sendToMembers" value="0" checked>
                                    <span class="numberofguests">No</span>
                                </label>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 hidden" id="SelectBoxMemberInfo">
                                <div class="col-md-12">
                                    <div class="numberofguests text-left">Select Members</div>
                                    <div class="input-group">
                                        <?= Select2::widget([
                                            'name' => 'membersList',
                                            'value' => '',
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'id' => 'membersList',
                                            'data' => ArrayHelper::map(Accounts::find()->all(), 'id', function ($account) {
                                                return $account->first_name . " " . $account->last_name;
                                            }),
                                            'options' => ['multiple' => true, 'placeholder' => 'Select Members ...', 'class' => 'pull-left']
                                        ]);
                                        ?>
                                        <span class="input-group-btn">
                                            <button id="sendInfoBtn" class="MySearchBtn btn btn-default" type="button">
                                                <!--<i class="fa fa-paper-plane" aria-hidden="true"></i>--> Send
                                            </button>
                                        </span>
                                    </div><!---->
                                    <br/>
                                </div>
                            </div>
                            <div class="FinishBtn clear-fix text-center">
                                <button class="Mybtn nextBtn" type="button">Back</button>
                                <button class="Mybtn nextBtn" type="button">Finish</button>
                            </div>

                        </div>
                        <h3>Share</h3>
                        <!-- AddToAny BEGIN -->
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_google_plus"></a>
                        </div>
                        <script async src="https://static.addtoany.com/menu/page.js"></script>
                        <!-- AddToAny END -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
    </div>
    </div>
</section>

