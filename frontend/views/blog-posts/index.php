<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;


$this->title = "Blog Posts";
$this->params['breadcrumbs'][] = $this->title;

$share_url = "";
$title = "";
?>

<section class="MainArea ">
    <div class="container LeftArea mainPage">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="membershipcontent">
                    <div class="Heading">
                        <h3>Blog Posts</h3><hr/>
                    </div>
                </div>
            </div>

            <?php
            if ($blogPosts <> null) {
                foreach ($blogPosts as $post) {
                    ?>
                    <div class="col-md-3">
                        <div class="Slides text-center">
                            <img class="img-fluid imgEventSlider"
                                 src="<?= ($post->banner_image) ? $post->banner_image : Yii::$app->params['no_image']; ?>"
                                 alt="">
                            <div class="Slides-body">
                                <h4><?= (strlen($post->title) > 60) ? substr($post->title, 0, 60) . '...' : $post->title; ?></h4>
                                <p><?= substr($post->short_description, 0, 90) . '...'; ?></p>
                                <?= Html::a('<button class="Mybtn" type="button">Read More</button>', ['/blog-posts/blog-details', 'id' => $post->id], []); ?>
                            </div>
                        </div>
                    </div>
                <?php }
            } else { ?>
                <div class="col-md-12">
                    <div class="col-md-12 alert alert-danger">No Record Found</div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
<style>
    .membershipcontent{
        padding: 20px 0px;
    }
    .mainPage{
        padding-top: 20px;
        padding-bottom: 20px;
    }
</style>



