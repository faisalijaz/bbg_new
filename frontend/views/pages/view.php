<?php

/* @var $this yii\web\View */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use frontend\widgets\homeBottom\HomeBottomWidget;


$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if($model->banner_image <> null && $model->banner_status == 1){ ?>

    <section class="bannerBG">
        <div class="container">
            <div class="row">
                <div class="col-7 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <div class="BannerText">
                        <h1></h1>
                        <h2></h2>
                    </div>
                </div>
                <div class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                    <img src="<?= $model->banner_image; ?>" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>

<?php } ?>

<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3><?= $this->title;?></h3>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="SiteText">
                                <?= $model->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
