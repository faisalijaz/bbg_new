<?php
/* @var $this yii\web\View */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('page_banner', ['model' => $model]); ?>

<div class="main-wraper padd-70-70">
    <div class="container">

        <div class="filter-content row">
            <div class="grid-sizer col-mob-12 col-xs-6 col-sm-4"></div>
            <?php
            if ($gallery <> null) {
                foreach ($gallery as $image) {
                    $sm = 'col-sm-4';
                    if ($image->size == 'large') {
                        $sm = 'col-sm-8';
                    }
                    ?>
                    <div class="item tours gal-item col-mob-12 col-xs-6 <?= $sm; ?> ">
                        <a class="mag-popup-link black-hover"
                           href="<?= $image->image; ?>">
                            <img class="img-full img-responsive"
                                 src="<?= $image->image; ?>"
                                 alt="">
                            <div class="tour-layer delay-1"></div>
                            <div class="vertical-align">
                                <h3 class="color-white"><b> view</b></h3>

                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
