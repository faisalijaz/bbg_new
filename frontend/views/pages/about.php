<?php

/* @var $this yii\web\View */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use yii\helpers\Html;
use frontend\widgets\homeBottom\HomeBottomWidget;

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if($model->banner_image <> null && $model->banner_status == 1){ ?>

    <section class="bannerBG">
        <div class="container">
            <div class="row">
                <div class="col-7 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <div class="BannerText">
                        <h1><?= $model->banner_title; ?></h1>
                        <h2><?= $model->banner_description; ?></h2>
                    </div>
                </div>
                <div class="col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                    <img src="<?= $model->banner_image; ?>" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>

<?php } ?>

<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>About BBG</h3>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="SiteText">
                                <?= $model->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <!--<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="SideHeading">
                        <h3>Quick Links</h3>
                    </div>
                    <div class="SideCalender">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">
                            <ul class="quicklinks">
                                <li><?/*= Html::a('About BBG',['pages/about-bbg']);*/?></li>
                                <li><?/*= Html::a('Objectives',['/pages/view-by-seo-url','seo_url'=> 'objectives']);*/?></li>
                                <li><?/*= Html::a('BBG Profile',['/pages/view-by-seo-url','seo_url'=> 'bbg-profile']);*/?></li>
                                <li><?/*= Html::a("Chairman's Message",['/pages/view-by-seo-url','seo_url'=> 'chairman-message']);*/?></li>
                                <li><?/*= Html::a("Constitution",['/pages/view-by-seo-url','seo_url'=> 'constitution']);*/?></li>
                                <li><?/*= Html::a('Committee','site/committee-info');*/?></li>
                                <li><?/*= Html::a("Focus Groups",['/pages/view-by-seo-url','seo_url'=> 'focus-groups']);*/?></li>
                                <?php
/*                                if (!\Yii::$app->user->isGuest) {
                                    */?>
                                    <li><?/*= Html::a("AGM", ['/pages/view-by-seo-url', 'seo_url' => 'agm']); */?></li>
                                    <?php
/*                                }
                                */?>
                                <li><?/*= Html::a("Doing Business in Dubai",['/pages/view-by-seo-url','seo_url'=> 'doing-business-in-dubai']);*/?></li>
                                <li><?/*= Html::a("Trade Visit to Dubai",['/pages/view-by-seo-url','seo_url'=> 'trade-visit-to-dubai']);*/?></li>
                                <li><?/*= Html::a("PROTECTION OF INTELLECTUAL PROPERTY",['/pages/view-by-seo-url','seo_url'=> 'poip']);*/?></li>
                                <li><?/*= Html::a("USEFUL LINKS",['/pages/view-by-seo-url','seo_url'=> 'useful-links']);*/?></li>
                            </ul>
                        </div>
                    </div>
                </div>-->
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>