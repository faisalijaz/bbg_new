<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inner-banner style-6 background-block" style="background:#ddd;background-image: url(&quot;img/detail/bg_5.jpg&quot;);">
    <img class="center-image" src="img/detail/bg_5.jpg" alt="" >
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <?= Breadcrumbs::widget([
                        'options' => ['class' => 'banner-breadcrumb color-white clearfix'],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'itemTemplate' => '<li class=\'link-blue-2\'>{link}</li> / '
                    ]) ?>
                    <h2 class="color-white"><?= Html::encode($this->title) ?></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wraper bg-grey-2 padd-90">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="color-dark-2-light">Blog #1</h4>
                    <h2>information for you</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="accordion style-5">
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How can I manage Instant Book settings?
                        </div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How do I list multiple rooms?</div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How do I use my calendar?</div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="accordion style-5">
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How do I turn off or delete my listing?
                        </div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>Why was my listing deactivated?</div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How do I edit my calendar?</div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How can I manage Instant Book settings?
                        </div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How do I use my calendar?</div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How do I list multiple rooms?</div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How can I manage Instant Book settings?
                        </div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                    <div class="acc-panel">
                        <div class="acc-title"><span class="acc-icon"></span>How do I use my calendar?</div>
                        <div class="acc-body">
                            <h5>metus Aenean eget massa</h5>
                            <p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus
                                eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et
                                hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu</p>
                            <ul>
                                <li>Shopping history</li>
                                <li>Hot offers according your settings</li>
                                <li>Multi-product search</li>
                                <li>Opportunity to share with friends</li>
                                <li>User-friendly interface</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>