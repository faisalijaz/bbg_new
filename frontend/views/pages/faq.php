
<?= $this->render('page_banner', ['model' => $model]); ?>

<div class="main-wraper bg-grey-2 padd-90">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h2><?= $model->title; ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="faq-accordian accordion style-4">
                    <?php foreach ($model->cmsTabs as $tab_data) {
                        if ($tab_data->status == 'active') {
                            ?>
                            <div class="acc-panel accordian-faq">
                                <div class="acc-title"><span class="acc-icon"></span><?= $tab_data->title; ?></div>
                                <div class="acc-body black-text"><?= $tab_data->description; ?></div>
                            </div>
                        <?php }
                    } ?>

                </div>
            </div>

        </div>
    </div>
</div>
