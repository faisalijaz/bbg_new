<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'theme/bbg/resources/css/evetsform.css',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css',
        'theme/bbg/resources/css/mystyles.css',
        'theme/bbg/resources/css/reset.css',
        'theme/bbg/resources/css/bootstrap.min.css',
        'theme/bbg/resources/font-awesome/css/font-awesome.min.css',
        'theme/bbg/resources/calender/style.css',
        'http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css',
        'theme/bbg/resources/css/swiper.min.css',
        'theme/bbg/resources/DataTables/datatables.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css',
        'theme/bbg/resources/Monthly-master/css/monthly.css',
        'theme/bbg/resources/css/jssocials.css',
        //'theme/bbg/resources/css/jssocials-theme-classic.css',
        'theme/bbg/resources/css/jssocials-theme-flat.css',
        //'theme/bbg/resources/css/jssocials-theme-minima.css',
        //'theme/bbg/resources/css/jssocials-theme-plain.css',
        '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        'theme/bbg/resources/css/mixedSlider.css'

    ];

    public $js = [

        'theme/bbg/resources/js/jquery-1.12.4.js',
        'theme/bbg/resources/js/bootstrap.js',
        'theme/bbg/resources/js/imported/DateTimePicker.min.js',
        "/theme/bbg/resources/miSlider/js/mislider.js",
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jcarousel/0.3.7/jquery.jcarousel-autoscroll.min.js',
        'theme/bbg/resources/calender/tempust.js',
        'theme/bbg/resources/js/jquery.flexisel.js',
        'theme/bbg/resources/js/swal.js',
        'theme/bbg/resources/js/waiting.js',
        'theme/bbg/resources/DataTables/datatables.min.js',
        'theme/bbg/resources/js/header_form_submit.js',
        'theme/bbg/resources/js/footer.js',
        'theme/bbg/resources/js/events_registeration.js',
        '//platform.linkedin.com/in.js',
        'https://platform.twitter.com/widgets.js',
        'theme/bbg/resources/Monthly-master/js/monthly.js',
        'theme/bbg/resources/js/jssocials.js',
        'theme/bbg/resources/js/jquery.flexisel.js',
        'http://pagead2.googlesyndication.com/pagead/show_ads.js',
        'theme/bbg/resources/js/multislider.js'

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        // 'Zelenin\yii\SemanticUI\assets\SemanticUICSSAsset'
    ];
}
