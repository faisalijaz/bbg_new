<?php
namespace frontend\models;

use common\helpers\EmailHelper;
use common\models\Members;
use yii\base\Model;


/**
 * Signup form
 */
class Additonal extends Model
{
    public $email;
    public $password;
    public $group_id;
    public $account_type;
    public $repassword;
    public $user_name;
    public $first_name;
    public $last_name;
    public $phone;
    public $title;
    public $designation;
    public $secondry_email;
    public $country_code;
    public $city;
    public $address;
    public $nationality;
    public $inforamtion_source;
    public $companyInfo = true;
    public $companyData = [];
    public $documents;
    public $phone_number;
    public $verifyCode;
    public $terms;
    public $newsletter = 0;
    public $sync_events_calander;
    public $user_docs;


    public $trade_licence;
    public $passport_copy;
    public $residence_visa;
    public $passport_size_pic;
    public $user_industry;

    public $upgrade;



    /**
     * @inheritdoc
     * @return string
     */
    public function rules()
    {
        return [

            [['title', 'designation', 'first_name', 'last_name'], 'required'],

            [['title', 'designation', 'first_name', 'last_name',], 'string', 'max' => 255],

            ['user_name', 'filter', 'filter' => 'trim'],
            ['user_name', 'required'],
            ['user_name', 'unique', 'targetClass' => '\common\models\Members', 'message' => 'This username has already been taken.'],
            ['user_name', 'string', 'min' => 4, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Members', 'message' => 'This email address has already been taken.'],

            [['documents', 'user_industry', 'trade_licence', 'passport_copy', 'residence_visa', 'passport_size_pic'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'mimeTypes' => 'image/jpeg, image/png'],

        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'terms' => 'I agree to the BBG Terms and Conditions .',
            'newsletter' => 'I want to subscribe to the BBG Newsletter.',
            'sync_events_calander' => 'I want to synchronise BBG events with my calendar. (Available only for MS Outlook.)',
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function createAdditional()
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {

            if (!$this->validate()) {
                return null;
            }

            $member = new Members();

            // $member->user_docs = $this->documents;
            $member->group_id = $this->group_id;
            $member->account_type = $this->account_type;

            $member->picture = $this->passport_size_pic;
            $member->parent_id = \Yii::$app->user->id;
            $member->company = \Yii::$app->user->identity->company;
            $member->title = $this->title;
            $member->first_name = $this->first_name;
            $member->last_name = $this->last_name;

            $member->email = $this->email;
            $member->password = $this->email;

            $member->country = $this->country_code;
            $member->city = $this->city;
            $member->address = $this->address;
            $member->phone_number = $this->phone_number;
            $member->designation = $this->designation;
            $member->nationality = $this->nationality;
            $member->user_name = $this->user_name;
            $member->invoiced = 0;
            $member->registeration_date = date('Y-m-d');
            $member->expiry_date = date('Y-m-d', strtotime('+1 year'));
            $member->status = 0;

            $member->setPassword($this->password);
            $member->setVerificationCode();
            $member->generateAuthKey();


            $member->user_docs['trade_licence'] = $this->trade_licence;
            $member->user_docs['residence_visa'] = $this->residence_visa;
            $member->user_docs['passport_copy'] = $this->passport_copy;
            $member->user_docs['passport_size_pic'] = $this->passport_size_pic;


            if (!$member->save()) {
               /* echo '<pre>';
                print_r($member->getErrors());
                die;*/
               \Yii::$app->session->setFlash('error',$member->getErrors());
                return false;
            }


            $transaction->commit();

            // Send email to customer
            $this->sendEmail($member);

            return true;

        } catch (\Exception $exception) {
            /*echo '<pre>';
            print_r($exception->getMessage());
            die;*/
            $transaction->rollback();
            return false;
        }
    }


    /**
     * Send an email to user
     * */
    public function sendEmail($user) {

        (new EmailHelper())->sendEmail($user->email, [], 'Application Received', 'account/signup', ['user' => $user]);
    }
}
