<?php

namespace frontend\models;

use common\helpers\EmailHelper;
use common\models\Members;
use yii\base\Model;


/**
 * Signup form
 */
class UpgradeForm extends Model
{
    public $email;
    public $password;
    public $group_id;
    public $account_type;
    public $repassword;
    public $user_name;
    public $first_name;
    public $last_name;
    public $phone;
    public $title;
    public $designation;
    public $secondry_email;
    public $country_code;
    public $city;
    public $address;
    public $nationality;
    public $inforamtion_source;
    public $companyInfo = true;
    public $companyData = [];
    public $documents;
    public $phone_number;
    public $verifyCode;
    public $terms;
    public $newsletter = 0;
    public $sync_events_calander;
    public $user_docs;

    public $compnay_name;
    public $company_type;
    public $company_phone;
    public $postal_zip;
    public $emirates;
    public $comapny_address;
    public $about_company;
    public $compnay_url;
    public $fax;

    public $trade_licence;
    public $passport_copy;
    public $residence_visa;
    public $passport_size_pic;
    public $user_industry;

    public $upgrade;

    /**
     * @inheritdoc
     * @return string
     */
    public function rules()
    {
        return [

            [['title', 'designation', 'first_name', 'last_name', 'companyInfo', 'email'], 'required'],

            [['title', 'designation', 'first_name', 'last_name',], 'string', 'max' => 255],

            [['compnay_name', 'company_type', 'company_phone', 'postal_zip', 'emirates', 'comapny_address', 'about_company'], 'required'],
            [['compnay_url', 'fax', 'upgrade'], 'safe'],

            [['secondry_email', 'sync_events_calander', 'newsletter', 'terms', 'phone_number', 'country_code', 'city', 'address', 'nationality', 'companyInfo', 'companyData', 'documents', 'user_docs', 'inforamtion_source'], 'safe'],

            [['documents', 'user_industry', 'trade_licence', 'passport_copy', 'residence_visa', 'passport_size_pic'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'mimeTypes' => 'image/jpeg, image/png'],

        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'terms' => 'I agree to the BBG Terms and Conditions .',
            'newsletter' => 'I want to subscribe to the BBG Newsletter.',
            'sync_events_calander' => 'I want to synchronise BBG events with my calendar. (Available only for MS Outlook.)',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function upgrade()
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {

            if (!$this->validate()) {
                return null;
            }

            if ($this->upgrade) {
                $user = Members::findOne(\Yii::$app->user->id);
            } else {
                $user = new Members();
            }

            if ($this->companyInfo) {

                $user->companyData['name'] = $this->compnay_name;
                $user->companyData['category'] = $this->company_type;
                $user->companyData['url'] = $this->compnay_url;
                $user->companyData['phonenumber'] = $this->company_phone;
                $user->companyData['fax'] = $this->fax;
                $user->companyData['postal_code'] = $this->postal_zip;
                $user->companyData['emirates_number'] = $this->emirates;
                $user->companyData['address'] = $this->comapny_address;
                $user->companyData['about_company'] = $this->about_company;
            }

            $user->user_docs = $this->documents;
            $user->group_id = $this->group_id;
            $user->account_type = $this->account_type;

            if (isset($this->documents['passport_size_pic'])) {
                $user->picture = $this->documents['passport_size_pic'];
            }

            $user->parent_id = 0;
            $user->title = $this->title;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->secondry_email = $this->secondry_email;
            $user->email = $this->email;
            $user->country = $this->country_code;
            $user->city = $this->city;
            $user->address = $this->address;
            $user->phone_number = $this->phone_number;
            $user->designation = $this->designation;
            $user->nationality = $this->nationality;
            $user->user_name = $this->user_name;
            $user->newsletter = $this->newsletter;
            $user->sync_events_calander = $this->sync_events_calander;
            $user->terms = $this->terms;
            $user->invoiced = 0;
            $user->expiry_date = date('Y-m-d', strtotime('+1 year'));

            if (!$user->save()) {
                return false;
            }

            $transaction->commit();

            $invoice = $user->upgradeMembership($user->id, $user->group_id,  $user->account_type);
            // Send email to customer
            $this->sendEmail($user);

            if($invoice){
                \Yii::$app->session->setFlash('success', 'Your application for Business membership upgrade to the British Business Group is received with thanks. Check your email for further instructions.');
                return $this->redirect('/site/invoice-payment?invoice_id=' . $invoice);
            }

            return true;

        } catch (\Exception $exception) {
            $transaction->rollback();
            return false;
        }
    }


    /**
     * Send an email to user
     * */
    public function sendEmail($user)
    {
        (new EmailHelper())->sendEmail($user->email, [], 'Application Received', 'account/signup', ['user' => $user]);
    }
}
