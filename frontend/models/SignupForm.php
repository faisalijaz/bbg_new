<?php

namespace frontend\models;

use common\helpers\EmailHelper;
use common\models\Members;
use common\models\NewsletterSubscriptions;
use yii\base\Model;


/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $group_id;
    public $account_type;
    public $repassword;
    public $user_name;
    public $first_name;
    public $last_name;
    public $phone;
    public $title;
    public $designation;
    public $secondry_email;
    public $country_code;
    public $city;
    public $address;
    public $nationality;
    public $inforamtion_source;
    public $companyInfo = true;
    public $companyData = [];
    public $documents;
    public $phone_number;
    public $verifyCode;
    public $terms;
    public $newsletter = 0;
    public $sync_events_calander;
    public $user_docs;

    public $compnay_name;
    public $company_type;
    public $company_phone;
    public $postal_zip;
    public $emirates;
    public $comapny_address;
    public $about_company;
    public $compnay_url;
    public $fax;
    public $vat_number;
    public $howDidYouHear;
    public $tellUsAboutYourself;
    public $purposeOfJoining;

    public $trade_licence;
    public $passport_copy;
    public $residence_visa;
    public $passport_size_pic;
    public $company_logo;
    // public $trade_link_proof;
    public $user_industry;
    public $other_country;
    public $other_city;
    public $other_emirates;
    public $referred_by;

    public $upgrade;


    /**
     * @inheritdoc
     * @return string
     */
    public function rules()
    {
        return [

            [['title', 'designation', 'first_name', 'last_name', 'verifyCode', 'phone_number', 'terms', 'companyInfo'], 'required'],

            [['title', 'designation', 'first_name', 'last_name',], 'string', 'max' => 255],

            ['user_name', 'filter', 'filter' => 'trim'],
            ['user_name', 'required'],
            ['user_name', 'unique', 'targetClass' => '\common\models\Members', 'message' => 'This username has already been taken.'],
            ['user_name', 'string', 'min' => 4, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Members', 'message' => 'This email address has already been taken.'],

            ['password', 'trim'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['repassword', 'compare', 'compareAttribute' => 'password', 'message' => 'The password does not match.'],

            [['compnay_name', 'company_type', 'company_phone', 'postal_zip', 'emirates', 'comapny_address', 'about_company'], 'required', 'when' => function ($model) {
                return $model->group_id != 2;
            }, "enableClientValidation" => true],

            [['other_country'], 'required', 'when' => function ($model) {
                return $model->country_code == 247;
            }, "enableClientValidation" => true],

            [['other_city'], 'required', 'when' => function ($model) {
                return $model->city == "Other";
            }, "enableClientValidation" => true],

            [['other_emirates'], 'required', 'when' => function ($model) {
                return $model->emirates == "Other";
            }, "enableClientValidation" => true],

            [['other_emirates', 'other_city', 'other_country', 'compnay_url', 'fax', 'upgrade', 'vat_number', 'howDidYouHear', 'tellUsAboutYourself', 'purposeOfJoining', 'referred_by'], 'safe'],

            [['secondry_email', 'sync_events_calander', 'newsletter', 'terms', 'country_code', 'city', 'address', 'nationality', 'companyInfo', 'companyData', 'documents', 'user_docs', 'inforamtion_source'], 'safe'],

            [['documents', 'user_industry', 'trade_licence', 'passport_copy', 'residence_visa', 'passport_size_pic', 'company_logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'mimeTypes' => 'image/jpeg, image/png'],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'terms' => 'I agree to the BBG Terms and Conditions .',
            'newsletter' => 'I want to subscribe to the BBG Newsletter.',
            'sync_events_calander' => 'I want to synchronise BBG events with my calendar. (Available only for MS Outlook.)',
            'howDidYouHear' => 'How did you hear about us?',
            'tellUsAboutYourself' => 'Tell us about yourself in a few words',
            'purposeOfJoining' => 'What are you looking to get out of being a BBG member'
        ];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function signup()
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {

          /*  if (!$this->validate()) {
                return null;
            }*/

            $user = new Members();

            if ($this->companyInfo) {
                $user->companyData['logo'] = $this->company_logo;
                $user->companyData['name'] = $this->compnay_name;
                $user->companyData['category'] = $this->company_type;
                $user->companyData['url'] = $this->compnay_url;
                $user->companyData['phonenumber'] = $this->company_phone;
                $user->companyData['fax'] = $this->fax;
                $user->companyData['postal_code'] = $this->postal_zip;

                if ($this->emirates == "Other") {
                    $user->companyData['emirates_number'] = $this->other_emirates;
                } else {
                    $user->companyData['emirates_number'] = $this->emirates;
                }

                $user->companyData['address'] = $this->comapny_address;
                $user->companyData['about_company'] = $this->about_company;
                $user->companyData['vat_number'] = $this->vat_number;
                $user->companyData['show_in_directory'] = '0';
            }

            $user->user_docs['passport_size_pic'] = $this->passport_size_pic;
            $user->user_docs['passport_copy'] = $this->passport_copy;
            $user->user_docs['residence_visa'] = $this->residence_visa;
            $user->user_docs['trade_licence'] = $this->trade_licence;
            // $user->user_docs['trade_link_proof'] = $this->trade_link_proof;

            // $user->user_docs = $this->documents;
            $user->group_id = $this->group_id;
            $user->account_type = $this->account_type;
            $user->picture = $this->passport_size_pic;

            $user->parent_id = 0;
            $user->title = $this->title;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->secondry_email = $this->secondry_email;
            $user->email = $this->email;
            $user->referred_by = $this->referred_by;

            if ($this->country_code == '247') {
                $user->country = $this->other_country;
            } else {
                $user->country = $this->country_code;
            }

            if ($this->city == 'Other') {
                $user->city = $this->other_city;
            } else {
                $user->city = $this->city;
            }

            $user->address = $this->address;
            $user->phone_number = $this->phone_number;
            $user->designation = $this->designation;
            $user->nationality = $this->nationality;
            $user->user_name = $this->user_name;
            $user->newsletter = $this->newsletter;
            $user->terms = $this->terms;
            $user->invoiced = 0;
            $user->registeration_date = date('Y-m-d');
            $user->expiry_date = date('Y-m-d', strtotime('+1 year'));
            $user->howDidYouHear = $this->howDidYouHear;
            $user->tellUsAboutYourself = $this->tellUsAboutYourself;
            $user->purposeOfJoining = $this->purposeOfJoining;
            $user->user_industry = $this->company_type;

            $user->setPassword($this->password);
            $user->setVerificationCode();
            $user->generateAuthKey();


            if ($this->newsletter) {

                $newsletter = new NewsletterSubscriptions();
                $newsletter->email = $user->email;
                $newsletter->active = '1';
                $newsletter->events_news = "1";
                $newsletter->weekly_newsletter = "1";
                $newsletter->special_offers = "1";
                $newsletter->is_member = 1;
                $newsletter->first_name = $user->first_name;
                $newsletter->last_name = $user->last_name;

                if (!$newsletter->save()) {
                    return false;
                }
            }

            if (!$user->save()) {
                return false;
            }

            $transaction->commit();

            // Send email to customer
            $this->sendEmail($user);

            return true;

        } catch (\Exception $exception) {
            $transaction->rollback();
            return false;
        }
    }


    /**
     * Send an email to user
     * */
    public function sendEmail($user)
    {

        (new EmailHelper())->sendEmail($user->email, [], 'Application Received', 'account/signup', ['user' => $user]);
    }
}
