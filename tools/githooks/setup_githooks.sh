#!/usr/bin/env bash

DIR=tools/githooks
GITDIR=.git/hooks

cp $DIR/pre-commit.sh $GITDIR/pre-commit
cp $DIR/pre-commit-check-json.sh $GITDIR/pre-commit-check-json
cp $DIR/pre-commit-check-php.sh $GITDIR/pre-commit-check-php
cp $DIR/pre-commit-check-phpcs.sh $GITDIR/pre-commit-check-phpcs
cp $DIR/pre-commit-check-xml.sh $GITDIR/pre-commit-check-xml
cp $DIR/pre-commit-check-linestyle.sh $GITDIR/pre-commit-check-linestyle
cp $DIR/commit-msg.sh $GITDIR/commit-msg

chmod -R +x $GITDIR/*
