#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

chmod -R 777 $SCRIPT_DIR/../backend/runtime
mkdir -p -m 777 $SCRIPT_DIR/../backend/runtime/cache